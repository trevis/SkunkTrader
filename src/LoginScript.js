/*
	Since SkunkWorks only allows one global login script setting that affects all characters. 
	This smaller script will check if the currently logged in character has 'Enable Login Script' enabled and will launch SkunkTrader.
	If not it'll shutdown gracefully.
*/

function chat() {
	var args = Array.prototype.slice.call(arguments);
	skapi.OutputLine("[SkunkTrader_LoginScript] " + args.join(', '), opmChatWnd);
}

function getJsonFile(params) {
	if (SkunkFS10.existsSync(params.path)) {
		var json = SkunkFS10.readFileSync(params.path);
		var items;
		try {
			items = JSON.parse(json);
		} catch (e) {
			chat("Error parsing " + params.path + ": " + e);
			return;
		}
		return items;
	}
}

function terminate() {
	if (typeof SkunkProcess10 === "undefined") {
		chat("SkunkProcess isn't loaded, cannot terminate Skunkworks process.");
		return;
	}
	var process = SkunkProcess10.getProcess();
	if (process) {
		chat("Terminating Skunkworks...");
		process.Terminate();
	} else {
		chat("Could not get Skunkworks process.");
	}
}

function main() {
	if (skapi.plig == pligNotRunning) {
		chat("Asheron's call isn't running. Cannot launch script.");
		return console.StopScript();
	} else if (skapi.plig == pligAtLogin) {
		chat("A character hasn't logged in yet. Cannot launch script.");
		return console.StopScript();
	}

	var character = getJsonFile({path: "src\\configs\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + ".json"});
	if (typeof character === "object") {
		if (character.autoStartScript == true) {
			chat("Starting SkunkTrader.swx... Make sure you have 'Activate on startup' enabled in SkunkTrader's config tab.");
			console.RunScript("SkunkTrader.swx");
			return;
		} else if (character.terminateLoginScript == true) {
			terminate();
		} else {
			chat(skapi.acoChar.szName + " (" + skapi.szWorld + ") does not have 'Enable Login Script' enabled in SkunkTrader's config tab. Stopping script.");
		}
	}
	console.StopScript(); // Prevents Skunkworks from sometimes crashing.
}