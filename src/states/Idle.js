(function() {
	var core        = SkunkTrader;
	var config = core.config;
	var states = core.states;

	var ONE_SECOND = 1000;


	core.on('onEnable', function onEnable() {
		core.addHandler(evidOnTradeReset, handler);
	});

	var handler = {};

	// Reset state if trade window opens.
	handler.OnTradeReset = function OnTradeReset(/*aco*/) {
		core.debug("<Idle|OnTradeReset>");
		core.stateStack.forEach(function(session2) {
			if (session2.stateName == "Idle") {
				core.popState(session2, new core.Error("TRADE_OPENED"));
			}
		});
	};

	core.queueOrganize = true; // default true to cleanup on startup.
	
	
	states["Idle"] = function onExecute(params, callback) {
		var session = params.session;
		var executeTime = params.executeTime;
		if (executeTime && executeTime != core.lastExecutionTime) return;
		
		//core.debug("<Idle>");
		
		if (!core.isActive()) {
			return callback(new core.Error("NOT_ACTIVE"));
		}
		
		if (core.tradePartner) {
			// We have a trade partner, don't do anything.
			return core.setTimeout(function() {
				return callback();
			}, ONE_SECOND);
		}
		
		if (core.queueOrganize == true) {
			if (session.droppedItems != true && config.dropItemsEnabled == true) {
				core.debug("Dropping items...");
				return core.dropExcessItems({}, function(err, results) {
					if (executeTime && executeTime != core.lastExecutionTime) return;
					if (err) return core.popState(session, err);
					session.droppedItems = true;
					
					core.setTimeout(function() {
						callback();
					}, 1000);
					
				});
			}

			if (config.keepInventorySorted == true && session.sorted != true) {
				core.debug("Stacking inventory...");
				return core.stackInventory(undefined, onStacked);
			}
		}
		function onStacked(err) {
			if (executeTime && executeTime != core.lastExecutionTime) return;
			if (err) return core.popState(session, err);
			
			core.debug("Organizing inventory...");
			core.organizeInventory(undefined, onOrganized);
		}
		function onOrganized(err) {
			if (executeTime && executeTime != core.lastExecutionTime) return;
			if (err) return core.popState(session, err);
			
			core.debug("Done orginize.");
			core.queueOrganize = false;
			session.sorted = true;
			callback();
		}


		return core.setTimeout(callback, ONE_SECOND);
	};
})();	