/*\
	These are the default settings. When you run SkunkTrader, a file containing your character's settings is saved in src/configs/<world>/<character>.json
	If you need to manually edit your settings, you may need to edit the json file for the changes to take effect. 
\*/

(function() {
	var core    = SkunkTrader;
	var config      = core.config;
	
	config.active = false;
	
	config.tickWait = 1000 / 10; // How long to sleep between ticks. 100ms default
	//config.opmDebug = opmConsole | opmChatWnd; // Where to send debug messages.
	config.debugStackTraceDepth = 5;
	config.debugToLog = false;
	config.maxDebugLogSize = 100 * 1024 * 1024;       // When debug log reaches this size, rename it with the current date and time and start a new debug log.
	config.compressOldLogs = true;                  // When log file reaches max size, compress log.

	// Don't stack these items. (Used to have issues with Casino tokens)
	config.dontStack = {};
	//config.dontStack["Something"] = true;
	
	/*
	config.moveToFirstPack = {};
	config.moveToFirstPack["Trade Note"]           = true;
	
	
	config.moveToLastPack = {};
	config.moveToLastPack["(.*) Pea$"]                  = true;
	config.moveToLastPack["Salvage"]                   = true;
	*/
	
	
	// Ignore selling/listing these items.
	config.ignoreItems = {};
	config.ignoreItems["^Pyreal$"]                   = true;
	config.ignoreItems["(.*) Pea$"]                   = true;
	config.ignoreItems["Prismatic Taper"]                   = true;

	config.chatAnnouncments = true;
	config.logoutOnPortal = true; // Logout if we somehow portal/teleport somewhere.
	config.terminateSkunkOnShutdown = false; // Terminate SkunkWorks when script shuts down.

	config.showDuplicateCommonItems = true; // Show duplicate common items in trade window. Since we add items one at a time, keeping this off should speed up populating the trade window.
	config.showDuplicateUniqueItems = false; // Show duplicate unique items in trade window. (atm only works for aetheria)
	
	config.myLocation = "Marketplace";
	
	// Respond to "!search <item>" chat commands. No UI for these yet.
	config.respondToPriceChecks = {};
	config.respondToPriceChecks[evidOnChatLocal] = true; // Local chat.
	config.respondToPriceChecks[evidOnTellAllegiance] = true; // Allegiance chat.
	config.respondToPriceChecks["General"] = true; // General chat.
	config.respondToPriceChecks["Trade"] = true; // General chat.
	
	// Messages to say into chat randomly. 
	config.announcments = [{enabled:true, message:"/e says, \"I am a trade bot. Open a trade window to see my items.\""}];
	
	// Settings for other players (supplier, discounts)
	config.players = {};
	
	// Auto set prices for salvage. UI Config>Salvage.
	config.salvageAutoValue = {};

	config.openWindowTimeout = 1000 * 60 * 5; // Close trade window after 5 minutes. Set to 0 to disable.

	config.itemSets = {}; // Items to be sold together. GUI 
		
	config.refreshStatusDelay = 10*1000; // Refresh status GUI once every x miliseconds.
	
	
	config.doPhantomScans = false; // On the emulator. When we trade away an item SkunkWorks thinks it's still in our inventory. This phantom check attempts to move items Skunkworks thinks is in our inventory to confirm they exist.
	
	config.ignoredPlayers = {};
	
	config.clientIdleTimeout = 60*60*24*7; // Set the client's idle logout timeout to 7 days.
	config.jumpTimer = false; // Jump every 5 minutes to prevent idle logout. 

	config.globalDiscount = 0; // Global discount percentage. For sales affecting all items.
	
	config.valueRounding = 2; // Round prices to 2 decimal places.
	config.workmanshipRounding = 2; // Round item/salvage workmanship to 2 decimal places.
	
	config.showGUIOnStartup = false;
	config.activateOnStartup = false;
	
	config.defaultPrice = null;
	
	config.allowResellPriceChange = false;
	
	config.restrictStackableSales = false; // Don't allow multiple of the same stackable item to be added to the trade window.
	
	config.addItemsOneAtATime = false; // Our original method of adding items to the trade window.
	
	config.equipItemsOnRequest = false;
	
	config.autoStartScript = false;
	config.terminateLoginScript = false;
	
	config.lootProfiles = {};
	
	// Items that have the same name but have different stats, treat them as unique items and not common.
	config.uniqueItems = {};
	config.uniqueItems["Coalesced Aetheria"] = true;
	config.uniqueItems["Aetheria"] = true;
	config.uniqueItems["(\w*) (.*) Essence \((\d*)\)"] = true;
	//config.uniqueItems["(\w*) (.*) Essence"] = true;
	config.uniqueItems["Asheron's (.*) Raiment"] = true;
	config.uniqueItems["Asheron's Raiment"] = true;
	
	config.itemAliases = {}; // aliases should be lowercase.
	config.itemAliases["mmd"] = "Trade Note (250,000)";
	config.itemAliases["trade note"] = "Trade Note (250,000)";
	config.itemAliases["tradenote"] = "Trade Note (250,000)";

	config.pointsProfiles = {};
	
	config.sharePricesPerWorld = false;

	config.priceDecayEnabled = false;
	config.priceDecayPercentage = 0.025; // decrease price x% each day we've had it.


	config.keepInventorySorted = true;
	
	config.dropItems = [];
	config.dropItemsEnabled = false;
	
	config.allowPartiallyUsedItemsAsPayment = false;
	
	config.promotionalCodes = [];
	
})();