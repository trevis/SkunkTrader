/********************************************************************************************\
	Script:         SkunkTrader-1.0
	Purpose:        Skunkworks trade bot script for Asheron's Call emulator server.
	Author:         Cyprias
	Date:           2018/07/17
	Licence:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

/*
	Useful links: 
		https://javascript.info - Javascript tutoral. Some features mentioned may not be available in Skunkworks' ES3 environment.
		https://github.com/airbnb/javascript/tree/es5-deprecated/es5 - Javascript style guide.
		http://www.jiehuang.net/wordPress/uncategorized/ecmascript - Differences between Skunkworks' ES3 environment and javascript's ES5 environment.
		http://skunkworks.sourceforge.net - SkunkWorks documentation.
		http://www.ac-tool.com/forums/index.php?/forum/8-skunkworks - Archived SkunkWorks forum.
		https://github.com/30-seconds/30-seconds-of-interviews/blob/master/questions/node-error-first-callback.md - Error first (parameter) callback pattern.
*/

var MAJOR = "SkunkTrader-1.1";
var MINOR = 221029; // Year Month Day

(function (factory) {
	var params = {};
	params.EventEmitter   = (typeof EventEmitter !== "undefined" && EventEmitter) || require("modules\\SkunkSuite\\EventEmitter"); 
	
	/*
	// These are already loaded via swx file, but if ST is loaded via ScriptManager this will load the necessary modules.
	require("SkunkSuite\\LibStub");
	require("SkunkSuite\\SkunkScript-1.0");
	require("SkunkSuite\\SkunkLogger-1.0");
	require("SkunkSuite\\SkunkTimer-1.0");
	require("SkunkSuite\\SkunkEvent-1.0");
	require("SkunkSuite\\SkunkComm-1.0");
	require("SkunkSuite\\SkunkAssess-1.0");
	require("SkunkSuite\\SkunkSchema-1.0");
	require("SkunkSuite\\SkunkFS-1.0");
	require("SkunkSuite\\SkunkDB-1.0");
	require("SkunkSuite\\SkunkHTTP-1.0");
	require("SkunkSuite\\SkunkLayout-1.0");
	require("SkunkSuite\\SkunkProcess-1.0");
	*/
	
	//require("TinkeringUtil");
	

	if (typeof module === 'object' && module.exports) {
		module.exports = factory(params);
	} else {
		SkunkTrader = factory(params); // Global object.
	}
}(function (params) {
	
	var EventEmitter = params.EventEmitter;
	
	var core = LibStub("SkunkScript-1.0").newScript(new EventEmitter(), 
		MAJOR, 
		"SkunkTimer-1.0", 
		"SkunkEvent-1.0", 
		"SkunkAssess-1.1", 
		"SkunkComm-1.0", 
		"SkunkLogger-1.0", 
		"SkunkSchema-1.0", 
		"SkunkFS-1.0", 
		"SkunkCache-1.0",
		"SkunkActions-1.0",
		"SkunkConfig-1.0");
		
	core.setShortName("ST");
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;

	core.version         = MAJOR + "." + MINOR; 
	core.title           = core.version;
	var config           = core.config = {};
	var _l               = core.locales = {};

	var defaultConfig = {
		profile: {
			logChatToFile: true,
			loadLootProfile3: true
		}
	};

	var fs                  = core.fs = LibStub("SkunkFS-1.0");
	var SkunkComm;
	var SkunkSchema;
	var SkunkDB;
	var SkunkHTTP;
	var SkunkLayout;
	var statusTimer;
	var SkunkProcess;
	var SkunkTimer;
	var SkunkAssess;
	var SkunkActions;
	
	core.states = {};
	core.stateStack = [];
	core.currentSession = null;
	core.lastExecutedSession = null;
	core.currentlyExecutingSession = null;
	core.squelchedPlayers = []; // Gets set during onEnable.
	core.accountList = [];
	core.uniqueItemsList = [];
	core.stats = {};
	core.allItemsShown = false;
	core.commonItemsList = [];
	core.controls = {};
	core.inWaitEvent = false;
	core.phantomItems = {}; // items Skunk thinks in our inventory but probably aren't.
	core.acoLastItemAdded = null;
	core.announcmentMethods = []; // List of functions to say stuff randomly into chat. (ads, stats, ect)
	core.startupGear = [];
	core.tradeWindow = null; // Trade window contents.
	core.tradePartner = null;
	core.equipmentChanged = false;
	core.commonBulkSelected = {};
	core.uniqueBulkSelected = {};
	core.tradePromo = null;
	
	
	var _dir = core._dir = (typeof __dirname !== "undefined" && __dirname + "\\" || ".\\");

	var myTradeSide = 1; // OnTradeAdd(aco, side)
	var theirTradeSide = 2;

	var icon_textures = 0x6000000; // Icon textures begin at 0x6000000.

	var materialNames = {};
	materialNames[materialAgate]            = "Agate";
	materialNames[materialAlabaster]        = "Alabaster";
	materialNames[materialAmber]            = "Amber";
	materialNames[materialAmethyst]         = "Amethyst";
	materialNames[materialAquamarine]       = "Aquamarine";
	materialNames[materialAzurite]          = "Azurite";
	materialNames[materialBlackGarnet]      = "Black Garnet";
	materialNames[materialBlackOpal]        = "Black Opal";
	materialNames[materialBloodstone]       = "Bloodstone";
	materialNames[materialBrass]            = "Brass";
	materialNames[materialBronze]           = "Bronze";
	materialNames[materialCarnelian]        = "Carnelian";
	materialNames[materialCeramic]          = "Ceramic";
	materialNames[materialCitrine]          = "Citrine";
	materialNames[materialCloth]            = "Cloth";
	materialNames[materialCopper]           = "Copper";
	materialNames[materialDiamond]          = "Diamond";
	materialNames[materialDilloHide]        = "Dillo Hide";
	materialNames[materialEbony]            = "Ebony";
	materialNames[materialEmerald]          = "Emerald";
	materialNames[materialFireOpal]         = "Fire Opal";
	materialNames[materialGem]              = "Gem";
	materialNames[materialGold]             = "Gold";
	materialNames[materialGranite]          = "Granite";
	materialNames[materialGreenGarnet]      = "Green Garnet";
	materialNames[materialGreenJade]        = "Green Jade";
	materialNames[materialGromnieHide]      = "Gromnie Hide";
	materialNames[materialHematite]         = "Hematite";
	materialNames[materialImperialTopaz]    = "Imperial Topaz";
	materialNames[materialIron]             = "Iron";
	materialNames[materialIvory]            = "Ivory";
	materialNames[materialJet]              = "Jet";
	materialNames[materialLapisLazuli]      = "Lapis Lazuli";
	materialNames[materialLavenderJade]     = "Lavender Jade";
	materialNames[materialLeather]          = "Leather";
	materialNames[materialLinen]            = "Linen";
	materialNames[materialMahogany]         = "Mahogany";
	materialNames[materialMalachite]        = "Malachite";
	materialNames[materialMarble]           = "Marble";
	materialNames[materialMetal]            = "Metal";
	materialNames[materialMoonstone]        = "Moonstone";
	materialNames[materialOak]              = "Oak";
	materialNames[materialObsidian]         = "Obsidian";
	materialNames[materialOnyx]             = "Onyx";
	materialNames[materialOpal]             = "Opal";
	materialNames[materialPeridot]          = "Peridot";
	materialNames[materialPine]             = "Pine";
	materialNames[materialPorcelain]        = "Porcelain";
	materialNames[materialPyreal]           = "Pyreal";
	materialNames[materialRedGarnet]        = "Red Garnet";
	materialNames[materialRedJade]          = "Red Jade";
	materialNames[materialReedsharkHide]    = "Reedshark Hide";
	materialNames[materialRoseQuartz]       = "Rose Quartz";
	materialNames[materialRuby]             = "Ruby";
	materialNames[materialSandstone]        = "Sandstone";
	materialNames[materialSapphire]         = "Sapphire";
	materialNames[materialSatin]            = "Satin";
	materialNames[materialSerpentine]       = "Serpentine";
	materialNames[materialSilk]             = "Silk";
	materialNames[materialSilver]           = "Silver";
	materialNames[materialSmokyQuartz]    	= "Smoky Quartz";
	materialNames[materialSteel]            = "Steel";
	materialNames[materialStone]            = "Stone";
	materialNames[materialSunstone]         = "Sunstone";
	materialNames[materialTeak]             = "Teak";
	materialNames[materialTigerEye]         = "Tiger Eye";
	materialNames[materialTourmaline]       = "Tourmaline";
	materialNames[materialTurquoise]        = "Turquoise";
	materialNames[materialVelvet]           = "Velvet";
	materialNames[materialWhiteJade]        = "White Jade";
	materialNames[materialWhiteQuartz]      = "White Quartz";
	materialNames[materialWhiteSapphire]    = "White Sapphire";
	materialNames[materialWood]             = "Wood";
	materialNames[materialWool]             = "Wool";
	materialNames[materialYellowGarnet]     = "Yellow Garnet";
	materialNames[materialYellowTopaz]      = "Yellow Topaz";
	materialNames[materialZircon]           = "Zircon";
	
	// https://github.com/ACEmulator/ACE/blob/master/Source/ACE.Entity/Enum/EquipmentSet.cs
	var setNames = core.setNames = {};
	setNames[27] = "Acid Proof";
	setNames[14] = "Adept";
	setNames[15] = "Archer";
	setNames[28] = "Cold Proof";
	setNames[18] = "Crafter";
	setNames[92] = "Darkened Mind";
	setNames[16] = "Defender";
	setNames[20] = "Dexterous";
	setNames[26] = "Flame Proof";
	setNames[23] = "Hardened";
	setNames[19] = "Hearty";
	setNames[40] = "Heroic Protector";
	setNames[25] = "Interlocking";
	setNames[29] = "Lightning Proof";
	setNames[24] = "Reinforced";
	setNames[51] = "Sigil of Armor Tinkering";
	setNames[35] = "Sigil of Defense";
	setNames[36] = "Sigil of Destruction";
	setNames[37] = "Sigil of Fury";
	setNames[38] = "Sigil of Growth";
	setNames[66] = "Sigil of Loyalty";
	setNames[39] = "Sigil of Vigor";
	setNames[13] = "Soldier";
	setNames[22] = "Swift";
	setNames[17] = "Tinker";
	setNames[49] = "Weave of Alchemy";
	setNames[50] = "Weave of Arcane Lore";
	setNames[83] = "Weave of Assess Creature";
	setNames[52] = "Weave of Assess Person";
	setNames[55] = "Weave of Cooking";
	setNames[56] = "Weave of Creature Enchantment";
	setNames[59] = "Weave of Deception";
	setNames[84] = "Weave of Dirty Fighting";
	setNames[85] = "Weave of Dual Wield";
	setNames[58] = "Weave of Finesse Weapons";
	setNames[60] = "Weave of Fletching";
	setNames[61] = "Weave of Healing";
	setNames[76] = "Weave of Heavy Weapons";
	setNames[62] = "Weave of Item Enchantment";
	setNames[63] = "Weave of Item Tinkering";
	setNames[64] = "Weave of Leadership";
	setNames[65] = "Weave of Life Magic";
	setNames[53] = "Weave of Light Weapons";
	setNames[68] = "Weave of Magic Defense";
	setNames[69] = "Weave of Magic Item Tinkering";
	setNames[70] = "Weave of Mana Conversion";
	setNames[71] = "Weave of Melee Defense";
	setNames[72] = "Weave of Missile Defense";
	setNames[54] = "Weave of Missile Weapons";
	setNames[86] = "Weave of Recklessness";
	setNames[73] = "Weave of Salvaging";
	setNames[87] = "Weave of Shield";
	setNames[88] = "Weave of Sneak Attack";
	setNames[90] = "Weave of Summoning";
	setNames[78] = "Weave of Two Handed Combat";
	setNames[80] = "Weave of Void Magic";
	setNames[81] = "Weave of War Magic";
	setNames[82] = "Weave of Weapon Tinkering";
	setNames[21] = "Wise";

	/* eslint-disable */
	// Raw item properties.
	var RAW_ITEMSET                     = 0x10109;
	var RAW_IMBUED                      = 0x100B3;
	var RAW_RATING_CRIT_DAMAGE_RESIST   = 0x10177;
	var RAW_RATING_CRIT_DAMAGE          = 0x10176;
	var RAW_RATING_CRIT_RESIST          = 0x10175;
	var RAW_RATING_CRIT                 = 0x10174;
	var RAW_RATING_DAMAGE_RESIST        = 0x10173;
	var RAW_RATING_DAMAGE               = 0x10172;
	var RAW_MAX_LEVEL                   = 0x1013F;
	var RAW_XP_STYLE                    = 0x10140;
	var RAW_BASE_XP                     = 0x20000005;
	var RAW_TOTAL_XP                    = 0x20000004;
	var RAW_WORKMANSHIP                 = 0x10069;
	var RAW_ATTACK_TYPE                 = 0x1002F;
	
	// Elemental values.
	var ELEMENT_NETHER = 0x0400;

	// Rend values from the RAW_IMBUED property.
	var REND_CIRTICAL_STRIKE    = 0x001;
	var REND_CRIPPLING_BLOW     = 0x002;
	var REND_ARMOR_RENDERING    = 0x004;
	var REND_SLASH              = 0x008;
	var REND_PIERCING           = 0x010;
	var REND_BLUDGEONING        = 0x020;
	var REND_ACID               = 0x040;
	var REND_COLD               = 0x080;
	var REND_ELECTRICAL         = 0x100;
	var REND_FIRE               = 0x200;

	// Time in miliseconds.
	var ONE_SECOND          = 1000;
	var THIRTY_SECONDS  	= 30 * 1000;
	var TENTH_OF_A_SECOND   = 100;
	var FIVE_SECONDS        = 5 * 1000;
	var QUARTER_SECOND      = 250;
	var ONE_DAY             = 1000 * 60 * 60 * 24;
	var TEN_SECONDS         = 10 * 1000;
	var TWO_SECONDS         = 2 * 1000;
	var HALF_SECOND         = 500;
	var FIFTHTEEN_SECONDS   = 10 * 1000;
	var FIVE_MINUTES        = 5 * 60 * 1000;
	
	// Decal icons
	var ICON_REMOVE = 4600;
	
	// HTTP status codes.
	var HTTP_STATUS_SUCCESS = 200;
	var HTTP_STATUS_FAILURE = 12029;
	
	var EQM_AETHERIA_BLUE       = 0x10000000;
	var EQM_AETHERIA_YELLOW     = 0x20000000;
	var EQM_AETHERIA_RED        = 0x40000000;
	
	var COLOUR_WHITE    = 0xFFFFFF;
	var COLOUR_RED      = 0x0000FF;

	var ROUND_STATS_TO = 4; // should be a config setting.
	var MIN_FREE_SLOTS = 6; // should be a config setting.
	
	var SPELL_LEGENDARY_BLOOD_THIRST_BONUS = 10;
	var SPELL_EPIC_BLOOD_THIRST_BONUS = 7;
	var SPELL_MAJOR_BLOOD_THIRST_BONUS = 4;
	var SPELL_MINOR_BLOOD_THIRST_BONUS = 2;
	
	var SPELL_LEGENDARY_SPIRIT_THRIST_BONUS = 0.07;
	var SPELL_EPIC_SPIRIT_THRIST_BONUS = 0.05;
	var SPELL_MAJOR_SPIRIT_THRIST_BONUS = 0.03;
	var SPELL_MINOR_SPIRIT_THRIST_BONUS = 0.01;
	
	var SPELL_LEGENDARY_DEFENDER_BONUS = 0.09;
	var SPELL_EPIC_DEFENDER_BONUS = 0.07;
	var SPELL_MAJOR_DEFENDER_BONUS = 0.05;
	var SPELL_MINOR_DEFENDER_BONUS = 0.03;
	
	/* eslint-enable */
	
	/*
	 * onInitialize - Loads the script's dependencies.
	 *
	 */
	core.on("onInitialize", function onInitialize() {
		core.debug("<onInitialize>");
		
		core.skunkConfig = new core.Config("./configs", defaultConfig).load().save();

		if (core.skunkConfig.profile.logChatToFile == true) {
			skapi.InvokeChatParser("/log " + skapi.szWorld + "-" + (skapi.acoChar && skapi.acoChar.szName || "UNKNOWN") + "-" + core.getDateString({seperator: " "}));
		}

		SkunkSchema     = core.SkunkSchema  = LibStub("SkunkSchema-1.0");
		SkunkComm       = core.SkunkComm    = LibStub("SkunkComm-1.0");
		SkunkDB         = core.SkunkDB      = LibStub("SkunkDB-1.0");
		SkunkHTTP       = core.SkunkHTTP    = LibStub("SkunkHTTP-1.0");
		SkunkLayout     = core.SkunkLayout  = LibStub("SkunkLayout-1.0");
		SkunkProcess    = core.SkunkProcess = LibStub("SkunkProcess-1.0");
		// eslint-disable-next-line no-unused-vars
		SkunkTimer      = core.SkunkTimer   = LibStub("SkunkTimer-1.0");

		//SkunkTimer.debug = core.debug;
		SkunkAssess      = core.SkunkAssess  = LibStub("SkunkAssess-1.1");
		SkunkAssess.debug = core.debug;
		
		SkunkActions      = core.SkunkActions  = LibStub("SkunkActions-1.0");
		SkunkActions.debug = core.debug;
		
		SkunkLogger        = LibStub("SkunkLogger-1.0");
		SkunkLogger.emitter.on("error", onLoggerMessage);
		SkunkLogger.emitter.on("warn", onLoggerMessage);
		SkunkLogger.emitter.on("info", onLoggerMessage);
		SkunkLogger.emitter.on("debug", onLoggerMessage);
		SkunkLogger.emitter.on("trace", onLoggerMessage);
		SkunkLogger.emitter.on("console", onLoggerMessage);
		core.chatPrefix = "ST: ";
		
		core.checkForUpdate();
		
		core.initInventory = core.getInventoryItems();

		core.info(core.title + " initialized.");
		core.info("Skunkworks " + skapi.szVersion + ", AC: " + skapi.szVersionAC);

		// Remove old allegianceInfo.
		var filePath = _dir + "src\\data\\" + skapi.szWorld + "\\allegianceInfo.json";
		if (core.existsSync(filePath)) {
			core.unlinkSync(filePath);
		}

		core.allegianceInfo = new core.Cache().load({
			path: _dir + "src\\data\\" + skapi.szWorld + "\\allegianceInfo_v2.json"
		});

		core.combineTransactions();

		core.stats.initializeTime = new Date().getTime();
	});
	core.initInventory = [];
	
	/*
	 * onEnable - Enables the script.
	 *
	 */
	core.on("onEnable", function onEnable() {
		core.debug("<onEnable>");
		
		if (config.debugToLog == true) {
			core.checkLogSize(true);
			core.setInterval(core.checkLogSize, 60 * ONE_SECOND);
		}

		
		core.loadCharacterConfig();
		
		if (core.skunkConfig.profile.loadLootProfile3 == true) {
			// Load LootProfile3 into memory.
			core.loadLootProfile();
		}
		
		core.createGUI();
		if (skapi.plig == pligInWorld) {
			//core.loadUILayout();
			core.showGUI(config.showGUIOnStartup);
			core.updateGUI();
			
			core.setupStatusList();
		}

		core.addHandler(evidOnTradeStart, core);
		core.addHandler(evidOnTradeAdd, core); // needed for tracking their items.
		core.addHandler(evidOnTradeEnd, core);
		core.addHandler(evidOnTradeReset, core);
		core.addHandler(evidOnTradeAccept, core);
		core.addHandler(evidOnTipMessage, core);
		core.addHandler(evidOnLogon, core);
		core.addHandler(evidOnCommand, core);
		core.addHandler(evidOnStartPortalSelf, core);
		core.addHandler(evidOnEnd3D, core);
		core.addHandler(evidOnDisconnect, core);
		core.addHandler(evidOnAddToInventory, core);
		core.addHandler(evidOnRemoveFromInventory, core);
		core.addHandler(evidOnAdjustStack, core);
		core.addHandler(evidOnAssessCreature, core);
		core.addHandler(evidOnChatBoxMessage, core);
		core.addHandler(evidOnChatServer, core);
		


		SkunkComm.emitter.on("onMessage", core.onMessage);
		
		core.setTimeout(function announceTimer() {
			core.debug("<announceTimer>");

			if (core.isActive()) {
				//var method = core.announcmentMethods.shift();
				var length = core.announcmentMethods.length;
				
				var method;
				while (length--) {
					method = core.announcmentMethods.shift();
					core.announcmentMethods.push(method);
					if (method()) break;
				}
			}
			
			core.setTimeout(announceTimer, (FIVE_MINUTES + (10 * 60 * ONE_SECOND * Math.random())));
		}, 60 * ONE_SECOND);
		core.announcmentMethods.push(core.announceSomething); // Rotate between multiple announce functions.
		
			
		skapi.SetIdleTime(config.clientIdleTimeout);
		
		if (config.jumpTimer == true) {
			core.setInterval(function jumpTimerWithSleep() {
				if (core.isActive()) {
					core.fakeJump();
				}
				core.debug("cmsecQueueDelay: " + skapi.cmsecQueueDelay);
			}, 60 * ONE_SECOND);
		}
		

		core.getSquelchedPlayers(undefined, function onSquelchedPlayers(err, players) {
			core.debug("<onSquelchedPlayers>", err, players);
			if (err) return;
			core.squelchedPlayers = players;
			core.info("Ignoring trades from " + players.length + " squelched players.");
		});
		
		
		core.loadScriptManager();


		//core.controls.chkActive.setChecked(config.activateOnStartup);

		// Items we were wearing on startup.
		core.startupGear = core.getEquippedItems();

		core.scannedInventory = new core.Cache({
			path      : _dir + "src\\data\\" + skapi.szWorld + "\\items",
			filePerKey: true
		});

		/*
		// Get list of assessed items in our inventory.
		var items = core.getAssessedInventoryItems();
		var flattened = core.getFlattenItems({
			items: items
		});
		*/
		
		var items = core.getInventoryItems();
		core.info("Assessing inventory... (" + items.length + " items)");
		core.refreshItemData({
			items: items
		}, onDoneFullItems);

		function onDoneFullItems(err, results) {
			if (err) {
				return core.error("Error while refreshing full item profiles", err);
			}
			core.debug("Finished scanning inventory, " + results.numItemsWithProfiles + " items match enabled loot profiles.");
			core.initializedInventoryData = true;
		}
		
		core.stackInventory(undefined, function(err) {
			if (err) return core.warn("Error while stacking inventory", err);
		});
		
		core.salvagePricing = new core.Cache({
			path: core.getSalvagePricesPath()
		}).load();
		
		function checkUnreadMessages() {
			var messages = core.getUnreadMessages();
			if (messages.length > 0) {
				core.info("You have " + messages.length + " unread messages. Check the 'Msg' tab.");
			}
		}
		core.setInterval(checkUnreadMessages, FIVE_MINUTES);
		checkUnreadMessages();
		
		if (core.isActive()) {
			core.emit("onActivate");
		}
	});

	/*
	 * OnLogon - Event handler for the OnLogon event.
	 *
	 * @param {object} acoChar - AC object for our character.
	 */
	core.OnLogon = function OnLogon(acoChar) {
		core.trace("<OnLogon>", acoChar, skapi.plig);
		
		if (skapi.plig == pligInPortal) {
			//core.flushStateStack();

			// Load character's config.
			core.loadCharacterConfig();
			
			// Create  GUI.
			core.showGUI(config.showGUIOnStartup);
			core.updateGUI();
			
			core.setupStatusList();
		}
	};
	
	/*
	 * onMessage - SkunkComm message handler.
	 *
	 * @param {Object} params - Function parameters
	 */
	core.onMessage = function onMessage(params) {
		var evid        = params.evid;
		var szSender    = params.szSender;
		var szMsg       = params.szMsg;
		var szChannel   = params.szChannel;
		
		core.debug("<onMessage>", evid || szChannel, szSender, szMsg);
		
		// Ignore messages if we're not active.
		if (!core.isActive()) return;

		// Ignore messages starting with // (usually our messages)
		if (szMsg.match(/^\/\//i)) return;
		
		// All channel commands.
		if (szMsg.match(/^(!|\$)search (.*)/i)) {
			var szItem = RegExp.$2;

			if (!config.respondToPriceChecks[evid] && !config.respondToPriceChecks[szChannel]) {
				core.debug("We're configured not to respond to pricechecks from evid " + evid + " / channel " + szChannel + ".");
				return;
			}
			
			core.processSearchCommand({
				szSender: szSender,
				szItem  : szItem
			});
			return;
		}

		if (evid != evidOnTell) return;
		
		if (core.waitingForPassword == true) return;
		
		// Tell only commands.
		if (szMsg.match(/^help$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpResponse)});
			
		} else if (szMsg.match(/^howto/i) || szMsg.match(/^help howto/i)) {
			return core.tell({
				szRecipient: szSender, 
				szMsg      : [
					"// Open trade window to view available items, use 'check' command to get the price of a item and 'points' to view acceptable payment.",
					"// Once decided, clear trade window and use the 'add' command to request a item be added to the trade window.",
					"// Place payment into the trade window and issue the 'buy' command to complete the trade."
				]
			});
		} else if (szMsg.match(/^help check$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpCheckResponse)});
		} else if (szMsg.match(/^help points$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpPointsResponse)});
		} else if (szMsg.match(/^help show$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpShowResponse)});
		} else if (szMsg.match(/^help clear$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpClearResponse)});
		} else if (szMsg.match(/^help add$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpAddResponse)});
		} else if (szMsg.match(/^help total$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpTotalResponse)});
		} else if (szMsg.match(/^help buy$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpBuyResponse)});
		} else if (szMsg.match(/^help balance$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpBalanceResponse)});
		} else if (szMsg.match(/^check$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.checkResponse)});
		} else if (szMsg.match(/^about$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.aboutResponse, {version: core.title})});
		} else if (szMsg.match(/^balance$/i)) {
			var theirBalance = core.getAccountBalance({szPlayer: szSender}) || 0;
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.balanceResponse, {balance: theirBalance})});
		} else if (szMsg.match(/^total$/i)) {
			
			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notMyPartner)});
			}
			
			//var myItems = core.coacoToArray(skapi.coacoTradeRight);
			var myItems = core.tradeWindow && core.tradeWindow[myTradeSide] || [];

			if (myItems.length == 0) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.nothingToBuy)});
			}
			
			var start = new Date().getTime();
			var total = core.getTotalCostOfItems({
				items: myItems,
				szWho: szSender
			});
			var elapsed = new Date().getTime() - start;
			core.debug("myItems: " + myItems.length + ", total: " + total + ", elapsed: " + elapsed);

			core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.myItemsValue, {count: myItems.length, value: total})});

			var discount = core.getPlayerDiscount({
				szPlayer: szSender
			});
		
			if (discount && typeof discount.amount !== "undefined" && discount.amount != 0) {
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.discountReason, {percentage: discount.amount, reason: discount.reason})});
			}
			
			return;

			/*
		} else if (szMsg.match(/^check (.*|)(:|,)(\d*)/i)) {
			var szName = RegExp.$1;
			var szValue = RegExp.$3;

			var value = Number(szValue);
			if (!isInt(value)) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.valueNotANumber, {value: szValue})});
			}
			
			core.handleCheckCommand({
				szName  : szName,
				szSender: szSender,
				value   : value
			});
			
			return;
			*/
		} else if (szMsg.match(/^check (.*)/i)) {
			var szName = RegExp.$1;
			
			core.handleCheckCommand({
				szName  : szName,
				szSender: szSender
			});
			
			return;
		} else if (szMsg.match(/^points$/i)) {

			var path = core.getPointsPath();
			var points = core.getJsonFile({path: path}) || {};
			
			var items = [];

			var i;
			for (var key in points) {
				//if (!points.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(points, key)) continue;
				if (!points[key].enabled) continue;
				i = items.push(points[key]);
				items[i - 1].szName = key;
			}
			
			if (items.length == 0) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noPointsYet)});
			}
			
			items.sort(function compare(a, b) {
				if (a.value < b.value) return -1;
				if (a.value > b.value) return 1;
				
				if (a.szName < b.szName) return -1;
				if (a.szName > b.szName) return 1;
				return 0;
			});
			
			var szItems = items.map(function(item, i) {
				if (i == 0) {
					return item.szName + " [" + item.value + " points]";
				} else {
					return item.szName + " [" + item.value + "]";
				}
			}).join(", ");

			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.paymentItems, {items: szItems})});
		} else if (szMsg.match(/^show (.*)/i)) {
			var string = RegExp.$1;
			
			skapi.TradeReset();
			var items = core.getItemsMatchingSearchString({string: string});

			core.tell({
				szRecipient: szSender, 
				szMsg      : "// Showing " + items.length + " items matching your query '" + string + "'."
			});
		
			return core.addItemsToTradeWindow({items: items}, function(err) {
				if (err) {
					return core.tell({
						szRecipient: core.tradePartner.szName, 
						szMsg      : core.arrayTemplate(_l.errorOccurred, {
							message: err.message
						})
					});
				}
			});
		} else if (szMsg.match(/^show$/i)) {
			
			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notMyPartner)});
			}
			
			skapi.TradeReset();
			
			core.showItemsForSale({szPlayer: szSender}, noop);

			return;
		} else if (szMsg.match(/^clear$/i)) {
			skapi.TradeReset();

			return;
		} else if (szMsg.match(/^add$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addResponse)});

			/*
		} else if (szMsg.match(/^add (.*|)(:|,)(\d*)/i)) {
			var szName = RegExp.$1;
			var szValue = RegExp.$3;

			var value = Number(szValue);
			if (!isInt(value)) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.valueNotANumber, {value: szValue})});
			}

			core.processAddRequest({
				szSender: szSender,
				szName  : szName,
				value   : value
			});

			return;
			*/
		} else if (szMsg.match(/^add (.*) \* (\d*)/i)) {
			var szName = RegExp.$1;
			var szAmount = RegExp.$2;
			var amount = Number(szAmount);
					
			if (!isInt(amount)) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.valueNotANumber, {value: szAmount})});
			}

			core.processAddRequest({
				szSender: szSender,
				szName  : szName,
				amount  : amount
			});
		
			return;
			
		} else if (szMsg.match(/^add (.*)/i)) {
			var szName = RegExp.$1;

			core.processAddRequest({
				szSender: szSender,
				szName  : szName
			});
			
			return;

			/*
		} else if (szMsg.match(/^add2 (.*)/i)) {
			var value = RegExp.$1;
			core.console("value: " + value);
			
			var items = core.getItemsMatchingSearchString({string: value});
			core.console("items1: " + typeof items);
			core.console("items2: " + items.length);
			
			core.console("items3: " + items.map(function(aco) {
				return core.getFullName(aco);
			}).join(", "));
			
			return;
			*/
		} else if (szMsg.match(/^buy/i)) {

			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notMyPartner)});
			}
			
			core.processBuyRequest({
				szSender: szSender
			});
		
			return;
		} else if (szMsg.match(/^version$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.versionResponse, {version: core.version})});
		} else if (szMsg.match(/^(loc|location)$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.locationResponse, {location: config.myLocation})});
		} else if (szMsg.match(/^resell$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.resellResponse, {location: config.myLocation})});
		} else if (szMsg.match(/^resell ([-+]?[0-9]*\.?[0-9]+)$/i)) {
			var value = Number(RegExp.$1);

			var slots = core.getFreeSpaces();
			core.debug("slots: " + slots);
			if (slots <= MIN_FREE_SLOTS) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.cantAcceptMoreItems, {slots: slots})});
			}
			
			
			if (!config.players[szSender]) {
				core.info(szSender + " is not listed in the Config>Players list.");
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notAuthorizedSupply)});
			} else if (!config.players[szSender].supplier) {
				core.info(szSender + " is not listed as a supplier in the Config>Players list.");
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notAuthorizedSupply)});
			}

			// Check if they've put any items in the trade window.
			//var theirItems = core.coacoToArray(skapi.coacoTradeLeft);
			var theirItems = core.tradeWindow && core.tradeWindow[theirTradeSide] || [];
			if (theirItems.length == 0 || theirItems.length > 1) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.oneAtATime)});
			}
			
			// Get the item.
			var aco = theirItems[0];
			
			var enabled = core.isSellEnabled({aco: aco});
			if (enabled == false) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.disabledThatItem)});
			}
			var szName = core.getFullName(aco);
			
			if (aco.mcm == mcmSalvageBag) {
				szName += " [w" + core.round(aco.workmanship, 2) + "]";
			}
			
			if (config.allowResellPriceChange == true) {
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.readyToAcceptResell, {value: value, item: szName})});
				sellValue = value;
			} else {
				// Check if we've already assigned a value to the item. Only var others assign a value if there's not one already. 
				var existingValue = core.getAssignedSellValue({aco: aco});
				var sellValue;
				if (existingValue) {
					core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.itemAlreadyValued, {value: existingValue, item: szName})});
					sellValue = existingValue;
				} else {
					core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.readyToAcceptResell, {value: existingValue, item: szName})});
					sellValue = value;
				}
				core.debug("existingValue: " + existingValue + ", sellValue: " + sellValue);
			}

			core.acceptTrade({logger: core.debug}, function onAcceptResellTrade(err, result) {
				if (err) {
					if (err.message == "OTHER_CANCELED_TRADE") {
						core.info("Trade was canceled.");
					} else if (err.message == "TIMED_OUT") {
						skapi.TradeDecline();
						core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.tradeTimedOut)});
					}
					return;
				}

				if (result == true) {
					core.setSellingValue({
						aco    : aco,
						value  : sellValue,
						enabled: true
					});
					core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.resellSuccess, {item: szName, value: sellValue})});
					
					core.queueOrganize = true;
				}
			});
			
			return;
		} else if (szMsg.match(/^addset$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addSetResponse)});
		} else if (szMsg.match(/^addset (.*)/i)) {
			var setName = RegExp.$1;

			var items;
			for (var key in config.itemSets) {
				//if (!config.itemSets.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(config.itemSets, key)) continue;
				if (key.toLowerCase() == setName.toLowerCase()) {
					items = config.itemSets[key].items;
					setName = key;
					break;
				}
			}
			if (!items) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noSetMatches, {name: setName})});
			}
			
			skapi.TradeReset();
			core.setTimeout(function() {
				core.addItemSetToTrade({
					setName: setName,
					szSender: szSender,
					logger: core.console
				});
			}, 250);

			return;
		} else if (szMsg.match(/^listsets$/i)) {
			var sets = [];
			for (var key in config.itemSets) {
				//if (!config.itemSets.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(config.itemSets, key)) continue;
				sets.push(key);
			}
			var szSets = sets.join(", ");
			core.debug("sets: " + sets.length + ", szsets: " + szSets);
			
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.availableSets, {sets: szSets})});
		} else if (szMsg.match(/^checkset$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpCheckSetResponse)});
		} else if (szMsg.match(/^checkset (.*)$/i)) {
			var setName = RegExp.$1;

			var items;
			for (var key in config.itemSets) {
				//if (!config.itemSets.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(config.itemSets, key)) continue;
				if (key.toLowerCase() == setName.toLowerCase()) {
					items = config.itemSets[key].items;
					setName = key;
					break;
				}
			}
			if (!items) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noSetMatches, {name: setName})});
			}
			
			var setItems = core.getEnabledItemSetItems({
				setName: setName
			});
			
			var total = setItems.reduce(function(accumulator, setItem) {
				var itemName = setItem.itemName;
				return accumulator + core.getSellValue({
					szName  : itemName, 
					szPlayer: szSender
				});
			}, 0);
			
			return core.tell({
				szRecipient: szSender,
				szMsg      : core.arrayTemplate(_l.checkSetResponse, {
					setName: setName,
					count  : setItems.length,
					value  : total
				})
			});
		} else if (szMsg.match(/^addspell$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addSpellResponse)});
		} else if (szMsg.match(/^addspell (.*)/i)) {
			var szSpell = RegExp.$1;
			
			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notMyPartner)});
			}
			
			core.processAddSpellRequest({
				szSender: szSender,
				szSpell : szSpell
			});
			
			return;
		} else if (szMsg.match(/^message$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.messageResponse)});
		} else if (szMsg.match(/^message (.*)/i)) {
			var szMessage = RegExp.$1;
			
			// Path to the messages file.
			var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\messages.sdb";
			
			// Create file if it doesn't exist yet.
			if (!core.existsSync(path)) core.touchSync(path);
			
			// Write message to file.
			SkunkDB.insert().into(path)
				.values([{
					date     : core.getDateString({seperator: " "}),
					szSender : szSender,
					szMessage: szMessage,
					time     : new Date().getTime(),
					read     : false
				}])
				.execute();
			
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.messageRecorded)});
		} else if (szMsg.match(/^equip$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.equipResponse)});
		} else if (szMsg.match(/^equip (.*)(:|,)(\d*)/i)) {
			var szName = RegExp.$1;
			var szValue = RegExp.$3;
			
			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notMyPartner)});
			}
			
			var value = Number(szValue);
			if (!isInt(value)) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.valueNotANumber, {value: szValue})});
			}
			
			if (config.equipItemsOnRequest != true) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.commandNotEnabled)});
			}

			core.processEquipRequest({
				szName  : szName,
				szValue : szValue,
				value   : value,
				szSender: szSender
			});
			return;
		} else if (szMsg.match(/^equip (.*)/i)) {
			var szName = RegExp.$1;
			
			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.notMyPartner)});
			}
			
			if (config.equipItemsOnRequest != true) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.commandNotEnabled)});
			}
			
			core.processEquipRequest({
				szName  : szName,
				szSender: szSender
			});
			return;
		} else if (szMsg.match(/^equipset$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.helpCheckSetResponse)});
		} else if (szMsg.match(/^equipset (.*)$/i)) {
			var setName = RegExp.$1;

			if (config.equipItemsOnRequest != true) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.commandNotEnabled)});
			}

			core.processEquipSetRequest({
				setName : setName,
				szSender: szSender
			});
			return;
		} else if (szMsg.match(/^count (.*)/i)) {
			var name = RegExp.$1;
			return core.handleCountCommand({
				szSender: szSender,
				name    : name,
				szMsg   : szMsg
			});
		} else if (szMsg.match(/^cmdcount (.*)/i)) {
			var name = RegExp.$1;
			return core.handleCountCommand({
				szSender: szSender,
				name    : name,
				json    : true,
				szMsg   : szMsg
			});
		} else if (szMsg.match(/^count$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.countHelp)});
		} else if (szMsg.match(/^search$/i)) {
			
			core.tell({szRecipient: szSender, szMsg: "// Search for items by name, element, skill, rend, set, slot, spell, wield and/or workmanship. ex: 'search diamond powder'"});
			
			var messages = [];
			var elements = Object.keys(core.elementNames).map(function(key) {
				return core.elementNames[key][0];
			});
			elements.sort();
			messages.push("// Element names: " + elements.join(", ") + ". ex: 'search element:acid'");

			var messages = [];
			var rends = Object.keys(core.rendNames).map(function(key) {
				return core.rendNames[key][0];
			});
			rends.sort();
			messages.push("// Rend names: " + rends.join(", ") + ". ex: 'search rend:acid slot:wand'");
			
			var skills = Object.keys(core.skillNames).map(function(key) {
				return core.skillNames[key][0];
			});
			skills.sort();
			messages.push("// Skill names: " + skills.join(", ") + ". ex: 'search skill:war'");

			messages.push("// Set names: Hearty, Sigil of Growth, Weave of War Magic, ect. ex 'search Aetheria set:Growth'");

			var slots = Object.keys(core.slotNames).map(function(key) {
				return core.slotNames[key][0];
			});
			slots.sort();
			messages.push("// Slot names: " + slots.join(", ") + ". ex: 'search slot:wand element:acid'");

			messages.push("// Spell example: 'search spell:\"epic defender\" slot=wand'");
			
			messages.push("// Wield example: 'search wield:310 slot:wand element:acid");

			messages.push("// Workmanship example: 'search steel salvage workmanship:6'");

			async_js.forEachSeries(messages, function(message, callback) {
				core.tell({
					szRecipient: szSender, 
					szMsg      : message
				});
				core.setTimeout(callback, ONE_SECOND);
			});

			return;
		} else if (szMsg.match(/^search (.*)$/i)) {
			var szItem = RegExp.$1;

			/*
			var props = szItem.split(":");
			var chunks = szItem.split(",");
			core.debug("props: " + props.length, "chunks: " + chunks.length);
			
			if (props.length - 1 > chunks.length) {
				return core.tell({
					szRecipient: szSender, 
					szMsg      : "// You need to use commas between property types."
				});
			}

			var items = core.parseStringForItems({
				string: szItem
			});
			*/
			var items = core.getItemsMatchingSearchString({
				string: szItem
			});
			
			if (items.length == 0) {
				core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.noItemMatching, {name: szItem}) 
				});
				
				var proposal = core.proposeCorrectItem({
					name: szItem
				});
				
				core.tell({
					szRecipient: szSender, 
					szMsg      : "// Perhaps you meant '" + proposal + "'."
				});
				return;
			}
			
			var count = items.reduce(function(tally, aco) {
				return tally += aco.citemStack;
			}, 0);
			
			core.debug("items: " + items.length, "stackCount: " + count);

			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.iHaveMatches, {
					count : count,
					string: szItem
				})
			});
			
			async_js.forEachSeries(items, function(aco, callback) {
				core.tell({
					szRecipient: szSender, 
					szMsg      : "// " + aco.oid + ": " + core.getItemStringSync({aco: aco})
				});
				core.setTimeout(callback, ONE_SECOND);
			});

			return;
		} else if (szMsg.match(/^promo$/i)) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.promoResponse)});
		} else if (szMsg.match(/^promo (.*)/i)) {
			var code = RegExp.$1;
			
			var entry = config.promotionalCodes.find(function(entry) {
				return entry.code == code;
			});
			if (!entry) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.invalidPromo)});
			}
			
			if (entry.enabled != true) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.promoDisabled)});
			}
			
			if (entry.expires > 0) {
				var timeleft = (item.time + (item.expires * 1000 * 60 * 60 * 24)) - new Date().getTime();
				core.info("timeleft: " + timeleft);
				if (timeleft < 0) {
					return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.promoExpired)});
				}
			}

			core.tradePromo = entry;

			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.promoApplying, {discount: entry.discount})});
		}

		var firstCharacter = szMsg.substring(0, 1);
		if (!core.isLetter(firstCharacter)) {
			return core.info("Ignoring unknown message: " + szMsg);
		}
		core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.unknownCommand, {command: szMsg})});
	};

	core.isLetter = function isLetter(str) {
		return str.length === 1 && str.match(/[a-z]/i);
	};

	core.logTransaction = function logTransaction(params) {
		
		var szPlayer = params.szPlayer;
		var myItemStrings = params.myItemStrings;
		var theirItemStrings = params.theirItemStrings;
		var myTotal = params.myTotal;
		var theirTotal = params.theirTotal;
		var debit = params.debit;
		var credit = params.credit;
		var preBalance = params.preBalance;
		var postBalance = params.postBalance;
		
		var discount = core.getPlayerDiscount({
			szPlayer: szPlayer
		});
		
		//var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\transactions\\" + szPlayer + ".sdb";
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\transactions.txt";
		if (!core.existsSync(path)) {
			core.touchSync(path);
		}

		var date = core.getDateString({seperator: " "});
		
		SkunkDB
			.insert()
			.into(path)
			.values([{
				date            : date,
				discount        : discount,
				myTotal         : myTotal,
				theirTotal      : theirTotal,
				preBalance      : preBalance,
				debit           : debit,
				credit          : credit,
				postBalance     : postBalance,
				myItemStrings   : myItemStrings,
				theirItemStrings: theirItemStrings,
				time            : new Date().getTime(),
				player          : szPlayer
			}])
			.execute();
	};
	
	core.combineTransactions = function combineTransactions() {
		// Transactions used to be kept per buyer in separate files.
		
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\transactions";
		if (core.existsSync(path)) {
			var files = core.readdirSync(path);
			var transactions = [];
			files.forEach(function(filePath) {
				var results = SkunkDB.select().from(filePath)
					.execute();
				var fileStats = core.statSync(filePath);
				results.rows.forEach(function(row) {
					row.player = fileStats.BaseName;
				});
				transactions = transactions.concat(results.rows);
				core.unlinkSync(filePath);
			});
			core.unlinkSync(path);
			if (transactions.length > 0) {
				transactions.sort(function(a, b) {
					return a.time - b.time;
				});
				SkunkDB
					.insert()
					.into(_dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\transactions.txt")
					.values(transactions)
					.execute();
			}
		}
	};
	
	/*
	 * getSellableItemMatch - Get a array of items we're selling that matches a string.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szName - Name of the item.
	 * @param {number} params.value - Optional pyreal value of the item.
	 * @returns {array} List of item (aco) matches. 
	 */
	core.getSellableItemMatch = function getSellableItemMatch(params) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		var szName = params.szName;
		var value = params.value;
		var values = params.values;
		var oid = params.oid;
		logger("<getSellableItemMatch>", szName, value, oid);

		var regExp = new RegExp(core.escapeRegExp(szName), "i");
		var items = params._items || core.getItemsForSale();
		var matches = [];
		
		if (oid) {
			var aco = skapi.AcoFromOid(oid);
			if (aco) {
				matches.push(aco);
			}
		}

		logger("Items for sale: " + items.length + ", regExp: " + regExp);

		items.forEach(function(aco) {
			// Check if we're just looking for items of matching pyreal values.
			if (values) {
				var index = values.indexOf(aco.cpyValue);
				if (index >= 0) {
					values.splice(index, 1);
					matches.push(aco);
				}
				return;
			}
			
			
			var szFull = core.getFullName(aco);
			if (szFull.match(regExp)) {
				if (value && value != aco.cpyValue) return;
				matches.push(aco);
			}
		});

		logger("Name matches: " + matches.length);
		
		return matches;
	};

	/*
	 * getItemPrice - Get a item's price.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.res - SkunkComm request object.
	 * @param {string} params.szName - (partial) item name.
	 * @param {string} params.value - Optional item pyreal value.
	 * @param {string} params.szSender - Name of the player requesting a price check.
	 * @param {string} params._matches - Used for unit testing.
	 * @param {string} params._sellValue - Used for unit testing.
	 * @returns {null}
	 
	core.getItemPrice = function getItemPrice(params, callback) {
		function resolve() {
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	// Call callback in a timer so whatever event triggered this resolve can be processed by other handlers first.
		}
		
		var szName = params.szName;
		var value = params.value; 
		var szSender = params.szSender;
		core.debug("<getItemPrice>", szSender, szName, value);
		
		core.getItemsFromChat({
			szName  : szName,
			value   : value,
			szSender: szSender
		}, onItems);
		
		function onItems(err, matches) {
			if (err) return callback(err);
			var aco = matches[0];

			var sellValue = core.getSellValue({
				aco     : aco, 
				szPlayer: szSender
			});
			core.debug("sellValue: " + sellValue);
		
			var szName = core.getFullName(aco);
			if (typeof sellValue === "undefined") {
				//return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.itemNotForSale, {item: szFull})});
				return resolve(new core.Error("NOT_FOR_SALE").set("szFull", szName));
			}

			if (aco.mcm == mcmSalvageBag) {
				szName += " [w" + core.round(aco.workmanship, 2) + "]";
			}
			core.debug("szName: " + szName);

			resolve(undefined, {
				szName: szName, 
				value : sellValue, 
				aco   : aco
			});
		}
	};
	*/
	
	/*
	 * loadScriptManager - Load ScriptManager (if available) to run additional scripts simultaneously.
	 */
	core.loadScriptManager = function loadScriptManager() {
		core.trace("<loadScriptManager>");
		
		if (typeof LibStub("SkunkScript-1.0").getScript("ScriptManager", true) !== "undefined") {
			core.console("ScriptManager is already running.");
			return;
		}
		
		var path = "..\\ScriptManager\\src\\core.js";
		if (fs.existsSync(path)) {
			core.debug("Loading ScriptManager...");
			var script = core.ScriptManager = require(path);
			script.initialize();
			script.enable();
		}
	};

	/*
	 * onDisable - Disables event handlers and timers.
	 *
	 * @param {none} params - Collection of arguments.
	 * @returns {null}
	 */
	core.on("onDisable", function onDisable() {
		core.debug("<onDisable>");
	});

	var statePadding = 0; // Gets updated in initializeStates();
	core.debugMessagePadding = 100;
	
	/*
	 * debugToLog - Write a debugging message to a log file.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.path - Path of the log file.
	 * @param {string} params.szMsg - Message to be written.
	 * @param {string} params.szSuffix - Optional message to be written to the end of the message.
	 * @param {array} params.stacktrace - Optional list of functions that are being called.
	 * @returns {undefined}
	 */
	
	var previousDebug;
	core.debugToLog = function debugToLog(params) { // path, szMsg, szSuffix
		var path = params.path;
		var szMsg = params.szMsg;
		var szSuffix = params.szSuffix;

		if (!core.existsSync(path)) {
			core.touchSync(path);
		}
		
		// Remove new line characters.
		var szMsg = szMsg.replace(/(\r\n|\n|\r)/gm, "");
		if (szMsg == previousDebug) return;
		previousDebug = szMsg;

		var szState = core.padString(core.getTopState() || "", statePadding);

		var stacktrace = params.stacktrace; // || getStackTrace();
		
		var szStack = "";
		if (stacktrace) {
			szStack = "| " + Array(stacktrace).join("/");

			//var stackSize = Math.max(stacktrace.length-1, 0);
			//var stackDepthSpace = Array(stackSize).join(" ");
			//szMsg = stackDepthSpace + szMsg;
			if (typeof szSuffix !== "undefined") {
				szMsg = core.padString(szMsg, core.debugMessagePadding - (szSuffix.length + 2) - szState.length);
				szMsg += "[" + szSuffix + "]";
			} else {
				szMsg = core.padString(szMsg, core.debugMessagePadding - szState.length);
			}
		}
		var nowString = core.getDateString({includeMS: true});
		core.appendFileSync(path, nowString + ": " + "[" + szState + "] " + szMsg + szStack);
	};
	
	/*
	 * getDateString - Get a date & time string.
	 *
	 * @param {Object} params - Function parameters
	 * @param {boolean} params.includeMS - Include miliseconds in the string.
	 * @param {string} params.seperator - Optional Seperator
	 * @returns {string} Current date and time.
	 */
	core.getDateString = function getDateString(params) {
		var includeMS = params && params.includeMS;
		var seperator = params && params.seperator || "_";
		
		var now = new Date();
		var day = now.getDate();
		if (day < 10) day = "0" + day;
		var month = now.getMonth() + 1;
		if (month < 10) month = "0" + month;
		var hour = now.getHours();
		if (hour < 10) hour = "0" + hour;
		var sec = now.getSeconds();
		if (sec < 10) sec = "0" + sec;
		var min = now.getMinutes();
		if (min < 10) min = "0" + min;
		var nowString = now.getFullYear();
		nowString += "-" + month;
		nowString += "-" + day;
		nowString += seperator + hour;
		nowString += "-" + min;// I would use : but I use this for file names as well.
		nowString += "-" + sec;
		if (includeMS == true) {
			var ms = String(now.getMilliseconds());
			while (ms.length < 3) {
				ms = "0" + ms;
			}
			nowString += "." + ms;
		}
		return nowString;
	};
	
	/*
	 * padString - Return a string of another string with padding at the end.
	 *
	 * @param {string} szString - String to be padded.
	 * @param {number} maxSize - How long the whole string should be. (Spaces will be added if szString is below maxSize)
	 * @returns {string} String with padding.
	 */
	core.padString = function padString(szString, maxSize) {
		if (szString.length < maxSize) {
			szString += Array(1 + (maxSize - szString.length)).join(" ");
		}
		return szString;
	};
	
	/*
	 * onModuleCreated - Fires when a SkunkScript module is added to a script.
	 *
	 * @param {object} module: Module being created.
	 */
	core.on("onModuleCreated", function onModuleCreated(module) {
		core.debug("<onModuleCreated>", module);
		if (module instanceof core.State) {
			statePadding = Math.max(statePadding, module._stateName.length);
		}
	});

	/*
	 * myDebugLogPath - Returns path to log file.
	 *
	 * @param {none}
	 * @returns {string} Path to log file.
	 */
	var d = new Date();
	var datestring = ("0" + (d.getYear())).slice(-2) + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2) + " " + ("0" + d.getHours()).slice(-2) + "-" + ("0" + d.getMinutes()).slice(-2)  + "-" + ("0" + d.getSeconds()).slice(-2);// + "." + ("00" + d.getMilliseconds()).slice(-3); 
	core.myDebugLogPath = function myDebugLogPath() {
		return "logs\\" + (skapi.szWorld || "WORLD") + "\\" + (skapi.acoChar && skapi.acoChar.szName || "UNKNOWN") + "\\" + datestring + ".log";
	};
	
	/*
	 * onLoggerMessage - Fires when SkunkLogger receives a log message.
	 * 
	 * @param {Object} params - Function parameters
	 */
	function onLoggerMessage(params) {
		//core.console("<onLoggerMessage>");
		if (config.debugToLog == true) { // config.debugToLog
			var args = params.messages;
			var szSuffix;
			if (params.methodName != "debug") {
				szSuffix = params.methodName.toUpperCase();
			}
			core.debugToLog({path: core.myDebugLogPath(), szMsg: args.join(', '), szSuffix: szSuffix, stacktrace: params.stacktrace});
		}
	}
	
	/*
	 * checkLogSize - Checks the size of a log file and rotates it if needed.
	 *
	 * @param {boolean} forceRotate - Force the creation of a new log file.
	 * @returns {undefined}
	 */
	core.checkLogSize = function checkLogSize(forceRotate) {
		//if (new Date() - lastLogSizeCheck < 1000*60) return;
		//lastLogSizeCheck = new Date();
		
		var maxDebugLogSize = config.maxDebugLogSize;

		var logPath = core.myDebugLogPath(); // _logsDir+"/"+GetLogName();
		core.debug("logPath: " + logPath);
		
		if (!core.existsSync(logPath)) {
			core.touchSync(logPath);
		}
		
		var stats = fs.statSync(logPath);
		if (!stats) {
			core.console("Failed to get log file size. logPath: " + logPath + ", exists: " + core.existsSync(logPath));
			return;
		}
		
		core.debug("Size:", stats.Size);
		if (forceRotate == true || stats.Size >= maxDebugLogSize) { // 
			core.info("Rotating log file...");
			var nowString = core.getDateString();
			core.debug("nowString: " + nowString);
			
			var newPath = "logs\\" + nowString + "_" + skapi.szWorld + "-" + skapi.acoChar.szName +  ".debug.log";
			core.debug("newPath: " + newPath);
			
			if (config.compressOldLogs == true) {
				newPath += ".zip";
				fs.compressFileSync(logPath, newPath);
			} else {
				fs.renameSync(logPath, newPath);
			}
			core.debug("Done rotating log.");
		}
	};
	
	core.on("onActivate", function onActivate() {
		core.debug("<onActivate>");
		core.startTicking();
		
		core.setInterval(function cmsecQueueDelayTimer() {
			if (skapi.cmsecQueueDelay > TEN_SECONDS) {
				core.warn("No events from client/server in " + core.round(skapi.cmsecQueueDelay / ONE_SECOND, 1) + " seconds!");
			}
			
			if (skapi.plig == pligNotRunning) {
				core.warn("AC has closed, shutting down... (plig: " + skapi.plig + ")");
				return core.disable();
			}
			
		}, ONE_SECOND);
	});
	
	core.on("onDeactivate", function onDeactivate() {
		core.debug("<onDeactivate>");

		core.currentSession = null;
		core.lastExecutionTime = 0;

		core.stateStack.forEach(function(session2) {
			if (session2._paused == 0) {
				core.emit("onSessionPause", session2);
				session2._paused = new Date().getTime();
			}
		});
	});
	
	(function() {
		
		var Session = core.Session = function Session(params) {
			EventEmitter.apply(this);
			for (var key in params) {
				if (!Object.prototype.hasOwnProperty.call(params, key)) continue;
				this[key] = params[key];
			}
			this._uuid = core.simpleUUID(); //core.generateUUID();
			this._paused = 0;
			this._started = 0;
			this._executing = 0;
			this._created = new Date().getTime();
		};
		Session.prototype = new EventEmitter();
		Session.prototype.constructor = Session;
		
		Session.prototype.toString = function () {
			return "[Session " + this.stateName + "|" + this._uuid + "]";
		};
		
		core.createSession = function createSession(params) {
			var session = new core.Session(params);
			core.emit("onSessionCreate", session);
			return session;
		};
		
		core.states = {};
		core.stateStack = [];
		core.currentSession = null;
		
		core.isStateInStack = function isStateInStack(params) {
			var stateName = params.stateName;
			return core.stateStack.some(function _isState(session) {
				return stateName == session.stateName;
			});
		};
		
		core.flushStateStack = function flushStateStack() {
			core.trace("<flushStateStack>");

			// Clear out the existing stack.
			var sessions = [];
			core.stateStack = core.stateStack.filter(function(session2) {
				//core.emit("onSessionStop", session2);
				//if (!session2._destoryed) {
				//	session2._destoryed = new Date().getTime();
				sessions.push(session2);

				//}
				return false;
			});
			
			var err = new core.Error("FLUSHED");
			sessions.forEach(function(session) {
				core.emit("onSessionDestroy", session, err);
			});
		};
		
		core.popState = function popState(session, err, results) {
			core.trace("<popState>", session, err, results);
			var sessions = [];
			
			core.stateStack = core.stateStack.filter(function(session2) {
				if (session == session2) {
					sessions.push(session);
					return false;
				}
				return true;
			});
			
			sessions.forEach(function(session) {
				core.emit("onSessionDestroy", session, err, results);
			});
		};
		
		core.switchState = function switchState(session, currentSession) {
			core.trace("<switchState>", session, currentSession);
			if (!(session instanceof core.Session)) {
				core.trace("Invalid session");
				throw new core.Error("INVALID_SESSION");
			}

			//var currentSession = (params.currentSession === undefined ? core.currentSession : params.currentSession);
			currentSession = currentSession || core.currentSession;
			if (currentSession) {
				//currentSession.pop(new core.Error("SWITCHED_STATES"));
				core.popState(currentSession);
			}
			core.pushState(session);
			return this;
		};
		
		core.setState = function setState(session) {
			core.trace("<setState>", session);
			if (!(session instanceof core.Session)) {
				core.trace("Invalid session");
				throw new core.Error("INVALID_SESSION");
			}
			core.flushStateStack();
			core.pushState(session);

			//core.setImmediate(core.tick);
			return this;
		};
		
		core.pushState = function pushState(session) {
			core.trace("<pushState>", session);
			
			if (!(session instanceof core.Session)) {
				core.trace("Invalid session");
				throw new core.Error("INVALID_SESSION");
			}

			core.stateStack.forEach(function(session2) {
				if (session2._paused == 0) {
					core.emit("onSessionPause", session2);
					session2._paused = new Date().getTime();
				}
			});
			
			core.stateStack.push(session);
			return this;
		};
		
		core.getTopSession = function getTopSession() {
			if (core.stateStack.length > 0) {
				return core.stateStack[ core.stateStack.length - 1];
			}
		};
		
		core.getTopState = function getTopState() {
			var session = core.getTopSession();
			return session && session.stateName || "??";
		};

		core.startTicking = function startTicking() {
			//core.debug("<startTicking>");

			if (!core.isActive()) {
				return;
			}
			
			if (core.stateStack.length == 0) {
				core.debug("stateStack is empty, adding Idle.");
				core.setState(core.createSession({stateName: "Idle"}));
			}

			var session = core.getTopSession();

			core.debug("Executing " + session.stateName + "...", core.currentSession);
			core.currentSession = session;
			
			core.executeTickFunction({
				session: session
			}, function(err) {
				if (err) {
					switch(err.message) {
					case "SESSION_PAUSED":
					case "SESSION_DESTOYED":
					case "FLUSHED":
					case "STATE_CHANGED":
					case "TRADE_OPENED":
						break;
					default:
						core.warn("Error executing " + session.stateName, err);
					}
				}

				if (session != core.getTopSession()) {
					return core.setImmediate(core.startTicking);
				}
				
				core.setTimeout(core.startTicking, 100);
			});
		};

		core.on("onSessionCreate", function(session, err, results) {
			//core.debug("<onSessionCreate>", session.stateName, err, results);
			session.emit("onSessionCreate", err, results);
		});
		
		core.on("onSessionStart", function onSessionStart(session, err, results) {
			//core.debug("<onSessionStart>", session.stateName, err, results);
			session.emit("onSessionStart", err, results);
		});
		
		core.on("onSessionResume", function onSessionResume(session, err, results) {
			//core.debug("<onSessionResume>", session.stateName, err, results);
			session.emit("onSessionResume", err, results);
		});
		
		core.on("onSessionPause", function onSessionPause(session, err, results) {
			//core.debug("<onSessionPause>", session.stateName, err, results);
			session.emit("onSessionPause", err, results);
		});
		
		core.on("onSessionStop", function onSessionStop(session, err, results) {
			//core.debug("<onSessionStop>", session.stateName, err, results);
			session.emit("onSessionStop", err, results);
		});
		
		core.on("onSessionDestroy", function onSessionDestroy(session, err, results) {
			//core.debug("<onSessionDestroy>", session.stateName, err, results);
			session.emit("onSessionDestroy", err, results);
		});
		
		
		core.executeTickFunction = function executeTickFunction(params, callback) {
			//core.debug("<executeTickFunction>");
			var session = params.session;

			function finish(err, results) {
				//core.debug("<executeTickFunction|finish>");
				if (callback) {
					core.clearTimeout(tid);
					core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
					callback = undefined;

					core.removeListener("onSessionPause", onSessionPause);
					core.removeListener("onSessionDestroy", onSessionDestroy);
					
					session._executing = 0;
					core.emit("onSessionStop", session, err, results);
				}
			}

			if (!session.stateName || !core.states[session.stateName]) {
				core.warn("Invalid state", session.stateName, core.states[session.stateName]);
				core.trace("Invalid state", JSON.stringify(session));
				return finish(new core.Error("INVALID_STATE"));
			}

			function onSessionPause(session2, err, results) {
				core.debug("A session was paused!", (session == session2), session2.stateName);
				if (session2 == session) {
					if (err || results) {
						return core.setImmediate(finish, err, results);
					}
					core.setImmediate(finish, new core.Error("SESSION_PAUSED"));
				}
			}
			core.on("onSessionPause", onSessionPause);
			
			function onSessionDestroy(session2, err, results) {
				core.debug("A session was destoryed!", (session == session2), session2.stateName);
				if (session2 == session) {
					if (err || results) {
						return core.setImmediate(finish, err, results);
					}
					core.setImmediate(finish, new core.Error("SESSION_DESTOYED"));
				}
			}
			core.on("onSessionDestroy", onSessionDestroy);

			core.emit("onSessionStart", session);
			session._started = session._started || new Date().getTime();

			if (session._paused > 0) {
				core.emit("onSessionResume", session);
				session._paused = 0;
			}

			var executeTime = session._executing = core.lastExecutionTime = new Date().getTime();
			var stateFunction = core.states[session.stateName];
			stateFunction({
				session    : session,
				executeTime: executeTime
			}, finish);

			var tid = core.setTimeout(core.timedOut, ONE_SECOND * 60 * 5, finish, "EXECUTE_TICK_TIMED_OUT");
		};
		
	})();

	
	/*
	 * getJsonFile - Get the contents of a text file that is formatted as json.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.path - File path to the file.
	 * @returns {variable} Contents of the file.
	 */
	core.getJsonFile = function getJsonFile(params) {
		if (core.existsSync(params.path)) {
			var json = core.readFileSync(params.path);
			var items;
			try {
				items = JSON.parse(json);
			} catch (e) {
				return;
			}
			return items;
		}
	};
	
	/*
	 * saveJsonFile - Stringify a object and save it to a text file.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.path - File path to the file.
	 * @param {variable} params.data - Data to stringify.
	 * @returns {variable} Contents of the file.
	 */
	core.saveJsonFile = function saveJsonFile(params) {
		var path = params.path;
		var data = params.data;
		var json = JSON.stringify(data, undefined, "\t");
		if (!core.existsSync(path)) {
			core.touchSync(path);
		}
		core.writeFileSync(path, json);
	};
		
	/*
	 * loadCharacterConfig - Reload config from disk.
	 */
	core.loadCharacterConfig = function loadCharacterConfig() {
		require("src\\config.js");
		if (skapi.plig == pligInWorld || skapi.plig == pligInPortal) {
			var character = core.getJsonFile({path: "src\\configs\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + ".json"});
			if (character) {
				for (var key in character) {
					//if (!character.hasOwnProperty(key)) continue;
					if (!Object.prototype.hasOwnProperty.call(character, key)) continue;
					config[key] = character[key];
				}
			}
		}
		
		// Update old config.
		for (var key in config.itemSets) {
			//if (!config.itemSets.hasOwnProperty(key)) continue;
			if (!Object.prototype.hasOwnProperty.call(config.itemSets, key)) continue;
			if (core.isArray(config.itemSets[key])) {
				var items = config.itemSets[key];
				config.itemSets[key] = {};
				config.itemSets[key].items = items;
				config.itemSets[key].restrictIndividualSales = true;
			}
		}

		core.saveCharacterConfig();
	};
	
	/*
	 * sortObject - sorts a object by its keys.
	 *
	 * @param {object} object - Objec to be sorted.
	 * @returns {object} Sorted object.
	 */
	core.sortObject = function factory() {
		function _compare(a, b) {
			if (a > b) return 1;
			if (a < b) return -1;
			return 0;
		}
		return function sortObject(obj, params) {
			var arr = [];
			for (var key in obj) {
				//if (!obj.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(obj, key)) continue;
				if (params && params.keyAsNumber == true) {
					arr.push(Number(key));
				} else {
					arr.push(key);
				}
			}
			arr.sort(_compare);
			var obj2 = {};
			arr.forEach(function(value) {
				obj2[ value ] = obj[ value ];
			});
			return obj2;
		};
	}();
	
	/*
	 * saveCharacterConfig - Save current config to disk.
	 */
	core.saveCharacterConfig = function saveCharacterConfig() {
		if (skapi.plig == pligInWorld || skapi.plig == pligInPortal) {
			var newConfig = core.sortObject(config);
			core.saveJsonFile({
				path: "src\\configs\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + ".json",
				data: newConfig
			});
		}
	};

	/*
	 * simpleUUID - Return a random 4 character string.
	 */
	var simpleUUID = core.simpleUUID = function simpleUUID() {
		// eslint-disable-next-line no-magic-numbers
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
			.substring(1);
	};
	
	function compareElapses(a, b)  {
		if (a.elapsed > b.elapsed) return -1;
		if (a.elapsed < b.elapsed) return 1;
		return 0;
	}

	/*
	 * WaitEvent - Call skapi.WaitEvent(), see skapi's docs for info.
	 *
	 * @param {number} duration - How long to wait in miliseconds.
	 * @param {number} wem - Skunkworks wem type.
	 * @param {number} evid - Skunkworks evid type.
	 */
	core.WaitEvent = function WaitEvent(duration, wem, evid) {
		if (core.inWaitEvent == true) {
			core.trace("<WaitEvent> WaitEvent inside a WaitEvent!");
			core.warn("WaitEvent inside a WaitEvent!");
		}
		
		core.inWaitEvent = true;
		var result;
			
		var uuid = simpleUUID();
		var starting = (new Date()).getTime();
		if (config.profileWaits && duration) {
			core.debug("<WaitEvent> " + uuid + ", ms: " + duration);
			var evidElapses = {};
			var waitTime = duration;
			var remaining = waitTime - ((new Date()).getTime() - starting);
			var waitStart;
			var elapsed;
			var count = 0;
			do {
				waitStart = (new Date()).getTime();
				remaining = waitTime - (waitStart - starting);
				result = skapi.WaitEvent(remaining, wemSingle); // Note to self, 0 can mean OnTimer.
				elapsed = (new Date()).getTime() - waitStart;
				evidElapses[result] = evidElapses[result] || 0;
				evidElapses[result] += elapsed;
				if (result > 0) {
					count += 1;
				}
				if (result == evid || wem == wemSingle) {
					break;
				}
			} while (remaining > 0);
			var events = [];
			for (var evid in evidElapses)  {
				//if (!evidElapses.hasOwnProperty(evid)) continue;
				if (!Object.prototype.hasOwnProperty.call(evidElapses, evid)) continue;
				events.push({evid: evid, elapsed: evidElapses[evid]});
			}
			events.sort(compareElapses);
			var evids = events.map(function(event) {
				return event.evid + "=" + event.elapsed;
			}).join(" ");
			
			elapsed = (new Date()).getTime() - starting;
			core.debug("</WaitEvent> " + uuid + ", count: " + count + ", elapsed: " + elapsed + ", evids: " + evids);
		} else {
			result = skapi.WaitEvent(duration, wem, evid);
		}
		core.inWaitEvent = false;
		return result;
	};
	
	/*
	 * core.round - core.round a number to a number of decimal places.
	 *
	 * @param {number} rnum - Number to core.round.
	 * @param {number} rlength - Number of deciaml places.
	 * @returns {number} core.rounded number.
	 */
	core.round = function round(rnum, rlength) { // Arguments: number to core.round, number of decimal places
		if (rlength == null) rlength = 0;
		return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
	};
	
	/*
	 * isActive - Determin if the Active checkbox is enabled.
	 *
	 * @returns {boolean} true/false
	 */
	core.isActive = function isActive() {
		return config.active;
	};

	/*
	 * doesAcoBelongsToMe - Determin if a aco belongs to our character.
	 *
	 * @param {object} acoItem - Item to check.
	 * @returns {boolean} true/false
	 */
	core.doesAcoBelongsToMe = function doesAcoBelongsToMe(acoItem) {
		if (acoItem.acoWearer && acoItem.acoWearer.oid != skapi.acoChar.oid) {
			return false;
		}
		if (acoItem.acoContainer && acoItem.acoContainer.oty & otyContainer) {
			if (!acoItem.acoContainer.acoContainer || acoItem.acoContainer.acoContainer.oid != skapi.acoChar.oid) {
				return false;
			}
		}
		return true;
	};
	
	/*
	 * getInvRegexCount - Get the number of a items matching a string in our inventory.
	 *
	 * @param {string} regex - String to check.
	 * @returns {number} Number of items in our inventory.
	 */
	core.getInvRegexCount = function getInvRegexCount(regex) {
		var acf = skapi.AcfNew();
		acf.olc = olcInventory | olcEquipped;
		var items = core.coacoToArray(acf.CoacoGet());
		var regExp = new RegExp(core.escapeRegExp(regex), "i");
		return items.reduce(function(accumulator, aco) {
			if (core.phantomItems[aco.oid]) return accumulator;
			if (!core.doesAcoBelongsToMe(aco)) return accumulator;
			var szFull = core.getFullName(aco);
			if (szFull.match(regExp)) {
				//count += aco.citemStack;
				return accumulator + aco.citemStack;
			}
			return accumulator;
		}, 0);
	};
	
	/*
	 * removePointsItem - Remove a item from our points list.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name to be removed.
	 * @returns {boolean} true/false if it's been removed.
	 */
	core.removePointsItem = function removePointsItem(params) {
		var szItem = params.szItem;
		
		var path = core.getPointsPath();
		var points = core.getJsonFile({path: path}) || {};
		delete points[szItem];
		
		core.saveJsonFile({
			path: path,
			data: points
		});
		return true;
	};
		
	/*
	 * setPointsItem - Set the value or enabled state of a item in our points list.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name to be removed.
	 * @param {number} params.value - Point value of the item.
	 * @param {boolean} params.enabled - Enabled state of the item.
	 * @returns {boolean} true/false if it's been updated.
	 */
	core.setPointsItem = function setPointsItem(params) {
		
		var szItem = params.szItem;
		var value = params.value;
		var enabled = params.enabled;

		var path = core.getPointsPath();
		var points = core.getJsonFile({path: path}) || {};
		
		if (!points[szItem]) {
			core.info("Item isn't in the points list.");
			return false;
		}
		
		if (typeof value !== "undefined") points[szItem].value = value;
		if (typeof enabled !== "undefined") points[szItem].enabled = enabled;

		core.saveJsonFile({
			path: path,
			data: points
		});
		return true;
	};
	
	core.pointsItemsList = [];
	
	/*
	 * updatePointsList - Update the points list GUI.
	 */
	core.updatePointsList = function factory() {
		function _compareNames(a, b) {
			if (a.szName > b.szName) return -1; // largest stack first.
			if (a.szName < b.szName) return 1;
			return 0;
		}
		function _processItem(item) {
			core.controls.lstPoints.addRow(
				item.enabled, 
				item.szName,
				item.value,
				(icon_textures + ICON_REMOVE)
			);
			core.pointsItemsList.push(item);
		}
		return function updatePointsList() {
			
			core.console("<updatePointsList>");
			
			core.pointsItemsList = [];
			core.controls.lstPoints.clear();
			core.controls.lstPoints.addRow(
				false, 
				"Item Name",
				"Value",
				(icon_textures + ICON_REMOVE)
			);
			
			var path = core.getPointsPath();
			var points = core.getJsonFile({path: path}) || {};
			
			var items = [];
			
			var i;
			for (var key in points) {
				//if (!points.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(points, key)) continue;
				i = items.push(points[key]);
				items[i - 1].szName = key;
			}
			
			items.sort(_compareNames);
			items.forEach(_processItem);
		};
	}();

	/*
	 * refreshAccountsList - Update the accounts list GUI.
	 */
	core.refreshAccountsList = function refreshAccountsList() {
		core.debug("<refreshAccountsList>");
		
		if (!core.controls.lstAccounts) return;
		
		core.accountList = [];
		var rows = [];
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\accounts\\";
		core.debug("path: " + path);
		if (!core.existsSync(path)) {
			core.mkdirSync(path);
		}
		var files = core.readdirSync(path);
		files.sort();
		
		files.forEach(function(filePath) {
			var stats = core.statSync(filePath);
			if (!stats || stats.Type != "JSON File") return;
			var data = core.getJsonFile({path: filePath});
			if (!data || typeof data.balance === "undefined") return;
			data.szName = stats.Name.substring(0, stats.Name.length - 5);
			core.accountList.push(data);
			rows.push([data.szName, data.balance]);
		});
		core.controls.lstAccounts.setRows(rows);
	};
	
	/*
	 * addPointsItem - Add a item to our points list.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.szItem -  Item name.
	 * @param {number} params.value - Item point value.
	 * @returns {boolean} true/false if it's been added.
	 */
	core.addPointsItem = function addPointsItem(params) {
		
		var szItem = params.szItem;
		var value = params.value;
		
		var path = core.getPointsPath();
		var points = core.getJsonFile({path: path}) || {};
		
		if (points[szItem]) {
			core.info(szItem + " is already in the points list.");
			return false;
		}
		
		points[szItem] = {
			enabled: true,
			value  : value
		};
		
		core.saveJsonFile({
			path: path,
			data: points
		});
		return true;
	};
	
	var lastTansferableItems;
	
	/*
	 * updateSellingLists - Update the selling GUI lists.
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.transferableItems - Array of acos to show.
	 */
	core.updateSellingLists = function updateSellingLists(params) {
		
		var transferableItems = lastTansferableItems = (params && params.transferableItems) || lastTansferableItems;
		
		var uniqueItems = [];
		var commonItems = [];

		var aco;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < transferableItems.length; i++) {
			aco = transferableItems[i];
			if (core.isUnique(aco)) {
				uniqueItems.push(aco);
			} else {
				commonItems.push(aco);
			}
		}
		
		core.debug("transferableitems: " + transferableItems.length + ", commonitems: " + commonItems.length + ", uniqueitems: " + uniqueItems.length);
		
		var path = core.getPricesPath();
		var sellingValues = core.getJsonFile({path: path}) || {};
		 
		// Reset
		core.commonBulkSelected = {};
		core.uniqueBulkSelected = {};
		
		if (core.controls.lstCommon) {
			core.updateCommonList({sellingValues: sellingValues, items: commonItems});
		}
		if (core.controls.lstUnique) {
			core.updateUniqueList({sellingValues: sellingValues, items: uniqueItems});
		}
	};
	
	/*
	 * setSellingValue - Update a selling item's value and/or enabled state.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name (for common items).
	 * @param {number} params.oid - Item oid (for unique items).
	 * @param {number} params.value - Item point value.
	 * @param {boolean} params.enabled - Enabled state.
	 */
	core.setSellingValue = function setSellingValue(params) {
		var aco = params.aco;
		var szItem = params.szItem || aco && core.getFullName(aco);
		var oid = params.oid || aco && core.isUnique(aco) && aco.oid;
		
		var value = params.value;
		var enabled = params.enabled;
		
		//var me = skapi.acoChar.szName;
		var path = core.getPricesPath();
		var items = core.getJsonFile({path: path}) || {};
		
		if (oid) {
			items.unique = items.unique || {};
			items.unique[oid] = items.unique[oid] || {};
			
			var _szItem = szItem;
			if (aco && aco.mcm == mcmSalvageBag) {
				_szItem += " [w" + core.round(aco.workmanship, config.workmanshipRounding) + "]";
			}
			
			if (typeof szItem !== "undefined") items.unique[oid]._szItem = _szItem; // Only there for manually editing json file.
			if (typeof value !== "undefined") items.unique[oid].value = value;
			if (typeof enabled !== "undefined") items.unique[oid].enabled = enabled;
			
		} else {
			items.common = items.common || {};
			
			items.common[szItem] = items.common[szItem] || {};
			if (typeof value !== "undefined") items.common[szItem].value = value;
			if (typeof enabled !== "undefined") items.common[szItem].enabled = enabled;
			
		}
		
		core.saveJsonFile({
			path: path,
			data: items
		});
	};

	/*
	 * createGUI - Create the controls of our GUI.
	 *
	 */
	core.createGUI = function factory() {
		function createStatusTab(notebook) {
			var page = new SkunkSchema.Page({
				parent: notebook, 
				label : "Status"
			});
			var layout = new SkunkSchema.FixedLayout({parent: page});
			

			var chkActive = core.controls.chkActive = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkActive", 
				text   : "Active",
				checked: config.active
			}).on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
				var value = config.active = control.getChecked();
				core.info("Active: " + value);
				core.saveCharacterConfig();
				
				if (value == true) {
					core.emit("onActivate");
				} else {
					core.emit("onDeactivate");
				}
			});

			/*
			var stTodo = new SkunkSchema.StaticText({
				parent: layout, 
				name  : "stTodo",
				text  : "Todo: Add status info to this area..."
			})
			.setAnchor(chkActive, "BOTTOMLEFT")
			.setWidth(200);
			*/
				
			// eslint-disable-next-line no-unused-vars
			var lstStatus = core.controls.lstStatus = new SkunkSchema.List({
				parent: layout, 
				name  : "lstStatus"
			})
				.setAnchor(chkActive, "BOTTOMLEFT")
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()});
				// eslint-disable-next-line no-unused-vars
				//.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {});
		}
			
		function createConfigMainTab(notebook) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Main"
				})
			});
			
			var lstConfig = core.controls.lstConfig = new SkunkSchema.List({
				parent: layout, 
				name  : "lstConfig"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.on("onShow", refreshConfigList)
				// eslint-disable-next-line no-magic-numbers
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth() * 0.75}) // Description
				// eslint-disable-next-line no-magic-numbers
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth() * 0.25}) // Value
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					if (intRow == 0)  return;
					var row = control.rows[intRow];
					var description = row[0];
					if (intCol == 0) {
						core.info(description);
					} else if (intCol == 1) {
						var info = configOptions[description];
						info.click();
					}
				});
			
			function refreshConfigList() {
				var rows = [];
				rows.push(["Description", "Value"]);
				var keys = Object.keys(configOptions);
				keys.sort();
				keys.forEach(function(key) {
					var info = configOptions[key];
					var value = info.value();
					if (value === "undefined") value = "undefined";
					rows.push([key, value]);

					//lstConfig.addRow(key, value);
				});
				lstConfig.setRows(rows);
			}
			
			var configOptions = {};

			configOptions["!Config profile"] = {
				value: function() {
					return String(core.skunkConfig._keyProfile);
				},
				click: function() {
					core.info("Config profiles are a work in progress, old settings haven't been migrated yet.");
					return core.showSelectProfileView({}, function(err, results) {
						if (err) return core.warn("Error: ", err);
						core.info("results: " + results);
						core.skunkConfig.setProfileKey(results);
						core.skunkConfig.loadProfile();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Log chat to file"] = {
				value: function() {
					return String(core.skunkConfig.profile.logChatToFile);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Enable debugging to log?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						core.skunkConfig.profile.logChatToFile = results;
						core.skunkConfig.saveProfile();
						refreshConfigList();
					});
				}
			};

			configOptions["Load LootProfile3 into memory"] = {
				value: function() {
					return String(core.skunkConfig.profile.loadLootProfile3);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Enable debugging to log?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						core.skunkConfig.profile.loadLootProfile3 = results;
						core.skunkConfig.saveProfile();
						refreshConfigList();
						core.info("Restart script if you enabled this option.");
					});
				}
			};

			configOptions["Debug to log file"] = {
				value: function() {
					return String(config.debugToLog);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Enable debugging to log?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.debugToLog = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Drop excess items"] = {
				value: function() {
					return String(config.dropItemsEnabled);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Drop excess items?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.dropItemsEnabled = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Say announcments in chat"] = {
				value: function() {
					return String(config.chatAnnouncments);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Say announcments in chat"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.chatAnnouncments = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Logout on portal space"] = {
				value: function() {
					return String(config.logoutOnPortal);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Logout on portal space"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.logoutOnPortal = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Show identical common items in trade window"] = {
				value: function() {
					return String(config.showDuplicateCommonItems);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Show identical common items?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.showDuplicateCommonItems = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Show identical unique items in trade window"] = {
				value: function() {
					return String(config.showDuplicateUniqueItems);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Show identical unique items?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.showDuplicateUniqueItems = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Location string"] = {
				value: function() {
					return String(config.myLocation);
				},
				click: function() {
					SkunkSchema.showEditPrompt({
						title: "Location string",
						value: config.myLocation
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking for point value", err);
						config.myLocation = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};

			configOptions["Show UI on startup"] = {
				value: function() {
					return String(config.showGUIOnStartup);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Show UI at startup?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.showGUIOnStartup = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Login script enabled"] = {
				value: function() {
					return String(config.autoStartScript);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Login script enabled"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.autoStartScript = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Login script termination"] = {
				value: function() {
					return String(config.terminateLoginScript);
				},
				click: function() {
					core.info("If the SkunkTrader_LoginScript runs without the 'Login Script' setting enabled, terminate skunkworks process.");
					SkunkSchema.showBooleanPrompt({
						title: "Login script termination"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.terminateLoginScript = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Sell stackable items one at a time"] = {
				value: function() {
					return String(config.restrictStackableSales);
				},
				click: function() {
					core.info("GDLe may have a bug that prevents some items from transfering to the buyer. This may fix it for now.");
					
					SkunkSchema.showBooleanPrompt({
						title: "Allow Resell Price Change"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.restrictStackableSales = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};

			configOptions["Equip items on request"] = {
				value: function() {
					return String(config.equipItemsOnRequest);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Equip items on request"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.equipItemsOnRequest = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Equip items on request"] = {
				value: function() {
					return String(config.equipItemsOnRequest);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Equip items on request"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.equipItemsOnRequest = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Save prices/points by world"] = {
				value: function() {
					return String(config.sharePricesPerWorld);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Save prices per world"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.sharePricesPerWorld = results;
						core.saveCharacterConfig();
						refreshConfigList();
						
						core.salvagePricing = new core.Cache({
							path: core.getSalvagePricesPath()
						}).load();

					});
				}
			};
			
			configOptions["Price decay enabled (unique items only)"] = {
				value: function() {
					return String(config.priceDecayEnabled);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Show identical items?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.priceDecayEnabled = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Price decay percentage per day"] = {
				value: function() {
					return String(config.priceDecayPercentage);
				},
				click: function() {

					return SkunkSchema.showEditPrompt({
						title: "Input price decay percentage",
						value: config.priceDecayPercentage
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking for point value", err);
						if (!core.isNumeric(results)) {
							core.warn("Value must be a number.");
							return SkunkSchema.showEditPrompt({
								title: "Input price decay percentage",
								value: config.priceDecayPercentage
							}, onValue);
						}
						if (results < 0 || results > 1) {
							core.warn("Number must be between 0 and 1.");
							return SkunkSchema.showEditPrompt({
								title: "Input price decay percentage",
								value: config.priceDecayPercentage
							}, onValue);
						}
						config.priceDecayPercentage = Number(results);
						core.saveCharacterConfig();
						refreshConfigList();
						
						var value = 25;
						core.console("With a item priced 25 points, here's the price for the next 100 days.");
						
						// eslint-disable-next-line no-restricted-syntax
						for (var i = 1; i <= 100; i++) {
							value = value - (value * config.priceDecayPercentage);
							core.console("Day #" + i, "Price: " + core.round(value, config.valueRounding));
						}
					});
					
					
				}
			};
			
			configOptions["Keep inventory sorted"] = {
				value: function() {
					return String(config.keepInventorySorted);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Keep inventory sorted"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.keepInventorySorted = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
			configOptions["Allow partially used items as payment"] = {
				value: function() {
					return String(config.allowPartiallyUsedItemsAsPayment);
				},
				click: function() {
					SkunkSchema.showBooleanPrompt({
						title: "Allow partially used payment?"
					}, function onValue(err, results) {
						if (err) return core.warn("Error asking question", err);
						config.allowPartiallyUsedItemsAsPayment = results;
						core.saveCharacterConfig();
						refreshConfigList();
					});
				}
			};
			
		}
			
		/*
		function createConfigMainTabOld(notebook) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Main2"
				})
			});

			var chkChatAnnouncments = core.controls.chkChatAnnouncments = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkChatAnnouncments", 
				text   : "Announcments",
				checked: config.chatAnnouncments
			})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.chatAnnouncments = control.getChecked();
					core.saveCharacterConfig();
				});
			
			// eslint-disable-next-line no-unused-vars
			var chkDebugToLog = core.controls.chkDebugToLog = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkDebugToLog", 
				text   : "Debug to log file",
				checked: config.debugToLog
			})
				.setAnchor(chkChatAnnouncments, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.debugToLog = control.getChecked();
					core.saveCharacterConfig();
					core.info("Writing debug messages to file: " + config.debugToLog);
				});
			
			
			var chkPortalLogout = core.controls.chkPortalLogout = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkPortalLogout", 
				text   : "Logout on portal",
				checked: config.logoutOnPortal
			})
				.setAnchor(chkChatAnnouncments, "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.logoutOnPortal = control.getChecked();
					core.saveCharacterConfig();
				});
			
			
			var chkShowCommonDupes = core.controls.chkShowCommonDupes = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkShowCommonDupes", 
				text   : "Show dupes",
				checked: config.showDuplicateCommonItems
			})
				.setAnchor(chkPortalLogout, "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Show duplicate common items in trade window.");
					config.showDuplicateCommonItems = control.getChecked();
					core.saveCharacterConfig();
				});
			
			var stLocation = new SkunkSchema.StaticText({parent: layout, name: "stLocation", text: "Location: "})
				.setAnchor(chkShowCommonDupes, "BOTTOMLEFT")
				.setWidth(50);

			// eslint-disable-next-line no-unused-vars
			var edLocation = new SkunkSchema.Edit({parent: layout, name: "edLocation", text: config.myLocation})
				.setAnchor(stLocation, "TOPRIGHT")
				.setWidth(50)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var value = control.getValue();
					core.info("Location set to " + value);
					config.myLocation = value;
					core.saveCharacterConfig();
				});
				

			// eslint-disable-next-line no-unused-vars
			var chkShowGUIOnStartup = core.controls.chkShowGUIOnStartup = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkShowGUIOnStartup", 
				text   : "Show UI on Startup",
				checked: config.showGUIOnStartup
			})
				.setAnchor(stLocation, "BOTTOMLEFT")
				.setWidth(200)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Show GUI on startup: " + control.getChecked());
					core.info("SkunkWorks uses Decal's old UI system which degrades FPS for most users.");
					config.showGUIOnStartup = control.getChecked();
					core.saveCharacterConfig();
				});

			// eslint-disable-next-line no-unused-vars
			var chkEnableLoginScript = core.controls.chkEnableLoginScript = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkEnableLoginScript", 
				text   : "Enable login script",
				checked: config.autoStartScript
			})
				.setAnchor(chkShowGUIOnStartup, "BOTTOMLEFT")
				.setWidth(150)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.autoStartScript = control.getChecked();
					core.saveCharacterConfig();
				});

			// eslint-disable-next-line no-unused-vars
			var chkTerminateLoginScript = core.controls.chkTerminateLoginScript = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkTerminateLoginScript", 
				text   : "Terminate login script",
				checked: config.terminateLoginScript
			})
				.setAnchor(chkEnableLoginScript, "TOPRIGHT")
				.setWidth(200)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Terminate SkunkWorks if LoginScript is run on a character that's not set to run SkunkTrader.");
					config.terminateLoginScript = control.getChecked();
					core.saveCharacterConfig();
				});

			// eslint-disable-next-line no-unused-vars
			var chkAllowResellPriceChange = core.controls.chkAllowResellPriceChange = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkAllowResellPriceChange", 
				text   : "Allow Resell Price Change",
				checked: config.allowResellPriceChange
			})
				.setAnchor(chkEnableLoginScript, "BOTTOMLEFT")
				.setWidth(200)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.allowResellPriceChange = control.getChecked();
					core.saveCharacterConfig();
				});

			// eslint-disable-next-line no-unused-vars
			var chkRestrictStackableSales = core.controls.chkRestrictStackableSales = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkRestrictStackableSales", 
				text   : "Restrict stackable sales",
				checked: config.restrictStackableSales
			})
				.setAnchor(chkAllowResellPriceChange, "BOTTOMLEFT")
				.setWidth(200)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.restrictStackableSales = control.getChecked();
					core.saveCharacterConfig();
					core.info("GDLe may have a bug that prevents some items from transfering to the buyer. This may fix it for now.");
					core.info("restrictStackableSales: " + config.restrictStackableSales);
				});

			// eslint-disable-next-line no-unused-vars
			var chkEquipItems = core.controls.chkEquipItems = new SkunkSchema.Checkbox({
				parent : layout, 
				name   : "chkEquipItems", 
				text   : "Equip items on request",
				checked: config.equipItemsOnRequest
			})
				.setAnchor(chkRestrictStackableSales, "BOTTOMLEFT")
				.setWidth(200)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					config.equipItemsOnRequest = control.getChecked();
					core.saveCharacterConfig();
					core.info("Allow players to use the equip command, for us to equip a item.");
				});

			// eslint-disable-next-line no-unused-vars
			var btnReloadConfig = core.controls.btnReloadConfig = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnReloadConfig", 
				text  : "Reload config file"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Reloading config...");
					core.loadCharacterConfig();
					core.info("Config loaded.");
				});

			// eslint-disable-next-line no-unused-vars
			var btnRestartScript = core.controls.btnRestartScript = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnRestartScript", 
				text  : "Restart script"
			})
				.setFaceColor(0x0000FF) // BGR
				.setTextColor(0xFFFFFF)
				.setWidth(70)
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.warn("Restarting SkunkTrader... ");
					core.restarting = true;
					core.disable();
				});
			
		}
		*/
			
		function createConfigAnnouncmentsTab(notebook) {
			var announcmentsLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Announcments"
				})
			});
		
			var btnReloadAnnouncments = core.controls.btnReloadAnnouncments = new SkunkSchema.PushButton({
				parent: announcmentsLayout, 
				name  : "btnReloadAnnouncments", 
				text  : "Reload"
			})
				.setWidth(50)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.refreshAnnouncmentsList();
				});
			
			// eslint-disable-next-line no-unused-vars
			var btnNewAnnouncment = core.controls.btnNewAnnouncment = new SkunkSchema.PushButton({
				parent: announcmentsLayout, 
				name  : "btnNewAnnouncment", 
				text  : "New"
			})
				.setWidth(50)
				.setAnchor(btnReloadAnnouncments, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.showInputWindow({
						szMessage: "New Announcment",
						value    : ""
					}, function onValue(err, value) {
						core.debug("<onValue>", err, value);
						if (value) {
							
							config.announcments.push({
								enabled: true,
								message: value
							});
							core.saveCharacterConfig();
							core.refreshAnnouncmentsList();
							core.info("Added new announcment.");
						}
					});
				});
		
			// eslint-disable-next-line no-unused-vars
			var lstAnnouncments = core.controls.lstAnnouncments = new SkunkSchema.List({
				parent: announcmentsLayout, 
				name  : "lstAnnouncments"
			})
				.setAnchor(btnReloadAnnouncments, "BOTTOMLEFT")
				.setWidth(announcmentsLayout.getWidth())
				.setHeight(announcmentsLayout.getHeight() - 20)
				.addColumn({progid: "DecalControls.CheckColumn"})	// enabled
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: announcmentsLayout.getWidth() - 60}) // text
				.addColumn({progid: "DecalControls.IconColumn"}) // delete
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					
					var row = control.rows[intRow];
					var message = row[1];
					
					if (intCol == 0) {
						var ad = config.announcments[intRow];
						
						control.getListPropertyAsync(0, intRow, function(err, value) {
							if (err) return;
							var enabled = value;
							ad.enabled = enabled;
							core.info("Enabled: " + enabled);
							core.saveCharacterConfig();
						});
						
					} else if (intCol == 1) {
						core.info("message: " + message);
						return;
					} else if (intCol == 2) {
						config.announcments.splice(intRow, 1);
						core.info("Announcment removed.");
						core.refreshAnnouncmentsList();
						core.saveCharacterConfig();
					}
				});
		}
		
		function createConfigPlayersTab(notebook) {
			var playersLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Players"
				})
			});
		
			var playersNotebook = new SkunkSchema.Notebook({
				parent: playersLayout, 
				name  : "playersNotebook"
			})
				.setWidth(playersLayout.getWidth())
				.setHeight(playersLayout.getHeight());
		
			var playersMainLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: playersNotebook, 
					label : "Main"
				})
			});

			var btnNewPlayer = core.controls.btnNewPlayer = new SkunkSchema.PushButton({
				parent: playersMainLayout, 
				name  : "btnNewPlayer", 
				text  : "New Player"
			})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var param = {
						szMessage: "New Player",
						value    : ""
					};
				
					core.showInputWindow(param, function onValue(err, value) {
						core.debug("<onValue>", err, value);
						if (err) return;
						
						if (!value) {
							core.warn("Failed to get player name, try again and hit [enter]");
							return core.showInputWindow(param, onValue);
						}

						config.players[value] = {
							supplier       : false,
							playerDiscount : 0,
							patronDiscount : 0,
							monarchDiscount: 0
						};
						core.saveCharacterConfig();
						
						core.refreshPlayersList();
						
						core.info("Added " + value + " to players list.");
					});
				});
		
			// eslint-disable-next-line no-unused-vars
			var lstPlayers = core.controls.lstPlayers = new SkunkSchema.List({
				parent: playersMainLayout, 
				name  : "lstPlayers"
			})
				.setAnchor(btnNewPlayer, "BOTTOMLEFT")
				.setWidth(playersMainLayout.getWidth())
				.setHeight(playersMainLayout.getHeight() - 20)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 100}) // Name
				.addColumn({progid: "DecalControls.CheckColumn"})	// isSupplier
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 50}) // Player Discount
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 50}) // Patron Discount
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 50}) // Monarch Discount
				.addColumn({progid: "DecalControls.IconColumn"}) // delete
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					
					var row = control.rows[intRow];
					
					if (intRow == 0) {
						if (intCol == 0) {
							return core.info("Player's Name");
						} else if (intCol == 1) {
							return core.info("isSupplier, used in resell command.");
						} else if (intCol == 2) {
							return core.info("Player Discount percentage");
						} else if (intCol == 3) {
							return core.info("Patron Discount percentage, their direct vassals will receive this discount percentage.");
						} else if (intCol == 4) {
							return core.info("Monarch Discount percentage, their followers will receive this discount percentage.");
						} else if (intCol == 5) {
							return core.info("Remove the player.");
						}
					}
					
					var szPlayer = row[0];
					core.debug("szPlayer: " + szPlayer);
					
					//var isSupplier = config.players[szPlayer].supplier;
					
					if (intCol == 0) {
						return core.info("Player: " + szPlayer);
					} else if (intCol == 1) {// isSupplier
						
						control.getListPropertyAsync(1, intRow, function(err, value) {
							if (err) return;
							config.players[szPlayer].supplier = value;
							core.saveCharacterConfig();
							core.info("isSupplier: " + value + ", used in resell command.");
						});
					} else if (intCol == 2) { // Player Discount

						core.info("Input a number between 0 and 100 to set " + szPlayer + "'s player discount.");
						core.showInputWindow({
							szMessage: "Player Discount Percentage",
							value    : config.players[szPlayer].playerDiscount || 0
						}, function onValue(err, value) {
							if (err) return;
							var v = Number(value);
							core.debug("value: " + value + ", v: " + v);
							if (!isInt(v) && !isFloat(v)) {
								return core.info("Value (" + value + ") must be a number.");
							}
							
							if (v > 100) { // v < 0 || 
								return core.info("Value (" + value + ") cannot be above 100%.");
							} else if (v < 0) {
								core.warn(v + "% means they'll be charged extra!");
							}

							config.players[szPlayer].playerDiscount = value;
							core.saveCharacterConfig();
							core.info(szPlayer + "'s player discount set to " + v + "%.");
							
							core.refreshPlayersList();
						});
						
					} else if (intCol == 3) { // Patron Discount

						core.info("Input a number between 0 and 100 to set " + szPlayer + "'s patron discount.");
						core.showInputWindow({
							szMessage: "Patron Discount Percentage",
							value    : config.players[szPlayer].patronDiscount || 0
						}, function onValue(err, value) {
							if (err) return;
							var v = Number(value);
							core.debug("value: " + value + ", v: " + v);
							if (!isInt(v) && !isFloat(v)) {
								return core.info("Value (" + value + ") must be a number.");
							}
							
							if (v > 100) {
								return core.info("Value (" + value + ") cannot be above 100%.");
							} else if (v < 0) {
								core.warn(v + "% means they'll be charged extra!");
							}

							config.players[szPlayer].patronDiscount = value;
							core.saveCharacterConfig();
							core.info(szPlayer + "'s patron discount set to " + v + "%. Their direct vassals will receive this discount.");
							
							core.refreshPlayersList();
						});
						
					} else if (intCol == 4) { // Monarch Discount

						core.info("Input a number between 0 and 100 to set " + szPlayer + "'s monarch discount.");
						core.showInputWindow({
							szMessage: "Monarch Discount Percentage",
							value    : config.players[szPlayer].monarchDiscount || 0
						}, function onValue(err, value) {
							if (err) return;
							var v = Number(value);
							core.debug("value: " + value + ", v: " + v);
							if (!isInt(v) && !isFloat(v)) {
								return core.info("Value (" + value + ") must be a number.");
							}
							
							if (v > 100) {
								return core.info("Value (" + value + ") cannot be above 100%.");
							} else if (v < 0) {
								core.warn(v + "% means they'll be charged extra!");
							}

							config.players[szPlayer].monarchDiscount = value;
							core.saveCharacterConfig();
							core.info(szPlayer + "'s monarch discount set to " + v + "%. Their followers will receive this discount.");
							
							core.refreshPlayersList();
						});
						
					} else if (intCol == 5) { // remove
						delete config.players[szPlayer];
						core.saveCharacterConfig();
						core.refreshPlayersList();
						core.info("Deleted " + szPlayer + ".");
					}
				});
				
			var ignoreLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: playersNotebook, 
					label : "Ignore"
				})
			});
		
			// eslint-disable-next-line no-unused-vars
			var btnNewIgnorePlayer = core.controls.btnNewIgnorePlayer = new SkunkSchema.PushButton({
				parent: ignoreLayout, 
				name  : "btnNewIgnorePlayer", 
				text  : "Ignore Player"
			})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var param = {
						szMessage: "Ignore Player",
						value    : ""
					};
				
					core.showInputWindow(param, function onValue(err, value) {
						core.debug("<onValue>", err, value);
						if (err) return;
						
						if (!value) {
							core.warn("Failed to get player name, try again and hit [enter]");
							return core.showInputWindow(param, onValue);
						}

						config.ignoredPlayers[value] = true;
						
						core.saveCharacterConfig();
						core.refreshIgnoredList();
						core.info("Added " + value + " to ignore list.");
					});
				});
		
			// eslint-disable-next-line no-unused-vars
			var lstIgnoredPlayers = core.controls.lstIgnoredPlayers = new SkunkSchema.List({
				parent: ignoreLayout, 
				name  : "lstIgnoredPlayers"
			})
				.setAnchor(btnNewPlayer, "BOTTOMLEFT")
				.setWidth(ignoreLayout.getWidth())
				.setHeight(ignoreLayout.getHeight() - 20)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: ignoreLayout.getWidth() - 40}) // Name
				.addColumn({progid: "DecalControls.IconColumn"}) // delete
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var szName = row[0];
					
					if (intRow == 0) {
						if (intCol == 0) {
							core.info("Player's name name");
						} else {
							core.info("Remove player from ignore list.");
						}
						return;
					}
					
					if (intCol == 0) {
						return core.info("Player: " + szName);
					} else if (intCol == 1) {
						
						delete config.ignoredPlayers[szName];
						core.saveCharacterConfig();
						core.refreshIgnoredList();
						core.info("Removed " + szName + " from ignore list.");
					}
				});
		}
			
		function createConfigTab(notebook) {
			var configPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Config"
			});
		
			var configLayout = new SkunkSchema.FixedLayout({
				parent: configPage
			});	
		
			var configNotebook = new SkunkSchema.Notebook({
				parent: configLayout, 
				name  : "configNotebook"
			})
				.setWidth(configPage.getWidth())
				.setHeight(configPage.getHeight());
				
			var layout1 = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: configNotebook, 
					label : "1"
				})
			});

			var configNotebook1 = new SkunkSchema.Notebook({
				parent: layout1, 
				name  : "configNotebook1"
			})
				.setWidth(layout1.getWidth())
				.setHeight(layout1.getHeight());


			createConfigMainTab(configNotebook1);
			createSellingTab(configNotebook1);
			createPointsTab(configNotebook1);
			createConfigAnnouncmentsTab(configNotebook1);
			createConfigPlayersTab(configNotebook1);
			createAccountsTab(configNotebook1);
			
			var layout2 = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: configNotebook, 
					label : "2"
				})
			});
			
			var configNotebook2 = new SkunkSchema.Notebook({
				parent: layout2, 
				name  : "configNotebook2"
			})
				.setWidth(layout2.getWidth())
				.setHeight(layout2.getHeight());
				
				
			createDropTab(configNotebook2);
			createPromoTab(configNotebook2);
			//createProfileTab(configNotebook2);
		}
		
		function createDropTab(notebook) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Drop"
				})
			});
			
			// eslint-disable-next-line no-unused-vars
			var lstDrop = core.controls.lstDrop = new SkunkSchema.List({
				parent: layout, 
				name  : "lstDrop"
			})
				.on("onShow", refreshList)
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.addColumn({progid: "DecalControls.CheckColumn"})	// isSupplier
				// eslint-disable-next-line no-magic-numbers
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth() - 130}) // Item Name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 70}) // Amount to keep
				.addColumn({progid: "DecalControls.IconColumn"}) // delete
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					if (intRow == 0) return;
					var row = control.rows[intRow];
					var itemName = row[1];
					var info = config.dropItems.find(function(value) {
						return value.itemName == itemName;
					});

					if (intCol == 0) {
						return control.getListPropertyAsync(intCol, intRow, function(err, value) {
							if (err) return;
							info.enabled = value;
							core.saveCharacterConfig();
							core.info(info.itemName + " enabled: " + value);
						});
					} else if (intCol == 2) {
						return SkunkSchema.showEditPrompt({
							title: "Input keep amount",
							value: info.keepAmount
						}, onAmount);

					} else if (intCol == 3) {
						return SkunkSchema.showConfirmationPrompt({
							title: "Remove " + info.itemName + "?"
						}, function onRemoveConfirm(err) {
							if (err) return core.warn("Error asking to row", err);
							config.dropItems = config.dropItems.filter(function(value) {
								return value != info;
							});
							core.saveCharacterConfig();
							refreshList();
						});
					}
					
					function onAmount(err, results) {
						if (err) return core.warn("Error when asking for amount", err);
						if (!core.isNumeric(results)) {
							core.warn("Amount must be a number.");
							return SkunkSchema.showEditPrompt({
								title: "Input keep amount",
								value: 0
							}, onAmount);
						}
						info.keepAmount = parseInt(results, 10);
						core.saveCharacterConfig();
						refreshList();
						core.info("Updated " + itemName + " to be dropped.");
					}
				});
				
			// eslint-disable-next-line no-unused-vars
			var btnAddDrop = core.controls.btnAddDrop = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnAddDrop", 
				text  : "Add item to drop"
			})
				.setAnchor(lstDrop, "BOTTOMLEFT")
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var itemName, keepAmount;
					SkunkSchema.showEditPrompt({
						title: "Input item name",
						value: skapi.acoSelected && core.getFullName(skapi.acoSelected) || ""
					}, onName);
					
					function onName(err, results) {
						if (err) return core.warn("Error when asking for name", err);
						itemName = results;
						SkunkSchema.showEditPrompt({
							title: "Input keep amount",
							value: 0
						}, onAmount);
					}
					
					function onAmount(err, results) {
						if (err) return core.warn("Error when asking for amount", err);
						if (!core.isNumeric(results)) {
							core.warn("Amount must be a number.");
							return SkunkSchema.showEditPrompt({
								title: "Input keep amount",
								value: 0
							}, onAmount);
						}
						keepAmount = parseInt(results, 10);
						
						//core.info("itemName: " + itemName, "keepAmount: " + keepAmount);
						
						config.dropItems.push({
							enabled   : true,
							itemName  : itemName,
							keepAmount: keepAmount
						});
						core.saveCharacterConfig();
						refreshList();
						core.info("Added " + itemName + " to be dropped.");
					}
					
				});
				
			var dropItems;
			function refreshList() {
				var rows = [
					[false, "Item Name", "Keep Amount", (icon_textures + ICON_REMOVE)]
				];
				
				dropItems = config.dropItems.slice();
				dropItems.sort(function(a, b) {
					if (a.itemName < b.itemName) return -1;
					if (a.itemName > b.itemName) return 1;
					return 0;
				});
				
				dropItems.forEach(function(item) {
					rows.push([item.enabled, item.itemName, item.keepAmount, (icon_textures + ICON_REMOVE)]);
				});
				
				lstDrop.setRows(rows);
			}
		}
		
		function createPromoTab(notebook) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Promo"
				})
			});
		
			var btnAddPromo = core.controls.btnAddPromo = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnAddPromo", 
				text  : "Add promo code"
			})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					
					var code, discount, expires;
					
					return SkunkSchema.showEditPrompt({
						title: "Input code",
						value: ""
					}, onCode);
					function onCode(err, result) {
						if (err) return core.warn("Error", err);
						code = result;
						return SkunkSchema.showEditPrompt({
							title: "Discount percentage",
							value: ""
						}, onDiscount);
					}
					function onDiscount(err, result) {
						if (err) return core.warn("Error", err);
						if (!core.isNumeric(result)) {
							core.warn("Value must be a number.");
							return SkunkSchema.showEditPrompt({
								title: "Discount percentage",
								value: ""
							}, onDiscount);
						}
						discount = Number(result);
						if (discount < 0 || discount > 100) {
							core.warn("Value must be between 0 and 100.");
							return SkunkSchema.showEditPrompt({
								title: "Discount percentage",
								value: ""
							}, onDiscount);
						}
						
						core.info("Input how long until this code expires, or 0 for unlimited.");
						return SkunkSchema.showEditPrompt({
							title: "Expires in days",
							value: "0"
						}, onExpires);
					}
					function onExpires(err, result) {
						if (err) return core.warn("Error", err);
						if (!core.isNumeric(result)) {
							core.warn("Value must be a number.");
							return SkunkSchema.showEditPrompt({
								title: "Expires in days",
								value: ""
							}, onExpires);
						}
						expires = Number(result);
						
						config.promotionalCodes.push({
							enabled: true,
							code: code,
							discount: discount,
							expires: expires,
							time: new Date().getTime()
						});
						core.saveCharacterConfig();
						populatePromos();
					}
				});

			var lstPromos = core.controls.lstPromos = new SkunkSchema.List({
				parent: layout, 
				name  : "lstPromos"
			})
				.setAnchor(btnAddPromo, "BOTTOMLEFT")
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.addColumn({progid: "DecalControls.CheckColumn"})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 80}) // name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 80}) // amount
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 80}) // timeleft
				.addColumn({progid: "DecalControls.IconColumn"}) // remove
				.on("onShow", populatePromos)
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					if (intRow == 0) return;
					
					var entry = config.promotionalCodes[intRow - 1];
					//core.info("entry: " + JSON.stringify(entry));
					
					if (intCol == 0) {
						return control.getListPropertyAsync(0, intRow, function(err, value) {
							if (err) return;
							entry.enabled = value;
							core.saveCharacterConfig();
						});
					} else if (intCol == 1) {
						return SkunkSchema.showEditPrompt({
							title: "Input code",
							value: entry.code
						}, onCode);
						function onCode(err, result) {
							if (err) return core.warn("Error", err);
							entry.code = result;
							core.saveCharacterConfig();
							populatePromos();
						}
					} else if (intCol == 2) {
						return SkunkSchema.showEditPrompt({
							title: "Discount percentage",
							value: entry.discount
						}, onDiscount);
						function onDiscount(err, result) {
							if (err) return core.warn("Error", err);
							if (!core.isNumeric(result)) {
								core.warn("Value must be a number.");
								return SkunkSchema.showEditPrompt({
									title: "Discount percentage",
									value: entry.discount
								}, onDiscount);
							}
							discount = Number(result);
							if (discount < 0 || discount > 100) {
								core.warn("Value must be between 0 and 100.");
								return SkunkSchema.showEditPrompt({
									title: "Discount percentage",
									value: entry.discount
								}, onDiscount);
							}
							entry.discount = discount;
							core.saveCharacterConfig();
							populatePromos();
						}
					} else if (intCol == 3) {
						return SkunkSchema.showEditPrompt({
							title: "Expires in days",
							value: entry.expires
						}, onExpires);
						function onExpires(err, result) {
							if (err) return core.warn("Error", err);
							if (!core.isNumeric(result)) {
								core.warn("Value must be a number.");
								return SkunkSchema.showEditPrompt({
									title: "Expires in days",
									value: "0"
								}, onExpires);
							}
							entry.expires = Number(result);
							entry.time = new Date().getTime();
							core.saveCharacterConfig();
							populatePromos();
						}
					} else if (intCol == 4) {
						return SkunkSchema.showConfirmationPrompt({
							title: "Remove " + entry.code + "?"
						}, function onRemoveConfirm(err) {
							if (err) return;
							config.promotionalCodes.splice(intRow - 1, 1);
							core.saveCharacterConfig();
							populatePromos();
						});
					}
				});
		
			function populatePromos() {
				
				var rows = [
					[false, "Code", "Discount", "Expires", (icon_textures + ICON_REMOVE)]
				];
				
				var promotionalCodes = config.promotionalCodes.slice();

				promotionalCodes.forEach(function(item) {
					var timeleft = "Never";
					if (item.expires > 0) {
						timeleft = (item.time + (item.expires * 1000 * 60 * 60 * 24)) - new Date().getTime();
						timeleft = core.getElapsedString(timeleft / 1000);
					}
					rows.push([item.enabled, item.code, item.discount, timeleft, (icon_textures + ICON_REMOVE)]);
				});
				
				lstPromos.setRows(rows);
			}
		}
		
		/*
		function createProfileTab(notebook) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: notebook, 
					label : "Profile"
				})
			});
			
			var stProfileDescription = new SkunkSchema.StaticText({
				parent: layout, 
				name  : "stProfileDescription", 
				text  : "Work in progress, to share config settings between multiple bots"
			}).setWidth(layout.getWidth());
			
			var stProfileSelected = new SkunkSchema.StaticText({
				parent: layout, 
				name  : "stProfileSelected", 
				text  : "Active profile: "
			})
			.setWidth(100)
			.setAnchor(stProfileDescription, "BOTTOMLEFT")
			
			var choSelectedProfile = core.controls.choSelectedProfile = new SkunkSchema.Choice({
				parent: layout, 
				name  : "choItemSet"
			})
				.setWidth(layout.getWidth() - 135)
				.setAnchor(stProfileSelected, "TOPRIGHT")
				.addOption({text: "SelectA..."})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					
				});
			
			choSelectedProfile.addOption({text: "SelectB..."})
			
			var selectedProfile = 0;
			var profiles = core.skunkConfig.getProfiles();
			profiles.forEach(function(profile, i) {
				core.info(i, profile);
				//choSelectedProfile.addOption(profile);
				choSelectedProfile.addOption({text: "SelectC..."})
				if (profile == core.skunkConfig._keyProfile) {
					selectedProfile = i + 1;
				}
			});
			choSelectedProfile.setSelected(selectedProfile);
			core.info("selectedProfile: " + selectedProfile);
			
		}
		*/
		
		function createDebugTab(notebook) {
			var page = new SkunkSchema.Page({
				parent: notebook, 
				label : "Debug"
			});
			var layout = new SkunkSchema.FixedLayout({parent: page});

			var debugView = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "debugView"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
				
			createDebugGeneralTab(debugView);
			createDebugTestsTab(debugView);

		}
		
		function createDebugGeneralTab(notebook) {
			var page = new SkunkSchema.Page({
				parent: notebook, 
				label : "General"
			});
			var layout = new SkunkSchema.FixedLayout({parent: page});

			// eslint-disable-next-line no-unused-vars
			var btnRestartScript = core.controls.btnRestartScript = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnRestartScript", 
				text  : "Restart script"
			})
				.setFaceColor(COLOUR_RED) // BGR
				.setTextColor(COLOUR_WHITE)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.warn("Restarting SkunkTrader... ");
					core.restarting = true;
					core.disable();
				});
				
			// eslint-disable-next-line no-unused-vars
			var btnExportInvHTML = core.controls.btnExportInvHTML = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnExportInvHTML", 
				text  : "Export Inv HTML"
			})
				.setAnchor(btnRestartScript, "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.exportInventoryHTML();
				});
				
			/*
			// eslint-disable-next-line no-unused-vars
			var btnShowNewUI = core.controls.btnShowNewUI = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnShowNewUI", 
				text  : "Show new UI"
			})
				.setAnchor(btnExportInvHTML, "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//core.exportInventoryHTML();
					core.showNewUI();
				});
			*/
		}

		function createDebugTestsTab(notebook) {
			var page = new SkunkSchema.Page({
				parent: notebook, 
				label : "Tests"
			});
			var layout = new SkunkSchema.FixedLayout({parent: page});
			
			// eslint-disable-next-line no-unused-vars
			var lstTests = core.controls.lstTests = new SkunkSchema.List({
				parent: layout, 
				name  : "lstTests"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // Name
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var name = row[0];
					core.info("Testing " + name + "...");
					tests[name]();
				});
			
			var tests = {};

			
			
			tests["!Compress past logs"] = function() {
				core.compressPreviousLogs();
			};
			
			tests["Show selected item's point value."] = function() {
				if (!skapi.acoSelected) return core.warn("Select something first.");
				var aco = skapi.acoSelected;
				return core.getItemPointValue({aco: aco, logger: core.console}, function onValue(err, value) {
					if (err) return core.warn("Error getting point value", err);
					if (typeof value !== "undefined") {
						core.info(core.getFullName(aco) + "'s point value is " + value);
					} else {
						core.info(core.getFullName(aco) + " has no value to us.");
					}
				});
			};
			
			tests["Stack inventory"] = function() {
				
				var start = new Date().getTime();
				var should = core.shouldStackInventory({logger: core.console});
				var elapsed = new Date().getTime() - start;
				core.info("should: " + should, "elapsed: " + elapsed);
				if (!should) {
					return;
				}
				
				core.stackInventory({logger: core.console}, function onStacked(err, results) {
					core.info("<onStacked>", err, results);
				});
			};
			
			tests["Sort Inventory"] = function() {
				core.sortInventory({verbose: core.info}, function onSort(err, results) {
					if (err) return core.warn("Error during sortInventory()", err);
					core.info("<onSort> results: " + JSON.stringify(results, undefined, "\t"));
				});
			};
			
			tests["getUniqueDuplicates()"] = function() {
				if (!skapi.acoSelected) return core.warn("Select something first.");
				var inv = core.getInventoryItems();
				var matches = core.getUniqueDuplicates({items: inv, item: skapi.acoSelected});
				core.info("matches: " + matches.length);
			};
			
			tests["removeUniqueDuplicates()"] = function() {
				var inv = core.getInventoryItems();
				var items = core.removeUniqueDuplicates({items: inv});
				core.info("inv: " + inv.length, "items: " + items.length);
			};

			tests["Move to pack"] = function() {
				
				
				core.moveToPack({
					aco: skapi.acoSelected
				}, function(err, results) {
					if (err) return core.warn("Error during test", err);
					core.info("results: " + JSON.stringify(results, undefined, "\t"));
				});
			};
			
			tests["Print selected stats"] = function() {
				if (!skapi.acoSelected) return core.warn("Select something first.");
				var string = core.getItemStringSync({aco: skapi.acoSelected});
				core.info(string);
				
				var flat = core.flattenAco({aco: skapi.acoSelected});
				core.console(JSON.stringify(flat, undefined, "\t"));
			};
			
			tests["Show Weapons"] = function() {
				core.showWeapons();
			};
			
			tests["Inscribe add command on Aetheria"] = function() {
				core.inscribeAetheria({
					logger: core.info
				}, function(err) {
					if (err) return core.warn("Error while inscribing aetheria", err);
					core.info("Done inscribing aetheria.");
				});
			};
			
			tests["Inscribe add command on selected item"] = function() {
				if (!skapi.acoSelected) return core.warn("Select something first.");
				
				var aco = skapi.acoSelected;
				var string = 'add ' + core.getFullName(aco);
				
				if (aco.szName == 'Aetheria') {
					
					var spells = core.getAcoSpells({aco: aco}).map(function(spellid) {
						return skapi.SpellInfoFromSpellid(spellid);
					});
					
					spells.forEach(function(spell) {
						if (spell.szName.match(/Surge of (.*)/i)) {
							var text = RegExp.$1;
							string += " spell:" + text;
						}
					});
					
					//core.setNames
					var itemSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
					var setName = itemSet && core.setNames[itemSet];
					
					if (setName && setName.match(/Sigil of (.*)/i)) {
						var text = RegExp.$1;
						string += " set:" + text;
					}
				}
				string += "\nadd " + aco.oid;
				
				
				if (!core.layoutLoaded) {
					core.loadUILayout();
				}
				
				SkunkLayout.inscriptionBox.click();
				SkunkLayout.clearText();
				skapi.PasteSz(string);
				skapi.Keys("{shift}", kmoUp);	// Try to unshift so we don't Shift-Esc logout accidentally
				skapi.Keys(cmidEscapeKey);
				
			};
			
			tests["Call TradeEnd"] = function() {
				core.callTradeEnd();
			};
			
			tests["getUnreadMessages()"] = function() {
				var messages = core.getUnreadMessages();
				core.info("messages: " + (messages.length));
				messages.forEach(function(info, i) {
					core.info(i, JSON.stringify(info));
				});
			};

			tests["Drop excess items"] = function() {
				core.dropExcessItems({logger: core.info}, function(err, results) {
					if (err) return core.warn("Error", err);
					core.info("Results: " + results);
				});
			};

			tests["Is selected unique"] = function() {
				if (!skapi.acoSelected) return core.warn("Select something first.");
				var aco = skapi.acoSelected;
				var unique = core.isUnique(aco);
				core.info("item: " + core.getFullName(aco) + ", unique: " + unique);
			};


			var keys = Object.keys(tests);
			keys.sort();
			keys.forEach(function(key) {
				lstTests.addRow(key);
			});
		}

		
		function createAccountsTab(notebook) {
			var pointsPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Accounts"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: pointsPage
			});	
		
			var LP_TAB = 5;
			notebook.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
				if (value == LP_TAB) {
					core.refreshLootProfiles();
				}
			});
		
			/*
			var btnAccountsRefresh = core.controls.btnAccountsRefresh = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnAccountsRefresh", 
				text  : "Refresh"
			})
				.setWidth(50)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.refreshAccountsList();
				});
				*/

			var btnAddAccount = core.controls.btnAddAccount = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnAddAccount", 
				text  : "Add"
			})
				.setWidth(50)

				//.setAnchor(btnAccountsRefresh, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return SkunkSchema.showEditPrompt({
						title: "Input character name",
						value: ""
					}, function onValue(err, name) {
						if (err) return core.warn("Error asking for point value", err);
						core.setAccount({
							szPlayer: name,
							balance : 0
						});
						core.refreshAccountsList();
						core.info("Added " + name + " to account list.");
					});
					
				});

			// eslint-disable-next-line no-unused-vars
			var lstAccounts = core.controls.lstAccounts = new SkunkSchema.List({
				parent: layout, 
				name  : "lstAccounts"
			})
				.setAnchor(btnAddAccount, "BOTTOMLEFT")
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 180}) // name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 60}) // value
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					
					if (intCol == 0) return;
					var name = row[0];
					var balance = row[1];
					
					if (intCol == 0) {
						core.info("name: " + name, "balance: " + balance);
					} else if (intCol == 1) {
						return SkunkSchema.showEditPrompt({
							title: "Input balance value",
							value: balance
						}, function onValue(err, results) {
							if (err) return core.warn("Error asking for point value", err);
							if (!core.isNumeric(results)) {
								core.warn("Value must be a number.");
								return SkunkSchema.showEditPrompt({
									title: "Input point value",
									value: balance
								}, onValue);
							}
							core.setAccount({
								szPlayer: name,
								balance : results
							});
							core.refreshAccountsList();
							core.info(name + "'s balance set to " + results + ".");
						});
					}
				});
			
		}
		
		function createSellingTab(notebook) {
			
			var sellingPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Selling"
			});
		
			var sellingLayout = new SkunkSchema.FixedLayout({
				parent: sellingPage
			});		
		
			var btnSellingScan = core.controls.btnSellingScan = new SkunkSchema.PushButton({
				parent: sellingLayout, 
				name  : "btnSellingScan", 
				text  : "Scan"
			})
				.setWidth(50)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Assessing inventory for transferable items, this may take a minute...");
					
					// Get results from previous scan to speed up scanning items.
					//var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\transferability.json";
					//var transferability = core.getJsonFile({path: path}) || {};

					var inventoryInfoPath = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\inventoryInfo.json";
					var inventoryInfo = core.getJsonFile({path: inventoryInfoPath}) || {};


					core.getTransferableItems({
						inventoryInfo : inventoryInfo,
						progressMethod: function progressMethod(args) {
							control.setText(args.i + "/" + args.total);
						}
					}, function onSellable(err, items) {
						control.setText("Scan");
						if (err) return core.error("Error while assessing transferable items", JSON.stringify(err));
						if (items) {
							core.updateSellingLists({transferableItems: items});
							core.recordScanResults({items: items});
							core.info("Lists updated.");
						}
					});
					control.setText("...");
				});

			var btnDiscount = core.controls.btnDiscount = new SkunkSchema.PushButton({
				parent: sellingLayout, 
				name  : "btnDiscount", 
				text  : "Discount: " + config.globalDiscount + "%"
			})
				.setWidth(80)
				.setAnchor(btnSellingScan, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Assign a discount percentage that'll affect all items, useful for 50% off sales.");
					core.info("Player discounts (in Config > Players) take priority over this settings.");
					core.showInputWindow({
						szMessage: "Discount Percentage",
						value    : config.globalDiscount
					}, function onValue(err, value) {
						if (err) return;
						var v = Number(value);
						core.debug("value: " + value + ", v: " + v);
						if (!isInt(v) && !isFloat(v)) {
							return core.info("Value (" + value + ") must be a number.");
						} else if (v < 0 || v > 100) {
							return core.info("Value (" + value + ") must be between 0 and 100.");
						}

						config.globalDiscount = value;
						core.saveCharacterConfig();
						core.info("Global discount set to " + v + "%.");
						control.setText("Discount: " + v + "%");
					});
				});
		
			// eslint-disable-next-line no-unused-vars
			var btnDefaultValue = core.controls.btnDefaultValue = new SkunkSchema.PushButton({
				parent: sellingLayout, 
				name  : "btnDefaultValue", 
				text  : "Default: " + (core.isNumber(config.defaultPrice) && config.defaultPrice || "off")
			})
				.setWidth(80)
				.setAnchor(btnDiscount, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Set your default price for items.");
					core.showInputWindow({
						szMessage: "Default price",
						value    : config.defaultPrice
					}, function onValue(err, value) {
						if (err) return;
						if (value == "") {
							config.defaultPrice = null;
							core.saveCharacterConfig();
							core.info("Default price disabled.");
							control.setText("Default: off");
							return;
						}
						
						var v = Number(value);
						core.debug("value: " + value + ", v: " + v);
						if (!isInt(v) && !isFloat(v)) {
							return core.info("Value (" + value + ") must be a number.");
						}

						config.defaultPrice = v;
						core.saveCharacterConfig();
						core.info("Default price set to " + v + ", click Scan to update list.");
						control.setText("Default: " + v);
					});
				});
		
			var sellingNotebook = new SkunkSchema.Notebook({
				parent: sellingLayout, 
				name  : "sellingNotebook"
			})
				.setWidth(sellingPage.getWidth())
				.setHeight(sellingPage.getHeight() - 20)
				.setAnchor(btnSellingScan, "BOTTOMLEFT")
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					if (value == 4) {
						core.refreshLootProfiles();
					}
				});
		
			// Common items.
			var commonLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: sellingNotebook, 
					label : "Common"
				})
			});		
		
			// eslint-disable-next-line no-unused-vars
			var lstCommon = core.controls.lstCommon = new SkunkSchema.List({
				parent: commonLayout, 
				name  : "lstCommon"
			})
				.setWidth(commonLayout.getWidth())
				.setHeight(commonLayout.getHeight() - 20)
				.addColumn({progid: "DecalControls.CheckColumn"})
				.addColumn({progid: "DecalControls.IconColumn"})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 190})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 30})
				.addColumn({progid: "DecalControls.CheckColumn"})
				.on("onControlEvent", function (control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);

					//core.info("lstCommon intCol: " + intCol + ", intRow: " + intRow);

					if (intRow == 0) {// header
						if (intCol == 0) {
							core.info("Enable/disable selling a item.");
						} else if (intCol == 1) {
							core.info("Item icon");
						} else if (intCol == 2) {
							core.info("Item name");
						} else if (intCol == 3) {
							core.info("Item price (points)");
						} else if (intCol == 4) {
							core.info("Bulk price set");
						}
						return;
					}
					
					
					var row = control.rows[intRow];
					var price = Number(row[3]);

					var aco = core.commonItemsList[intRow - 1];
					var szFull = core.getFullName(aco);
					
					if (aco.mcm == mcmSalvageBag) {
						szFull += " [w" + core.round(aco.workmanship, config.workmanshipRounding) + "]";
					}
					
					if (intCol == 0) {
						if (price == 0) {
							core.warn("Careful, " + szFull + "'s price is zero!");
						}
						
						control.getListPropertyAsync(0, intRow, function(err, value) {
							if (err) return;
							var enabled = value;
							core.info("Selling " + szFull + ": " + enabled);
							core.setSellingValue({
								aco    : aco,
								szItem : szFull,
								enabled: enabled
							});
						});
					} else if (intCol == 1 || intCol == 2) {
						//var aco = core.acoFromSz(szItem); //skapi.AcoFromSz(szItem);

						skapi.SelectAco(aco);
						
						//core.info(aco.szName + " is unique: " + core.isUnique(aco));
						var c = core.flattenAco({aco: aco});
						core.console(JSON.stringify(c, undefined, "\t"));
						
					} else if (intCol == 3) {

						// Open set price window.
						core.showItemValueWindow({
							szItem: szFull,
							value : price
						}, function onValue(err, value) {
							core.debug("<onValue>", err, value);
							if (value) {
								control.setListProperty(3, intRow, value);
								control.rows[intRow][3] = value;

								core.setSellingValue({
									aco    : aco,
									szItem : szFull,
									value  : value,
									enabled: true
								});
								
								core.updateSellingLists();
								
								
								core.info("Saved " + szFull + "'s value (" + value + ")");
							}
						});
					} else if (intCol == 4) {
						control.getListPropertyAsync(4, intRow, function(err, value) {
							if (err) return;
							core.commonBulkSelected[aco.oid] = value;
							core.info(szFull + " selected for bulk price: " + value);
						});
					}
				});

			
			var btnCommonBulkPrice = core.controls.btnCommonBulkPrice = new SkunkSchema.PushButton({
				parent: commonLayout, 
				name  : "btnCommonBulkPrice", 
				text  : "Bulk Set Price"
			})
				.setWidth(80)
				.setAnchor(lstCommon, "BOTTOMRIGHT", "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var oids = Object.keys(core.commonBulkSelected).filter(function(oid) {
						return core.commonBulkSelected[oid] == true;
					});
					if (oids.length == 0) {
						return core.info("No items selected for bulk price.");
					}
					return core.showItemValueWindow({
						szItem: "Price for " + oids.length + " items.",
						value : 0
					}, function onValue(err, value) {
						if (err) return core.warn("Error asking for bulk price", err);
						
						oids.map(function(oid) {
							return skapi.AcoFromOid(oid);
						}).forEach(function(aco) {
							core.setSellingValue({
								aco    : aco,
								value  : value,
								enabled: true
							});
						});
						core.updateSellingLists();
						core.info("Price for " + oids.length + " items set to " + value + ".");
					});
				});



			// Unique items.
			var uniqueLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: sellingNotebook, 
					label : "Unique"
				})
			});
		
			// eslint-disable-next-line no-unused-vars
			var lstUnique = core.controls.lstUnique = new SkunkSchema.List({
				parent: uniqueLayout, 
				name  : "lstUnique"
			})
				.setWidth(uniqueLayout.getWidth())
				.setHeight(uniqueLayout.getHeight() - 20)
				.addColumn({progid: "DecalControls.CheckColumn"})
				.addColumn({progid: "DecalControls.IconColumn"})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 190})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 30})
				.addColumn({progid: "DecalControls.CheckColumn"})
				.on("onControlEvent", function (control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);

					//core.info("lstCommon intCol: " + intCol + ", intRow: " + intRow);

					if (intRow == 0) {// header
						if (intCol == 0) {
							core.info("Enable/disable selling a item.");
						} else if (intCol == 1) {
							core.info("Item icon");
						} else if (intCol == 2) {
							core.info("Item name");
						} else if (intCol == 3) {
							core.info("Item price (points)");
						}
						return;
					}
					
					var row = control.rows[intRow];
					var szItem = row[2];
					var price = Number(row[3]);
					
					var aco = core.uniqueItemsList[intRow - 1];
					szItem = aco && core.getFullName(aco) || szItem;
					
					//core.info("szItem: " + szItem + ", aco: " + (aco && aco.szName));
					
					if (intCol == 0) {
						if (price == 0) {
							core.warn("Careful, " + szItem + "'s price is zero!");
						}
						
						control.getListPropertyAsync(0, intRow, function(err, value) {
							if (err) return;
							var enabled = value;
							core.info("Selling " + szItem + " (" + aco.oid + "): " + enabled);
							core.setSellingValue({
								aco    : aco,
								szItem : szItem,
								oid    : aco.oid,
								enabled: enabled
							});
						});
					} else if (intCol == 1 || intCol == 2) {
						skapi.SelectAco(aco);
						
						//core.info(aco.szName + " is unique: " + core.isUnique(aco));
						var c = core.flattenAco({aco: aco});
						core.console(JSON.stringify(c, undefined, "\t"));
						
						
					} else if (intCol == 3) {

						// Open set price window.
						core.showItemValueWindow({
							szItem: szItem,
							value : price
						}, function onValue(err, value) {
							core.debug("<onValue>", err, value);
							if (typeof value !== "undefined") {
								control.setListProperty(3, intRow, value);
								control.rows[intRow][3] = value;

								core.setSellingValue({
									aco    : aco,
									szItem : szItem,
									oid    : aco.oid,
									value  : value,
									enabled: true
								});
								
								core.updateSellingLists();
								core.info("Saved " + szItem + "'s (" + aco.oid +  ") value (" + value + ")");
							} else {
								core.warn("Failed to get item value, try again and hit [enter]");
							}
						});
					} else if (intCol == 4) {
						control.getListPropertyAsync(4, intRow, function(err, value) {
							if (err) return;
							core.uniqueBulkSelected[aco.oid] = value;
						});
					}
				});
			
			/**/
			var btnUniqueBulkPrice = core.controls.btnUniqueBulkPrice = new SkunkSchema.PushButton({
				parent: uniqueLayout, 
				name  : "btnUniqueBulkPrice", 
				text  : "Bulk Set Price"
			})
				.setWidth(80)
				.setAnchor(lstCommon, "BOTTOMRIGHT", "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var oids = Object.keys(core.uniqueBulkSelected).filter(function(oid) {
						return core.uniqueBulkSelected[oid] == true;
					});
					if (oids.length == 0) {
						return core.info("No items selected for bulk price.");
					}
					return core.showItemValueWindow({
						szItem: "Price for " + oids.length + " items.",
						value : 0
					}, function onValue(err, value) {
						if (err) return core.warn("Error asking for bulk price", err);
						
						oids.map(function(oid) {
							return skapi.AcoFromOid(oid);
						}).forEach(function(aco) {
							core.setSellingValue({
								aco    : aco,
								value  : value,
								enabled: true
							});
						});
						core.updateSellingLists();
						core.info("Price for " + oids.length + " items set to " + value + ".");
					});
				});
				
				
			////////////////////////////////////////
		
			var salvageLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: sellingNotebook, 
					label : "Salvage"
				})
			});
		
			// eslint-disable-next-line no-unused-vars
			var lstSalvage = core.controls.lstSalvage = new SkunkSchema.List({
				parent: salvageLayout, 
				name  : "lstSalvage"
			})
				.setWidth(salvageLayout.getWidth())
				.setHeight(salvageLayout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) //id
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 60}) //name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 1
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 2
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 3
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 4
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 5
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 6
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 7
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 8
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 9
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 22}) // workmanship 10
				// eslint-disable-next-line no-unused-vars
				.on("onShow", core.refreshSalvageList)
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					
					var materialId = row[0];
					var materialName = materialNames[materialId];
					var workmanship = intCol - 1;
					core.debug("<lstSalvage> materialId: " + materialId, "materialname: " + materialName, "workmanship: " + workmanship);
					
					if (intRow == 0) {
						if (intCol == 1) {
							core.info("Salvage name");
						} else {
							core.info("Workmanship: " + workmanship + "+");
						}
						return;
					}
					
					if (!materialName) {
						return;
					}
					
					if (intCol == 1) {
						core.info("Salvage: " + materialName);
						return;
					}
					
					var param = {
						szMessage: "Price for " + materialName + " workmanship " + workmanship + "+",
						value    : ""
					};
				
					core.showInputWindow(param, function onSalvageValue(err, value) {
						core.debug("<onSalvageValue>", err, value);
						if (err) return;

						if (!value || value == "") {
							//if (config.salvageAutoValue[materialName]) {
							//	delete config.salvageAutoValue[materialName][workmanship];
							//}
							if (core.salvagePricing.get(materialName)) {
								core.salvagePricing.set(materialName, undefined).save();
							}
							
							core.info("No value input, disabling " + materialName + " w" + workmanship + ".");

							//core.saveCharacterConfig();
							control.setListProperty(intCol, intRow, {value: "?", color: "&H777777"});
							return;
						}

						var num = Number(value);
						if (!isInt(num) && !isFloat(num)) {
							core.warn("Value must be a value.");
							core.showInputWindow(param, onSalvageValue);
							return;
						}
						
						var data = core.salvagePricing.get(materialName) || {};
						data[workmanship] = num;
						core.salvagePricing.set(materialName, core.clone(data)).save();
						
						//core.salvagePricing.set();
						
						//config.salvageAutoValue[materialName] = config.salvageAutoValue[materialName] || {};
						//config.salvageAutoValue[materialName][workmanship] = num;
						//core.saveCharacterConfig();
						
						control.setListProperty(intCol, intRow, {value: num, color: "&HFFFFFF"});	
					});
				});
		
			///////////////////////////////////////////
		
			var itemSetsLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: sellingNotebook, 
					label : "Item Sets"
				})
			});
		
			var btnNewItemSet = core.controls.btnNewItemSet = new SkunkSchema.PushButton({
				parent: itemSetsLayout, 
				name  : "btnNewItemSet", 
				text  : "New Set"
			})
				.setWidth(55)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Input a name for a group of items. ex: 'Ancient Armor'");
					core.showInputWindow({
						szMessage: "Item set name"
					}, function onValue(err, value) {
						if (err) return;
						
						if (typeof config.itemSets[value] !== "undefined") {
							core.warn("That name is already in use, try again.");
							return;
						}
							
						var itemSet = config.itemSets[value] = {
							items                  : [],
							restrictIndividualSales: true
						};
						
							
						core.refreshItemSetChoice({
							selectedText: value
						});
						
						core.controls.chkRestrictIndividualSales.setChecked(itemSet.restrictIndividualSales);
						
						core.info("Created item set '" + value + "'.");
						core.saveCharacterConfig();
					});
				});
		
			var btnDeleteItemSet = core.controls.btnDeleteItemSet = new SkunkSchema.PushButton({
				parent: itemSetsLayout, 
				name  : "btnDeleteItemSet", 
				text  : "Delete Set"
			})
				.setWidth(55)
				.setAnchor(btnNewItemSet, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var setName = core.controls.choItemSet.getValue();
					
					if (setName == "Select...") {
						core.warn("Select a item set from the drop down menu first.");
						return;
					}

					delete config.itemSets[setName];
					core.refreshItemSetChoice();
					core.info("Deleted item set '" + setName + "'.");
					core.saveCharacterConfig();
				});
		
			var choItemSet = core.controls.choItemSet = new SkunkSchema.Choice({
				parent: itemSetsLayout, 
				name  : "choItemSet"
			})
				.setWidth(itemSetsLayout.getWidth() - 135)
				.setAnchor(btnDeleteItemSet, "TOPRIGHT")
				.addOption({text: "Select..."})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.controls.lstItemSets.clear();
					
					var setName = control.getValue();
					
					if (setName == "Select...") return;

					core.refreshItemSetList({
						setName: setName
					});
				});

			// eslint-disable-next-line no-unused-vars
			var btnItemSetInfo = core.controls.btnItemSetInfo = new SkunkSchema.PushButton({
				parent: itemSetsLayout, 
				name  : "btnItemSetInfo", 
				text  : "?"
			})
				.setWidth(25)
				.setAnchor(choItemSet, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Item sets are items to be sold together in groups instead of individually.");
					core.info("Click New Set to create a new set grouping, then type item names into the textbox below to add items to the list.");
				});
			
			var chkRestrictIndividualSales = core.controls.chkRestrictIndividualSales = new SkunkSchema.Checkbox({
				parent : itemSetsLayout, 
				name   : "chkRestrictIndividualSales", 
				text   : "Restrict individual sales",
				checked: false
			})
				.setAnchor(btnNewItemSet, "BOTTOMLEFT")
				.setWidth(200)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var setName = core.controls.choItemSet.getValue();
					core.debug("setname: " + setName);
					if (setName == "Select...") {
						core.warn("Select a set from the drop down menu first.");
						return;
					}
					
					var v = config.itemSets[setName].restrictIndividualSales = control.getChecked();
					core.info("Restrict individual sales of " + setName + "'s items: " + v);
					core.saveCharacterConfig();
				});

			var lstItemSets = core.controls.lstItemSets = new SkunkSchema.List({
				parent: itemSetsLayout, 
				name  : "lstItemSets"
			})
				.setAnchor(chkRestrictIndividualSales, "BOTTOMLEFT")
				.setWidth(uniqueLayout.getWidth())
				.setHeight(uniqueLayout.getHeight() - 60)
				.addColumn({progid: "DecalControls.CheckColumn"})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: uniqueLayout.getWidth() - 80})
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 20}) // quantity
				.addColumn({progid: "DecalControls.IconColumn"})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];

					var itemName = row[1];

					var setName = core.controls.choItemSet.getValue();

					if (intCol == 0) {
						//var v = config.itemSets[setName].items[intRow].enabled = enabled;
						//core.info("setname: " + setName + ", intRow: " + intRow + ", v: " + v);
						//core.saveCharacterConfig();

						control.getListPropertyAsync(0, intRow, function(err, value) {
							if (err) return;
							config.itemSets[setName].items[intRow].enabled = value;
							core.info("setname: " + setName + ", intRow: " + intRow + ", value: " + value);
							core.saveCharacterConfig();
						});
						
					} else if (intCol == 1) {
						core.info("Item name: " + itemName);
					} else if (intCol == 2) { // quantity
						//core.info("Can't change quantity yet.");
						
						core.info("Input stack quantity to sell.");
						core.showInputWindow({
							szMessage: "quantity",
							value    : config.itemSets[setName].items[intRow].quantity || 1
						}, function onValue(err, value) {
							if (err) return;
							var v = Number(value);
							core.debug("value: " + value + ", v: " + v);
							if (!isInt(v) && !isFloat(v)) {
								return core.info("Value (" + value + ") must be a number.");
							}
							
							if (v == 0) return core.info("Value cannot be zero.");
							
							config.itemSets[setName].items[intRow].quantity = v;
							core.saveCharacterConfig();
							
							core.refreshItemSetList({
								setName: setName
							});
						});
						
					} else if (intCol == 3) {
						config.itemSets[setName].items.splice(intRow, 1);
						
						core.saveCharacterConfig();
						core.info("Removed '" + itemName + "' from " + setName + ".");
						core.refreshItemSetList({
							setName: setName
						});
					}
				});

			// eslint-disable-next-line no-unused-vars
			var btnItemSetAddSelected = core.controls.btnItemSetAddSelected = new SkunkSchema.PushButton({
				parent: itemSetsLayout, 
				name  : "btnItemSetAddSelected", 
				text  : "Add Selected"
			})
				.setWidth(100)
				.setAnchor(lstItemSets, "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var aco = skapi.acoSelected;
					if (!aco) {
						core.info("Select a item first.");
						return;
					}

					var setName = core.controls.choItemSet.getValue();
					if (setName == "Select...") {
						core.warn("Select a item set from the drop down menu first.");
						return;
					}

					var itemName = core.getFullName(aco);
					
					var itemInfo = {
						enabled : true,
						itemName: itemName,
						quantity: 1
					};
					
					if (core.isUnique(aco)) {
						itemInfo.oid = aco.oid;

						// eslint-disable-next-line no-restricted-syntax
						for (var i = 0; i < config.itemSets[setName].items.length; i++) {
							if (config.itemSets[setName].items[i].oid == aco.oid) {
								return core.warn(itemName + " (" + aco.oid + ") is already on the list.");
							}
						}
					}
					
					core.info("itemName: " + itemName, itemInfo.oid);
					config.itemSets[setName].items.push(itemInfo);
					
					core.saveCharacterConfig();
				
					core.refreshItemSetList({
						setName: setName
					});
				
					var value = core.getAssignedSellValue({aco: aco});
					if (!value) {
						core.warn("Don't forget to set a point value to " + itemName + "!");
					}
				});
				
				
			///////////////////////////////
			var lootProfileLayout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: sellingNotebook, 
					label : "Profiles"
				})
			});

			if (typeof core.LootProfile3 !== "undefined") {
				var lpNotebook = core.controls.lpNotebook = new SkunkSchema.Notebook({
					parent: lootProfileLayout, 
					name  : "lpNotebook"
				})
					.setWidth(lootProfileLayout.getWidth())
					.setHeight(lootProfileLayout.getHeight())
					// eslint-disable-next-line no-unused-vars
					.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
						if (value == 0) {
							core.refreshLootProfiles();
						} else if (value == 1) {
							// Refresh items list.
							core.refreshLootProfileItems();
						}
					});
					
				var lpProfilesLayout = new SkunkSchema.FixedLayout({
					parent: new SkunkSchema.Page({
						parent: lpNotebook, 
						label : "Profiles"
					})
				});

				// eslint-disable-next-line no-unused-vars
				var lstLootProfiles = core.controls.lstLootProfiles = new SkunkSchema.List({
					parent: lpProfilesLayout, 
					name  : "lstLootProfiles"
				})
					.setWidth(lpProfilesLayout.getWidth())
					.setHeight(lpProfilesLayout.getHeight())
					.addColumn({progid: "DecalControls.CheckColumn"}) // Enabled
					.addColumn({progid: "DecalControls.IconColumn"})
					.addColumn({progid: "DecalControls.TextColumn", fixedwidth: lpProfilesLayout.getWidth() - 100}) // Profile name
					.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 40}) // Profile price
					// eslint-disable-next-line no-unused-vars
					.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
						var intCol = parseInt(value.split(",")[0], 10);
						var intRow = parseInt(value.split(",")[1], 10);

						if (intRow == 0) {
							if (intCol == 0) core.info("Prices enabled for profile.");
							if (intCol == 1) core.info("Profile icon");
							if (intCol == 2) core.info("Profile name");
							if (intCol == 3) core.info("Profile Price");
							return;
						}
						
						var row = control.rows[intRow];
						core.console("intCol: " + intCol + ", intRow: " + intRow + ", value: " + value);
						
						var profileName = row[2];
						var profileInfo = config.lootProfiles[profileName];
						
						if (intCol == 0) { // Enabled column
							return control.getListPropertyAsync(0, intRow, function(err, value) {
								if (err) return core.error(err);
								profileInfo.enabled = value;
								core.saveCharacterConfig();
								core.info(profileName + " enabled: " + value);
								
								core.queueItemRefresh();
							});
							
						} else if (intCol == 1 || intCol == 2) {// Icon and Name
						
							/*
							core.showProfileItems({
								profileName: profileName
							});
							*/
						
						} else if (intCol == 3) {// Price column
							
							return core.showItemValueWindow({
								szItem: profileName,
								value : profileInfo.price
							}, function onValue(err, value) {
								if (err) return core.error("Error while asking profile price", err);
								if (core.isNumber(value)) {
									profileInfo.price = value;
									core.saveCharacterConfig();
									core.info(profileName + " price: " + value);
									control.setListProperty(intCol, intRow, value);
									
									core.queueItemRefresh();
								} else {
									core.error("Value must be a number");
								}
							});
						}

					});


				var lpItemsLayout = new SkunkSchema.FixedLayout({
					parent: new SkunkSchema.Page({
						parent: lpNotebook, 
						label : "Items"
					})
				});
				
				// eslint-disable-next-line no-unused-vars
				var lstLPItems = core.controls.lstLPItems = new SkunkSchema.List({
					parent: lpItemsLayout, 
					name  : "lstLPItems"
				})
					.setWidth(lpItemsLayout.getWidth())
					.setHeight(lpItemsLayout.getHeight())
					.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // item id
					.addColumn({progid: "DecalControls.IconColumn"})
					.addColumn({progid: "DecalControls.TextColumn", fixedwidth: lpItemsLayout.getWidth() - 100}) // Item Name
					.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 40}) // Matching profile
					// eslint-disable-next-line no-unused-vars
					.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
						//var intCol = parseInt(value.split(",")[0], 10);
						var intRow = parseInt(value.split(",")[1], 10);
						if (intRow == 0) return;
						
						var row = control.rows[intRow];
						var oid = row[0];
						var aco = skapi.AcoFromOid(oid);
						
						skapi.SelectAco(aco);
						if (core.itemLootProfiles[aco.oid]) {
							var profiles = core.itemLootProfiles[aco.oid];
							var names = profiles.filter(function(name) {
								return config.lootProfiles[name] && config.lootProfiles[name].enabled;
							}).map(function(name) {
								return name + " ($" + config.lootProfiles[name].price + ")";
							});

							core.info(core.getFullName(aco) + " matches: " + names.join(", ") + ".");
						} else {
							core.info(core.getFullName(aco) + " matches no loot profiles.");
						}

					});


			} else {
				var stLPUnavailable = new SkunkSchema.StaticText({
					parent: lootProfileLayout, 
					name  : "stLPUnavailable", 
					text  : "LootProfile3 isn't loaded."
				})
					.setWidth(200);
				
				// eslint-disable-next-line no-unused-vars
				var btnbHowToInstallLP = core.controls.btnbHowToInstallLP = new SkunkSchema.PushButton({
					parent: lootProfileLayout, 
					name  : "btnbHowToInstallLP", 
					text  : "Howto get LootProfile3"
				})
					.setWidth(200)
					.setAnchor(stLPUnavailable, "BOTTOMLEFT")
					.on("onControlEvent", function onControlEvent() {
						core.info("Download the LootProfile3 script from https://gitlab.com/Cyprias/lootprofile3/-/releases");
						core.info("Extract the folder next to your SkunkTrader folder and restart SkunkTrader.");
					});
			}
		}

		function createPointsTab(notebook) {
			var pointsPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Point"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: pointsPage
			});	
			
			var pointsView = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "pointsView"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
			createPointsGeneralTab(pointsView);
			createPointsLPTab(pointsView);
		}

		function createPointsGeneralTab(notebook) {
			var pointsPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "General"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: pointsPage
			});	
			
			var stName = new SkunkSchema.StaticText({parent: layout, name: "stName", text: "Item name: "});

			var edPointsName = core.controls.edPointsName = new SkunkSchema.Edit({parent: layout, name: "edPointsName", text: ""})
				.setAnchor(stName, "TOPRIGHT");

			// eslint-disable-next-line no-unused-vars
			var btnPointsSelected = core.controls.btnPointsSelected = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnPointsSelected", 
				text  : "Selected Item"
			})
				.setAnchor(edPointsName, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var aco = skapi.acoSelected;
					if (!aco) {
						core.info("Select a item first.");
						return;
					}
					var szName = core.getFullName(aco);
					
					core.controls.edPointsName.setText(szName);
				});
		
			var stValue = new SkunkSchema.StaticText({parent: layout, name: "stValue", text: "Value (each):"})
				.setAnchor(stName, "BOTTOMLEFT");
				
			var edPointsValue = core.controls.edPointsValue = new SkunkSchema.Edit({parent: layout, name: "edPointsValue", text: ""})
				.setAnchor(stValue, "TOPRIGHT");
			
			// eslint-disable-next-line no-unused-vars
			var btnPointsAdd = core.controls.btnPointsAdd = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnPointsAdd", 
				text  : "Add"
			})
				.setAnchor(edPointsValue, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var itemName, itemValue;
					core.controls.edPointsName.getPropertyAsync("text", onName);

					function onName(err, value) {
						if (err) return core.error(err);
						itemName = value;
						if (itemName == "") {
							return core.warn("Input a item name into the box.");
						}
						core.controls.edPointsValue.getPropertyAsync("text", onValue);
					}
					
					function onValue(err, value) {
						if (err) return core.error(err);
						itemValue = Number(value);
						if (itemValue == "") {
							return core.warn("Input a point value into the box.");
						} else if (!isInt(itemValue) && !isFloat(itemValue)) {
							return core.warn("Value must be a number.");
						}
						
						if (itemValue <= 0) {
							return core.warn("Number must be above zero.");
						}

						if (core.addPointsItem({
							szItem: itemName,
							value : itemValue
						})) {
							core.info("Added " + itemName + " to points list.");
							core.updatePointsList();
						}
					}
				});
			
			// eslint-disable-next-line no-unused-vars
			var lstPoints = core.controls.lstPoints = new SkunkSchema.List({
				parent: layout, 
				name  : "lstPoints"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 40)
				.addColumn({progid: "DecalControls.CheckColumn"}) // enabled
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 180}) // name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 60}) // value
				.addColumn({progid: "DecalControls.IconColumn"}) // delete
				.setAnchor(stValue, "BOTTOMLEFT")
				.on("onShow", core.updatePointsList)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);

					//core.info("lstCommon intCol: " + intCol + ", intRow: " + intRow);

					if (intRow == 0) {// header
						if (intCol == 0) {
							core.info("Enable/disable accepting a item.");
						} else if (intCol == 1) {
							core.info("Item name");
						} else if (intCol == 2) {
							core.info("Item value (points), click to update.");
						} else if (intCol == 3) {
							core.info("Remove item");
						}
						return;
					}
					
					var row = control.rows[intRow];
					var szItem = row[1];
					var value = Number(row[2]);
					
					var item = core.pointsItemsList[intRow - 1];
					
					if (intCol == 0) {
						control.getListPropertyAsync(0, intRow, function(err, value) {
							if (err) return;
							var enabled = value;
							core.setPointsItem({
								szItem : szItem,
								enabled: enabled
							});
							core.info("Accepting " + szItem + ": " + enabled);
						});
						
						
					} else if (intCol == 1) {
						core.info("name: " + szItem);
					} else if (intCol == 2) {
						
						core.showItemValueWindow({
							szItem: szItem,
							value : value
						}, function onValue(err, value) {
							core.debug("<onValue>", err, value);
							if (value) {
								control.setListProperty(2, intRow, value);
								control.rows[intRow][2] = value;

								core.setPointsItem({
									szItem: szItem,
									value : value
								});
								core.info("Saved " + szItem + "'s value (" + value + ")");
								core.updatePointsList();
							}
						});
						
					} else if (intCol == 3) {
						//core.info("TODO removing");
						
						
						core.removePointsItem({
							szItem: item.szName
						});
						core.updatePointsList();
						
						core.info("Removed " + item.szName + ".");
					}
				});
		}

		function createPointsLPTab(notebook) {
			var pointsPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Loot Profiles"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: pointsPage
			});	
			
			if (typeof core.LootProfile3 === "undefined") {
				var stLPUnavailable = new SkunkSchema.StaticText({
					parent: layout, 
					name  : "stLPUnavailable", 
					text  : "LootProfile3 isn't loaded."
				})
					.setWidth(200);
				
				// eslint-disable-next-line no-unused-vars
				var btnbHowToInstallLP = core.controls.btnbHowToInstallLP = new SkunkSchema.PushButton({
					parent: layout, 
					name  : "btnbHowToInstallLP", 
					text  : "Howto get LootProfile3"
				})
					.setWidth(200)
					.setAnchor(stLPUnavailable, "BOTTOMLEFT")
					.on("onControlEvent", function onControlEvent() {
						core.info("Download the LootProfile3 script from https://gitlab.com/Cyprias/lootprofile3/-/releases");
						core.info("Extract the folder next to your SkunkTrader folder and restart SkunkTrader.");
					});
					
				return;
			}
			
			function refreshPointsProfiles() {
				lstPointsProfiles.clear();
				lstPointsProfiles.addRow(true, "Name", "Value", (icon_textures + ICON_REMOVE));
				var keys = Object.keys(config.pointsProfiles);
				keys.sort();
				keys.forEach(function(name) {
					var info = config.pointsProfiles[name];
					lstPointsProfiles.addRow(info.enabled, name, info.value, (icon_textures + ICON_REMOVE));
				});
			}
			
			var lstPointsProfiles = core.controls.lstNPCSupplies = new SkunkSchema.List({
				parent: layout, 
				name  : "lstPointsProfiles"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.on("onShow", refreshPointsProfiles)
				.addColumn({progid: "DecalControls.CheckColumn"})	// enabled
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 150}) // Name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 35}) // Value
				.addColumn({progid: "DecalControls.IconColumn"}) // delete
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var name = row[1];
					
					if (intCol == 0) {
						if (intRow == 0) return core.info("Enable/disable accepting items matching profile.");
						return control.getListPropertyAsync(intCol, intRow, function(err, value) {
							if (err) return;
							config.pointsProfiles[name].enabled = value;
							core.saveCharacterConfig();
							refreshPointsProfiles();
						});
					} else if (intCol == 1) {
						
						var profiles = core.LootProfile3.getLoadedProfiles();
						var names = profiles.filter(function(value) {
							return typeof value.data.name !== "undefined";
						}).map(function(value/*, index*/) {
							return value.data.name;
						});
						names.sort();
						var selected = names.findIndex(function(profileName) {
							return profileName == name;
						});
						
						return SkunkSchema.showDropdownMenuPrompt({
							selected: selected,
							options : names.map(function(name) {
								return {
									text: name
								};
							})
						}, function onProfile(err, results) {
							core.console("<onProfile>", err, results);
							if (err) return core.warn("Error asking for profile name", err);
							var profileName = results.text;
							var info = config.pointsProfiles[name];
							delete config.pointsProfiles[name];
							config.pointsProfiles[profileName] = info;
							core.saveCharacterConfig();
							core.info("Name set to " + profileName + ".");
							refreshPointsProfiles();
						});
					} else if (intCol == 2) {
						var info = config.pointsProfiles[name];
						
						return SkunkSchema.showEditPrompt({
							title: "Input point value",
							value: info.value
						}, function onValue(err, results) {
							if (err) return core.warn("Error asking for point value", err);
							if (!core.isNumeric(results)) {
								core.warn("Value must be a number.");
								return SkunkSchema.showEditPrompt({
									title: "Input point value",
									value: info.value
								}, onValue);
							}
							info.value = Number(results);
							core.saveCharacterConfig();
							core.info("Value set to " + results + ".");
							refreshPointsProfiles();
						});
					} else if (intCol == 3) {
						return SkunkSchema.showConfirmationPrompt({
							title: "Remove " + name + "?"
						}, function onRemoveConfirm(err) {
							if (err) return;
							delete config.pointsProfiles[name];
							core.saveCharacterConfig();
							core.info("Removed " + name + ".");
							refreshPointsProfiles();
						});
					}
					
					
				});
			
			// eslint-disable-next-line
			var btnAddPointsProfile = core.controls.btnAddPointsProfile = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnAddPointsProfile", 
				text  : "Add Profile"
			})
				.setAnchor(lstPointsProfiles, "BOTTOMLEFT")
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					
					var profiles = core.LootProfile3.getLoadedProfiles();
					
					//core.console("profiles: " + JSON.stringify(profiles, undefined, "\t"));
					
					var names = profiles.filter(function(value) {
						return typeof value.data.name !== "undefined";
					}).map(function(value/*, index*/) {
						return value.data.name;
					});

					//core.console("names: " + JSON.stringify(names, undefined, "\t"));
					names.sort();
					
					return SkunkSchema.showDropdownMenuPrompt({
						options: names.map(function(name) {
							return {
								text: name
							};
						})
					}, onProfile);
					
					var profileName;
					function onProfile(err, results) {
						core.console("<onProfile>", err, results);
						if (err) return core.warn("Error while asking for profile", err);
						profileName = results.text;
						return SkunkSchema.showEditPrompt({
							title: "Input point value",
							value: 1
						}, onValue);
					}
					
					function onValue(err, results) {
						core.console("<onValue>", err, results);
						if (err) return core.warn("Error while asking for value", err);
						if (!core.isNumeric(results)) {
							core.warn("Value must be a number.");
							return SkunkSchema.showEditPrompt({
								title: "Input point value",
								value: 1
							}, onValue);
						}
						
						config.pointsProfiles[profileName] = {
							enabled: true,
							value  : Number(results)
						};
						core.saveCharacterConfig();
						core.info(profileName + "'s value set to " + results);
						refreshPointsProfiles();
					}
					
					
				});
		}
		
		function createInventoryTab(notebook) {
			var pointsPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Inv"
			});
		
			notebook.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
				if (value == 2) {
					core.info("Populating inventory list...");
					var start = new Date().getTime();
					core.refreshInventoryList();
					core.info("Inventory list populated after " + (new Date().getTime() - start) + "ms.");
				}
			});
			
		
			var layout = new SkunkSchema.FixedLayout({
				parent: pointsPage
			});	

			// eslint-disable-next-line no-unused-vars
			var lstInventory = core.controls.lstInventory = new SkunkSchema.List({
				parent: layout, 
				name  : "lstInventory"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) //id
				.addColumn({progid: "DecalControls.IconColumn"}) // Icon
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 180}) // name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 50}) // Age
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 50}) // Price
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					
					var oid = row[0];
					var aco = skapi.AcoFromOid(oid);
					
					
					if (intCol >= 0 && intCol <= 3) {
						var itemString = core.getItemStringSync({aco: aco});
						core.info("Selecting " + itemString + ".");
						skapi.SelectAco(aco);
					} else if (intCol == 4) {
						if (aco.mcm == mcmSalvageBag) {
							return core.info("Use the Config>Selling>Salvage tab to change salvage price.");
						}
						
						var fullName = core.getFullName(aco);
						var price = core.getSellValue({
							aco: aco
						});

						core.showItemValueWindow({
							szItem: fullName,
							value : price
						}, function onValue(err, value) {
							core.debug("<onValue>", err, value);
							if (err) return core.warn("Error asking for price", err);
							
							if (typeof value !== "undefined") {
								if (core.isUnique(aco)) {
									core.setSellingValue({
										aco   : aco,
										szItem: fullName,
										oid   : aco.oid,
										value : value
									});
								} else {
									core.setSellingValue({
										aco   : aco,
										szItem: fullName,
										value : value
									});
								}
								core.info("Saved " + fullName + "'s value (" + value + ").");
								core.refreshInventoryList();
							}
						});
					}
				});
		}

		function createMessagesTab(notebook) {
			var pointsPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Msg"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: pointsPage
			});	
		
			notebook.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
				if (value == 3) {
					core.refreshMessagesList();
				}
			});

			var btnMarkAsRead = core.controls.btnMarkAsRead = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnMarkAsRead", 
				text  : "Mark as read"
			})
				.setWidth(100)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.debug("<btnMarkAsRead>");
					var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\messages.sdb";
					var results = SkunkDB.update().from(path)
						.set({read: true})
						.where({
							read: false
						})
						.execute();

					//core.info("results: " + JSON.stringify(results));
					core.info("Marked " + results.rowsModified + " messages as read.");
				});
		
			// eslint-disable-next-line no-unused-vars
			var lstMessages = core.controls.lstMessages = new SkunkSchema.List({
				parent: layout, 
				name  : "lstMessages"
			})
				.setAnchor(btnMarkAsRead, "BOTTOMLEFT")
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // time
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 110}) // date
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 70}) // name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth() - 60}) // message
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.debug("<lstMessages>");
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var time = row[0];
					var date = row[1];
					var szSender = row[2];
					var szMessage = row[3];
					
					var elapsed = new Date().getTime() - time;
					var szElapsed = core.getElapsedString(elapsed / ONE_SECOND, 1);
					core.info(date + " (" + szElapsed + " ago) " + szSender + " said " + szMessage);
				});
		}
		
		function createTransactionsTab(notebook) {
			var page = new SkunkSchema.Page({
				parent: notebook, 
				label : "Transactions"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: page
			});	
			
			var transactions;
			function refreshList() {
				core.info("Refreshing transactions list...");
				var start = new Date().getTime();
				
				var rows = [];
				rows.push([
					"Item",
					"Points"
				]);

				var results = SkunkDB.select().from(_dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\transactions.txt")
					.execute();
				transactions = results.rows;
				transactions.reverse();

				transactions.forEach(function(transaction) {
					rows.push([transaction.myItemStrings.join(", "), transaction.myTotal]);
				});

				lstTransactions.setRows(rows);
				core.info(transactions.length + " transactions shown after " + (new Date().getTime() - start) + "ms.");
			}
			
			
			// eslint-disable-next-line no-unused-vars
			var lstTransactions = core.controls.lstTransactions = new SkunkSchema.List({
				parent: layout, 
				name  : "lstTransactions"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth() - 60}) // name
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 60}) // points
				
				.on("onShow", refreshList)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intRow = parseInt(value.split(",")[1], 10);
					if (intRow == 0) return core.info("Transaction header");
					var transaction = transactions[intRow - 1];
					var elapsed = (new Date().getTime() - transaction.time) / ONE_SECOND;

					core.info(transaction.player + " bought " + transaction.myItemStrings.length + " items" + transaction.myItemStrings.map(function(string) {
						return "\n   * " + string;
					}) + "\n   for " + transaction.myTotal + " points " + transaction.theirItemStrings.map(function(string) {
						return "\n   * " + string;
					}) + "\n   on " + transaction.date + " (" + core.getElapsedString(elapsed) + " ago)." 
					+ "\n   preBalance: " + transaction.preBalance + ", debit: " + transaction.debit + ", credit: " + transaction.credit + ", postBalance: " + transaction.postBalance
					);
					
				});
		}
			
		// eslint-disable-next-line no-unused-vars
		function createTestTab(notebook) {
			// Testing SkunkSchema code
			
			var testPage = new SkunkSchema.Page({
				parent: notebook, 
				label : "Test"
			});
		
			var layout = new SkunkSchema.FixedLayout({
				parent: testPage
			});	
		
			core.info("testPage: " + testPage.getWidth() + " x " + testPage.getHeight());
		
			/*
			var lstTest = new SkunkSchema.List({
				parent: layout, 
				name  : "lstTest"
			})
			.setWidth(testPage.getWidth())
			.setHeight(testPage.getHeight())
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 180}) // name
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 60}) // value
			.on("onControlEvent", onControlEvent);
			*/
				
			// eslint-disable-next-line no-unused-vars
			var btnTestB = core.controls.btnTestB = new SkunkSchema.PushButton({
				parent: layout, 
				name  : "btnTestB", 
				text  : "Test"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT");
				
		
			//core.info("lstTest: " + lstTest.getWidth() + " x " + lstTest.getHeight());
		}
		
		
		//7401, 7699, 8184, 8863, 9771
		return function createGUI() {
			var view = core.controls.view = core.createView({
				title : core.title, 
				width : 300, 
				height: 300, 
				icon  : 9771
			});

			/*
			// View panels can access ACHooks directly, add a function we can send a list of item ids so it can add them to trade faster than skapi can.
			view.addCDATA("function TradeAdd(str) {\
				var oids = str.split(',');\
				for (var i = 0; i < oids.length; i++) {\
					hooks.TradeAdd(oids[i]);\
				}\
			}");
			
			
			view.addCDATA("function TradeEnd() {\
				hooks.TradeEnd();\
			}");
			
			
			// Throws error.
			view.addCDATA("function RemoveControls() {\
				RemoveControls();\
			}");
			*/
			
			var fixedLayout = new SkunkSchema.FixedLayout({parent: view});

			var notebookView = new SkunkSchema.Notebook({
				parent: fixedLayout, 
				name  : "notebookView"
			})
				.setWidth(view.getWidth())
				.setHeight(view.getHeight() - 20); // leave 20px for footer buttons

			createStatusTab(notebookView);
			createConfigTab(notebookView);
			
			createInventoryTab(notebookView);
			createMessagesTab(notebookView);
			createTransactionsTab(notebookView);
			
			createDebugTab(notebookView);

			
			// eslint-disable-next-line no-unused-vars
			var btnQuit = core.controls.btnQuit = new SkunkSchema.PushButton({
				parent: fixedLayout, 
				name  : "btnQuit", 
				text  : "Quit"
			})
				.setAnchor(fixedLayout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(40)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Quitting...");
					core.quitting = true;
					core.disable();
				});
		
		};
	}();
	
	/*
	 * showGUI - Show GUI.
	 *
	 * @param {boolean} visible - Show window.
	 */
	core.showGUI = function showGUI(visible) {
		core.debug("<showGUI>");
		visible = typeof visible !== 'undefined' ? visible : true;
		core.controls.view.showControls(visible);
	};

	/*
	 * updateCommonList - Update common GUI list.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.sellingValues - Point values of items.
	 * @param {array} params.items - List of items to show.
	 */
	core.updateCommonList = function factory() {
		function _compareNames(a, b) { 
			if (a.szName < b.szName) return -1;
			if (a.szName > b.szName) return 1;
			return 0;
		}
		function _filterItemName(aco) {
			return core.getFullName(aco) == this.name;
		}
		function _tallyItems(tally, aco) {
			return tally + aco.citemStack;
		}
		return function updateCommonList(params) {

			var sellingValues = params.sellingValues;
			var commonValues = sellingValues && sellingValues.common || {};
			
			var items = params.items;
			
			// Remove duplicate item names.
			var names = {};
			items.forEach(function(aco) {
				names[core.getFullName(aco)] = aco;
			});

			items = [];
			for (var key in names) {
				//if (!names.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(names, key)) continue;
				items.push(names[key]);
			}

			core.commonItemsList = []; // reset
			
			var rows = [];

			rows.push([
				false, 
				0, 
				"Item Name",
				"Price", 
				false
			]);

			// Sort alphabetical
			items.sort(_compareNames);
			
			var inv = core.getInventoryItems();
			
			items.forEach(function(aco) {
				var szFull = core.getFullName(aco);
				
				var enabled = commonValues[szFull] && commonValues[szFull].enabled || false;
				var value;
				
				if (commonValues[szFull] && typeof commonValues[szFull].value !== "undefined") {
					value = commonValues[szFull].value;
				} else {
					value = {
						value: config.defaultPrice || "", 
						color: "&H777777"
					};
				}

				szFull += " * " + inv.filter(_filterItemName, {name: szFull}).reduce(_tallyItems, 0);

				rows.push([
					enabled, 
					icon_textures + aco.icon, 
					szFull,
					value || "",
					false
				]);
				core.commonItemsList.push(aco);
			});
			
			core.controls.lstCommon.setRows(rows);
		};
	}();


	/*
	 * updateUniqueList - Update unique GUI list.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.sellingValues - Point values of items.
	 * @param {array} params.items - List of items to show.
	 */
	core.updateUniqueList = function factory() {
		return function updateUniqueList(params) {

			var sellingValues = params.sellingValues;
			var uniqueValues = sellingValues && sellingValues.unique || {};
			
			var items = params.items;
			
			var rows = [];
			
			core.uniqueItemsList = []; // reset
			
			rows.push([false, 0, "Item Name", "Price", false]);
			
			items.sort(core.compare_items);
			
			items.forEach(function _eachItem(aco) {
				var enabled = uniqueValues[aco.oid] && uniqueValues[aco.oid].enabled || false;
				var value;

				if (uniqueValues[aco.oid] && typeof uniqueValues[aco.oid].value !== "undefined") {
					value = uniqueValues[aco.oid].value;
				} else {
					value = {
						value: config.defaultPrice || "",
						color: "&H777777"
					};
				}


				if (aco.mcm == mcmSalvageBag) {
					return;
				}

				var szName = core.getFullName(aco);

				rows.push([
					enabled, 
					icon_textures + aco.icon, 
					szName,
					value || "",
					false
				]);
				core.uniqueItemsList.push(aco);
			});
			
			core.controls.lstUnique.setRows(rows);
		};
	}();


	/*
	 * updateGUI - Refresh various GUI controls.
	 *
	 * @param {none}
	 */
	core.updateGUI = function updateGUI() {
		//core.updatePointsList();

		core.refreshAccountsList();
		core.refreshAnnouncmentsList();
		core.refreshPlayersList();
		
		
		core.refreshItemSetChoice();
		core.refreshMessagesList();
		core.refreshIgnoredList();

		core.setupStatusList();
	};

	/*
	 * refreshAnnouncmentsList() Refresh announcments list.
	 *
	 * @param {none}
	 */
	core.refreshAnnouncmentsList = function factory() {
		function _handleAd(ad) {
			core.controls.lstAnnouncments.addRow(
				ad.enabled,
				ad.message,
				(icon_textures + ICON_REMOVE)
			);
		}
		return function refreshAnnouncmentsList() {
			if (!core.controls.lstAccounts) return;
			core.controls.lstAnnouncments.clear();
			config.announcments.forEach(_handleAd);
		};
	}();

	core.shouldStackInventory =  function shouldStackInventory(params) {
		core.debug("<shouldStackInventory>");
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		if (params && params.mcm) {
			acf.mcm = params.mcm;
		}
		var items = core.coacoToArray(acf.CoacoGet()).filter(function _canStack(aco) {
			return aco.citemMaxStack > 1 && aco.citemStack < aco.citemMaxStack;
		});
		return items.some(function _hasPartner(aco) {
			var partners = core.getItemStackPartners({aco: aco, logger: logger, max: aco.citemMaxStack - 1});
			return partners.length > 0;
		});
	};

	core.stackInventory = function stackInventory(params, callback) {
		core.debug("<stackInventory>");

		//var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		
		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		if (params && params.mcm) {
			acf.mcm = params.mcm;
		}
		var items = core.coacoToArray(acf.CoacoGet()).filter(function(aco) {
			return core.phantomItems[aco.oid] != true;
		}).filter(function _canStack(aco) {
			return aco.citemMaxStack > 1 && aco.citemStack < aco.citemMaxStack;
		});

		items.sort(function(a, b) {
			return a.citemStack - b.citemStack;
		});
		
		return async_js.eachSeries(items, function _each(aco, callback) {
			//core.debug("<stackInventory|_each>");
			if (!aco || !aco.fExists) return core.setImmediate(callback);

			var partners = core.getItemStackPartners({aco: aco, logger: logger, max: aco.citemMaxStack - 1});
			if (partners.length == 0) return core.setImmediate(callback);
			logger("aco: " + aco.szName, aco.citemStack, "partners: " + partners.length);

			partners.sort(function(a, b) {
				return b.citemStack - a.citemStack;
			});

			var acoPartner = partners[0];
			
			logger(aco.szName + " (" + aco.citemStack + "/" + aco.citemMaxStack + ")'s stack partner is " + acoPartner.szName + " (" + acoPartner.citemStack + "/" + acoPartner.citemMaxStack + ")");
			if (!acoPartner || !acoPartner.fExists) return core.setImmediate(callback);

			core.mergeItems({
				aco       : aco,
				acoPartner: acoPartner,
				logger    : logger
			}, function(err, results) {
				if (err) {
					if (err.message == "MUST_PICKUP_FIRST") {
						core.warn("Merging " + aco.szName + " threw MUST_PICKUP_FIRST error. Phantom?");
						core.phantomItems[aco.oid] = true;
						return callback(undefined, true);
					}
					return callback(err);
				}
				callback(undefined, results);
			});
		}, callback);
	};

	core.getItemStackPartners = function getItemStackPartners(params) {
		//core.debug("<getItemStackPartner>");
		var aco = params.aco;
		var max = (!params || params.max === undefined ? aco.citemMaxStack : params.max);

		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		acf.szName = aco.szName;
		acf.citemStackMax = max;

		var items = core.coacoToArray(acf.CoacoGet()).filter(function(aco) {
			return core.phantomItems[aco.oid] != true;
		});
		
		// Remove aco from available items.
		return items.filter(function(acoPartner) {
			return acoPartner.citemMaxStack > 1 && aco.icon == acoPartner.icon && acoPartner.oid != aco.oid;
		});
	};
	
	/*
	 * main -  Main skunkworks' function.
	 */
	core.main = function main() {
		if (skapi.plig == pligNotRunning) {
			core.console("****************************************************");
			core.console("Asheron's call isn't running. Cannot launch script.");
			core.console("****************************************************");
			return console.StopScript();
		} else if (skapi.plig == pligAtLogin) {
			core.console("****************************************************");
			core.console("A character hasn't logged in yet. Cannot launch script.");
			core.console("****************************************************");
			return console.StopScript();
		}

		if (config.debugToLog == true) {
			core.compressPreviousLogs();
		}
		core.initialize();

		// SkunkWorks can launch scripts at the character script, so wait until you're in world before proceeding)
		while (skapi.plig != pligInWorld) {
			core.console("Waiting to finish entering world... (plig: " + skapi.plig + ")");
			skapi.WaitEvent(ONE_SECOND, wemFullTimeout);
		}

		core.enable();

		while (core._script.enabledState == true) {
			core.tick();
			skapi.WaitEvent(QUARTER_SECOND, wemFullTimeout);
		}

		CollectGarbage();

		if (config.terminateSkunkOnShutdown == true && !core.quitting) {
			core.terminate();
		}
		
		// Tell SkunkWorks to remove all panels except its own.
		skapi.RemoveControls("!SkunkWorks");
		
		if (core.restarting == true) {
			skapi.WaitEvent(0, wemFullTimeout); // Flush any pending events.
			console.RunScript(console.szScript);
		}

		skapi.OutputLine("console.StopScript()", opmConsole);
		// eslint-disable-next-line no-console
		console.StopScript();
	};
	
	/*
	 * getFreeSpaces - Returns the number of free spaces in a pack.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.acoPack - Optional pack. Default main pack.
	 * @returns {number} free slots.
	 */
	core.getFreeSpaces = function factory() {
		function _phantomCheck(accumulator, aco) {
			if (!aco || core.phantomItems[aco.oid]) return accumulator;
			return accumulator + 1;
		}
		return function getFreeSpaces(params) {
			var acoPack = params && params.acoPack || skapi.AcoFromIpackIitem(0, -1);
			var contents = core.getPackContents(acoPack);
			var numContents = contents.reduce(_phantomCheck, 0);
			return (acoPack.citemMax - numContents);
		};
	}();

	core.getPackContents = function getPackContents(acoPack) {
		var items = [];
		// eslint-disable-next-line no-restricted-syntax
		for (var slot = 0; slot < acoPack.citemContents; slot++) {
			items.push(acoPack.AcoFromIitem(slot));
		}
		return items;
	};

	/*
	 * fakeJump - Hold down space then hit escape to cancel it. Used to keep us from idling out while script is running.
	 *
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 */
	core.fakeJump = function factory() {
		function jumpDowned() {
			var thisArg = this;
			core.setImmediate(shortDownWait, thisArg);
		}
		
		function shortDownWait(thisArg) {
			core.Keys({cmid: "{ESC}", kmo: kmoClick, thisArg: thisArg}, escClicked);
		}
		
		function escClicked() {
			var thisArg = this;
			core.Keys({cmid: cmidJump, kmo: kmoUp, thisArg: thisArg}, jumpUped);
		}
		
		function jumpUped() {
			var thisArg = this;
			thisArg.callback.call(thisArg.params && thisArg.params.thisArg);
		}

		function escClicked2() {
			var thisArg = this;
			core.Keys({cmid: cmidJump, kmo: kmoDown, thisArg: thisArg}, jumpDowned);
		}
		
		return function fakeJump(params, callback) {
			callback = callback || noop;
			params = params || {};
			params.thisArg = params.thisArg || params.env;
			var localEnv = {params: params, callback: callback};

			if (skapi.fInChatBuffer) {
				core.Keys({cmid: "{ESC}", kmo: kmoClick, thisArg: localEnv}, escClicked2);
				return;
			}
			core.Keys({cmid: cmidJump, kmo: kmoDown, thisArg: localEnv}, jumpDowned);
		};
	}();
	
	/*
	 * Keys - Call skapi.keys in its own function. Used for function profiling.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  function to call when done.
	 */
	core.Keys = function factory() {
		function skapi_Keys(params) {
			var result      = skapi.Keys(params.cmid, params.kmo);
			
			// Call the callback inside another timer so their code doesn't stretch out the time taken in this function.
			setImmediate(callKeysCallback, {thisArg: params.thisArg, callback: params.callback, result: result}); 
		}
		function callKeysCallback(params) {
			params.callback.call(params.thisArg, params.result);
		}
		return function Keys(params, callback) {
			if (typeof params !== "object") { // skapi.Keys arguments.
				var cmid = params;
				params = {};
				params.cmid = cmid;
				params.kmo = arguments[1];
				params.callback = arguments[2] || noop;
			} else {
				params.callback = callback || noop;
			}

			params.thisArg = params.thisArg || params.env;
			
			setImmediate(skapi_Keys, params);
		};
	}();
	
	var noop = function() {}; // Blank function.
	
	/*
	 * announceSomething - Say announcment in chat.
	 */
	core.announceSomething = function announceSomething() {
		if (config.chatAnnouncments == true) {
			var ads = config.announcments;
			
			// remove disabled ads.
			ads = ads.filter(function(e) {
				return e.enabled;
			}); 
			
			if (ads.length > 0) {
				var ad = ads[Math.floor(Math.random() * ads.length)];
				core.announce(ad.message);
				return true;
			}
		}
		return false;
	};
	
	core.announce = function announce(szText) {
		var szText = core.trimStr(szText);

		// Check if the text starts with a @, use skapi.Keys() to input the message. Decal plugins don't seem to catch commands via skapi.InvokeChatParser().
		if (szText.match(/^@/)) {
			// Check if the user is typing something, forgo pasting command into chat.
			if (skapi.fInChatBuffer) {
				return;
			}
			skapi.Keys("{return}");
			skapi.PasteSz(szText);
			skapi.Keys("{return}");
			return;
		}

		// Check if ad is a emote, don't add -t- suffix.
		if (!szText.match(/^\*(.*)\*$/)) {
			szText += ' -t-';
		}
		
		return skapi.InvokeChatParser(szText);
	};
	
	/*
	 * moveToFirstPack - Move a item to the first available pack.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item to move.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 */
	core.moveToFirstPack = (function() {
		return function moveToFirstPack(params, callback) {
			var aco = params.aco;
			var timeout = params.timeout || FIVE_SECONDS;
			
			var acoPack;
			var pack = 0;
			while (++pack < skapi.cpack) {
				acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
				if (acoPack.citemContents < acoPack.citemMax) {
					break;
				}
			}

			function resolve() {
				if (handler) core.removeHandler(evidNil, handler);
				if (tid) clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}
			
			if (!acoPack || acoPack.citemContents >= acoPack.citemMax) {
				return resolve(new core.Error("CONTAINER_FULL"));
			}

			var handler = {};
			handler.OnMoveItem = function OnMoveItem(acoItem, acoContainer /*, iitem*/) {
				if (aco.oid == acoItem.oid) {
					resolve(undefined, acoContainer);
				}
			};
			core.addHandler(evidOnMoveItem, handler);
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);

			aco.MoveToPack(pack);
		};
	})();
	
	/*
	 * moveToLastPack - Move a item to the last available pack.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item to move.
	 * @param {number} params.timeout - (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg - (Optional) Object to be given to callback that will assessable via 'this'.
	 */
	core.moveToLastPack = (function() {
		return function moveToLastPack(params, callback) {
			var aco = params.aco;
			var timeout = params.timeout || FIVE_SECONDS;
			
			var acoPack;
			var pack = skapi.cpack;
			while (--pack) {
				acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
				if (acoPack.citemContents < acoPack.citemMax) {
					break;
				}
			}

			if (!acoPack || acoPack.citemContents >= acoPack.citemMax) {
				return callback.call(params && params.thisArg, new core.Error("CONTAINER_FULL"));
			}
			
			
			var handler = {};
			handler.OnMoveItem = function OnMoveItem(acoItem, acoContainer/*, iitem*/) {
				if (aco.oid == acoItem.oid) {
					return resolve(undefined, acoContainer);
				}
			};
			core.addHandler(evidOnMoveItem, handler);
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			function resolve() {
				core.removeHandler(evidNil, handler);
				clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			aco.MoveToPack(pack);
		};
	})();
	
	/*
	 * stripSpecialCharacters - Strip special characters from a string.
	 *
	 * @param {string} str - Input string.
	 * @returns {string} Output string.
	 */
	core.stripSpecialCharacters = function stripSpecialCharacters(str) {
		return str.replace(/[^\w\s\']/gi, '');
	};
	
	/*
	 * isMatchingObjectKeys - Determin if a string matches a regex key in a object.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.object - Object to check keys.
	 * @param {object} params.text - Text to look for.
	 * @returns {boolean} true/false
	 */
	core.isMatchingObjectKeys = function isMatchingObjectKeys(params) {
		var object = params.object;
		var text = core.stripSpecialCharacters(params.text);
		for (var szRegex in object) {
			//if (!object.hasOwnProperty(szRegex)) continue;
			if (!Object.prototype.hasOwnProperty.call(object, szRegex)) continue;
			if (object[szRegex] != true) continue;
			if (text.match(szRegex)) {
				return true;
			}
		}
		return false;
	};
	
	/*
	 * organizeInventory - Stack and move items to side packs.
	 *
	 * @param {Object} params - Function parameters
	 * @param {function} callback - Function to call when done.
	 */
	core.organizeInventory = (function() {
		return function organizeInventory(params, callback) {
			core.debug("<organizeInventory>");
			var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);

			//var acf = skapi.AcfNew();
			//acf.olc = olcInventory;
			//var coaco = acf.CoacoGet(skapi.acoChar.coacoContents); 
			//var acos = core.coacoToArray(coaco);
			
			var session = core.currentSession;
			
			core.sortInventory({
				verbose: verbose,
				session: session
			}, callback);
		};
	})();
 
	/*
	 * escapeRegExp - Escape regex characters.
	 *
	 * @param {string} str - Input string.
	 * @returns {string} Output string.
	 */
	core.escapeRegExp = function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	};
	
	/*
	 * getSingleValue() Get single pyreal value of a stack of items.
	 *
	 * @param {object} aco - Item stack.
	 * @returns {number} Single value.
	 */
	core.getSingleValue = function getSingleValue(aco) {
		return aco.cpyValue / aco.citemStack;
	};
	
	/*
	 * compareBySingleValue - Compare two items by single value.
	 *
	 * @param {object} a - Item stack.
	 * @param {object} b - Item stack.
	 * @returns {number}-1, 0, 1
	 */
	core.compareBySingleValue = function compareBySingleValue(a, b)  {
		if (core.getSingleValue(a) > core.getSingleValue(b)) return -1;
		if (core.getSingleValue(a) < core.getSingleValue(b)) return 1;
		return 0;
	};

	/*
	 * OnTradeStart - Skunkworks' event handler.
	 *
	 * @param {object} acoSender - AC Object
	 * @param {object} acoReceiver - AC Object
	 */
	core.OnTradeStart = function OnTradeStart(acoSender, acoReceiver) {// event doesn't fire.
		core.debug("<OnTradeStart>", acoSender, acoReceiver);
		core.OnTradeReset(acoSender);
	};

	/*
	 * OnTradeAccept - Skunkworks' event handler.
	 *
	 * @param {object} aco - AC Object
	 */
	core.OnTradeAccept = function OnTradeAccept(aco) {
		core.debug("<OnTradeAccept>", aco);
		
		//var myItems = core.coacoToArray(skapi.coacoTradeRight);
		//core.debug("myItems: " + myItems.length + ". " + myItems.join(", "));

		//var theirItems = core.coacoToArray(skapi.coacoTradeLeft);
		//core.debug("theirItems: " + theirItems.length + ". " + theirItems.join(", "));

		if (core.tradePartner && core.tradePartner.szName == aco.szName && !core.waitingForTradeAccept) {
			// Partner has clicked the Trade Accept button.
				
			core.processBuyRequest({
				szSender: aco.szName
			});
		}
	};
	
	/*
	 * OnTipMessage - Skunkworks' event handler.
	 */
	core.OnTipMessage = function OnTipMessage(szMsg) {
		core.debug("<OnTipMessage>", szMsg);
		/*
		if (szMsg.match(/You cannot trade that item!/)) {
			var aco = core.acoLastItemAdded;
			if (aco) {
				core.info(core.getFullName(aco) + " (" + aco.oid + ") might be a phantom item!");
				core.phantomItems[aco.oid] = true;
			}
		}
		*/
	};

	/*
	 * OnTradeAdd - Skunkworks' event handler.
	 */
	core.OnTradeAdd = function OnTradeAdd(aco, side) {//2 = them, 1=us
		core.debug("<OnTradeAdd>", aco, side);
		
		// <OnTradeAdd>, Gold Pea, 2
		if (!core.tradePartner) {
			return;
		}

		// skapi.coacoTradeLeft doesn't work, so we need to track what items they place in the window ourselves. 
		core.tradeWindow = core.tradeWindow || {};
		core.tradeWindow[side] = core.tradeWindow[side] || [];
		
		// Ignore items we've already registered. 
		if (core.tradeWindow[side].indexOf(aco) >= 0) {
			core.debug(aco.szName + " is already in the trade window?");
			return;
		}
		
		core.tradeWindow[side].push(aco);
		
		if (side == 2) {
			core.info("side: " + side, "items: " + core.tradeWindow[side]);
		}

		if (side == 2) {// Them
			// Assess item now so we won't need to later.
			return core.getItemPointValue({aco: aco, logger: core.info}, function onValue(err, value) {
				if (err) {
					return core.tell({
						szRecipient: core.tradePartner.szName, 
						szMsg      : core.arrayTemplate(_l.errorOccurred, {
							message: err.message
						})
					});
				}
				var szFull = core.getFullName(aco);
				if (value) {
					return core.tell({
						szRecipient: core.tradePartner.szName, 
						szMsg      : core.arrayTemplate(_l.itemValuedAt, {points: value, name: szFull, amount: aco.citemStack})
					});
				} else {
					return core.tell({
						szRecipient: core.tradePartner.szName, 
						szMsg      : core.arrayTemplate(_l.itemWorthless, {item: szFull})
					});
				}
			});
		}
	};
	
	core.getItemPointValue = function getItemPointValue(params, callback) {
		var aco = params.aco;
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		function finish() {
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var szFull = core.getFullName(aco);
		var value = core.getTotalSellPointsOfItems({items: [aco], logger: logger});
		
		if (value) {
			logger("Their " + core.getFullName(aco) + " is worth " + value + " points.");
			return finish(undefined, value);
		}
		
		if (typeof core.LootProfile3 === "undefined") {
			return finish();
		}
		
		core.debug("assessing " + aco);
		return core.assessAco({aco: aco}, function onAssess(err, results) {
			core.debug("<onAssess>", err, results);
			if (err) return finish(err);

			//var aco = results.aco;
			core.debug("Getting matching profiles... aco: " + aco);
			return core.getMatchingPointProfiles({aco: aco}, onProfiles);
		});
		
		function onProfiles(err, results) {
			core.debug("<onProfiles>", err, results);
			if (err) return finish(err);
			
			if (results.length == 0) {
				return finish();
			}

			var profileValues = results.map(function(profileName) {
				var info = config.pointsProfiles[profileName];
				return {
					name : profileName,
					value: info && info.value
				};
			});
			
			profileValues.sort(function(a, b) {
				return b.value - a.value;
			});

			logger(szFull + " matches profiles: " + profileValues.map(function(info) {
				return info.name + " (" + info.value + ")";
			}).join(", "));

			return finish(undefined, profileValues[0].value);
		}
	};
	
	/*
	 * OnTradeEnd - Skunkworks' event handler.
	 *
	 * @param {number} arc - Skunkworks arc type.
	 */
	core.OnTradeEnd = function OnTradeEnd(arc) {
		core.debug("<OnTradeEnd>", arc);
		if (core.tradePartner) {
			core.tell({
				szRecipient: core.tradePartner.szName, 
				szMsg      : core.arrayTemplate(_l.tradeFarewell)
			});
		}
		core.resetTradePartner();
		core.queueOrganize = true;

		core.processingBuyRequest = {};
		
		core.stackInventory(undefined, function(err) {
			if (err) return core.warn("Error while stacking inventory", err);
		});
	};

	core.resetTradePartner = function factory() {
		function _isStartupGear(aco) {
			return core.startupGear.indexOf(aco) < 0;
		}
		function _acoToParams(aco) {
			return {
				aco: aco
			};
		}
		function _onEquippedItems(err) {
			if (err) {
				core.warn("Error while equipping startup gear.", err);
			}
			
			core.equipmentChanged = false;
		}
		function _onUnequip(err) {
			if (err) {
				core.warn("Error while unequipping current gear.", err);
			}

			core.info("Equipping " + core.startupGear.length + " items the script started with.");
			async_js.eachSeries(core.startupGear.map(_acoToParams), core.equipItem, _onEquippedItems);
		}
		return function resetTradePartner() {
			core.debug("<resetTradePartner>");
			core.tradePartner = null;
			core.tradeWindow = null;
			core.tradePromo = null;
			
			if (config.equipItemsOnRequest == true && core.equipmentChanged == true) {
				// Remove any items that don't match our startup gear.
				var equippedItems = core.getEquippedItems();
				equippedItems = equippedItems.filter(_isStartupGear);

				if (equippedItems.length > 0) {
					core.info("We need to unequip " + equippedItems.length + " items.");

					async_js.eachSeries(equippedItems.map(_acoToParams), core.unequipItem, _onUnequip);
				} else {
					core.info("Equipping " + core.startupGear.length + " items the script started with.");
					async_js.eachSeries(core.startupGear.map(function(aco) {
						return {
							aco: aco
						};
					}), core.equipItem, _onEquippedItems);
				}
			}
		};
	}();

	/*
	 * getFullName - Return a aco's name with its material name.
	 *
	 * @param {object} aco - Item.
	 * @returns {string} Full item name.
	 */
	core.getFullName = function factory() {
		var materials = {};
		materials[materialAgate]            = "Agate";
		materials[materialAlabaster]        = "Alabaster";
		materials[materialAmber]            = "Amber";
		materials[materialAmethyst]         = "Amethyst";
		materials[materialAquamarine]       = "Aquamarine";
		materials[materialAzurite]          = "Azurite";
		materials[materialBlackGarnet]      = "Black Garnet";
		materials[materialBlackOpal]        = "Black Opal";
		materials[materialBloodstone]       = "Bloodstone";
		materials[materialBrass]            = "Brass";
		materials[materialBronze]           = "Bronze";
		materials[materialCarnelian]        = "Carnelian";
		materials[materialCeramic]          = "Ceramic";
		materials[materialCitrine]          = "Citrine";
		materials[materialCloth]            = "Cloth";
		materials[materialCopper]           = "Copper";
		materials[materialDiamond]          = "Diamond";
		materials[materialDilloHide]        = "Dillo Hide";
		materials[materialEbony]            = "Ebony";
		materials[materialEmerald]          = "Emerald";
		materials[materialFireOpal]         = "Fire Opal";
		materials[materialGem]              = "Gem";
		materials[materialGold]             = "Gold";
		materials[materialGranite]          = "Granite";
		materials[materialGreenGarnet]      = "Green Garnet";
		materials[materialGreenJade]        = "Green Jade";
		materials[materialGromnieHide]      = "Gromnie Hide";
		materials[materialHematite]         = "Hematite";
		materials[materialImperialTopaz]    = "Imperial Topaz";
		materials[materialIron]             = "Iron";
		materials[materialIvory]            = "Ivory";
		materials[materialJet]              = "Jet";
		materials[materialLapisLazuli]      = "Lapis Lazuli";
		materials[materialLavenderJade]     = "Lavender Jade";
		materials[materialLeather]          = "Leather";
		materials[materialLinen]            = "Linen";
		materials[materialMahogany]         = "Mahogany";
		materials[materialMalachite]        = "Malachite";
		materials[materialMarble]           = "Marble";
		materials[materialMetal]            = "Metal";
		materials[materialMoonstone]        = "Moonstone";
		materials[materialOak]              = "Oak";
		materials[materialObsidian]         = "Obsidian";
		materials[materialOnyx]             = "Onyx";
		materials[materialOpal]             = "Opal";
		materials[materialPeridot]          = "Peridot";
		materials[materialPine]             = "Pine";
		materials[materialPorcelain]        = "Porcelain";
		materials[materialPyreal]           = "Pyreal";
		materials[materialRedGarnet]        = "Red Garnet";
		materials[materialRedJade]          = "Red Jade";
		materials[materialReedsharkHide]    = "Reedshark Hide";
		materials[materialRoseQuartz]       = "Rose Quartz";
		materials[materialRuby]             = "Ruby";
		materials[materialSandstone]        = "Sandstone";
		materials[materialSapphire]         = "Sapphire";
		materials[materialSatin]            = "Satin";
		materials[materialSerpentine]       = "Serpentine";
		materials[materialSilk]             = "Silk";
		materials[materialSilver]           = "Silver";
		materials[materialSmokyQuartz]     = "Smoky Quartz";
		materials[materialSteel]            = "Steel";
		materials[materialStone]            = "Stone";
		materials[materialSunstone]         = "Sunstone";
		materials[materialTeak]             = "Teak";
		materials[materialTigerEye]         = "Tiger Eye";
		materials[materialTourmaline]       = "Tourmaline";
		materials[materialTurquoise]        = "Turquoise";
		materials[materialVelvet]           = "Velvet";
		materials[materialWhiteJade]        = "White Jade";
		materials[materialWhiteQuartz]      = "White Quartz";
		materials[materialWhiteSapphire]    = "White Sapphire";
		materials[materialWood]             = "Wood";
		materials[materialWool]             = "Wool";
		materials[materialYellowGarnet]     = "Yellow Garnet";
		materials[materialYellowTopaz]      = "Yellow Topaz";
		materials[materialZircon]           = "Zircon";

		var translate = {};
		translate["Salvaged Granite"] = "Granite Salvaged"; // Skunkworks sees item as 'Salvaged Granite', but in game it says 'Granite Salvage'
		translate["Salvaged Steel"] = "Steel Salvaged";

		return function getFullName(aco) {
			var szName = aco && aco.szName;
			szName = translate[szName] || szName;
			if (aco.mcm == mcmSalvageBag && !szName.match(/Salvage \((\d*)\)/)) {
				return szName;
			} else if (aco && aco.material > 0 && materials[aco.material] && !szName.match(materials[aco.material])) {
				szName = materials[aco.material] + " " + szName;
			}
			return szName;
		};
	}();
	
	core.getNameWithWorkmanship = function getNameWithWorkmanship(aco) {
		var szName = core.getFullName(aco);
		if (aco.mcm == mcmSalvageBag) {
			szName += " [w" + core.round(aco.workmanship, config.workmanshipRounding) + "]";
		}
		return szName;
	};

	// Sort items by categories before by name.
	core.compare_items = function compare_items(a, b) {
		// Item background for rends, ect.

		
		if (a.mcm != mcmSalvageBag && b.mcm == mcmSalvageBag) return -1;
		if (a.mcm == mcmSalvageBag && b.mcm != mcmSalvageBag) return 1;
		
		if (!core.isUnique(a) && core.isUnique(b)) return -1;
		if (core.isUnique(a) && !core.isUnique(b)) return 1;

		if (a.iconUnderlay > b.iconUnderlay) return -1;
		if (a.iconUnderlay < b.iconUnderlay) return 1;

		if (a.mcm < b.mcm) return -1;
		if (a.mcm > b.mcm) return 1;

		if (a.eqm < b.eqm) return -1;
		if (a.eqm > b.eqm) return 1;

		var skidA = a["oai.iwi.skid"] || a["oai.ibi.skidWieldReq"] || a.oai && a.oai.iwi && a.oai.iwi.skid || a.oai && a.oai.ibi && a.oai.ibi.skidWieldReq;
		var skidB = b["oai.iwi.skid"] || b["oai.ibi.skidWieldReq"] || b.oai && b.oai.iwi && b.oai.iwi.skid || b.oai && b.oai.ibi && b.oai.ibi.skidWieldReq;
		if (skidA < skidB) return -1;
		if (skidA > skidB) return 1;

		var lvlWieldReqA = a["oai.ibi.lvlWieldReq"] || a.oai && a.oai.ibi && a.oai.ibi.lvlWieldReq || 0;
		var lvlWieldReqB = b["oai.ibi.lvlWieldReq"] || b.oai && b.oai.ibi && b.oai.ibi.lvlWieldReq || 0;
		if (lvlWieldReqA < lvlWieldReqB) return -1;
		if (lvlWieldReqA > lvlWieldReqB) return 1;
		
		if (a.eqm & (eqmWeapon | eqmTwoHand) && b.eqm & (eqmWeapon | eqmTwoHand)) {
			var A_dmg = core.getDamageAvg(a);
			var B_dmg = core.getDamageAvg(b);
			if (A_dmg != B_dmg) return B_dmg - A_dmg;
		}
		
		if (a.eqm & (eqmFocusWeapon) && b.eqm & (eqmFocusWeapon)) {
			var A_scalePvMElemBonus = core.getScalePvMElemBonus(a);
			var B_scalePvMElemBonus = core.getScalePvMElemBonus(b);
			if (A_scalePvMElemBonus != B_scalePvMElemBonus) return B_scalePvMElemBonus - A_scalePvMElemBonus;
			
			var A_MeleeD = core.getDefenseBonus(a);
			var B_MeleeD = core.getDefenseBonus(b);
			if (A_MeleeD != B_MeleeD) return B_MeleeD - A_MeleeD;
		}
		
		if (a.eqm & (eqmRangedWeapon) && b.eqm & (eqmRangedWeapon)) {
			var A_scaleDamageBonus = a["oai.iwi.scaleDamageBonus"] || a.oai && a.oai.iwi && a.oai.iwi.scaleDamageBonus || 0;
			var B_scaleDamageBonus = b["oai.iwi.scaleDamageBonus"] || b.oai && b.oai.iwi && b.oai.iwi.scaleDamageBonus || 0;
			if (A_scaleDamageBonus != B_scaleDamageBonus) return B_scaleDamageBonus - A_scaleDamageBonus;
			
			var A_dhealthElemBonus = core.getMissileElemBonus(a);
			var B_dhealthElemBonus = core.getMissileElemBonus(b);
			if (A_dhealthElemBonus != B_dhealthElemBonus) return B_dhealthElemBonus - A_dhealthElemBonus;
			var A_MeleeD = core.getDefenseBonus(a);
			var B_MeleeD = core.getDefenseBonus(b);
			if (A_MeleeD != B_MeleeD) return B_MeleeD - A_MeleeD;
		}
		
		var hasMaterialA = a.material > 0;
		var hasMaterialB = b.material > 0;
		if (hasMaterialA && !hasMaterialB) return -1;
		if (!hasMaterialA && hasMaterialB) return 1;


		// Overlay icon, like the item level on aetheria.
		if (a.iconOverlay < b.iconOverlay) return -1;
		if (a.iconOverlay > b.iconOverlay) return 1;

		// Sort bags by workmanship.
		if (a.mcm == mcmSalvageBag && b.mcm == mcmSalvageBag) {
			if (core.getFullName(a) != core.getFullName(b)) {
				if (core.getFullName(a) < core.getFullName(b)) return -1;
				if (core.getFullName(a) > core.getFullName(b)) return 1;
			}
			if (a.workmanship < b.workmanship) return -1;
			if (a.workmanship > b.workmanship) return 1;
		}

		// Icon outline colors that apply to this object. Enchanted items, for instance, have a blue halo around their icons. Healing foods have a red halo, and so on.
		if (a.ioc > b.ioc) return -1;
		if (a.ioc < b.ioc) return 1;

		// Put orbs together, coats together, ect.
		if (a.szName < b.szName) return -1;
		if (a.szName > b.szName) return 1;

		// Sort with material names.
		if (core.getFullName(a) < core.getFullName(b)) return -1;
		if (core.getFullName(a) > core.getFullName(b)) return 1;

		// Sort by cheapest item.
		if (a.cpyValue < b.cpyValue) return -1;
		if (a.cpyValue > b.cpyValue) return 1;

		var itemSetA = a.oai && a.oai.ibi && a.oai.ibi.raw.Item(RAW_ITEMSET) || 0;
		var itemSetB = b.oai && b.oai.ibi && b.oai.ibi.raw.Item(RAW_ITEMSET) || 0;
		if (itemSetA < itemSetB) return -1;
		if (itemSetA > itemSetB) return 1;
		
		if (a.oid < b.oid) return -1;
		if (a.oid > b.oid) return 1;
		
		return 0;
	};

	/*
	 * core.trimStr - Trim a string of spaces on either side.
	 *
	 * @param {string} strInput - Input string.
	 * @returns {string} Output string.
	 */
	core.trimStr = function trimStr(strInput) {
		var objRegex = new RegExp("(^\\s+)|(\\s+$)");
		return strInput.replace(objRegex, "");
	};
	
	/*
	 * OnCommand - Skunkworks' event handler.
	 *
	 * @param {string} szCmd - String after /sw
	 */
	core.OnCommand = function OnCommand(szCmd) {
		core.debug("<OnCommand>", szCmd);
		szCmd = core.trimStr(szCmd);

		if (szCmd && szCmd.match(/^st/i)) {
			if (szCmd.match(/^st help/i)) {
				core.info("Available commands: active, restart, terminate");
				return;
			} else if (szCmd.match(/^st active$/i)) {
				core.info("Set the active status of the script, include true or false after active. eg: '/sw st active true'");
				return;
			} else if (szCmd.match(/^st active (.*)/i)) {
				var rest = RegExp.$1;
				var toggle = (rest == "true" && true || false);
				core.controls.chkActive.setChecked(toggle);
				config.active = toggle;
				core.saveCharacterConfig();
				core.info("active set to " + toggle);
				return;
			} else if (szCmd.match(/^st restart/i)) {
				core.warn("Restarting SkunkTrader... ");
				core.restarting = true;
				core.disable();
				return;
			} else if (szCmd.match(/^st terminate/i)) {
				core.warn("Terminating SkunkWorks... ");
				core.quitting = true;
				core.disable();
				core.terminate();
				return;
			} else if (szCmd.match(/^st test1/i)) {
				core.sortInventory();
				return;
			} else if (szCmd.match(/^st ping/i)) {
				core.tell({
					szRecipient: skapi.acoChar.szName, 
					szMsg      : "// pong"
				});
			} else if (szCmd.match(/^st password/i)) {
				if (!skapi.acoSelected) {
					return core.warn("Select a item first.");
				}
				var aco = skapi.acoSelected;
				var szFull = core.getFullName(aco);
				
				var password = core.getSellPassword({
					aco: aco
				});

				return core.showInputWindow({
					szMessage: "Password to purchase " + szFull,
					value    : password
				}, function onValue(err, value) {
					core.debug("<onValue>", err, value);
					
					if (value == "") value = undefined;
					
					if (core.isUnique(aco)) {
						core.setSellingPassword({
							oid     : aco && aco.oid,
							szItem  : szFull,
							password: value
						});
					} else {
						core.setSellingPassword({
							szItem  : szFull,
							password: value
						});
					}

					if (value) {
						core.info("Password for " + szFull + " set to " + value + ".");
					} else if (password) {
						core.info("Password removed for " + szFull + ".");
					}
				});
			}

			core.info("Unrecognized chat command: " + szCmd);
		}
	};
	
	core.lastTradeResetTime = 0;
	/*
	 * OnTradeReset - Skunkworks' event handler.
	 *
	 * @param {object} aco - AC Object
	 */
	core.OnTradeReset = function OnTradeReset(aco) {// Fires for start and reset (clear all).
		core.debug("<core|OnTradeReset>", aco);

		// Temp ACE fix for missing aco object.
		aco = aco || skapi.acoChar;

		core.lastTradeResetTime = new Date().getTime();
		core.allItemsShown = false;
		core.tradeWindow = null;
		
		// OnTradeReset fires for both start and reset. Check if we already have a trade partner, if so then they probably reset the window.
		if (core.tradePartner != null) {
			core.info(aco.szName + " cleared the trade window.");
			return;
		}

		if (!core.isActive()) {
			core.info(aco.szName + " opened a trade window, but script isn't active at the moment.");
			return;
		}
		
		if (aco.szName && core.isIgnoredPlayer({szName: aco.szName})) {
			core.info("Ignoring " + aco.szName + "'s trade request.");

			//core.setImmediate(core.applyMethod, core.controls.view, core.controls.view.callPanelFunction, ["TradeEnd"]);
			core.callTradeEnd();
			return;
		}
		
		// + player as our partner.
		core.tradePartner = {
			szName: aco.szName,
			aco   : aco
		};

		// Assess player for discount info later.
		if (!aco.oai) {
			core.assessAco({aco: aco});
		}
	
		function onWindowTimeout(err, results) {
			core.debug("<onWindowTimeout> " + err + ", " + results);
			if (results == true) {
				core.info("Trade window timed out, closing trade...");
				core.tell({
					szRecipient: aco.szName, 
					szMsg      : core.arrayTemplate(_l.tradeSessionTimeout)
				});
				core.resetTradePartner();

				//core.setImmediate(core.applyMethod, core.controls.view, core.controls.view.callPanelFunction, ["TradeEnd"]);
				core.callTradeEnd();
			}
		}

		core.showItemsForSale({szPlayer: aco.szName}, function onShow(err, result) {
			if (err) return;
			core.debug("onShow result: " + result);
			core.tell({
				szRecipient: aco.szName, 
				szMsg      : core.arrayTemplate(_l.itemsShown)
			});

			if (config.openWindowTimeout > 0) {
				core.waitForWindowTimeout({timeout: config.openWindowTimeout}, onWindowTimeout); // 
			}
		});
		
		core.stats.totalVisits = core.stats.totalVisits || 0;
		core.stats.totalVisits += 1;
		
		core.stats.uniqueVisis = core.stats.uniqueVisis || {};
		core.stats.uniqueVisis[aco.szName] = new Date().getTime();
	};

	/*
	 * showItemsForSale - Show our items for sale in the trade window.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szPlayer - Optional player name to tell we're showing items.
	 * @param {function} callback - Function to be called with a result and acoItem if successful.
	 */
	core.showItemsForSale = function showItemsForSale(params, callback) {
		var szPlayer = params && params.szPlayer;
		var items = core.getItemsForSale();

		if (config.showDuplicateCommonItems != true) {
			// Remove common items.
			var names = {};
			items = items.filter(function(aco) {
				if (!core.isUnique(aco)) {
					names[core.getFullName(aco)] = aco;
					return false;
				}
				return true;
			});
			
			
			// Add one of each common item back.
			for (var key in names) {
				//if (!names.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(names, key)) continue;
				items.push(names[key]);
			}
		}
			
		items.sort(core.compare_items);
		items.reverse(); // So items appear alphabetical in trade window.
		
		//core.info("szPlayer: " + szPlayer);
		if (szPlayer) {
			var current = core.getAccountBalance({szPlayer: szPlayer}) || 0;
			if (current > 0) {
				core.tell({
					szRecipient: szPlayer, 
					szMsg      : core.arrayTemplate(_l.tradeGreetingWithBalance, {
						count : items.length,
						points: current
					})
				});
			} else {
				core.tell({
					szRecipient: szPlayer, 
					szMsg      : core.arrayTemplate(_l.tradeGreeting, {
						count: items.length
					})
				});
			}
		}
		core.allItemsShown = true;
		
		// Fire addItemsToTradeWindow on next tick.
		return core.addItemsToTradeWindow({items: items}, callback);
	};
	
	/*
	 * numberWithCommas() Format number with commas.
	 *
	 * @param {number} x - Number to format.
	 * @returns {string} Formatted number.
	 */
	var numberWithCommas = core.numberWithCommas = function numberWithCommas(x) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	};

	/*
	 * formatPyreals() Format number as pyreals with 'p' at the end.
	 *
	 * @param {number} p - Number to format.
	 * @returns {string} Formatted number.
	 */
	core.formatPyreals = function formatPyreals(p) {
		return numberWithCommas(Math.round(p)) + "p";
	};
	
	/*
	 * addItemToTradeWindow() Add single item to trade window.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.acoItem: Item to be added.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 */
	core.addItemToTradeWindow = function addItemToTradeWindow(params, callback) {
		core.debug("<addItemToTradeWindow>");
		var acoItem = params.acoItem;
		var timeout = params.timeout || 2 * ONE_SECOND;
		var quantity = params.quantity;
		
		if (quantity && acoItem.citemStack > quantity) {

			if (core.getFreeSpaces() <= 2) {
				return callback(new core.Error("NOT_ENOUGH_SPACE"));
			}

			return core.splitAco({
				aco     : acoItem,
				quantity: quantity
			}, function onSplit(err, acoSplit) {
				if (err) return callback(err);
				return addItemToTradeWindow({
					acoItem: acoSplit
				}, callback);
			});
		}
		
		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You cannot trade that item!/)) {
				resolve(new core.Error("CANNOT_TRADE_ITEM"));
			}
		};
		core.addHandler(evidOnTipMessage,            handler);

		handler.OnTradeAdd = function OnTradeAdd(aco, side) {
			if (side == 1 && aco == acoItem) {
				return resolve(undefined, true);
			}
		};
		core.addHandler(evidOnTradeAdd,            handler);

		// Watch for other player resetting trade window. Cancel our current add if that happens.
		handler.OnTradeReset = function OnTradeReset(aco) {
			core.debug("<addItemsToTradeWindow|OnTradeReset>", aco);
			if (aco != skapi.acoChar) {
				resolve(new core.Error("OTHER_CANCELED_TRADE"));
			}
		};
		core.addHandler(evidOnTradeReset, handler);

		var tid = core.setTimeout(core.timedOut, timeout, resolve);
		function resolve() {
			core.removeHandler(evidNil, handler);
			clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
		}
		
		acoItem.AddToTrade(); // Takes ~100ms per call.
	};
	
	/*
	 * addItemsToTradeWindow() Add array of items to trade window.
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.items - Array of acos.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 */
	core.addItemsToTradeWindow = (function() {
		function _getOid(aco) {
			return aco.oid;
		}
		return function addItemsToTradeWindow(params, callback) {
			core.debug("<addItemsToTradeWindow>");
			var items = params.items;
			var timeout = params.timeout || (Number(items.length) * ONE_SECOND);
			core.debug("addItemsToTradeWindow items: " + items.length);
			
			callback = callback || noop;
			
			var cancellation = params.cancellation || new core.Cancellation();
			
			var handler = {};
			handler.OnTradeEnd = function OnTradeEnd(/*arc*/) {
				core.debug("<addItemsToTradeWindow|OnTradeEnd>");
				resolve(new core.Error("OTHER_CANCELED_TRADE"));
			};
			core.addHandler(evidOnTradeEnd, handler);
			
			handler.OnTradeReset = function OnTradeReset(aco) {
				core.debug("<addItemsToTradeWindow|OnTradeReset>", aco);
				if (aco != skapi.acoChar) {
					resolve(new core.Error("OTHER_CANCELED_TRADE"));
				}
			};
			core.addHandler(evidOnTradeReset, handler);

			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			function resolve(err) {
				core.debug("<addItemsToTradeWindow|resolve>", err);
				if (err && cancellation) cancellation.canceled = true;
				core.removeHandler(evidNil, handler);
				clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}


			if (config.addItemsOneAtATime == true) {
				//var session = core.currentSession;
				async_js.eachSeries(items, function each(aco, callback) {
					if (cancellation && cancellation.canceled) return callback(new core.Error("CANCELLATION"));
					
					//core.debug("Adding " + aco.szName + " (" + aco.oid + ") to trade window.");
					core.acoLastItemAdded = aco;
					aco.AddToTrade(); // Takes ~100ms per call.
					core.setTimeout(callback, 1); // Process events for possible TradeEnd, then move to the next item.
					
				}, resolve);
			} else {
				var oids = items.map(_getOid);
				
				core.debug("Using new method to add " + oids.length + " items to trade window...");

				//core.controls.view.callPanelFunction("TradeAdd", oids.join(","));
				
				core.callTradeAdd({oids: oids});
				
				// TODO: Add event confirmation that items were added.
				resolve();
			}
		};
	})();

	/*
	 * coacoToArray - Convert Skunkworks coaco object to javascript array.
	 *
	 * @param {object} coaco - Collection AC Object
	 * @returns {array} list of items in the coaco.
	 */
	core.coacoToArray = function coacoToArray(coaco) {
		var arr = [];
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < coaco.Count; i++) {
			arr.push(coaco.Item(i));
		}
		return arr;
	};
	
	/*
	 * getInventoryItems -  Get items in our inventory.
	 *
	 * @returns {array} list of items.
	 */
	core.getInventoryItems = function factory() {
		function _shouldRemoveItem(aco) {
			if (core.phantomItems[aco.oid]) return false;
			if (core.isMatchingObjectKeys({text: aco.szName, object: config.ignoreItems})) return false;
			if (!aco.szName) false;

			return true;
		}
		return function getInventoryItems(params) {
			//core.debug("<getInventoryItems>");
			var items = params && params._items || core.getAllInventoryItems();
			return items.filter(_shouldRemoveItem);
		};
	}();
	
	/*
	 * getAllInventoryItems -  Get all items in our inventory.
	 *
	 * @returns {array} list of items.
	 */
	core.getAllInventoryItems = function getAllInventoryItems() {
		//core.debug("<getAllInventoryItems>");
		var items = [];
		var acoPack, aco;
		// eslint-disable-next-line no-restricted-syntax
		for (var pack = 0; pack < skapi.cpack; pack++) {
			acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
			// eslint-disable-next-line no-restricted-syntax
			for (var slot = 0; slot < acoPack.citemContents; slot++) {
				aco = skapi.AcoFromIpackIitem(pack, slot);
				items.push(aco);
			}
		}
		
		// Include items we're wearing, in case user asked us to equip a item.
		var equippedItems = core.getEquippedItems();
		return items.concat(equippedItems);
	};
	
	
	core.getPhantomItem = function getPhantomItem() { // debug function
		var acoPack, numSlots, aco;
		// eslint-disable-next-line no-restricted-syntax
		for (var pack = 0; pack < skapi.cpack; pack++) {
			acoPack = skapi.AcoFromIpackIitem(pack, iitemNil);
			numSlots = acoPack.citemContents;
			// eslint-disable-next-line no-restricted-syntax
			for (var slot = 0; slot < numSlots; slot++) {
				aco = skapi.AcoFromIpackIitem(pack, slot);
				if (core.phantomItems[aco.oid]) {
					return aco;
				}
			}
		}
	};
	
	/*
	 * isInt - Determin if a value is a integer.
	 *
	 * @param {varible} n - value to check.
	 * @returns {boolean} true/false
	 */
	var isInt = core.isInt = function isInt(n) {
		return Number(n) === n && n % 1 === 0;
	};

	/*
	 * isFloat - Determin if a value is a float.
	 *
	 * @param {varible} n - value to check.
	 * @returns {boolean} true/false
	 */
	var isFloat = core.isFloat = function isFloat(n) {
		return Number(n) === n && n % 1 !== 0;
	};
	
	/*
	 * showItemValueWindow - Show a GUI with controls for user to input a new value.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name.
	 * @param {number} params.value - Default item value.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 */
	core.showItemValueWindow = (function() {
		return function showItemValueWindow(params, callback) {
			core.debug("<showItemValueWindow>");
			
			var szItem = params.szItem || params.aco && core.getFullName(params.aco);
			var value = params.value || 0;
			
			//var tid = core.setTimeout(core.timedOut, timeout, resolve);
			function resolve() {
				//core.removeHandler(evidNil, handler);
				//clearTimeout(tid);
				view.removeControls();
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}
			
			var view = core.createView({
				title : szItem + "'s value", 
				width : 200, 
				height: 30 + (20 * 3),
				icon  : 0
			});
			var layout = new SkunkSchema.FixedLayout({parent: view});
			
			var stItem = new SkunkSchema.StaticText({parent: layout, name: "stItem", text: "item: " + szItem})
				.setWidth(200);
			
			var stPrice = new SkunkSchema.StaticText({parent: layout, name: "stPrice", text: "Value (each): "})
				.setAnchor(stItem, "BOTTOMLEFT");

			var edPrice = new SkunkSchema.Edit({parent: layout, name: "edPrice", text: value})
				.setAnchor(stPrice, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Value is " + value);
				});
			
			// eslint-disable-next-line no-unused-vars
			var btnSave = new SkunkSchema.PushButton({parent: layout, name: "btnSave", text: "Save"})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					edPrice.getPropertyAsync("text", function(err, text) {
						if (err) return;
						var v = Number(text);
						if (!isInt(v) && !isFloat(v)) {
							return core.info(v + " is not a number.");
						}
						return resolve(undefined, v);
					});
				});
				
			// eslint-disable-next-line no-unused-vars
			var btnCancel = new SkunkSchema.PushButton({parent: layout, name: "btnCancel", text: "Cancel"})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return resolve(new core.Error("CANCELED"));
				});

			//core.info("Input a value and hit [enter] on your keyboard.");
			view.showControls(true);
		};
	})();
	
	/*
	 * showInputWindow - Show a GUI with controls for user to input a new value.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name.
	 * @param {number} params.value - Default item value.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 */
	core.showInputWindow = (function() {
		return function showInputWindow(params, callback) {
			core.debug("<showInputWindow>");
			
			var szMessage = params.szMessage;
			var value = params.value;
			
			//var tid = core.setTimeout(core.timedOut, timeout, resolve);
			function resolve() {
				//core.removeHandler(evidNil, handler);
				//clearTimeout(tid);
				view.removeControls();
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}
			
			var view = core.createView({
				title : "Input Value", 
				width : 200, 
				height: 30 + (20 * 3),
				icon  : 0
			});
			var layout = new SkunkSchema.FixedLayout({parent: view});
			
			var stMessage = new SkunkSchema.StaticText({parent: layout, name: "stMessage", text: szMessage})
				.setWidth(200);
			
			var stInput = new SkunkSchema.StaticText({parent: layout, name: "stInput", text: "Input: "})
				.setAnchor(stMessage, "BOTTOMLEFT");

			var edInput = new SkunkSchema.Edit({parent: layout, name: "edInput", text: value})
				.setAnchor(stInput, "TOPRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.info("Value is " + value);
				});
			
			// eslint-disable-next-line no-unused-vars
			var btnSave = new SkunkSchema.PushButton({parent: layout, name: "btnSave", text: "Save"})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					edInput.getPropertyAsync("text", function(err, text) {
						if (err) return;
						resolve(undefined, text);
					});
				});
				
			// eslint-disable-next-line no-unused-vars
			var btnCancel = new SkunkSchema.PushButton({parent: layout, name: "btnCancel", text: "Cancel"})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return resolve(new core.Error("CANCELED"));
				});

			//core.info("Input a value, hit [enter] on your keyboard then click Save.");
			view.showControls(true);
		};
	})();
	
	/*
	 * canTransfer - Determin if we can transfer a item to another container/player.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item to check.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 */
	core.canTransfer = (function() {
		return function canTransfer(params, callback) {
			core.debug("<canTransfer>", params.aco.oid);
			var aco = params.aco;
			var timeout = params.timeout || TEN_SECONDS;

			var cancellation = params.cancellation;// || new core.Cancellation();
			
			function onAssess(err, results) {
				core.debug("<onAssess>", aco.oid, (results && results.aco && results.aco.oid));
				if (err) return resolve(err);
				if (cancellation && cancellation.canceled) return resolve(new core.Error("CANCELLATION"));
				if (aco.oai && aco.oai.ibi && aco.oai.ibi.fAttuned) {
					return resolve(undefined, false);
				}
				resolve(undefined, true);
			}

			function resolve(err) {
				core.debug("<canTransfer|resolve>", err, aco.oid);
				clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			var tid = core.setTimeout(function() {
				var error = new core.Error("TIMED_OUT");
				error.set("aco", aco);
				error.set("szName", core.getFullName(aco));
				error.set("oid", aco.oid);
				resolve(error);
			}, timeout);

			if (aco.oai) {
				// Call onAssess asynchronously so resolve doesn't get called synchronously.
				setImmediate(onAssess);
			} else {
				core.assessAco({aco: aco}, onAssess);
			}
		};
	})();

	/*
	 * getTransferableItems - Get a list of transferable items.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 */
	core.getTransferableItems = (function() {
		return function getTransferableItems(params, callback) {
			core.debug("<getTransferableItems>");
			var timeout = params && params.timeout || FIVE_MINUTES;
			var progressMethod = params && params.progressMethod;
			
			var inventoryInfo = params && params.inventoryInfo || {};
		
			
			function resolve() {
				clearTimeout(tid);
				cancellation.canceled = true;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			
			var inv = core.getInventoryItems();
			core.debug("inv: " + inv.length);

			var i = 0;
			var prevValues = {};
			
			var cancellation = params.cancellation || new core.Cancellation();
			
			var szFull;
			async_js.concatSeries(inv, each, resolve);

			function each(aco, callback) {
				if (cancellation && cancellation.canceled) return callback(new core.Error("CANCELLATION"));

				szFull = core.getFullName(aco);
				
				i++;
				if (progressMethod) {
					progressMethod({
						aco  : aco,
						i    : i,
						total: inv.length
					});
				}

				if (typeof inventoryInfo[aco.oid] !== "undefined" && typeof inventoryInfo[aco.oid].transferable !== "undefined") {
					if (inventoryInfo[aco.oid].transferable == true) {
						return callback(undefined, aco);
					} else {
						return callback();
					}
				}
				
				if (!core.isUnique(aco) && typeof prevValues[ szFull ] !== "undefined") {
					if (prevValues[ szFull ]) {
						return callback(undefined, aco);
					} else {
						return callback();
					}
				}
				
				core.debug("Checking " + aco.szName + "...", aco.oid);
				
				// Remove attuned items.
				core.canTransfer({aco: aco, cancellation: cancellation}, onCan);
				
				function onCan(err, can) {
					core.debug("<onCan>", err, can, aco.oid);
					if (err) {
						if (err.message == "ASSESS_TIMED_OUT") return callback();
						return callback(err);
					}
					if (cancellation && cancellation.canceled) return callback(new core.Error("CANCELLATION"));
					
					if (!core.isUnique(aco)) {
						prevValues[ szFull ] = can;
					}
					
					if (can) {
						return callback(undefined, aco);
					} else {
						return callback();
					}
				}
			}
		};
	})();
	
	core.getCommonItems = function getCommonItems(params, callback) {
		core.debug("<getTransferableItems>");
		//var timeout = params && params.timeout || FIVE_MINUTES;
		//var progressMethod = params && params.progressMethod;
		//var inventoryInfo = params && params.inventoryInfo || {};

		var inv = core.getInventoryItems();
		//async_js.concatSeries(inv, each, resolve);
		return inv.filter(function(aco) {
			if (aco.oai && aco.oai.ibi && aco.oai.ibi.fAttuned) {
				return false;
			}
			
			return !core.isUnique(aco);
		});
	};
	
	
	
	/*
	 * getItemsForSale - Get a list of items in our inventory for sale.
	 *
	 * @returns {array} List of items.
	 */
	core.getItemsForSale = function getItemsForSale(/*params*/) {
		core.debug("<getItemsForSale>");
		var inv = core.getInventoryItems();
		
		if (config.showDuplicateUniqueItems != true) {
			inv = core.removeUniqueDuplicates({items: inv});
		}
		
		var path = core.getPricesPath();
		var values = core.getJsonFile({path: path}) || {};
		core.debug("inv: " + inv.length);
		return inv.filter(function(aco) {
			if (core.isUnique(aco)) {
				if (values.unique && values.unique[aco.oid] && values.unique[aco.oid].enabled == true) {
					return true;
				}
			} else {
				var szFull = core.getFullName(aco);
				if (values.common && values.common[szFull] && values.common[szFull].enabled == true) {
					return true;
				}
			}
			
			if (aco && aco.mcm == mcmSalvageBag) {
				var value = core.getSalvageAutoValue({material: aco.material, workmanship: aco.workmanship});
				if (value) {
					return true;
				}
			}

			var lpPrice = core.getItemProfilePrice({aco: aco});
			if (lpPrice) {
				return true;
			}
			return false;
		});
	};

	core.removeUniqueDuplicates = function removeUniqueDuplicates(params) {
		var items = params.items;
		var newItems = Array.prototype.slice.call(items);
		var aco;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = newItems.length - 1; i >= 0; i--) {
			aco = newItems[i];
			if (!core.isUnique(aco)) continue;
			
			if (aco.szName != "Aetheria") continue;

			var rest = newItems.slice(0, i);
			var has = core.hasUniqueDuplicate({item: aco, items: rest});
			if (has) {
				newItems.splice(i, 1);
			}
		}
		
		return newItems;
	};
	
	
	core.hasUniqueDuplicate = function hasUniqueDuplicate(params) {
		var items = params.items;
		var item = params.item;
		
		var itemSet = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_ITEMSET);
		var itemSpells = core.getAcoSpells({aco: item});
		var itemTotalXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_TOTAL_XP);
		var itemBaseXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_BASE_XP);
		
		return items.some(function(aco) {
			if (aco.oid == item.oid) return false;
			if (aco.szName != item.szName) return false;
			if (aco.icon != item.icon) return false;
			if (aco.iconOverlay != item.iconOverlay) return false;
			if (aco.iconUnderlay != item.iconUnderlay) return false;
			
			var acoSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
			if (itemSet != acoSet) return false;
			
			var totalXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_TOTAL_XP);
			if (itemTotalXp != totalXp) return false;
			
			var baseXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_BASE_XP);
			if (itemBaseXp != baseXp) return false;

			var acoSpells = core.getAcoSpells({aco: aco});

			if (itemSpells.toString() != acoSpells.toString()) return false;

			//core.info("aco: " + aco.szName, "spells: " + acoSpells.join());

			return true;
		});
	};
	

	core.getUniqueDuplicates = function getUniqueDuplicates(params) {
		var items = params.items;
		var item = params.item;
		
		var itemSet = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_ITEMSET);
		var itemSpells = core.getAcoSpells({aco: item});
		
		var itemTotalXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_TOTAL_XP);
		var itemBaseXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_BASE_XP);
		
		return items.filter(function(aco) {
			if (aco.oid == item.oid) return false;
			if (aco.szName != item.szName) return false;
			if (aco.icon != item.icon) return false;
			if (aco.iconOverlay != item.iconOverlay) return false;
			if (aco.iconUnderlay != item.iconUnderlay) return false;
			
			var acoSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
			if (itemSet != acoSet) return false;
			
			var totalXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_TOTAL_XP);
			if (itemTotalXp != totalXp) return false;
			
			var baseXp = item.oai && item.oai.ibi && item.oai.ibi.raw.Item(RAW_BASE_XP);
			if (itemBaseXp != baseXp) return false;

			var acoSpells = core.getAcoSpells({aco: aco});

			if (itemSpells.toString() != acoSpells.toString()) return false;

			core.info("aco: " + aco.szName, "spells: " + acoSpells.join());

			return true;
		});
	};

	/*
	 * isUnique - Determin if a item is unique. (like loot generated gear).
	 *
	 * @param {object} aco - Item to check.
	 * @returns {boolean} true/false.
	 */
	core.isUnique = function isUnique(aco) {
		if (aco.szName == "Foolproof") {
			return false;
		}
		
		if (aco && aco.material > 0) {	
			return true;
		}

		if (aco && aco.workmanship > 0) {
			return true;
		}

		if (core.isMatchingObjectKeys({text: aco.szName, object: config.uniqueItems})) {
			return true;
		}
		
		
		return false;
	};

	/*
	 * flattenAco - Clone a aco's properties and return a flattened object.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item to clone.
	 * @returns {object} flattned object.
	 */
	core.flattenAco = function flattenAco(params) {
		var aco = params.aco;
		
		// Object was already flattened. I'd rename this to _flattened but LP3 already uses _cloned.
		if (aco._cloned) return aco;
		if (aco.time) return aco;
		
		var o = {};
		o.time              = new Date().getTime();
		o.oid				= aco.oid;
		o.szName			= aco.szName;
		o.oty				= aco.oty;
		o.ocm				= aco.ocm;
		o.olc				= aco.olc;
		o.mcm				= aco.mcm;
		o.eqm				= aco.eqm;
		o.cpyValue			= aco.cpyValue;
		o.material			= aco.material;
		o.workmanship		= aco.workmanship;
		o.citemStack		= aco.citemStack;
		o.citemMaxStack		= aco.citemMaxStack;
		o.cuseLeft			= aco.cuseLeft;
		o.cuseMax			= aco.cuseMax;
		o.burden			= aco.burden;
		o.iitem				= aco.iitem;
		o.eqmWearer			= aco.eqmWearer;
		o.spellid			= aco.spellid;
		o.citemMax			= aco.citemMax;
		o.citemContents		= aco.citemContents;
		o.icon				= aco.icon;
		o.iconOverlay		= aco.iconOverlay;
		o.iconUnderlay		= aco.iconUnderlay;
		
		
		if (aco.oai != null) {// Check assessed info.
			o["oai.time"] = new Date(new Date().getTime() - aco.oai.cmsecSinceAssess).getTime();

			if (aco.oai.cbi != null) { 	// Creature Basic Information 
				o["oai.cbi.species"]    = aco.oai.cbi.species;
				o["oai.cbi.lvl"]        = aco.oai.cbi.lvl;
				o["oai.cbi.healthCur"]  = aco.oai.cbi.healthCur;
				o["oai.cbi.healthMax"]  = aco.oai.cbi.healthMax;
			}
			
			if (aco.oai.cai != null) {	// Creature Attribute Information
				o["oai.cai.strength"] = aco.oai.cai.strength;
				o["oai.cai.endurance"] = aco.oai.cai.endurance;
				o["oai.cai.quickness"] = aco.oai.cai.quickness;
				o["oai.cai.coordination"] = aco.oai.cai.coordination;
				o["oai.cai.focus"] = aco.oai.cai.focus;
				o["oai.cai.self"] = aco.oai.cai.self;
				o["oai.cai.staminaCur"] = aco.oai.cai.staminaCur;
				o["oai.cai.staminaMax"] = aco.oai.cai.staminaMax;
				o["oai.cai.manaCur"] = aco.oai.cai.manaCur;
				o["oai.cai.manaMax"] = aco.oai.cai.manaMax;
			}	
			
			if (aco.oai.ibi != null) {	//  Item Basic Information 
				o["oai.ibi.citemSalvaged"] = aco.oai.ibi.citemSalvaged;
				o["oai.ibi.szDescSimple"] = aco.oai.ibi.szDescSimple;
				o["oai.ibi.szDescDetailed"] = aco.oai.ibi.szDescDetailed;
				o["oai.ibi.szComment"] = aco.oai.ibi.szComment;
				o["oai.ibi.szInscription"] = aco.oai.ibi.szInscription;
				o["oai.ibi.szInscriber"] = aco.oai.ibi.szInscriber;
				o["oai.ibi.cpageTotal"] = aco.oai.ibi.cpageTotal;
				o["oai.ibi.cpageUsed"] = aco.oai.ibi.cpageUsed;
				
				o["oai.ibi.manaCur"] = aco.oai.ibi.manaCur;
				o["oai.ibi.fractEfficiency"] = aco.oai.ibi.fractEfficiency;
				o["oai.ibi.probDestroy"] = aco.oai.ibi.probDestroy;
				o["oai.ibi.skidWieldReq"] = aco.oai.ibi.skidWieldReq;
				o["oai.ibi.lvlWieldReq"] = aco.oai.ibi.lvlWieldReq;
				o["oai.ibi.fractManaConvMod"] = aco.oai.ibi.fractManaConvMod;
				o["oai.ibi.vitalRestored"] = aco.oai.ibi.vitalRestored;
				o["oai.ibi.dlvlRestored"] = aco.oai.ibi.dlvlRestored;
				o["oai.ibi.fractRestorationBonus"] = aco.oai.ibi.fractRestorationBonus;
				o["oai.ibi.manaCost"] = aco.oai.ibi.manaCost;
				o["oai.ibi.ctink"] = aco.oai.ibi.ctink;
				o["oai.ibi.szTinkerer"] = aco.oai.ibi.szTinkerer;
				o["oai.ibi.szImbuer"] = aco.oai.ibi.szImbuer;
				o["oai.ibi.szCreator"] = aco.oai.ibi.szCreator;
				o["oai.ibi.ckeyOnRing"] = aco.oai.ibi.ckeyOnRing;
				o["oai.ibi.fAttuned"] = aco.oai.ibi.fAttuned;
				o["oai.ibi.fBonded"] = aco.oai.ibi.fBonded;
				o["oai.ibi.fRetained"] = aco.oai.ibi.fRetained;
				o["oai.ibi.fDropOnDeath"] = aco.oai.ibi.fDropOnDeath;
				o["oai.ibi.fDestroyOnDeath"] = aco.oai.ibi.fDestroyOnDeath;
				o["oai.ibi.fUnenchantable"] = aco.oai.ibi.fUnenchantable;
				o["oai.ibi.fSellable"] = aco.oai.ibi.fSellable;
				o["oai.ibi.fIvoryable"] = aco.oai.ibi.fIvoryable;
				o["oai.ibi.fDyeable"] = aco.oai.ibi.fDyeable;
				o["oai.ibi.fUnlimitedUses"] = aco.oai.ibi.fUnlimitedUses;
				o["oai.ibi.rareid"] = aco.oai.ibi.rareid;

				if (aco.oai.ibi.raw != null) {
					var raw = core.dictionaryToObject(aco.oai.ibi.raw);
					Object.keys(raw).forEach(function(key) {
						// eslint-disable-next-line newline-per-chained-call, no-magic-numbers
						o["oai.ibi.raw." + Number(key).toString(16).toUpperCase()] = raw[key];
					});
				}
			}

			if (aco.oai.iai != null) {	// Item Armor Information
			
				o["oai.iai.al"] = aco.oai.iai.al;
				o["oai.iai.protSlashing"] = aco.oai.iai.protSlashing;
				o["oai.iai.protPiercing"] = aco.oai.iai.protPiercing;
				o["oai.iai.protBludgeoning"] = aco.oai.iai.protBludgeoning;
				o["oai.iai.protFire"] = aco.oai.iai.protFire;
				o["oai.iai.protAcid"] = aco.oai.iai.protAcid;
				o["oai.iai.protCold"] = aco.oai.iai.protCold;
				o["oai.iai.protElectrical"] = aco.oai.iai.protElectrical;
			}
			
			if (aco.oai.iwi != null) {	//  Item Weapon Information
				o["oai.iwi.dmty"] = aco.oai.iwi.dmty;
				o["oai.iwi.speed"] = aco.oai.iwi.speed;
				o["oai.iwi.skid"] = aco.oai.iwi.skid;
				o["oai.iwi.dhealth"] = aco.oai.iwi.dhealth;
				o["oai.iwi.scaleDamageRange"] = aco.oai.iwi.scaleDamageRange;
				o["oai.iwi.scaleDamageBonus"] = aco.oai.iwi.scaleDamageBonus;
				o["oai.iwi.scaleDefenseBonus"] = aco.oai.iwi.scaleDefenseBonus;
				o["oai.iwi.scaleMissileDBonus"] = aco.oai.iwi.scaleMissileDBonus;
				o["oai.iwi.scaleMagicDBonus"] = aco.oai.iwi.scaleMagicDBonus;
				o["oai.iwi.scaleAttackBonus"] = aco.oai.iwi.scaleAttackBonus;
				o["oai.iwi.scalePvMElemBonus"] = aco.oai.iwi.scalePvMElemBonus;
				o["oai.iwi.scalePvPElemBonus"] = aco.oai.iwi.scalePvPElemBonus;
				o["oai.iwi.dhealthElemBonus"] = aco.oai.iwi.dhealthElemBonus;
				o["oai.iwi.dwHighlights"] = aco.oai.iwi.dwHighlights;
				o["oai.iwi.distRange"] = aco.oai.iwi.distRange;
				o["oai.iwi.vLaunch"] = aco.oai.iwi.vLaunch;
				o["oai.iwi.fCriticalStrike"] = aco.oai.iwi.fCriticalStrike;
				o["oai.iwi.fCripplingBlow"] = aco.oai.iwi.fCripplingBlow;
				o["oai.iwi.fArmorRending"] = aco.oai.iwi.fArmorRending;
				o["oai.iwi.fSlashRending"] = aco.oai.iwi.fSlashRending;
				o["oai.iwi.fPierceRending"] = aco.oai.iwi.fPierceRending;
				o["oai.iwi.fBludgeonRending"] = aco.oai.iwi.fBludgeonRending;
				o["oai.iwi.fAcidRending"] = aco.oai.iwi.fAcidRending;
				o["oai.iwi.fColdRending"] = aco.oai.iwi.fColdRending;
				o["oai.iwi.fFireRending"] = aco.oai.iwi.fFireRending;
				o["oai.iwi.fLightningRending"] = aco.oai.iwi.fLightningRending;
				o["oai.iwi.fPhantasmal"] = aco.oai.iwi.fPhantasmal;
			}
			
			if (aco.oai.iei != null) {	//  Item Enchantment Information
				o["oai.iei.difficulty"] = aco.oai.iei.difficulty;
				o["oai.iei.spellcraft"] = aco.oai.iei.spellcraft;
				o["oai.iei.manaCur"] = aco.oai.iei.manaCur;
				o["oai.iei.manaMax"] = aco.oai.iei.manaMax;
				o["oai.iei.csecPerMana"] = aco.oai.iei.csecPerMana;
				o["oai.iei.fractEfficiency"] = aco.oai.iei.fractEfficiency;
				o["oai.iei.szRaceReq"] = aco.oai.iei.szRaceReq;
				o["oai.iei.skidReq"] = aco.oai.iei.skidReq;
				o["oai.iei.sklvlReq"] = aco.oai.iei.sklvlReq;
				o["oai.iei.rankReq"] = aco.oai.iei.rankReq;
				o["oai.iei.cspellid"] = aco.oai.iei.cspellid;

				if (aco.oai.iei.cspellid > 0) {
					o["oai.iei._cspells"] = core.getItemSpells(aco).map(function(spell) {
						return spell.spellid;
					});
					o["oai.iei._cspells"].sort();
				}
			}
			
			if (aco.oai.ipi != null) {	//  Item Portal Information
				o["oai.ipi.lvlMin"] = aco.oai.ipi.lvlMin;
				o["oai.ipi.lvlMax"] = aco.oai.ipi.lvlMax;
				o["oai.ipi.szDest"] = aco.oai.ipi.szDest;
				o["oai.ipi.flags"] = aco.oai.ipi.flags;
			}	
		}

		return o;
	};
	
	/*
	 * getTotalCostOfItems - Determin the selling value of a list of items. (Items we're selling)
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.items - List of items.
	 * @returns {number} Cost in points.
	 */
	core.getTotalCostOfItems = function getTotalCostOfItems(params) {
		core.debug("<getTotalCostOfItems>");
		var items = params.items;
		var szWho = params.szWho;
		var path = core.getPricesPath();
		var values = params.values || core.getJsonFile({path: path}) || {};
		return core.round(items.reduce(function(accumulator, aco) {
			var value = core.getSellValue({
				aco     : aco, 
				values  : values, 
				szPlayer: szWho
			}) || 0;
			
			return accumulator + value * aco.citemStack;
		}, 0), config.valueRounding);
	};
	
	/*
	 * getTotalSellPointsOfItems - Determin the point value of a list of items. (Items we'll accept as payment)
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.items - List of items.
	 * @returns {number} Cost in points.
	 */
	core.getTotalSellPointsOfItems = function factory() {
		return function getTotalSellPointsOfItems(params) {
			var logger = (!params || params.logger === undefined ? core.debug : params.logger);
			var items = params.items;
			var path = core.getPointsPath();
			var points = core.getJsonFile({path: path}) || {};
			return core.round(items.reduce(function _addItemValue(accumulator, aco) {
				var szFull = core.getFullName(aco);
				var value = 0;
				if (points[szFull] && points[szFull].enabled == true) {
					value = points[szFull].value * aco.citemStack;
					if (aco.cuseLeft < aco.cuseMax) {
						if (config.allowPartiallyUsedItemsAsPayment == true) {
							var useRatio = aco.cuseLeft / aco.cuseMax;
							logger("cuseLeft: " + aco.cuseLeft, "cuseMax: " + aco.cuseMax, useRatio);
							value *= useRatio;
						} else {
							value = 0;
						}
					}
				}
				return accumulator + value;
			}, 0), config.valueRounding);
		};
	}();

	core.waitingForTradeAccept = false;

	/*
	 * acceptTrade - Accept a trade and wait for partner to accept, reset or timeout.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will assessable via 'this'.
	 * @param {function} callback -  Function to be called when finished.
	 */
	 /*
	core.acceptTrade = (function() {
		return function acceptTrade(params, callback) {
			core.debug("<acceptTrade>");
			var timeout = params && params.timeout || FIFTHTEEN_SECONDS;

			function resolve() {
				clearTimeout(tid);
				core.removeHandler(evidNil, handler);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
				core.waitingForTradeAccept = false;
			}
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			
			var handler = {};
			handler.OnTipMessage = function OnTipMessage(szMsg) {
				if (szMsg.match(/Trade Complete!/)) {
					resolve(undefined, true);
				}
			};
			core.addHandler(evidOnTipMessage, handler);

			handler.OnTradeReset = function OnTradeReset(aco) {
				if (aco != skapi.acoChar) {
					// reset seems to fire before Trade Complete message when the trade was accepted. So wait 100ms before resolving as a canceled trade.
					clearTimeout(tid);
					tid = core.setTimeout(resolve, HALF_SECOND, new core.Error("OTHER_CANCELED_TRADE"));
				}
			};
			core.addHandler(evidOnTradeReset, handler);
			
			core.waitingForTradeAccept = true;
			skapi.TradeAccept();
		};
	})();
	*/
	
	/*
	 * setAccount - Set a player account with an amount of points.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.szPlayer - Player name.
	 * @param {number} params.amount - Amount to credit.
	 * @returns {number} Player's current balance.
	 */
	core.setAccount = function setAccount(params) {
		var szPlayer = params.szPlayer;
		var balance = params.balance;
		
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\accounts\\" + szPlayer + ".json";

		var data = {
			balance: balance
		};
		
		core.saveJsonFile({
			path: path,
			data: data
		});
		return data.balance;
	};
	
	/*
	 * creditAccount - Credit a player account an amount of points.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.szPlayer - Player name.
	 * @param {number} params.amount - Amount to credit.
	 * @returns {number} Player's current balance.
	 */
	core.creditAccount = function creditAccount(params) {
		var szPlayer = params.szPlayer;
		var amount = params.amount;
		
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\accounts\\" + szPlayer + ".json";
		var data = core.getJsonFile({path: path}) || {balance: 0};
		data.balance += amount;

		data.balance = core.round(Math.max(data.balance, 0), config.valueRounding);
		
		core.saveJsonFile({
			path: path,
			data: data
		});
		return data.balance;
	};
	
	/*
	 * debitAccount - Debit a player account an amount of points.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.szPlayer - Player name.
	 * @param {number} params.amount - Amount to debit.
	 * @returns {number} Player's current balance.
	 */
	core.debitAccount = function debitAccount(params) {
		var szPlayer = params.szPlayer;
		var amount = params.amount;
		
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\accounts\\" + szPlayer + ".json";
		var data = core.getJsonFile({path: path}) || {balance: 0};
		data.balance -= amount;

		data.balance = core.round(Math.max(data.balance, 0), config.valueRounding);
		
		core.saveJsonFile({
			path: path,
			data: data
		});
		return data.balance;
	};
	
	/*
	 * getAccountBalance - Get a player's current balance.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.szPlayer - Player name.
	 * @returns {number} Player's current balance.
	 * @returns {number} Player's current balance.
	 */
	core.getAccountBalance = function getAccountBalance(params) {
		var szPlayer = params.szPlayer;
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\accounts\\" + szPlayer + ".json";
		var data = core.getJsonFile({path: path});
		return data && core.round(Math.max(data.balance, 0), config.valueRounding);
	};
	
	/*
	 * getWorkmanship - Gets a items workmanship if aco.workmanship is invalid. GDLe shows some aco.workmanship as being Infinity, so assess and get the raw value.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.thisArg: 'this' object for callback.
	 * @param {number} params.aco - Item aco.
	 */
	// GDLe shows some aco.workmanship as being Infinity, so assess and get the raw value.
	core.getWorkmanship = (function() {
		function onAssess() {
			//core.console("<getWorkmanship|onAssess>", this.aco, results);
			var aco = this.aco;
			if (aco.oai && aco.oai.ibi) {
				this.callback.call(this.thisArg, undefined, aco.oai.ibi.raw.Item(RAW_WORKMANSHIP));
			} else {
				this.callback.call(this.thisArg);
			}
		}
		return function getWorkmanship(params, callback) {
			if (typeof callback !== "function" && typeof Promise !== "undefined") {
				return new Promise(function(resolve, reject) {
					getWorkmanship(params, function promise_callback(err, results) {
						if (err) return reject(err);
						resolve(results);
					});
				});
			}

			
			var aco = params.aco;
			
			//core.console("<getWorkmanship>", aco && aco.szName);
			//core.console(" aco.workmanship: ", aco && aco.workmanship);
			//core.console(" typeof " + (typeof aco.workmanship));
			
			if (aco.workmanship && isFinite(aco.workmanship)) {
				//core.console(" Returning aco.workmanship");
				return core.setImmediate(core.applyMethod, params && params.thisArg, callback, [undefined, aco.workmanship]);
			}

			// Check for flattened object.
			if (typeof aco["oai.ibi.raw.65641"] !== "undefined") {
				var rawWork = aco["oai.ibi.raw.65641"];
				return core.setImmediate(core.applyMethod, params && params.thisArg, callback, [undefined, rawWork]);
			}
			
			
			var thisArg = {
				callback: callback, 
				aco     : aco, 
				thisArg : params.thisArg
			};
		
			if (aco.oai || aco._cloned) {
				//core.console(" item already assessed.");
				onAssess.call(thisArg);
			} else {
				//core.console(" Assessing " + aco.szName + "...");
				core.assessAco({
					aco    : aco, 
					thisArg: thisArg
				}, onAssess);
			}
		};
	})();
	
	/*
	 * getWieldSkill - Get the skill ID requirment for a item.
	 *
	 * @param {number} aco - Item
	 * @returns {number} Skill id (skid)
	 */
	core.getWieldSkill = function getWieldSkill(aco) {
		var acoValue;
		if (aco.oai) {
			if (aco.oai.iwi) {
				acoValue = aco.oai.iwi.skid;
			}
			
			if (aco.oai.ibi && aco.oai.ibi.skidWieldReq && aco.oai.ibi.skidWieldReq > 0) { // Has an actual wield req, go with that instead.
				acoValue = aco.oai.ibi.skidWieldReq;
			}
		}
		return acoValue;
	};

	var spellStats = {};
	spellStats["Legendary Blood Thirst"] = 10;
	spellStats["Epic Blood Thirst"] = 7;
	spellStats["Major Blood Thirst"] = 4;
	spellStats["Minor Blood Thirst"] = 2;

	/*
	 * getDamageMax - Return the max damage value of a item including minor-legondary spells.
	 *
	 * @param {number} aco - AC Object
	 * @returns {string} Damage type.
	 */
	core.getDamageMax = function factory() {
		function _getSpellBonus(accumulator, spell) {
			if (typeof spellStats[spell.szName] !== "undefined") {
				return accumulator + spellStats[spell.szName];
			}
		}
		return function getDamageMax(aco) {
			core.debug("<getDamageMax> " + aco);
			if (aco.oai && aco.oai.iwi) {
				var value = aco.oai.iwi.dhealth;
				if (aco.oai.iei) {
					value += core.getItemSpells(aco).reduce(_getSpellBonus, 0);
				}
				return value;
			}
		};
	}();

	/*
	 * getDamageType - Return a human readable name for a given damage type mask.
	 *
	 * @param {number} dmty - Weapon's damage value
	 * @returns {string} Damage type.
	 */
	core.getDamageType = function getDamageType(dmty) {
		if (dmty & dmtySlashing && dmty & dmtyPiercing) return "Slash/Pierce";
		if (dmty & dmtySlashing) return "Slash";
		if (dmty & dmtyPiercing) return "Pierce";
		if (dmty & dmtyBludgeoning) return "Bludge";
		if (dmty & dmtyCold) return "Cold";
		if (dmty & dmtyFire) return "Fire";
		if (dmty & dmtyAcid) return "Acid";
		if (dmty & dmtyElectrical) return "Electrical";
		if (dmty & dmtyNether) return "Nether";
		return dmty;
	};
	
	core.getWorkmanshipSync = function() {
		return function getWorkmanshipSync(params) {
			var aco = params.aco;
			if (aco.workmanship && isFinite(aco.workmanship)) {
				return aco.workmanship;
			}
			if (typeof aco["oai.ibi.raw.65641"] !== "undefined") {
				return aco["oai.ibi.raw.65641"];
			}
			if (aco.oai && aco.oai.ibi) {
				return aco.oai.ibi.raw.Item(RAW_WORKMANSHIP);
			}
		};
	}();

	core.getObjectValue = function getObjectValue(target, path, def) {
		if (typeof target[path] !== "undefined") {
			return target[path];
		}
		
		var segs = path.split(".");
		var len = segs.length;
		var idx = 0;
		
		// eslint-disable-next-line no-unused-vars
		var value;
		do {
			var prop = segs[idx];
			if (typeof prop === 'number') {
				prop = String(prop);
			}
			
			try {
				// eslint-disable-next-line no-unused-vars
				value = target[prop];
			} catch (e) {
				break;
			}

			if (typeof prop === "undefined") break;
			
			target = target[prop];
		} while (++idx < len && typeof target !== "undefined");
		
		if (idx === len) {
			return target;
		}
		
		return def;
	};
	
	core.getItemStringSync = function() {
		function compare_spells(a, b) {
			if (a.diff == b.diff) {
				if (a.szName < b.szName) return 1;
				if (a.szName > b.szName) return -1;
			} else {
				if (a.diff < b.diff) return 1;
				if (a.diff > b.diff) return -1;
			}
			return 0;
		}

		function _usefulSpell(spell) {
			if (spell.szName.match(/(Minor|Major|Epic|Legendary|Moderate|Surge|Prodigal)/)) {
				return true;
			}
			if (spell.szDesc.match(/Additional spells can be layered over this./)) {
				return true;
			}
			return false;
			
		}
		
		function _idToSpell(spellid) {
			return skapi.SpellInfoFromSpellid(spellid);
		}

		return function getItemStringSync(params) {
			var aco = params.aco;
			var s = core.getFullName(aco);
			
			if (aco.eqm & EQM_AETHERIA_BLUE) {
				s += " (Blue)";
			} else if (aco.eqm & EQM_AETHERIA_YELLOW) {
				s += " (Yellow)";
			} else if (aco.eqm & EQM_AETHERIA_RED) {
				s += " (Red)";
			}

			var v;
			
			var single = false;
			if (typeof params.single !== "undefined") {
				single = params.single;
			}

			if (aco.citemStack > 1 && single == false) {
				s += " (" + aco.citemStack + ")";
			}

			s += " :";
			
			var workmanship = core.getWorkmanshipSync({aco: aco});
			if (workmanship > 0) {
				s += " w" + core.round(workmanship, 2);
			}

			var imbuneValue = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_IMBUED);
			if (imbuneValue == REND_SLASH) {
				s += " SlashR";
			} else if (imbuneValue == REND_PIERCING) {
				s += " PierceR";
			} else if (imbuneValue == REND_BLUDGEONING) {
				s += " BludgeR";
			} else if (imbuneValue == REND_ACID) {
				s += " AcidR";
			} else if (imbuneValue == REND_COLD) {
				s += " ColdR";
			} else if (imbuneValue == REND_ELECTRICAL) {
				s += " ElectricalR";
			} else if (imbuneValue == REND_FIRE) {
				s += " FireR";
			}
			
			// TODO, other imbune types.

			var al = core.getObjectValue(aco, "oai.iai.al", 0);

			if (al) {
				s += " AL" + al;
				var protAL = 0;
				protAL += al * (core.getObjectValue(aco, "oai.iai.protSlashing", 0));
				protAL += al * (core.getObjectValue(aco, "oai.iai.protPiercing", 0));
				protAL += al * (core.getObjectValue(aco, "oai.iai.protBludgeoning", 0));
				protAL += al * (core.getObjectValue(aco, "oai.iai.protFire", 0));
				protAL += al * (core.getObjectValue(aco, "oai.iai.protCold", 0));
				protAL += al * (core.getObjectValue(aco, "oai.iai.protAcid", 0));
				protAL += al * (core.getObjectValue(aco, "oai.iai.protElectrical", 0));
				// eslint-disable-next-line no-magic-numbers
				protAL = protAL / 7;
				s += " [" + core.round(protAL) + "]";
			}

			var skill = core.getWieldSkill(aco);

			if (skill) {
				if (skill == skidMissileWeapons) {
					var scaleDamageBonus = core.getObjectValue(aco, "oai.iwi.scaleDamageBonus", 0);
					v = core.round((scaleDamageBonus - 1) * 100, ROUND_STATS_TO);
					s += " M:" + v + "%";
				} else {
					var maxDamage = core.getDamageMax(aco);
					var minDamage = maxDamage - (core.getObjectValue(aco, "oai.iwi.scaleDamageRange") * maxDamage);
					var avgDamage = (maxDamage + minDamage) / 2;
					if (core.round(avgDamage, 2) > 0) {
						s += " D:" + core.round(avgDamage, 0) + " " + core.getDamageType(core.getObjectValue(aco, "oai.iwi.dmty", 0));
					}
				}
			}
			
			var scaleAttackBonus = core.getObjectValue(aco, "oai.iwi.scaleAttackBonus", 0);
			if (core.round(scaleAttackBonus, ROUND_STATS_TO) > 1) {
				v = core.round((scaleAttackBonus - 1) * 100, ROUND_STATS_TO);
				s += " A+" + v + "%";
			}
			
			var scalePvMElemBonus = core.getObjectValue(aco, "oai.iwi.scalePvMElemBonus", 0);
			if (core.round(scalePvMElemBonus, ROUND_STATS_TO) > 1) {
				v = core.round((scalePvMElemBonus - 1) * 100, ROUND_STATS_TO);
				var dmty = core.getObjectValue(aco, "oai.iwi.dmty", 0);
				s += " " + core.getDamageType(dmty) + "+" + v + "%";
			}

			var scaleDefenseBonus = core.getObjectValue(aco, "oai.iwi.scaleDefenseBonus", 0);
			if (core.round(scaleDefenseBonus, ROUND_STATS_TO) > 1) {
				v = core.round((scaleDefenseBonus - 1) * 100, ROUND_STATS_TO);
				s += " MeD+" + v + "%";
			}

			var scaleMissileDBonus = core.getObjectValue(aco, "oai.iwi.scaleMissileDBonus", 0);
			if (core.round(scaleMissileDBonus, ROUND_STATS_TO) > 1) {
				v = core.round((scaleMissileDBonus - 1) * 100, ROUND_STATS_TO);
				s += " MiD+" + v + "%";
			}

			var fractManaConvMod = core.getObjectValue(aco, "oai.ibi.fractManaConvMod");
			if (fractManaConvMod) {
				if (core.round(fractManaConvMod, ROUND_STATS_TO) > 0) {
					v = core.round(fractManaConvMod * 100, ROUND_STATS_TO);
					s += " MaC+" + v + "%";
				}
			}

			if (aco.cpyValue > 0) {
				if (single == true) {
					s += " V:" + core.getSingleValue(aco);
				} else {
					s += " V:" + aco.cpyValue;
				}
			}
				
			var fAttuned = core.getObjectValue(aco, "oai.ibi.fAttuned");
			if (fAttuned) {
				s += " attuned";
			}
			
			var fBonded = core.getObjectValue(aco, "oai.ibi.fBonded");
			if (fBonded) {
				s += " bonded";
			}

			var lvlWieldReq = core.getObjectValue(aco, "oai.ibi.lvlWieldReq");
			var skidWieldReq = core.getObjectValue(aco, "oai.ibi.skidWieldReq", 0);
			
			if (lvlWieldReq > 0) {
				if (skidWieldReq) {
					s += " W:" + core.skidToName(skidWieldReq) + lvlWieldReq;
				} else {
					s += " LW:" + lvlWieldReq; // Level Wield
				}
			}
			
			var difficulty = core.getObjectValue(aco, "oai.iei.difficulty");
			if (difficulty) {
				s += " Lo:" + difficulty;
			}

			var spells = core.getAcoSpells({aco: aco}).map(_idToSpell)
				.filter(_usefulSpell);
			if (spells.length > 0) {
				spells.sort(compare_spells);
				var spell = spells[0];
				
				s += " [" + spell.szName;
				
				if (spells.length > 1) {
					// eslint-disable-next-line no-restricted-syntax
					for (var i = 1; i < (spells.length - 1); i++) {
						spell = spells[i];
						s += ", " + spell.szName;
					}
					spell = spells[spells.length - 1];
					s += " & " + spell.szName;
				}
				s += "]";
			}
			
			var itemSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
			if (itemSet && setNames[itemSet]) {
				s += " [" + setNames[itemSet] + " Set]";
			}
			
			// Level & XP info.
			var totalXp = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_TOTAL_XP);
			var baseXp = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_BASE_XP);
			var xpStyle = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_XP_STYLE);

			if (totalXp) {
				var level = core.getItemTotalXPToLevel({
					totalXp : totalXp,
					baseXp  : baseXp,
					maxLevel: maxLevel,
					xpStyle : xpStyle
				});

				var maxLevel = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_MAX_LEVEL);
				if (maxLevel) {
					s += " Level:" + level + "/" + maxLevel;
				}
				
				var levelTotalXP = core.getItemLevelToTotalXP({
					itemLevel: level + 1,
					baseXP   : baseXp,
					maxLevel : maxLevel,
					xpStyle  : xpStyle
				});
				
				core.debug("totalXp: " + totalXp, "levelTotalXP: " + levelTotalXP, "level: " + level, "baseXp: " + baseXp, "maxLevel: " + maxLevel, "xpStyle: " + xpStyle);
				
				// eslint-disable-next-line no-magic-numbers
				var percentage = (totalXp / levelTotalXP) * 100;
				s += " XP:" + core.round(percentage) + "%";
			}

			var ratings = [];
			
			// Rating: Damage
			var ratingDam = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_RATING_DAMAGE);
			if (ratingDam) ratings.push("Dam " + ratingDam);
			
			// Rating: Dam Resist
			var ratingDamResist = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_RATING_DAMAGE_RESIST);
			if (ratingDamResist) ratings.push("Dam Resist " + ratingDamResist);
			
			// Rating: Crit
			var ratingCrit = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_RATING_CRIT);
			if (ratingCrit) ratings.push("Crit " + ratingCrit);
			
			// Rating: Crit Resist
			var ratingCritResist = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_RATING_CRIT_RESIST);
			if (ratingCritResist) ratings.push("Crit Resist " + ratingCritResist);
			
			// Rating: Crit Dam
			var ratingCritDam = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_RATING_CRIT_DAMAGE);
			if (ratingCritDam) ratings.push("Crit Dam " + ratingCritDam);
			
			// Rating: Crit Dam Resist
			var ratingCritDamResist = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_RATING_CRIT_DAMAGE_RESIST);
			if (ratingCritDamResist) ratings.push("Crit Dam Resist " + ratingCritDamResist);
			
			if (ratings.length > 0) {
				s += " Ratings: " + ratings.join(", ");
			}
			
			return s;
		};
	}();

	core.getAcoSpells = function getAcoSpells(params) {
		var aco = params.aco;
		var spells = [];
		if (typeof aco["oai.iei.cspellid"] !== "undefined") {
			for (var key in aco)  {
				//if (!aco.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(aco, key)) continue;
				// eslint-disable-next-line no-magic-numbers
				if (key.substring(0, 18) == "oai.iei.rgspellid.") {
					// eslint-disable-next-line no-magic-numbers
					spells.push(Number(key.substring(18, key.length)));
				}
			}
		} else {
			if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid > 0) {
				// eslint-disable-next-line no-restricted-syntax
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					spells.push(aco.oai.iei.rgspellid(i));
				}
			}
		}
		return spells;
	};

	/*
	 * getItemStrings - Gets a list of item stats in string format.
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.items - List of aco objects.
	 * @param {function} callback -  Callback to receive the results.
	 */
	core.getItemStrings = (function factory() {
		function each(aco, callback) {
			/*
			core.getItemString({
				aco    : aco, 
				thisArg: {
					callback: callback
				}
			}, callback);
			*/
			callback(undefined, core.getItemStringSync({aco: aco}));
		}
		return function getItemStrings(params, callback) {
			var items = params.items;
			var timeout = params.timeout || items.length * TWO_SECONDS;

			function resolve() {
				clearTimeout(tid);
				callback.apply(params.thisArg, arguments);
			}
			
			// Timeout timer.
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			
			async_js.concatSeries(items, each, resolve);
		};
	})();

	/*
	 * OnStartPortalSelf - Event handler.
	 */
	core.OnStartPortalSelf = function OnStartPortalSelf() {
		core.trace("<OnStartPortalSelf>");
		
		if (core.isActive() && config.logoutOnPortal == true) {
			core.info("We're portaling somewhere, Logging out...");
			skapi.Logout();
		}
	};
	
	/*
	 * OnEnd3D - Event handler.
	 */
	core.OnEnd3D = function OnEnd3D() {
		core.trace("<OnEnd3D>");
		core.disable();
	};
	
	/*
	 * OnDisconnect - Event handler.
	 */
	core.OnDisconnect = function OnDisconnect() {
		core.trace("<OnDisconnect>", skapi.plig);
		core.disable();
	};
	
	/*
	 * terminate - Terminate SkunkWorks process.
	 *
	 */
	core.terminate = function terminate() {
		core.debug("<terminate>");
		if (typeof SkunkProcess === "undefined") {
			core.warn("SkunkProcess isn't loaded, cannot terminate Skunkworks process.");
			return;
		}
		var process = SkunkProcess.getProcess();
		if (process) {
			core.warn("Terminating Skunkworks...");
			process.Terminate();
		} else {
			core.warn("Could not get Skunkworks process.");
		}
	};
	
	/*
	 * nudgeItem - Move items to their current pack and slot to check if the item is real or not. Used to detect phantom items.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item.
	 * @param {number} params.timeout: Optional how long in miliseconds to wait.
	 * @param {number} params.thisArg: Optional 'this' object for callback.
	 * @param {function} callback -  Callback to receive the results.
	 */
	core.nudgeItem = (function factory() {
		function OnTipMessage(szMsg) {
			var resolve = this.resolve;
			if (szMsg.match(/That item is not valid!/)) {
				return resolve(new core.Error("INVALID_ITEM"));
			} else if (szMsg.match(/You're too busy!/)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) can't be put in the container - you're too busy/)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You can only move or use one item at a time/)) {
				return resolve(new core.Error("TOO_BUSY_ONE_AT_A_TIME"));
			} else if (szMsg.match(/ - unable to move to object/)) {
				return resolve(new core.Error("UNABLE_TO_MOVE_OBJECT"));
			} else if (szMsg.match(/Unable to move to object!/)) {
				return resolve(new core.Error("UNABLE_TO_MOVE_OBJECT"));
			}
			core.info("<nudgeItem|OnTipMessage>", "[" + szMsg + "]");
		}
		function OnMoveItem(acoItem, acoContainer, iitem) {
			core.debug("<nudgeItem|OnMoveItem>", acoItem, acoContainer, iitem);
			var resolve = this.resolve;
			var aco = this.aco;
			if (acoItem.oid == aco.oid) {
				resolve(undefined, true);
			}
		}
		return function nudgeItem(params, callback) {
			var aco = params.aco;
			var timeout = params.timeout || ONE_SECOND;
			
			//core.setTimeout(callback,1000);
			
			if (!aco || !aco.fExists) {
				// Call callback asynchronously.
				return core.setImmediate(core.applyMethod, params && params.thisArg, callback, [new core.Error("INVALID_ITEM")]);
			}

			//var cancellation = params.cancellation;// || new core.Cancellation();
			
			function resolve() {
				core.debug("<nudgeItem|resolve>");
				clearTimeout(tid);
				core.removeHandler(evidNil, handler);
				core.removeListener("onStateChange", onStateChange);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			function onStateChange(/*session*/) {
				core.debug("<nudgeItem|onStateChange>");
				resolve(new core.Error("STATE_CHANGE"));
			}
			core.on("onStateChange", onStateChange);

			// Event handler. Either we'll get OnMoveItem if it's in our pack, or a tooltip message that we can't move the item.
			var handler = {
				aco         : aco,
				resolve     : resolve,
				OnTipMessage: OnTipMessage,
				OnMoveItem  : OnMoveItem
			};
			core.addHandler(evidOnTipMessage, handler);
			core.addHandler(evidOnMoveItem, handler);
			
			// Timeout timer.
			var tid = core.setTimeout(core.timedOut, timeout, resolve);

			// Nudge the item.
			try {
				aco.MoveToContainer(aco.acoContainer, aco.iitem, false);
			} catch (e) {
				resolve(e);
			}
		};
	})();
	
	/*
	 * skidToName - Convert skill id to name.
	 *
	 * @param {object} skid - Skill ID.
	 * @returns {string} Skill name.
	 */
	core.skidToName = function skidToName(skid) {
		switch(skid) { // Handle the types we define
		case skidLightWeapons:
			return "Light";
		case skidHeavyWeapons:
			return "Heavy";
		case skidFinesseWeapons:
			return "Finesse";
		case skidTwoHanded:
			return "2H";
		case skidMissileWeapons:
			return "MissileW";
		case skidMissileDefense:
			return "MissileD";
		case skidMeleeDefense:
			return "MeleeD";
		case skidWarMagic:
			return "War";
		case skidVoidMagic:
			return "Void";
		}
		return "SkillId:" + skid + " ";
	};
	
	/*
	 * once - Function wrapper to only execute a function once.
	 *
	 * @param {function} fn - Function to wrap.
	 * @returns {function} Wrapper function.
	 */
	core.callOnce = function callOnce(fn) {
		return function onceWrapper() {
			if (fn === null) {
				core.warn("<callOnce> Function called again!");
				return;
			}
			var callFn = fn;
			fn = null;
			callFn.apply(this, arguments);
		};
	};
	
	/*
	 * applyMethod - Apply a function with a thisArg object and argument. 
	 *
	 * @param {object} thisArg - Optional object to be the 'this' object in the function.
	 * @param {function} method - Function to be called.
	 * @param {array} args - List of arguments for the function.
	 * @returns {variable} Whatever the method returns.
	 */
	core.applyMethod = function applyMethod() {
		//core.trace("<applyMethod>");
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};

	/*
	 * checkForUpdate - Check repo if there's a newer version available.
	 *
	 */
	core.checkForUpdate = (function factory() {
		function onResponse(err, res) {
			core.debug("<checkForUpdate|onResponse>", err, res);
			if (err) return core.warn("Error while checking if updates", err);
			if (res.statusCode == HTTP_STATUS_SUCCESS) {
				var data = res.json();
				if (!data || !data.length) return;
				var latestTag = data[0];
				core.debug("Latest version is " + latestTag.name);
				if (latestTag.name.match(/(\d*)\.(\d*)\.(\d*)/i)) {// 1.0.180726
					var minor = Number(RegExp.$3);
					if (minor > core.MINOR) {
						core.warn("SkunkTrader version " + latestTag.name + " is available.");
						core.warn("https://gitlab.com/Cyprias/SkunkTrader/-/releases");
					} else {
						core.console("We're running the latest version (v" + latestTag.name + ").");
					}
				}
			} else if (res.statusCode == HTTP_STATUS_FAILURE) {
				core.warn("Error while checking if updates: The attempt to connect to the server failed.");
			} else {
				core.warn("Unknown response when checking for updates, code: " + res.statusCode + " message: " + res.statusMessage);
			}
		}
		return function checkForUpdate() {
			core.debug("<checkForUpdate>");
			SkunkHTTP.get({
				url    : "https://gitlab.com/api/v4/projects/7651186/repository/tags",
				headers: {
					"Content-Type": "application/json",
					"Accept"      : "application/json,text/html"
				}
			}, onResponse);
		};
	})();

	/*

		if (typeof self.headers["Content-Type"] === "undefined") {
			self.headers["Content-Type"] = "application/json";
		}
		
		if (typeof self.headers["Accept"] === "undefined") {
			self.headers["Accept"] = "application/json,text/html";
		}
		
	*/
	
	/*
	 * getSellValue - Get the sell value of an item including default price.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item object.
	 * @returns {number} Item sell value.
	 */
	core.getSellValue = function getSellValue(params) {
		var szPlayer = params.szPlayer;
		core.debug("<getSellValue> szPlayer: " + szPlayer);
		
		var value = core.getAssignedSellValue(params);
		core.debug("value: " + value);
		
		if (typeof value === "undefined" && (isInt(config.defaultPrice) || isFloat(config.defaultPrice))) {
			value = config.defaultPrice;
		}
		
		if (value) {
			var discount = config.globalDiscount || 0;
			if (szPlayer) {
				var discountInfo = core.getPlayerDiscount({
					szPlayer: szPlayer
				});
				if (discountInfo && typeof discountInfo.amount !== "undefined" && discountInfo.amount > discount) {
					discount = discountInfo.amount;
				}
			}

			discount = Math.min(100, discount);
			
			var discountedValue = value - (value * (discount / 100));
			core.debug(szPlayer + " receives a " + discount + "% discount. value: " + value + ", discountedvalue: " + core.round(discountedValue, config.valueRounding));
			value = discountedValue;

			if (config.priceDecayEnabled == true) {
				// Price decay
				var oid = params.oid || params.aco && params.aco.oid;
				if (oid) {
					var aco = skapi.AcoFromOid(oid);
					if (aco && core.isUnique(aco)) {
						var info = core.getItemInvInfo({oid: oid});
						if (info) {
							var elapsed = new Date().getTime() - info.created;
							var days = Math.floor(elapsed / ONE_DAY);

							//core.console("value: " + value, "created: " + info.created, "elapsed: " + elapsed, "days: " + days);
							var priceBefore = value;
							
							// eslint-disable-next-line no-restricted-syntax
							for (var i = 0; i < days; i++) {
								value = value - (value * config.priceDecayPercentage);
							}
							if (priceBefore != value) {
								core.debug(core.getFullName(aco) + " is " + days + " days old, reducing price from " + core.round(priceBefore, config.valueRounding) + " to " + core.round(value, config.valueRounding) + ".");
							}
							value = core.round(value, config.valueRounding);
						}
					}
				}
			}
		}

		return value;
	};
	
	/*
	 * getAssignedSellValue - Get sell value assigned to item.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item object.
	 * @returns {number} Item sell value.
	 */
	core.getAssignedSellValue = function getAssignedSellValue(params) {
		var aco = params.aco;
		var oid = params.oid || params.aco && params.aco.oid;
		var szName = params.szName || params.aco && core.getFullName(params.aco);

		var path = core.getPricesPath();
		var values = params.values || core.getJsonFile({path: path}) || {};

		if (core.isUnique(aco)) {
			if (values.unique && oid && values.unique[oid] && values.unique[oid].enabled) {
				return values.unique && oid && values.unique[oid] && values.unique[oid].value;
			}
		} else {
			if (values.common && szName && values.common[szName] && values.common[szName].enabled) {
				return values.common && szName && values.common[szName] && values.common[szName].value;
			}
		}
		
		if (aco && aco.mcm == mcmSalvageBag) {
			return core.getSalvageAutoValue({material: aco.material, workmanship: aco.workmanship});
		}
		
		var lpPrice = core.getItemProfilePrice({aco: aco});
		
		return lpPrice;
	};
	
	/*
	 * isSellEnabled - Get whether or not a item is enabled to sell.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item object.
	 * @returns {number} Item sell value.
	 */
	core.isSellEnabled = function isSellEnabled(params) {
		var aco = params.aco;
		var oid = params.oid || params.aco && params.aco.oid;
		var szName = params.szName || params.aco && core.getFullName(params.aco);

		var path = core.getPricesPath();
		var values = params.values || core.getJsonFile({path: path}) || {};
		
		if (core.isUnique(aco)) {
			if (values.unique && oid && values.unique[oid]) {
				return values.unique && oid && values.unique[oid] && values.unique[oid].enabled;
			}
		} else {
			if (values.common && szName && values.common[szName]) {
				return values.common && szName && values.common[szName] && values.common[szName].enabled;
			}
		}
		
		if (aco && aco.mcm == mcmSalvageBag) {
			return typeof core.getSalvageAutoValue({material: aco.material, workmanship: aco.workmanship}) !== "undefined";
		}
	};
	
	/*
	 * refreshPlayersList - Refresh player config list.
	 *
	 * @returns {undefined}
	 */
	core.refreshPlayersList = function factory() {
		function _sortNames(a, b) {
			if (a.szName < b.szName) return -1;
			if (a.szName > b.szName) return 1;
			return 0;
		}
		function _processPlayer(player) {
			core.controls.lstPlayers.addRow(
				player.szName,
				player.data.supplier,
				core.round(player.data.playerDiscount || 0, 1) + "%",
				core.round(player.data.patronDiscount || 0, 1) + "%",
				core.round(player.data.monarchDiscount || 0, 1) + "%",
				(icon_textures + ICON_REMOVE) // remove
			);
		}
		return function refreshPlayersList() {
			if (!core.controls.lstPlayers) return;
			core.controls.lstPlayers.clear();

			core.controls.lstPlayers.addRow(
				"Player Name",
				false,
				"Pla D",
				"Pat D",
				"Mon D",
				(icon_textures + ICON_REMOVE)
			);
			
			var players = [];
				
			for (var key in config.players) {
				//if (!config.players.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(config.players, key)) continue;
				players.push({
					szName: key,
					data  : config.players[key]
				});
			}
				
			players.sort(_sortNames);
			players.forEach(_processPlayer);
		};
	}();


	/*
	 * refreshIgnoredList - Refresh ignored player config list.
	 *
	 * @returns {undefined}
	 */
	core.refreshIgnoredList = function factory() {
		function _sortNames(a, b) {
			if (a.szName < b.szName) return -1;
			if (a.szName > b.szName) return 1;
			return 0;
		}
		function _processPlayer(szName) {
			core.controls.lstIgnoredPlayers.addRow(
				szName,
				(icon_textures + ICON_REMOVE) // remove
			);
		}
		return function refreshIgnoredList() {
			core.debug("<refreshIgnoredList>");
			if (!core.controls.lstIgnoredPlayers) return;
			core.controls.lstIgnoredPlayers.clear();
			
			core.controls.lstIgnoredPlayers.addRow(
				"Player Name",
				(icon_textures + ICON_REMOVE)
			);
		
			var players = [];
				
			for (var key in config.ignoredPlayers) {
				//if (!config.ignoredPlayers.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(config.ignoredPlayers, key)) continue;
				players.push(key);
			}
				
			players.sort(_sortNames);

			players.forEach(_processPlayer);
		};
	}();

	/*
	 * refreshSalvageList - Refresh salvage config list.
	 */
	core.refreshSalvageList = (function factory() {
		function byName(a, b) {
			if (a.szName < b.szName) return -1;
			if (a.szName > b.szName) return 1;
			return 0;
		}
		
		// Blank workmanship value.
		var blank = {value: "?", color: "&H777777"};
		
		function _processMaterial(material, i) {
			// eslint-disable-next-line no-magic-numbers
			if (i > 0 && i % 9 == 0) {
				core.controls.lstSalvage.addRow("", "", "w1", "w2", "w3", "w4", "w5", "w6", "w7", "w8", "w9", "w10");
			}
			
			// Check if we have settings for this material.
			//var settings = config.salvageAutoValue[material.szName];
			var settings = core.salvagePricing.get(material.szName);
			if (settings) {
				var row = [];
				row.push(material.id);
				row.push(material.szName);
				
				// eslint-disable-next-line no-restricted-syntax, no-magic-numbers
				for (var w = 1; w <= 10; w++) {
					if (typeof settings[w] !== "undefined") {
						row.push({value: settings[w], color: "&HFFFFFF"});
					} else {
						row.push(blank);
					}
				}
				
				core.controls.lstSalvage.addRow.apply(core.controls.lstSalvage, row);
				return;
			}

			// No settings for material, add blank row.
			core.controls.lstSalvage.addRow(material.id, material.szName, blank, blank, blank, blank, blank, blank, blank, blank, blank, blank);
		}

		return function refreshSalvageList() {
			core.debug("<refreshSalvageList>");
			
			if (!core.controls.lstSalvage) return;
			
			// Clear list, add header.
			core.controls.lstSalvage.clear();
			core.controls.lstSalvage.addRow("", "Salvage", "w1", "w2", "w3", "w4", "w5", "w6", "w7", "w8", "w9", "w10");
		
			// Create array of material names.
			var matList = [];
			for (var key in materialNames) {
				//if (!materialNames.hasOwnProperty(key)) continue;
				if (!Object.prototype.hasOwnProperty.call(materialNames, key)) continue;
				matList.push({
					szName: materialNames[key],
					id    : key
				});
			}
			
			// Sort by name.
			matList.sort(byName);
			matList.forEach(_processMaterial);
		};
	})();
	
	/*
	 * getSalvageAutoValue - Get auto value for a material and workmanship.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.material - Salvage material id.
	 * @param {object} params.workmanship - Salvage workmanship.
	 * @returns {number} Price
	 */
	core.getSalvageAutoValue = function getSalvageAutoValue(params) {
		var material = params.material;
		var workmanship = params.workmanship || 1;
		var materialName = params.materialName || materialNames[material];
		workmanship = Math.floor(workmanship);
		
		var data = core.salvagePricing.get(materialName);
		if (data) {
			// eslint-disable-next-line no-restricted-syntax
			for (var w = workmanship; w >= 1; w--) {
				if (data[w]) {
					return data[w];
				}
			}
		}
		
		/*
		if (config.salvageAutoValue[materialName]) {
			// eslint-disable-next-line no-restricted-syntax
			for (var w = workmanship; w >= 1; w--) {
				if (config.salvageAutoValue[materialName][w]) {
					return config.salvageAutoValue[materialName][w];
				}
			}
		}*/
	};
	
	var refreshInventoryTimer;
	
	var invChangeTimer;
	var previousInventorySize = 0;
	core.OnAddToInventory = function OnAddToInventory(aco) {
		core.debug("<OnAddToInventory>", aco, (previousInventorySize != core.getInventoryItems().length));
		if (core.tradePartner) {
			core.queueOrganize = true;
		} else if (previousInventorySize != core.getInventoryItems().length) {
			previousInventorySize = core.getInventoryItems().length;
			clearTimeout(invChangeTimer);
			invChangeTimer = core.setTimeout(function() {
				if (core.tradePartner) return;
				var session = core.getTopSession();
				if (session && session.stateName == "Idle") {
					core.queueOrganize = true;
					session.droppedItems = false;
				}
			}, FIVE_SECONDS);
			
		}

		delete core.phantomItems[aco.oid];

		core.emit("onInterruptInventoryRefresh");

		core.debug("Received item(s), refreshing inventory data...");
		core.queueItemRefresh({delay: FIVE_SECONDS});
	};

	
	core.refreshingItemData = false;
	core.queueItemRefresh = function queueItemRefresh(params) {
		var delay = (!params || params.delay === undefined ? TWO_SECONDS : params.delay);

		// Cancel a previous refresh inventory timer.
		clearTimeout(refreshInventoryTimer);
		
		core.refreshingItemData = true;
		
		// Allow a short time to receive more items before refreshing inventory.
		refreshInventoryTimer = core.setTimeout(function() {

			var items = core.getInventoryItems();
			core.refreshItemData({
				items: items
			}, function onRefresh(err, results) {
				if (err) return core.error("Error while refreshing item profiles", err);
				core.debug("Finished scanning inventory, " + results.numItemsWithProfiles + " items match enabled loot profiles.");
				core.refreshingItemData = false;
			});
		}, delay);
	};

	core.OnRemoveFromInventory = function OnRemoveFromInventory(aco, acoContainer, iitem) {
		core.debug("<OnRemoveFromInventory>", aco, acoContainer, iitem);
		if (core.tradePartner) {
			core.queueOrganize = true;
		}
	};
	
	core.OnAdjustStack = function OnAdjustStack(aco, citemPrev) {
		core.debug("<OnAdjustStack>", aco, citemPrev);
		if (core.tradePartner) {
			core.queueOrganize = true;
		}
	};
	
	/*
	 * waitForWindowTimeout() Call a callback if trade window is still open after a certain amount of time.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.timeout: Miliseconds to callback.
	 * @param {function} callback(err, result): Function to callback when done.
	 */
	core.waitForWindowTimeout = (function factory() {
		return function waitForWindowTimeout(params, callback) {
			// If no callback is provided, return a Promise.
			if (typeof callback !== "function" && typeof Promise !== "undefined") {
				return new Promise(function(resolve, reject) {
					waitForWindowTimeout(params, function promise_callback(err, results) {
						if (err) return reject(err);
						resolve(results);
					});
				});
			}
			
			core.debug("<waitForWindowTimeout>");
			
			var timeout = params.timeout || config.openWindowTimeout;
			
			function resolve(result) {
				core.debug("<waitForWindowTimeout|resolve>", result);
				clearTimeout(tid);
				core.removeHandler(evidNil, handler);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			// Event handler. 
			var handler = {};
			
			// Watch for trade window to end.
			handler.OnTradeEnd = function OnTradeEnd(/*arc*/) {
				resolve(new core.Error("OnTradeEnd"));
			};
			core.addHandler(evidOnTradeEnd, handler);

			// Main timer. Returns 
			var tid = core.setTimeout(resolve, timeout, undefined, true);
		};
	})();
	
	/*
	 * loadUILayout() Load panel locations for mouse postions.
	 */
	core.layoutLoaded = false;
	core.loadUILayout = function loadUILayout() {
		var uuid = simpleUUID();
		skapi.InvokeChatParser("/saveui " + uuid);
		if (SkunkLayout.exists(uuid)) {
			SkunkLayout.loadUI(uuid);
			SkunkLayout.deleteUI(uuid);
		}
		core.warn("Loaded client UI layout, don't move panel positions.");
		core.layoutLoaded = true;
	};
	
	/*
	 * OnAssessCreature() Skunkworks event handler to catch assessing creatures/players.
	 *
	 * @param {Object} aco - AC Object
	 */
	core.OnAssessCreature = function OnAssessCreature(aco/*, fSuccess, cbi, cai, chi*/) {
		if (aco.oty & otyPlayer && aco.oai && aco.oai.chi) {
			var start = new Date().getTime();
			core.updateAllegianceInfo({
				szPlayer  : aco.szName,
				szPatron  : aco.oai.chi.szPatron,
				szMonarch : aco.oai.chi.szMonarch,
				oidMonarch: aco.oidMonarch
			});
			var elapsed = new Date().getTime() - start;
			core.debug("updateAllegianceInfo elapsed: " + elapsed);
		}
	};

	/*
	 * stripRankTitle() Strip a string of allegiance rank titles.
	 *
	 * @param {string} szName - Player name.
	 * @returns {string} Player name without rank title.
	 */
	core.stripRankTitle = (function factory() {
		var rankTitles = [
			/^Adept (.*)$/i,
			/^Admiral (.*)$/i,
			/^Aetheling (.*)$/i,
			/^Acolyte (.*)$/i,
			/^Amir (.*)$/i,
			/^Amira (.*)$/i,
			/^Amploth (.*)$/i,
			/^Annointed (.*)$/i,
			/^Archduchess (.*)$/i,
			/^Archduke (.*)$/i,
			/^Arintoth (.*)$/i,
			/^Aulia (.*)$/i,
			/^Aulin (.*)$/i,
			/^Banner (.*)$/i,
			/^Baron (.*)$/i,
			/^Baroness (.*)$/i,
			/^Baronet (.*)$/i,
			/^Captain (.*)$/i,
			/^Centurion (.*)$/i,
			/^Commander (.*)$/i,
			/^Commodore (.*)$/i,
			/^Consul (.*)$/i,
			/^Corporal (.*)$/i,
			/^Count (.*)$/i,
			/^Countess (.*)$/i,
			/^Dame (.*)$/i,
			/^Duchess (.*)$/i,
			/^Duke (.*)$/i,
			/^Dux (.*)$/i,
			/^Ealdor (.*)$/i,
			/^Ensign (.*)$/i,
			/^Esquire (.*)$/i,
			/^Extas (.*)$/i,
			/^Gigas (.*)$/i,
			/^Grand Duchess (.*)$/i,
			/^Grand Duke (.*)$/i,
			/^High King (.*)$/i,
			/^High Queen (.*)$/i,
			/^Highborn (.*)$/i,
			/^Highness (.*)$/i,
			/^Ipharsin (.*)$/i,
			/^Ipharsia (.*)$/i,
			/^Jinin (.*)$/i,
			/^Jo-Chueh (.*)$/i,
			/^Jo-Ou (.*)$/i,
			/^Kantos (.*)$/i,
			/^Kauh (.*)$/i,
			/^King (.*)$/i,
			/^Knight (.*)$/i,
			/^Kou (.*)$/i,
			/^Koutei (.*)$/i,
			/^Kun-chueh (.*)$/i,
			/^Laigus (.*)$/i,
			/^Legatus (.*)$/i,
			/^Lieutenant (.*)$/i,
			/^Lithos (.*)$/i,
			/^Malik (.*)$/i,
			/^Malika (.*)$/i,
			/^Marquis (.*)$/i,
			/^Marquise (.*)$/i,
			/^Maulan (.*)$/i,
			/^Maulana (.*)$/i,
			/^Mu'allim (.*)$/i,
			/^Mu'allima (.*)$/i,
			/^Mushir (.*)$/i,
			/^Mushira (.*)$/i,
			/^Naqib (.*)$/i,
			/^Naqiba (.*)$/i,
			/^Nan-chueh (.*)$/i,
			/^Neophyte (.*)$/i,
			/^Nuona (.*)$/i,
			/^Nurea (.*)$/i,
			/^Obeloth (.*)$/i,
			/^Ona (.*)$/i,
			/^On a (.*)$/i,
			/^Optio (.*)$/i,
			/^Ou (.*)$/i,
			/^Praefectus (.*)$/i,
			/^Primus (.*)$/i,
			/^Principes (.*)$/i,
			/^Qadi (.*)$/i,
			/^Qadiyr (.*)$/i,
			/^Queen (.*)$/i,
			/^Raigus (.*)$/i,
			/^Rea (.*)$/i,
			/^Reeve (.*)$/i,
			/^Sayyid (.*)$/i,
			/^Sayyida (.*)$/i,
			/^Secondus (.*)$/i,
			/^Shade (.*)$/i,
			/^Shayk (.*)$/i,
			/^Shayka (.*)$/i,
			/^Shi-chueh (.*)$/i,
			/^Squire (.*)$/i,
			/^Sultan (.*)$/i,
			/^Sultana (.*)$/i,
			/^Sutah (.*)$/i,
			/^Ta-chueh (.*)$/i,
			/^Tah (.*)$/i,
			/^Taikou (.*)$/i,
			/^Tenebrous (.*)$/i,
			/^Thane (.*)$/i,
			/^Tiatus (.*)$/i,
			/^Tribunus (.*)$/i,
			/^Tuona (.*)$/i,
			/^Turea (.*)$/i,
			/^Viscount (.*)$/i,
			/^Viscountess (.*)$/i,
			/^Void Lady (.*)$/i,
			/^Void Lord (.*)$/i,
			/^Void Knight (.*)$/i,
			/^Warlord (.*)$/i,
			/^Xutua (.*)$/i,
			/^Yeoman (.*)$/i
		];
		return function stripRankTitle(szName) {
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < rankTitles.length; i++) {
				if (szName.match(rankTitles[i])) {
					return RegExp.$1;
				}
			}
			return szName;
		};
	})();
	
	/*
	 * getPlayerDiscount - Return player discount based on their name and recorded allegiance info.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szPlayer - Player's name
	 * @returns {object} Object containing discount details.
	 */
	core.getPlayerDiscount = function getPlayerDiscount(params) {
		var szPlayer = params.szPlayer;
		
		var discounts = [];

		if (config.players[szPlayer]) {
			discounts.push({
				amount: config.players[szPlayer].playerDiscount,
				reason: "Player discount"
			});
			if (config.players[szPlayer].monarchDiscount) {
				discounts.push({
					amount: config.players[szPlayer].monarchDiscount,
					reason: "Monarch discount"
				});
			}
		}
		
		var start = new Date().getTime();
		var allegianceInfo = core.allegianceInfo.get(szPlayer);
		var elapsed = new Date().getTime() - start;
		core.debug("allegianceInfo elapsed: " + elapsed);
		
		if (allegianceInfo) {
			core.debug("allegianceInfo: " + allegianceInfo.szPatron + ", " + allegianceInfo.szMonarch);
			
			var szPatron = allegianceInfo.szPatron && core.stripRankTitle(allegianceInfo.szPatron);
			var szMonarch = allegianceInfo.szMonarch && core.stripRankTitle(allegianceInfo.szMonarch);

			core.debug("szPatron: " + szPatron + ", szMonarch: " + szMonarch);

			if (szPatron && config.players[szPatron]) {
				discounts.push({
					amount: config.players[szPatron].patronDiscount,
					reason: "Patron discount"
				});
			}

			if (szMonarch && config.players[szMonarch]) {
				discounts.push({
					amount: config.players[szMonarch].monarchDiscount,
					reason: "Monarch discount"
				});
			}
		}

		if (core.tradePromo) {
			discounts.push({
				amount: core.tradePromo.discount,
				reason: "Promo code"
			});
		}
		
		core.debug("discounts: " + discounts.length);

		if (discounts.length > 0) {
			discounts.sort(function(a, b) {
				return b.amount - a.amount;
			});
			core.debug("discounts: " + (config.debugToLog && JSON.stringify(discounts)));
			return discounts[0];
		}
	};

	/*
	 * refreshItemSetChoice - Populate the item set dropdown menu.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.selectedText - Text to select.
	 * @returns {undefined}
	 */
	core.refreshItemSetChoice = function refreshItemSetChoice(params) {
		var selected = 0;

		if (!core.controls.choItemSet) return;

		var choItemSet = core.controls.choItemSet;
		choItemSet.clear();
		choItemSet.addOption({text: "Select..."});
		
		var setNames = [];
		
		for (var key in config.itemSets) {
			//if (!config.itemSets.hasOwnProperty(key)) continue;
			if (!Object.prototype.hasOwnProperty.call(config.itemSets, key)) continue;
			setNames.push(key);
		}
		
		setNames.sort();

		setNames.forEach(function(value, index) {
			choItemSet.addOption({text: value});
			if (params && typeof params.selectedText !== "undefined") {
				if (params.selectedText == value) {
					selected = index + 1;
				}
			}
		});
		
		choItemSet.setSelected(selected);
	};
	
	/*
	 * refreshItemSetList - Populate the item set list.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.setName - Item set name.
	 * @returns {undefined}
	 */
	core.refreshItemSetList = function refreshItemSetList(params) {
		var setName = params.setName;

		if (!config.itemSets[setName]) return;
		var itemSet = config.itemSets[setName];
		
		itemSet.items.sort(function compare(a, b) {
			if (a.itemName < b.itemName) return -1;
			if (a.itemName > b.itemName) return 1;
			return 0;
		});
		
		var lstItemSets = core.controls.lstItemSets;
		
		lstItemSets.clear();
		
		itemSet.items.forEach(function(value/*, index*/) {
			lstItemSets.addRow(
				value.enabled, 
				value.itemName,
				value.quantity || 1,
				(icon_textures + ICON_REMOVE) // remove
			);
			
		});

		core.controls.chkRestrictIndividualSales.setChecked(itemSet.restrictIndividualSales);
	};

	/*
	 * getMatchingItemSet - Return Item Set name that a item name matches.
	 *
	 * @param {string} szItem -  Item name.
	 * @returns {string} Item set name.
	 */
	core.getMatchingItemSet = function getMatchingItemSet(szItem) {
		for (var setName in config.itemSets) {
			if (!Object.prototype.hasOwnProperty.call(config.itemSets, setName)) continue;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < config.itemSets[setName].items.length; i++) {
				if (!config.itemSets[setName].items[i].enabled) continue;
				if (config.itemSets[setName].items[i].itemName.toLowerCase() == szItem.toLowerCase()) {
					return setName;
				}
			}
		}
	};
	
	/*
	 * getEnabledItemSetItems - Return array of enabled items in a item set.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.setName -  Item set name.
	 * @returns {array} Objects of item set pieces.
	 */
	core.getEnabledItemSetItems = function getEnabledItemSetItems(params) {
		var setName = params.setName;
		return config.itemSets[setName].items.filter(function(item) {
			return item.enabled == true;
		});
	};
	
	/*
	 * getElapsedString - Convert seconds to a more human readable string.
	 *
	 * @param {number} seconds -  Seconds
	 * @param {number} precision -  How many chunks (d, h, m, s) should be returned. Default all.
	 * @returns {string} Time string.
	 */
	core.getElapsedString = function getElapsedString(seconds, precision) {
		// eslint-disable-next-line no-magic-numbers
		var numdays = Math.floor((seconds % 31536000) / 86400); 
		// eslint-disable-next-line no-magic-numbers
		var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
		// eslint-disable-next-line no-magic-numbers
		var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
		// eslint-disable-next-line no-magic-numbers
		var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;

		numseconds = Math.max(numseconds, 1);
		
		var parts = 0;
		
		var string = ""; //core.round(numseconds) + "s";

		if (numdays > 0) {
			string = numdays + "d";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}
		
		if (numhours > 0) {
			string += " " + numhours + "h";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}
		
		if (numminutes > 0) {
			string += " " + numminutes + "m";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}

		if (numseconds > 0) {
			string += " " + core.round(numseconds) + "s";
			parts++;
			if (precision && parts >= precision) return core.trimStr(string);
		}
		
		return core.trimStr(string);
	};
	
	/*
	 * setupStatusList() Refresh status list on GUI.
	 */
	core.setupStatusList = function setupStatusList() {

		function refresh() {
			var rows = [];
			
			var uptime = (new Date().getTime() - core.stats.initializeTime) / ONE_SECOND;
			var stUptime = core.getElapsedString(uptime);
			rows.push(["Uptime: " + stUptime]);
			
			var totalVisits = core.stats.totalVisits || 0;
			rows.push(["Total visits: " + totalVisits]);
			
			var unique = 0;
			for (var key in core.stats.uniqueVisis) {
				if (!Object.prototype.hasOwnProperty.call(core.stats.uniqueVisis, key)) continue;
				unique += 1;
			}
			rows.push(["Unique player visits: " + unique]);
			
			var itemsSold = core.stats.itemsSold || 0;
			rows.push(["Items sold: " + itemsSold]);
			
			var itemsReceived = core.stats.itemsReceived || 0;
			rows.push(["Items received: " + itemsReceived]);
			
			var profit = core.stats.profit || 0;
			rows.push(["Profit: " + core.round(profit, config.valueRounding) + " points"]);

			core.controls.lstStatus.setRows(rows);
		}
		refresh();
		
		// Refresh the list every 10 seconds.
		clearTimeout(statusTimer);
		statusTimer = core.setInterval(refresh, config.refreshStatusDelay);
	};
	
	/*
	 * isArray() Check if a object is a array.
	 *
	 * @param {number} arr -  Object to check.
	 * @returns {boolean}
	 */
	core.isArray = function isArray(arr) {
		return Object.prototype.toString.call(arr) == '[object Array]';
	};
	
	/*
	 * stringTemplate() Replace parts of string with values in a object.
	 *
	 * @param {string} string -  String template.
	 * @param {object} data - Keys and values to put in the string.
	 * @returns {string} Transformed string.
	 */
	core.stringTemplate = function stringTemplate(string, data) {
		var proxyRegEx = /\{\{([^\}]+)?\}\}/g;
		return string.replace(proxyRegEx, function(_, key) {
			var keyParts = key.split('.');
			var value = data;
			keyParts.forEach(function(v) {
				value = value[v];
			});
			if (typeof value !== "undefined") {
				return value;
			}
			return '';
		});
	};
	
	/*
	 * arrayTemplate - Run array of strings through stringTemplate()
	 *
	 * @param {array} arr -  Array of strings.
	 * @param {object} data - Keys and values to put in the string.
	 * @returns {array} New array of transformed string.
	 */
	core.arrayTemplate = function arrayTemplate(arr, data) {
		if (typeof data === "undefined") return arr;
		return arr.map(function(value) {
			return core.stringTemplate(value, data);
		});
	};

	/*
	 * Cancellation() Object passed between multiple async functions to indicate that we want to cancel a current process.
	 *
	 * @returns {object} Object containing 'canceled' boolean.
	 */
	core.Cancellation = (function factory() {
		function Cancellation() {
			//EventEmitter.apply(this);
			this.canceled = false;
		}
		
		/*
		Cancellation.prototype = new EventEmitter();
		Cancellation.prototype.constructor = Cancellation;
		Cancellation.prototype.cancel = function cancel(value) {
			if (this.canceled) return;
			this.canceled = true;
			this.emit("canceled", value);
			delete this.events;
			return this;
		};
		*/
		return Cancellation;
	})();
	
	core.OnMoveItem = function OnMoveItem(aco, acoContainer, iitem) {
		core.console("<OnMoveItem>", aco, acoContainer, iitem);
	};

	/*
	 * processBuyRequest - Process user's request to finialize the trade request.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szSender -  Name of the player making the request.
	 * @returns {undefined}
	 */
	core.processingBuyRequest = {};
	core.processBuyRequest = function factory() {
		function _totalStack(accumulator, aco) {
			return accumulator + aco.citemStack;
		}
		return function processBuyRequest(params) {
			core.debug("<processBuyRequest>");
			
			var szSender = params.szSender;
			var logger = (!params || params.logger === undefined ? core.debug : params.logger);
			
			var lastTradeResetTime = core.lastTradeResetTime;
			
			if (core.processingBuyRequest[szSender] == true) {
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.stillProcessing)});
				return;
			}

			var myItems = core.tradeWindow && core.tradeWindow[myTradeSide] || [];

			if (myItems.length == 0) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.nothingToBuy)});
			}
			
			core.processingBuyRequest[szSender] = true;

			var myValue = core.getTotalCostOfItems({
				items: myItems,
				szWho: szSender
			});
			
			var theirItems = core.tradeWindow && core.tradeWindow[theirTradeSide] || [];
			logger("theirItems: " + theirItems.length);
			
			return async_js.mapSeries(theirItems, function(aco, callback) {
				
				return core.getItemPointValue({aco: aco, logger: logger}, function onValue(err, value) {
					if (err) return callback(err);
					logger("their item: " + aco.szName, "value: " + value);
					return callback(undefined, {
						aco  : aco,
						value: value
					});
				});
			}, function onDone(err, results) {
				if (err) {
					core.warn("Error getting their item values", err);
					delete core.processingBuyRequest[szSender];
					return core.tell({
						szRecipient: core.tradePartner.szName, 
						szMsg      : core.arrayTemplate(_l.errorOccurred, {
							message: err.message
						})
					});
				}

				var theirValue = results.reduce(function(tally, info) {
					return tally + info.value;
				}, 0);
				
				var theirBalance = core.getAccountBalance({szPlayer: szSender}) || 0;
				var theirTotal = core.round(theirValue + theirBalance, config.valueRounding);
				
				logger("myitems: " + myItems.length + ", theiritems: " + theirItems.length);
				logger("myvalue: " + myValue + ", theirvalue: " + theirValue + ", theirBalance: " + theirBalance + ", theirTotal: " + theirTotal);

				
				var needed = core.round(Math.abs(myValue - theirTotal), config.valueRounding);

				// Amount to subtract from their account balance.
				var debit = core.round(Math.max(Math.min(theirBalance, theirBalance - needed), 0), config.valueRounding);

				// Amount to add to their account balance.
				var credit = core.round(Math.max((theirTotal - theirBalance) - myValue, 0), config.valueRounding);

				logger("needed: " + needed + ", credit: " + credit + ", debit: " + debit);
				
				// Check if they provided enough payment.
				if (myValue > theirTotal) {
					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.insufficientFunds, {
							cost    : myValue,
							provided: theirValue,
							balance : theirBalance,
							needed  : needed
						})
					});
					delete core.processingBuyRequest[szSender];
					return;
				}

				// Clone items for array.
				var myItemStrings;
				var theirItemStrings;

				core.info("Assessing items in trade window...");
				return core.getItemStrings({items: myItems}, core.callOnce(onMyStrings));
				function onMyStrings(err, results) {
					logger("<onMyStrings>", err, results);
					if (err) {
						delete core.processingBuyRequest[szSender];
						return core.tell({
							szRecipient: core.tradePartner.szName, 
							szMsg      : core.arrayTemplate(_l.errorOccurred, {
								message: err.message
							})
						});
					}
					myItemStrings = results; // Used in onAcceptTrade()
					core.getItemStrings({items: theirItems}, core.callOnce(onTheirStrings));
				}
				
				function onTheirStrings(err, results) {
					logger("<onTheirStrings>", err, results);
					if (err) {
						delete core.processingBuyRequest[szSender];
						return core.tell({
							szRecipient: core.tradePartner.szName, 
							szMsg      : core.arrayTemplate(_l.errorOccurred, {
								message: err.message
							})
						});
					}
					theirItemStrings = results; // Used in onAcceptTrade()
					logger(" myvalue: " + myValue + ", theirTotal: " + theirTotal);
						
					// They have provided more than required.
					if (theirTotal >= myValue) {
					//if (credit > 0) {
						if (credit > 0) {
							var current = core.getAccountBalance({szPlayer: szSender}) || 0;
							var afterBalance = core.round(current + credit, config.valueRounding);
							
							logger("myvalue: " + myValue + ", theirTotal: " + theirTotal + ", current: " + current + ", credit: " + credit + ", total: " + afterBalance);
							
							core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.acceptableTradeBalance, {credit: credit, total: afterBalance})});
						} else {
							core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.acceptableTrade)});
						}
						
						if (lastTradeResetTime != core.lastTradeResetTime) {
							skapi.TradeDecline();
							core.info("The window has reset before we could accept trade.");
							return core.tell({
								szRecipient: core.tradePartner.szName, 
								szMsg      : core.arrayTemplate(_l.errorOccurred, {
									message: "Please try again"
								})
							});
						}
						
						core.info("Accepting trade...");
						core.waitingForTradeAccept = true;
						partnerName = core.tradePartner.szName;
						core.acceptTrade({logger: core.debug}, core.callOnce(onAcceptTrade));
						return;
					}
				}
				
				var partnerName;
				function onAcceptTrade(err, result) {
					logger("<onAcceptTrade>", err, result);
					core.waitingForTradeAccept = false;
					delete core.processingBuyRequest[szSender];
					if (err) {
						core.warn("Error while accepting trade", err);
						skapi.TradeDecline();
						skapi.TradeReset();
						if (err.message == "TIMED_OUT") {
							return core.tell({szRecipient: partnerName, szMsg: core.arrayTemplate(_l.buyTimeout)});
						} else if (err.message == "PARTNER_NOT_ENOUGH_SPACE") {
							return core.tell({szRecipient: partnerName, szMsg: core.arrayTemplate(_l.youDontHaveEnoughSpace)});
						} else if (err.message == "PARTNER_INVALID_ITEM") {
							return core.tell({szRecipient: partnerName, szMsg: core.arrayTemplate(_l.yourItemWasInvalid)});
						} else if (err.message == "PARTNER_TOO_ENCUMBERED") {
							return core.tell({szRecipient: partnerName, szMsg: core.arrayTemplate(_l.youreTooEncumbered)});
							
						}
						return core.tell({
							szRecipient: partnerName, 
							szMsg      : core.arrayTemplate(_l.errorOccurred, {
								message: err.message
							})
						});
					}

					if (result == true) {
						var balance;// = core.getAccountBalance({szPlayer: szSender});
						
						if (debit > 0) {
							logger("Debiting them " + debit);
							balance = core.debitAccount({
								szPlayer: szSender,
								amount  : debit
							});
						}
							
						if (credit > 0) {
							logger("Crediting them " + credit);
							balance = core.creditAccount({
								szPlayer: szSender,
								amount  : credit
							});
						}
						
						balance = balance || core.getAccountBalance({szPlayer: szSender});
						
						if (typeof balance !== "undefined") {
							core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.buySuccessBalance, {balance: balance})});
							core.refreshAccountsList();
						} else {
							core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.buySuccess)});
						}
						
						core.logTransaction({
							szPlayer        : szSender,
							myItemStrings   : myItemStrings, 
							theirItemStrings: theirItemStrings,
							myTotal         : myValue,
							preBalance      : theirBalance,
							debit           : debit,
							theirTotal      : theirTotal,
							credit          : credit,
							postBalance     : balance
						});

						core.stats.itemsSold = core.stats.itemsSold || 0;
						core.stats.itemsSold += theirItems.reduce(_totalStack, 0);

						core.stats.itemsReceived = core.stats.itemsReceived || 0;
						core.stats.itemsReceived += theirItems.reduce(_totalStack, 0);

						core.stats.profit = core.stats.profit || 0;
						core.stats.profit += myValue;
						
						core.setImmediate(function() {
							skapi.TradeReset();
							core.showItemsForSale({szPlayer: szSender}, noop);
						});
					}
				}
			});
		};
	}();

	/*
	 * processSearchCommand - Process a search command. 
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szSender -  Name of the player making the request.
	 * @param {string} params.szItem -  (Partial) item name.
	 * @returns {undefined}
	 */
	core.processSearchCommand = function factory() {
		return function processSearchCommand(params) {
			var szSender = params.szSender;
			var szItem  = params.szItem;
				
			/*
			var items = core.parseStringForItems({
				string: szItem
			});
			*/
			var items = core.getItemsMatchingSearchString({
				string: szItem
			});
			
			if (items.length == 0) {
				core.info("We have no items matching " + szItem);
				return;
			}
			
			var count = items.reduce(function(tally, aco) {
				return tally += aco.citemStack;
			}, 0);
			
			core.debug("items: " + items.length, "stackCount: " + count);

			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.iHaveMatchesGlobal, {
					count : count,
					string: szItem
				})
			});
		};
	}();

	/*
	core.parseStringForItems = function factory() {
		function _trimString(str) {
			return core.trimStr(str);
		}
		function _oidToAco(oid) {
			return skapi.AcoFromOid(oid);
		}
		return function parseStringForItems(params) {
			var string = params.string;
			core.debug("<parseStringForItems>", string);

			var chunks = string.split(",").map(_trimString);
			core.debug("chunks: " + chunks.length);
			var items = core.getItemsForSale();
			core.debug("items: " + items.length);

			var chunkMatches = [];
			chunks.forEach(function(chunk) {
				var property, opperator, value, itemName;

				if (chunk.match(/(.*)>=(.*)/) || chunk.match(/(.*)=>(.*)/)) {
					property = RegExp.$1;
					opperator = ">=";
					value = RegExp.$2;
				} else if (chunk.match(/(.*)<=(.*)/) || chunk.match(/(.*)=<(.*)/)) {
					property = RegExp.$1;
					opperator = "<=";
					value = RegExp.$2;
				} else if (chunk.match(/(.*)<(.*)/)) {
					property = RegExp.$1;
					opperator = "<";
					value = RegExp.$2;
				} else if (chunk.match(/(.*)>(.*)/)) {
					property = RegExp.$1;
					opperator = ">";
					value = RegExp.$2;
				} else if (chunk.match(/(.*)(:|=)(.*)/)) {
					property = RegExp.$1;
					opperator = "=";
					value = RegExp.$3;
				} else if (chunk.match(/(.*)!=(.*)/)) {
					property = RegExp.$1;
					opperator = "!=";
					value = RegExp.$2;
				} else {
					property = "name";
					opperator = "=";
					value = chunk;
				}
				
				core.debug("property: " + property, "opperator: " + opperator, "value: " + value);

				switch(property) {
				case "name":
				case "spell":
				case "slot":
				case "skill":
				case "rend":
				case "set":
					break;
				case "workmanship":
				case "wield":
				case "value":
					value = Number(value);
					break;
				default:
					//core.console("Unknown property type: " + property);
					//return;
					itemName = property;
					property = "value";
					value = Number(value);
				} 
				
				core.debug("property 2: " + property, "opperator: " + opperator, "value: " + value, "itemName: " + itemName);

				var matches = items.filter(function(aco) {
					
					if (itemName) {
						var fullName = core.getFullName(aco);
						var regExp = new RegExp(core.escapeRegExp(itemName), "i");
						if (!fullName.match(regExp)) {
							return false;
						}
					}
					
					if (property == "name") {
						var regExp = new RegExp(core.escapeRegExp(value), "i");
						var fullName = core.getFullName(aco);
						if (opperator == "=" && fullName.match(regExp)) {
							return true;
						} else if (opperator == "!=" && !fullName.match(regExp)) {
							return true;
						}
					} else if (property == "workmanship") {
						var itemValue = core.getWorkmanshipSync({aco: aco}); //core.round(aco.workmanship, core.decimalCounter(value));
						//core.info("itemValue: " + itemValue, typeof itemValue, "value: " + value, typeof value);
						if (opperator == "=") {
							return itemValue == value;
						} else if (opperator == "!=") {
							return itemValue != value;
						} else if (opperator == ">") {
							return itemValue > value;
						} else if (opperator == ">=") {
							return itemValue >= value;
						} else if (opperator == "<") {
							return itemValue < value;
						} else if (opperator == "<=") {
							return itemValue <= value;
						}
					} else if (property == "spell") {
						var regExp = new RegExp(value, "i");
						var spells = core.getItemSpells(aco);
						return spells.some(function(spell) {
							if (opperator == "=" && spell.szName.match(regExp)) {
								return true;
							} else if (opperator == "!=" && !spell.szName.match(regExp)) {
								return true;
							}
							return false;
						});
					} else if (property == "slot") {
						var eqm = Object.keys(core.slotNames).find(function(eqm) {
							return core.slotNames[eqm].find(function(name) {
								return name.toLowerCase() == value.toLowerCase();
							});
						});
						if (eqm) {
							return aco.eqm & eqm;
						}
					} else if (property == "skill") {
						var skid = Object.keys(core.skillNames).find(function(eqm) {
							return core.skillNames[eqm].find(function(name) {
								return name.toLowerCase() == value.toLowerCase();
							});
						});
						var itemSkill;
						if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq) {
							itemSkill = aco.oai.ibi.skidWieldReq;
						} else if (aco.oai && aco.oai.iwi && aco.oai.iwi.skid) {
							itemSkill = aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						} else {
							return;
						}

						if (opperator == "=" && skid == itemSkill) {
							return true;
						} else if (opperator == "!=" && skid != itemSkill) {
							return true;
						}
					} else if (property == "wield") {
						var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq;

						if (opperator == "=") {
							return itemValue == value;
						} else if (opperator == "!=") {
							return itemValue != value;
						} else if (opperator == ">") {
							return itemValue > value;
						} else if (opperator == ">=") {
							return itemValue >= value;
						} else if (opperator == "<") {
							return itemValue < value;
						} else if (opperator == "<=") {
							return itemValue <= value;
						}
					} else if (property == "rend") {
						var rendMask = Object.keys(core.rendNames).find(function(key) {
							return core.rendNames[key].find(function(name) {
								return name.toLowerCase() == value.toLowerCase();
							});
						});
						var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_IMBUED);
						if (opperator == "=" && rendMask == itemValue) {
							return true;
						} else if (opperator == "!=" && rendMask != itemValue) {
							return true;
						}
					} else if (property == "set") {
						var itemSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
						var setName = itemSet && core.setNames[itemSet];
						if (!setName) return;
						var regExp = new RegExp(core.escapeRegExp(value), "i");
						if (opperator == "=" && setName.match(regExp)) {
							return true;
						} else if (opperator == "!=" && !setName.match(regExp)) {
							return true;
						}
					} else if (property == "value") {
						var itemValue = aco.cpyValue;
						if (opperator == "=") {
							return itemValue == value;
						} else if (opperator == "!=") {
							return itemValue != value;
						} else if (opperator == ">") {
							return itemValue > value;
						} else if (opperator == ">=") {
							return itemValue >= value;
						} else if (opperator == "<") {
							return itemValue < value;
						} else if (opperator == "<=") {
							return itemValue <= value;
						}
						
					}
					return false;
				});
				chunkMatches = chunkMatches.concat(matches);
			});

			var matchCounts = {};
			chunkMatches.forEach(function(aco) {
				matchCounts[aco.oid] = matchCounts[aco.oid] || 0;
				matchCounts[aco.oid] += 1;
			});
			
			var oids = [];
			Object.keys(matchCounts).forEach(function(oid) {
				if (matchCounts[oid] >= chunks.length) {
					//core.info("oid: " + oid, "count: " + matchCounts[oid], "chunks: " + chunks.length);
					oids.push(oid);
				}
			});

			var acos = oids.map(_oidToAco);
			acos.sort(compare_items);
			return acos;
		};
	}();
	*/

	/*
	 * processAddSpellRequest - Process a addspell request to search for items with a matching spell name.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szSender - Name of the player making the request.
	 * @param {string} params.szSpell - (Partial) spell name.
	 * @returns {undefined}
	 */
	core.processAddSpellRequest = function factory() {
		function _hasEqm(aco) {
			return aco.eqm > 0;
		}
		return function processAddSpellRequest(params) {
			var szSender = params.szSender;
			var szSpell = params.szSpell;
			var regExpSpell = new RegExp(szSpell, "i");

			// Item all items for sale.
			var items = core.getItemsForSale();
			core.debug("itemsForSale: " + items.length);
			
			// Remove items that can't be equipped.
			items = items.filter(_hasEqm);

			// Tell customer if we have no equippable items to scan.
			if (items.length == 0) {
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.noItemMatching, {name: szSpell}) 
				});
			}
			
			core.debug("Equippable items: " + items.length);
			
			// Tell customer we're scanning our items for matching spells.
			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.scanningPleaseWait, {
					count: items.length
				})
			});

			var cancellation = params.cancellation || new core.Cancellation();
			
			// Watch trade window events that indicate we should we cancel our scan.
			var handler = {};
			handler.OnTradeEnd = function OnTradeEnd(/*arc*/) {
				cancellation.canceled = true;
			};
			core.addHandler(evidOnTradeEnd, handler);
			handler.OnTradeReset = function OnTradeReset(/*aco*/) {
				cancellation.canceled = true;
			};
			core.addHandler(evidOnTradeReset, handler);
		
			// Assess each item and check spells for matching name.
			async_js.eachSeries(items, each, finished);
			
			// Function to check each item.
			function each(aco, callback) {
				if (cancellation && cancellation.canceled) return callback(new core.Error("CANCELLATION"));
				
				// Check if we need to request the item's stats from the server.
				if (aco.oai) {
					onAssess();
				} else {
					core.assessAco({aco: aco}, onAssess);
				}
				
				// Received item stats, check spell names for a match.
				function onAssess(err/*, results*/) {
					if (err) return callback(err);
					if (cancellation && cancellation.canceled) return callback(new core.Error("CANCELLATION"));
					
					if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid) {
						var spellID, spell;
						
						// eslint-disable-next-line no-restricted-syntax
						for (var i = 0; i < aco.oai.iei.cspellid; i++) {
							spellID = aco.oai.iei.rgspellid(i);
							spell = skapi.SpellInfoFromSpellid(spellID);
							if (spell && spell.szName.match(regExpSpell)) {
								
								// Name matches, get a human readable string of the item stats.
								/*
								core.getItemString({
									aco: aco
								}, onItemString);
								*/
								
								onItemString(undefined,  core.getItemStringSync({aco: aco}));
								
								return;
							}
						}
					}

					// No match, move on to the next item.
					callback();
				}
				
				// Received human readable string of the item's stats. Tell the customer and add the item to the trade window.
				function onItemString(err, itemString) {
					if (err) return callback(err);
					if (cancellation && cancellation.canceled) return callback(new core.Error("CANCELLATION"));

					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.foundItem, {
							item: itemString
						})
					});

					aco.AddToTrade();
					
					// Short delay to prevent chat flood.
					core.setTimeout(callback, ONE_SECOND);
				}
			}
			
			// Finished checking each item. Remove our event handler.
			function finished(err) {
				core.removeHandler(evidNil, handler);
				if (err) {
					core.debug("<processAddSpellRequest|finished> err: " + err);
					return;
				}
				
				// Tell customer we're done with their addspell request.
				core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.finishedScan)
				});
			}
			
		};
	}();

	/*
	 * refreshMessagesList() Populate the messages list with recent messages.
	 */
	core.refreshMessagesList = function factory() {
		function _addRow(row) {
			core.controls.lstMessages.addRow(
				row.time,
				row.date,
				row.szSender,
				row.szMessage
			);
		}
		return function refreshMessagesList() {
			var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\messages.sdb";
			
			// Create file if it doesn't exist yet.
			if (!core.existsSync(path)) {
				core.info("You have no messages yet.");
				return;
			}

			core.controls.lstMessages.clear();
			
			var results = SkunkDB
				.select()
				.from(path)
				.execute();

			results.rows.sort(function(a, b) {
				return b.time - a.time;
			});

			results.rows.forEach(_addRow);
		};
	}();

	core.getUnreadMessages = function getUnreadMessages() {
		var path = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\messages.sdb";
		if (!core.existsSync(path)) {
			return [];
		}
		
		var results = SkunkDB
			.select()
			.from(path)
			.where({read: false})
			.execute();
		
		//core.info("results: " + JSON.stringify(results));
		
		return results.rows;
	};

	core.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
		core.debug("<OnChatBoxMessage>", szMsg, cmc);
	};

	core.OnChatServer = function OnChatServer(szMsg, cmc) {
		var path = _dir + "logs\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\itemsGiven.txt";
		if (szMsg.match(/^(.*) gives you ([0-9,]*) (.*).$/i)) {
			var szWho = RegExp.$1;
			var quantity = RegExp.$2;
			var szItem = RegExp.$3;
			core.appendFileSync(path, core.getDateString({seperator: " "}) + " " + szMsg);
		} else if(szMsg.match(/^(.*) gives you (.*).$/i)) {
			var szWho = RegExp.$1;
			var szItem = RegExp.$2;
			core.appendFileSync(path, core.getDateString({seperator: " "}) + " " + szMsg);
		}
	};

	/*
	 * getSquelchedPlayers - Get a list of squelched player names from the server.
	 *
	 * @param {Object} params - Function parameters
	 * @param {number} params.timeout - How long to wait for a response from the server.
	 * @param {function} callback - Function to call with the results.
	 */
	core.getSquelchedPlayers = function getSquelchedPlayers(params, callback) {
		var timeout = params && params.timeout || FIVE_SECONDS;

		var handler = {};
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			core.debug("<OnChatBoxMessage>", szMsg, cmc);
			if (cmc == 0 && szMsg.match(/denotes a character whose account has also been squelched/)) {
				// Split message by lines.
				var lines = szMsg.split('\n');
				
				// Removed header lines.
				// eslint-disable-next-line no-magic-numbers
				lines.splice(0, 4);

				var players = [];
				
				// Parse player names.
				lines.forEach(function(line/*, index*/) {
					//core.debug("line: " + line);
					if (line.match(/^ {2}Name\: (.*) \(account\) {2}All message types$/i)) {
						var szName = RegExp.$1;
						core.debug("szName: " + szName);
						players.push(szName);
					}
				});
				
				core.debug("players: " + players.length + " (" + players.join(", ") + ")");

				resolve(undefined, players);
			}
		};
		core.addHandler(evidOnChatBoxMessage,            handler);

		var tid = core.setTimeout(core.timedOut, timeout, resolve);
		function resolve() {
			core.removeHandler(evidNil, handler);
			clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
		}

		core.info("Getting squelched player names (to ignore trade requests)...");
		skapi.InvokeChatParser("/squelch");
	};
	
	/*
	 * isIgnoredPlayer - Check if we should ignore a player.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.name - The name of the player.
	 * @returns {boolean} ignored
	 */
	core.isIgnoredPlayer = function isIgnoredPlayer(params) {
		var szName = params.szName;
		
		if (core.isMatchingObjectKeys({text: szName, object: config.ignoredPlayers})) {
			core.debug(szName + " is a ignored player.");
			return true;
		}
		
		if (core.squelchedPlayers.indexOf(szName) >= 0) {
			core.debug(szName + " is a squelched player.");
			return true;
		}
		
		return false;
	};

	/*
	 * recordScanResults - Save item stats after a scan.
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.items - Sellable items from a inventory scan.
	 * @returns {undefined}
	 */
	core.recordScanResults = function recordScanResults(params) {
		var items = params.items;

		// Save info on the items in our inventory to file.
		var inv = core.getInventoryItems();
		var inventoryInfoPath = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\inventoryInfo.json";
		var inventoryInfo = core.getJsonFile({
			path: inventoryInfoPath
		}) || {};

		inv.forEach(function(aco) {
			var info = inventoryInfo[ aco.oid ] = inventoryInfo[ aco.oid ] || {};
			
			// Save when we first seen the item so we can see how long we've had a given item.
			info.created = info.created || new Date().getTime();
			
			// Save if the item is transferable (not attuned/ignored).
			info.transferable = items.indexOf(aco) >= 0;
		});
		
		core.saveJsonFile({
			path: inventoryInfoPath,
			data: inventoryInfo
		});
	};

	/*
	 * refreshInventoryList - Refresh inventory tab list.
	 *
	 * @returns {undefined}
	 */
	core.refreshInventoryList = function refreshInventoryList() {

		var inventoryInfoPath = _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\inventoryInfo.json";
		var inventoryInfo = core.getJsonFile({
			path: inventoryInfoPath
		}) || {};
		
		// Debugging
		//Object.keys(inventoryInfo).forEach(function(oid) {
		//	inventoryInfo[oid].created = new Date().getTime() - Math.random() * 1000 * 60 * 60 * 24 * 15;
		//});
		
		var values = core.getJsonFile({path: core.getPricesPath()});
		
		//var start = new Date().getTime();
		var inv = core.getInventoryItems();

		//core.info("getInventoryItems: " + (new Date().getTime() - start));

		//var start = new Date().getTime();
		inv.sort(function(a, b) {
			var aInfo = inventoryInfo[ a.oid ];
			var bInfo = inventoryInfo[ b.oid ];
			var acreated = aInfo && Math.floor(aInfo.created / (ONE_DAY)) || 0;
			var bcreated = bInfo && Math.floor(bInfo.created / (ONE_DAY)) || 0;
			if (acreated > bcreated) return -1;
			if (acreated < bcreated) return 1;
			
			if (core.getFullName(a) < core.getFullName(b)) return -1;
			if (core.getFullName(a) > core.getFullName(b)) return 1;
			
			if (a.workmanship < b.workmanship) return -1;
			if (a.workmanship > b.workmanship) return 1;

			return 0;
		});

		//core.info("inv.sort: " + (new Date().getTime() - start));


		var rows = [];
		rows.push(["", 0, "Item", "Age", "Price"]);

		//var start = new Date().getTime();
		//var times = {};
		inv.forEach(function(aco) {
			var info = inventoryInfo[ aco.oid ] = inventoryInfo[ aco.oid ] || {};
			
			// Save when we first seen the item so we can see how long we've had a given item.
			info.created = info.created || new Date().getTime();

			//var start2 = new Date().getTime();
			var szName = core.getNameWithWorkmanship(aco);

			//times["getName"] = times["getName"] || 0;
			//times["getName"] += new Date().getTime() - start2;
			//var itemString = core.getItemStringSync({aco: aco});
			
			//var start2 = new Date().getTime();
			var age = core.getElapsedString((new Date().getTime() - info.created) / ONE_SECOND, 2);

			//times["getElapsedString"] = times["getElapsedString"] || 0;
			//times["getElapsedString"] += new Date().getTime() - start2;
			
			
			var price;
			if (aco.mcm == mcmSalvageBag) {
				price = "Salv";
			} else {
				price = core.getSellValue({
					aco   : aco,
					values: values
				});
				
				//var start2 = new Date().getTime();
				price = core.getAssignedSellValue({aco: aco, values: values});

				//times["getAssignedSellValue"] = times["getAssignedSellValue"] || 0;
				//times["getAssignedSellValue"] += new Date().getTime() - start2;
			}

			rows.push([
				aco.oid,
				icon_textures + aco.icon, 
				szName, 
				age,
				price
			]);
		});

		//core.info("inv.forEach: " + (new Date().getTime() - start));
		//core.info("times: " + JSON.stringify(times));

		core.controls.lstInventory.setRows(rows);
		
		core.saveJsonFile({
			path: inventoryInfoPath,
			data: inventoryInfo
		});
	};

	/*
	 * timedOut - Throw a timed out error when called.
	 *
	 * @returns {undefined}
	 */
	core.timedOut = function timedOut(resolve) {
		resolve(new core.Error("TIMED_OUT"));
	};

	/*
	 * Error - Error constructor that calls core.trace when created.
	 *
	 * @returns {undefined}
	 */
	core.Error = function CustomError(message) {
		this.message = message;
		var error = new Error(this.message);
		this.stack = error.stack;
		if (config.debugToLog == true) {
			core.trace("Error occurred: " + message);
		}
	};
	core.Error.prototype = new Error();
	core.Error.prototype.name = core.Error.name;
	core.Error.prototype.constructor = core.Error;
	core.Error.prototype.set = function set(key, value) {
		this[key] = value;
		return this;
	};
	
	/*
	 * addMatchToTradeWindow - We've got a list of items matching a add request, so add one of them to the trade window.
	 *
	 * @param {Object} params - Function parameters
	 * @param {array} params.matches - List of ACObjects matching the partner's request.
	 * @param {string} params.szSender -  Sender's name.
	 * @param {number} params.amount - Amount to add to trade window.
	 * @param {string} params.szName - Requetsed item name, for chat messages.
	 * @returns {undefined}
	 */
	core.addMatchToTradeWindow = function addMatchToTradeWindow(params, callback) {
		core.debug("<addMatchToTradeWindow>");
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var silent = (!params || params.silent === undefined ? false : params.silent);
		var skipRestrictedSaleCheck = (!params || params.skipRestrictedSaleCheck === undefined ? false : params.skipRestrictedSaleCheck);
		
		logger("skipRestrictedSaleCheck: " + skipRestrictedSaleCheck);
		
		var matches = params.matches;
		var szSender = params.szSender;
		var amount = params.amount || 1;
		var szName = params.szName;
		var addAll = false;
		if (typeof params.addAll !== "undefined") {
			addAll = params.addAll;
		}

		function resolve() {
			// Call callback in a timer so whatever event triggered this resolve can be processed by other handlers first.
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
		}

		// Check if item(s) belong to a item set, and if we need to restrict individual sales of a item.
		var aco, setName;
		if (skipRestrictedSaleCheck != true) {
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < matches.length; i++) {
				aco = matches[i];
				setName = core.getMatchingItemSet(aco.szName);
				logger("matches #" + i + ", aco: " + aco.szName + ", setname: " + setName);
				if (setName) {
					if (config.itemSets[setName].restrictIndividualSales == true) {
						//return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.individualSaleRestricted, {item: aco.szName, setname: setName})});
						return resolve(new core.Error("RESTRICTED_INDIVIDUAL_SALE") 
							.set("item", aco.szName)
							.set("setName", setName));
					}
				}
			}
		}


		var myItems = core.tradeWindow && core.tradeWindow[myTradeSide] || [];
		logger("We have " + myItems.length + " items in the trade window.");

		var aco = matches.shift();

		if (config.restrictStackableSales == true && aco.citemMaxStack > 1) {
			// Check if that stakcable item is already in the trade window.
			// One user reports issue with not all the items being transfered as expected.

			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < myItems.length; i++) {
				if (myItems[i].szName == aco.szName) {
					core.info("We already have a " + aco.szName + " in the trade window and restrictStackableSales is enabled.");
					return resolve(new core.Error("RESTRICTED_STACK_COUNT").set("item", aco.szName));
				}
			}
		}

		logger("amount: " + amount + ", citemStack: " + aco.citemStack + ", citemMax: " + aco.citemMax, "citemMaxStack: " + aco.citemMaxStack);
		
		if (silent != true) {
			if (amount > 1) {
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addingMultipleItem, {item: core.getNameWithWorkmanship(aco)})});
			} else {
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addingItem, {item: core.getNameWithWorkmanship(aco)})});
			}

			if (aco.mcm == mcmSalvageBag) {
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.salvageWorkmanshipExample)});
			}
		}
		
		// If item equals the amount requested, add to trade window. Else split the item first before adding.
		
		if (addAll == true) {
			return core.addItemToTradeWindow({
				acoItem: aco
			}, onAdd);
		} else if (aco.citemStack == Math.min(amount, Math.max(1, aco.citemStack))) {
			return core.addItemToTradeWindow({
				acoItem: aco
			}, onAdd);
		} else {
			// Stack is larger than the requested amount, split the item first.
			var quantity = Math.min(amount, aco.citemStack);
			core.info("Splitting " + aco.szName + "... (" + quantity + ")");
			
			if (core.getFreeSpaces() <= 2) {
				return resolve(new core.Error("NOT_ENOUGH_SPACE"));
			}
			
			return core.splitAco({
				aco     : aco,
				quantity: quantity
			}, onSplit);
		}
		
		// onAdd item to trade window callback.
		function onAdd(err, result) {
			logger("<onAdd> " + err + ", " + result);

			if (err && err.message == "CANNOT_TRADE_ITEM") {
				core.info(core.getFullName(aco) + " (" + aco.oid + ") might be a phantom item.");
				core.phantomItems[aco.oid] = true;
			}

			if (result == true) {
				amount -= aco.citemStack;
			}
			
			
			logger("Remaining amount wanted: " + amount, addAll);
			if (amount <= 0 && addAll == false) {
				return resolve(undefined, aco);
			}
			
			logger("matches.length: " + matches.length);
			
			if (addAll == true) {
				if (matches.length > 0) {
					aco = matches.shift();
					core.addItemToTradeWindow({acoItem: aco}, onAdd);
				} else {
					// done?
					resolve(undefined, aco);
				}
				return;
			}
			
			if (matches.length > 0) {
				aco = matches.shift();
				
				
				if (aco.citemStack == Math.min(amount, Math.max(1, aco.citemMax))) {
					core.addItemToTradeWindow({acoItem: aco}, onAdd);
				} else {
					
					var quantity = Math.min(amount, aco.citemStack);
					
					if (core.getFreeSpaces() <= 2) {
						return resolve(new core.Error("NOT_ENOUGH_SPACE"));
					}
					
					core.splitAco({
						aco     : aco,
						quantity: quantity
					}, onSplit);
				}
			} else {
				//core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noMoreItemAvailable, {item: szName})});
				return resolve(new core.Error("NOT_ENOUGH_AVAILABLE").set("szName", szName));
			}
		}

		// On split item callback.
		// eslint-disable-next-line handle-callback-err
		function onSplit(err, acoSplit) {
			core.queueOrganize = true;
			if (acoSplit) {
				aco = acoSplit;
				core.addItemToTradeWindow({
					acoItem: aco
				}, onAdd);
				return;
			}
			
			//core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.failedToSplitItem, {item: core.getFullName(aco)})});
			resolve(new core.Error("FAILED_TO_SPLIT").set("item", core.getFullName(aco)));
		}
	};

	/*
	 * processAddRequest - Process a add request from another player.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szName - Item name.
	 * @param {string} params.szValue - Optional item value.
	 * @param {string} params.szSender -  Sender's name.
	 * @returns {undefined}
	 */
	core.processAddRequest = function factory() {
		function _getTotal(accumulator, acoInTrade) {
			return accumulator + acoInTrade.citemStack;
		}
		return function processAddRequest(params) {
			var szName = params.szName;
			var value = params.value;
			var amount = params.amount || 1;
			var szSender = params.szSender;
			core.debug("<processAddRequest>", szName, value, amount, szSender);

			if (!core.tradePartner || core.tradePartner.szName != szSender) {
				core.debug("Sender is not my trade partner.");
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.notMyPartner)
				});
			}


			var allNumbers = szName.split(" ").every(function(str) {
				return core.isNumeric(str);
			});
			if (allNumbers == true) {
				var numbers = szName.split(" ").map(function(str) {
					return Number(str);
				});

				var forSale = core.getItemsForSale();
				var items = [];
				var missing = [];
				
				numbers.forEach(function(num) {
					var item = forSale.find(function(aco) {
						return aco.cpyValue == num || aco.oid == num || Math.abs(aco.oid) == num;
					});
					if (item) {
						items.push(item);
					} else {
						missing.push(num);
					}
				});
				
				if (missing.length > 0) {
					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.noItemMatching, {
							name: missing.join(", ")
						})
					});
				}

				if (items.length > 0) {
					return onItems(undefined, items);
				}
			} else {
				return core.getItemsFromChat({
					szName  : szName,
					value   : value,
					szSender: szSender
				}, onItems);
			}
			
			function onItems(err, matches) {
				if (err) {
					if (err.message == "NO_MATCHES") {
						core.tell({
							szRecipient: szSender, 
							szMsg      : core.arrayTemplate(_l.noItemMatching, {
								name: err.szName
							})
						});
						
						var proposal = core.proposeCorrectItem({
							name: err.szName
						});
						
						core.tell({
							szRecipient: szSender, 
							szMsg      : "// Perhaps you meant '" + proposal + "'."
						});
						return;
					} else if (err.message == "TOO_MANY_MATCHES") {

						var matches = err.matches;
						core.tell({
							szRecipient: szSender, 
							szMsg      : core.arrayTemplate(_l.tooManyMatches, {
								name : err.szName,
								count: matches.length
							})
						});
						
						// Max 10 told to customer.
						//matches.splice(Math.min(10, matches.length));
						
						// Send item IDs and their stats.
						async_js.eachSeries(matches, function each(aco, callback) {
							var stats = err.itemStats[aco.oid];
							core.tell({
								szRecipient: szSender,
								szMsg      : aco.oid + ": " + stats
							});
							
							// Wait a second before sending the next tell, to prevent flooding.
							core.setTimeout(callback, ONE_SECOND);
						});
						return;
					} else if (err.message == "NOT_FOR_SALE") {
						return core.tell({
							szRecipient: szSender, 
							szMsg      : core.arrayTemplate(_l.itemNotForSale, {
								item: err.szFull
							})
						});
					}
					
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.errorOccurred, {
							message: err.message
						})
					});
				}
				
				if (core.allItemsShown == true) {
					core.debug("Window is full.");
					skapi.TradeReset();
					core.tradeWindow = null;
				}
				
				var aco;
				
				// Remove items already in trade window.
				var inWindow = core.tradeWindow && core.tradeWindow[myTradeSide] || [];
				matches = matches.filter(function(aco) {
					return inWindow.indexOf(aco) < 0;
				});
				
				// Check if no matches remain.
				if (matches.length == 0) {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.noMoreItemAvailable, {
							item: szName
						})
					});
				}

				aco = matches[0];
				
				var password = core.getSellPassword({
					aco: aco
				});
				
				if (typeof password !== "undefined") {
					core.info("Asking for password (" + password + ")...");
					
					core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.requestPassword, {item: aco.szName})});
					
					core.waitForPassword({
						szPlayer: szSender,
						password: password
					}, function onPassword(err) {
						if (err) {
							if (err.message == "INCORRECT_PASSWORD") {
								return core.tell({
									szRecipient: szSender, 
									szMsg      : core.arrayTemplate(_l.incorrectPassword, {
										provided: err.provided
									})
								});
							}

							return core.tell({
								szRecipient: szSender, 
								szMsg      : core.arrayTemplate(_l.errorOccurred, {
									message: err.message
								})
							});
						}
						

						// Password was correct, add item to trade window.
						core.addMatchToTradeWindow({
							matches : matches,
							szSender: szSender,
							amount  : amount,
							szName  : szName
						}, onAdd);
					});

				} else {
					// Add items to trade window.
					core.addMatchToTradeWindow({
						matches : matches,
						szSender: szSender,
						amount  : amount,
						szName  : szName
					}, onAdd);
				}
				
				function onAdd(err, aco) {
					core.debug("<processAddRequest|onAdd>", err, aco);
					
					if (err) {
						if (err.message == "RESTRICTED_STACK_COUNT") {
							return core.tell({
								szRecipient: szSender, 
								szMsg      : core.arrayTemplate(_l.restrictedStackableSale, {
									item: err.item
								})
							});
						} else if (err.message == "NOT_ENOUGH_AVAILABLE") {
							return core.tell({
								szRecipient: szSender, 
								szMsg      : core.arrayTemplate(_l.noMoreItemAvailable, {
									item: err.szName
								})
							});
						} else if (err.message == "FAILED_TO_SPLIT") {
							return core.tell({
								szRecipient: szSender, 
								szMsg      : core.arrayTemplate(_l.failedToSplitItem, {
									item: err.item
								})
							});
						} else if (err.message == "RESTRICTED_INDIVIDUAL_SALE") {
							return core.tell({
								szRecipient: szSender, 
								szMsg      : core.arrayTemplate(_l.individualSaleRestricted, {
									item   : err.item,
									setName: err.setName
								})
							});
						}

						// Unhandled error.
						return core.tell({
							szRecipient: szSender, 
							szMsg      : core.arrayTemplate(_l.errorOccurred, {
								message: err.message
							})
						});
					}

					// var partner know if we have more available.
					if (aco && !core.isUnique(aco)) {
						var total = core.getInvRegexCount(aco.szName);
						var inTrade = core.tradeWindow[myTradeSide].filter(function isMatchingName(acoInTrade) {
							return (acoInTrade.szName == aco.szName);
						}).reduce(_getTotal, 0);

						var reminaing = total - inTrade;
						core.debug("total: " + total + ", inTrade: " + inTrade + ", reminaing: " + reminaing);
						
						if (reminaing > 0) {
							core.tell({
								szRecipient: szSender, 
								szMsg      : core.arrayTemplate(_l.additionalItemAvailable, {
									name     : core.getFullName(aco), 
									reminaing: reminaing
								})
							});
						}
					}
				}
			}
		};
	}();

	/*
	 * getItemsFromChat - Find a item we're selling that matches a name and optional price. 
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szName - (partial) item name.
	 * @param {string} params.value - Optional item pyreal value.
	 * @param {string} params.szSender - Name of the player interacting with us.
	 * @param {string} params._matches - Used for unit testing.
	 * @returns {null}
	 */
	core.getItemsFromChat = function getItemsFromChat(params, callback) {
		function resolve() {
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	// Call callback in a timer so whatever event triggered this resolve can be processed by other handlers first.
		}
		
		var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);

		var szName = params.szName;

		var value = params.value;
		var salvageStats = false;
		if (typeof params.salvageStats !== "undefined") {
			salvageStats = params.salvageStats;
		}

		verbose("<getItemsFromChat>", "szName: " + szName, "value: " + value);

		if (typeof config.itemAliases[szName.toLowerCase()] !== "undefined") {
			szName = config.itemAliases[szName.toLowerCase()];
		}
		

		// Check if name is a nunber, for itemId.
		var itemId = Number(szName);
		verbose("itemId: " + itemId);
		
		if (szName.length > 0 && core.isNumber(itemId)) {
			verbose("checking for oid...");
			
			var items = core.getItemsForSale();
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < items.length; i++) {
				if (items[i].oid == itemId) {
					return resolve(undefined, [items[i]]);
				}
			}
			return resolve(new core.Error("ID_NOT_FOUND").set("itemId", itemId));
		}
		
		/*
		var matches = core.getSellableItemMatch({
			szName: szName,
			value : value
		});
		*/
		
		/*
		var props = szName.split(":");
		var chunks = szName.split(",");
		core.debug("props: " + props.length, "chunks: " + chunks.length);
		
		if (props.length - 1 > chunks.length) {
			return core.tell({
				szRecipient: szSender, 
				szMsg      : "// You need to use commas between property types."
			});
		}
		
		
		var matches = core.parseStringForItems({
			string: szName
		});
		*/
		var matches = core.getItemsMatchingSearchString({
			string : szName,
			verbose: verbose
		});

		verbose("matches 1: " + matches.length);

		// Check if there's no matches.
		if (matches.length == 0) {
			var proposal = core.proposeCorrectItem({
				name: szName
			});
			return resolve(new core.Error("NO_MATCHES").set("szName", szName)
				.set("proposal", proposal));
		}

		// Group matches by their stats, common items will be grouped.
		var itemsByStats = {};
		var itemStats = {};
		async_js.eachSeries(matches, function each(aco, callback) {
			var itemString = core.getItemStringSync({aco: aco, single: true});
			
			if (aco.mcm == mcmSalvageBag && salvageStats == false) {
				var fullName = core.getFullName(aco);
				itemsByStats[fullName] = itemsByStats[fullName] || [];
				itemsByStats[fullName].push(aco);
			} else {
				itemsByStats[itemString] = itemsByStats[itemString] || [];
				itemsByStats[itemString].push(aco);
			}

			itemStats[aco.oid] = itemString;
			callback();
			
		}, function onFinishStrings(err) {
			if (err) return callback(err);

			// Merge one item per stats into a array.
			var uniqueItems = Object.keys(itemsByStats).map(function(stats) {
				return itemsByStats[stats][0];
			});
			
			matches.sort(core.compare_items);
			uniqueItems.sort(core.compare_items);
			
			if (uniqueItems.length > 1) {
				return resolve(new core.Error("TOO_MANY_MATCHES")
					.set("szName", szName)
					.set("itemStats", itemStats)
					.set("matches", uniqueItems)
				);
			}

			// Return array of matches.
			resolve(undefined, matches);
		});
	};

	/*
	 * processEquipRequest - Equip a item we're selling that matches a name and optional price. 
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szName - (partial) item name.
	 * @param {string} params.value - Optional item pyreal value.
	 * @param {string} params.szSender - Name of the player interacting with us.
	 * @returns {null}
	 */
	core.processEquipRequest = function processEquipRequest(params) {
		core.debug("<processEquipRequest>");
		var szName   = params.szName;
		var value    = params.value;
		var szSender = params.szSender;
		
		core.getItemsFromChat({
			szSender: szSender,
			szName  : core.escapeRegExp(szName),
			value   : value
		}, onItem);
		
		var acoChosen;
		function onItem(err, matches) {
			core.debug("<processEquipRequest|onItem>", err, aco);
			if (err) {
				if (err.message == "NO_MATCHES") {
					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.noItemMatching, {
							name: err.szName
						})
					});
					
					var proposal = core.proposeCorrectItem({
						name: err.szName
					});
					
					core.tell({
						szRecipient: szSender, 
						szMsg      : "// Perhaps you meant '" + proposal + "'."
					});
					return;
				} else if (err.message == "TOO_MANY_MATCHES") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.tooManyMatches, {
							name   : err.szName,
							matches: err.names.join(", ")
						})
					});
				}
				
				// Unhandled error.
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}
			var aco = matches[0];

			if (aco.eqm == 0) {
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.itemNotEquippable, {
						item: core.getFullName(aco)
					})
				});
			}
			
			if (aco.eqmWearer & eqmAll) {
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.itemAlreadyEquipped, {
						item: core.getFullName(aco)
					})
				});
			}
			
			acoChosen = aco;
			
			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.attemptingToEquip, {
					item: core.getFullName(aco)
				})
			});

			// Get naked first.
			var equippedItems = core.getEquippedItems();
			core.debug(" equippedItems: " + equippedItems.length);
			
			core.equipmentChanged = true;

			async_js.eachSeries(equippedItems.map(function(aco) {
				return {
					aco: aco
				};
			}), core.unequipItem, onUnequippedItems);
		}

		function onUnequippedItems(err, results) {
			core.debug("<processEquipRequest|onUnequippedItems>", err, results);
			if (err) {
				// Unhandled error.
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}

			core.equipItem({
				aco: acoChosen
			}, onEquip);
		}

		function onEquip(err, results) {
			core.debug("<processEquipRequest|onEquip>", err, results);
			if (err) {
				if (err.message == "CANT_BE_WIELDED") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.cantWieldItem, {
							item: core.getFullName(acoChosen)
						})
					});
				}

				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}

			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.itemEquipped, {
					item: core.getFullName(acoChosen)
				})
			});
		}
	};

	/*
	 * equipItem - Equip a item.
	 *
	 * @param {object} aco - Item to equip.
	 * @param {function} callback - Function to call when we're done.
	 * @returns {null}
	 */
	core.equipItem = function equipItem(params, callback) {
		core.debug("<equipItem>");

		var aco = params.aco;

		function resolve(err, result) {
			core.debug("<equipItem|resolve>", err, result);
			
			tid && clearTimeout(tid);
			tid2 && clearTimeout(tid2);
			
			handler && core.removeHandler(evidNil, handler);
			
			// Call callback in a timer so ensure asynchronicity.
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	
		}

		if (!aco) {
			return resolve(new core.Error("MISSING_ACO"));
		}

		if (aco.eqmWearer & eqmAll) {
			core.info("Item already equipped");
			resolve();
			return;
		}

		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			core.debug("<equipItem|OnTipMessage>", szMsg);
			if (szMsg.match(/You can only move or use one item at a time/)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/In Portal Space - Please Wait.../)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) can't be put in the container/)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You're too busy!/))   {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/A shield may not be worn with the (.*)/))   {
				return resolve(new core.Error("SHILED_WITH_WEAPON")); //(_e.results.cantEquip);
			} else if (szMsg.match(/You cannot move or use an item while attacking/)) {
				return resolve(new core.Error("CANT_WHILE_ATTACKING"));
			} else if (szMsg.match(/The (.*) can't be wielded/)) {
				var szItem = RegExp.$1;
				return resolve(new core.Error("CANT_BE_WIELDED").set("szItem", szItem));
			}
			core.debug("Uncaught OnTipMessage", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		var tid = core.setTimeout(resolve, FIVE_SECONDS, new Error("TIMED_OUT"));
		
		// Continously check if we've equipped the item. 
		var tid2 = core.setInterval(function checkEquipped() {
			if (aco.eqmWearer & eqmAll) {
				core.debug("<equipItem|checkEquipped> We've equipped the item.");
				resolve(undefined, true);
			}
		}, QUARTER_SECOND);
		

		aco.Use();
	};

	/*
	 * unequipItem - Unequip a item.
	 *
	 * @param {object} aco - Item to equip.
	 * @param {function} callback - Function to call when we're done.
	 * @returns {null}
	 */
	core.unequipItem = function unequipItem(params, callback) {
		core.debug("<unequipItem>");
		var aco = params.aco;
		
		function resolve() {
			clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			
			// Call callback in a timer so ensure asynchronicity.
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	
		}

		
		if (aco.eqmWearer == 0) {
			return resolve();
		}

		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			core.debug("<unequipItem|OnTipMessage>", szMsg);
			if (szMsg.match(/You can only move or use one item at a time/)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/In Portal Space - Please Wait.../)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) can't be put in the container/)) {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You're too busy!/))   {
				return resolve(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/A shield may not be worn with the (.*)/))   {
				return resolve(new core.Error("SHILED_WITH_WEAPON")); //(_e.results.cantEquip);
			} else if (szMsg.match(/You cannot move or use an item while attacking/)) {
				return resolve(new core.Error("CANT_WHILE_ATTACKING"));
			}
		};
		core.addHandler(evidOnTipMessage, handler);
		
		handler.OnAddToInventory = function OnAddToInventory(aco2) {
			core.debug("<unequipItem|OnAddToInventory>", aco2);
			if (aco2.oid == aco.oid) {
				resolve(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);

		var tid = core.setTimeout(resolve, FIVE_SECONDS, new Error("TIMED_OUT"));

		aco.MoveToPack(0, 0, false);
	};

	/*
	 * getEquippedItems - Get a array of our equipped items.
	 *
	 * @returns {array}
	 */
	core.getEquippedItems = function getEquippedItems() {
		var acf = skapi.AcfNew();	
		acf.olc = olcEquipped;
		var items = core.coacoToArray(acf.CoacoGet());
		return items.filter(function(item) {
			if (item.eqmWearer == 0) return false;
			if (item.eqm == 0) return false;
			if (!core.doesAcoBelongsToMe(item)) return false;
			return true;
		});
	};

	/*
	 * processEquipSetRequest - Process a request to equip a item set.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.setName - Item set name.
	 * @param {string} szSender - Name of the player interacting with us.
	 * @returns {null}
	 */
	core.processEquipSetRequest = function processEquipSetRequest(params) {
		var setName = params.setName;
		var szSender = params.szSender;
		
		var items;
		for (var key in config.itemSets) {
			//if (!config.itemSets.hasOwnProperty(key)) continue;
			if (!Object.prototype.hasOwnProperty.call(config.itemSets, key)) continue;
			if (key.toLowerCase() == setName.toLowerCase()) {
				items = config.itemSets[key].items;
				setName = key;
				break;
			}
		}
		if (!items) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noSetMatches, {name: setName})});
		}
		
		var setItemNames = core.getEnabledItemSetItems({
			setName: setName
		});
		core.debug("setItemNames", setItemNames.length, setItemNames.join());
		
		if (setItemNames.length == 0) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noItemEnabledSet)});
		}

		var items = setItemNames.map(function(item) {
			var matches = core.getSellableItemMatch({
				szName: item.itemName,
				oid   : item.oid
			});
			return matches[0];
		});

		// Tell user we're attempting to equip the set.
		core.tell({
			szRecipient: szSender, 
			szMsg      : core.arrayTemplate(_l.attemptingToEquip, {
				item: setName
			})
		});
		
		// Get naked first.
		var equippedItems = core.getEquippedItems();
		core.debug(" equippedItems: " + equippedItems.length);

		async_js.eachSeries(equippedItems.map(function(aco) {
			return {
				aco: aco
			};
		}), core.unequipItem, onUnequippedItems);
		
		function onUnequippedItems(err, results) {
			core.debug("<processEquipRequest|onUnequippedItems>", err, results);
			if (err) {
				// Unhandled error.
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}

			async_js.eachSeries(items.map(function(aco) {
				return {
					aco: aco
				};
			}), core.equipItem, onEquip);
		}
		
		function onEquip(err, results) {
			core.debug("<processEquipRequest|onEquip>", err, results);
			if (err) {
				if (err.message == "CANT_BE_WIELDED") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.cantWieldItem, {
							item: err.szItem || "unknown"
						})
					});
				}

				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}

			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.itemEquipped, {
					item: setName
				})
			});
		}
	};

	/*
	 * setSellingPassword - Update the password required to buy a item.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name (for common items).
	 * @param {number} params.oid - Item oid (for unique items).
	 * @param {number} params.password - Password to purchase item.
	 */
	core.setSellingPassword = function setSellingPassword(params) {
		var aco = params.aco;
		var szItem = params.szItem || aco && core.getFullName(aco);
		var oid = params.oid || aco && core.isUnique(aco) && aco.oid;
		
		var password = params.password;

		var path = core.getPricesPath();
		var items = core.getJsonFile({path: path}) || {};
		
		if (oid) {
			items.unique = items.unique || {};
			items.unique[oid] = items.unique[oid] || {};
			
			var _szItem = szItem;
			if (aco && aco.mcm == mcmSalvageBag) {
				_szItem += " [w" + core.round(aco.workmanship, config.workmanshipRounding) + "]";
			}
			
			if (typeof szItem !== "undefined") items.unique[oid]._szItem = _szItem; // Only there for manually editing json file.
			items.unique[oid].password = password;
			
		} else {
			items.common = items.common || {};
			
			items.common[szItem] = items.common[szItem] || {};
			items.common[szItem].password = password;
		}
		
		core.saveJsonFile({
			path: path,
			data: items
		});
	};

	/*
	 * getSellPassword - Get the password for a given item.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szItem -  Item name (for common items).
	 * @param {number} params.oid - Item oid (for unique items).
	 */
	core.getSellPassword = function getSellPassword(params) {
		var aco = params.aco;
		var oid = params.oid || params.aco && params.aco.oid;
		var szItem = params.szItem || aco && core.getFullName(aco);

		var path = core.getPricesPath();
		var values = params.values || core.getJsonFile({path: path}) || {};
		
		if (aco && core.isUnique(aco)) { //  || typeof oid !== "undefined"
			if (values.unique && values.unique[oid]) {
				return values.unique && values.unique[oid] && values.unique[oid].password;
			}
		}
		
		if (typeof szItem !== "undefined") {
			if (values.common && values.common[szItem]) {
				return values.common && values.common[szItem] && values.common[szItem].password;
			}
		}
	};

	core.waitingForPassword = false;

	/*
	 * waitForPassword - Wait for a player to provide us with a password.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.password - Message we're waiting for.
	 * @param {string} params.szPlayer - Player name we're listening for.
	 */
	core.waitForPassword = function waitForPassword(params, callback) {
		core.waitingForPassword = true;
		var password = params.password;
		var szPlayer = params.szPlayer;

		function resolve() {
			clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			
			core.setTimeout(function() {
				core.waitingForPassword = false;
			}, TENTH_OF_A_SECOND);
		}
		
		var handler = {};
		handler.OnTell = function OnTell(acoSender, acoReceiver, szMsg) {
			if (acoSender.szName != szPlayer) return;
			if (szMsg == password) {
				return resolve(undefined, true);
			}
			resolve(new core.Error("INCORRECT_PASSWORD").set("provided", szMsg));
		};
		core.addHandler(evidOnTell, handler);

		// Timeout after awhile
		var tid = core.setTimeout(core.timedOut, THIRTY_SECONDS, resolve);
	};
	

	/*
	 * updateAllegianceInfo() Record patron and moanrch names of a player.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szPlayer - Player's name
	 * @param {string} params.szPatron - Patron's name
	 * @param {string} params.szMonarch - Patron's name
	 * @param {number} params.oidMonarch - Monarch's object ID.
	 */
	core.updateAllegianceInfo = function updateAllegianceInfo(params) {
		var szPlayer = params.szPlayer;
		var szPatron = params.szPatron;
		var szMonarch = params.szMonarch;
		var oidMonarch = params.oidMonarch;
		core.debug("<updateAllegianceInfo>", szPlayer, szPatron, szMonarch, oidMonarch);

		var info = {};
		info.szPatron = szPatron;
		info.szMonarch = szMonarch;
		info.oidMonarch = oidMonarch;
		info.date = core.getDateString({seperator: " "}); // Pretty version if reading file in text editor.
		info.time = new Date().getTime();

		if (core.allegianceInfo.equals(szPlayer, info, ["time", "date"])) {
			core.debug("We already have their current allegiance info.");
			return;
		}

		var start = new Date().getTime();
		core.allegianceInfo.set(szPlayer, info).save();
		var elapsed = new Date().getTime() - start;
		core.debug("Took " + elapsed + " to set " + szPlayer + "'s allegiance info.");
	};

	core.loadLootProfile = function loadLootProfile() {
		core.debug("<loadLootProfile>");
		
		function onProfileChange() {
			core.info("A LootProfile has changed, refreshing prices...");
			
			// Cancel any current refreshes.
			core.emit("onInterruptLootProfileRefresh");
			var items = core.getInventoryItems();
			core.refreshItemData({
				items: items
			});
		}
		
		var path = "..\\LootProfile3\\src\\LootProfile3.js";
		if (fs.existsSync(path)) {
			core.debug("Loading LootProfile3...");
			var script = require(path, {
				dirnameOverride: "..\\LootProfile3"
			});
			script.initialize();
			script.showUI(false);
			core.LootProfile3 = script;
			
			script.on("onProfileSaved", onProfileChange);
			script.on("onProfileDeleted", onProfileChange);
			core.info("Loaded LootProfile3, icon should be on your Decal bar.");
		} else {
			core.debug("LootProfile3 not available.");
		}
	};
	
	core.refreshLootProfiles = function refreshLootProfiles() {
		core.debug("<refreshLootProfiles>");
		if (typeof core.LootProfile3 === "undefined") {
			return;
		}
		
		var profiles = core.LootProfile3.getLoadedProfiles();
		core.controls.lstLootProfiles.clear();
		core.controls.lstLootProfiles.addRow(false, 0, "Profile", "Price");
		
		profiles.forEach(function(value/*, index*/) {
			var profileInfo = config.lootProfiles[value.data.name] = config.lootProfiles[value.data.name] || {
				enabled: false,
				price  : 0
			};

			core.controls.lstLootProfiles.addRow(profileInfo.enabled, icon_textures + (value.data.icon || 0), value.data.name, profileInfo.price);
		});

		core.saveCharacterConfig();
	};

	core.showProfileItems = function showProfileItems(params) {
		var profileName = params.profileName;
		
		core.getItemsMatchingProfile({
			profileName: profileName
		}, function onItems(err, results) {
			if (err) return core.error("Error getting items matching profile", err);
			core.debug("<onItems>", err, results);
			
			
		});
	};

	core.getItemsMatchingProfile = function getItemsMatchingProfile(params, callback) {
		core.debug("<getItemsMatchingProfile>");
		
		if (typeof core.LootProfile3 === "undefined") {
			return callback(new core.Error("LOOTPROFILE_NOT_LOADED"));
		}
		
		var profileName = params.profileName;
		var profile = core.LootProfile3.getProfileByName(profileName);
		if (!profile) {
			return callback(new core.Error("MISSING_PROFILE"));
		}
		
		var inv = core.getInventoryItems();
		core.debug("Assessing " + inv.length + " items if they match '" + profileName + "'.");
		async_js.filterSeries(inv, function each(aco, callback) {
			var cloned = core.scannedInventory.get(aco.oid);
			cloned._cloned = true;
			profile.evaluateRulesAsync({
				aco: cloned
			}, function onEvaled(err, results) {
				if (err) return callback(err);
				callback(undefined, results.match);
			});
		}, callback);
	};

	core.getFlattenItems = function factory() {
		function _getFlattenedAco(aco) {
			return core.flattenAco({
				aco: aco
			});
		}
		return function getFlattenItems(params) {
			core.debug("<getFlattenItems>");
			var items = params.items;
			return items.map(_getFlattenedAco);
		};
	}();

	core.assessItems = function factory() {
		function _each(aco, callback) {
			if (aco.oai) {
				return callback(undefined, true);
			}
			//core.debug("Assessing " + aco.szName + "...");
			core.assessAco({
				aco: aco
			}, function (err) {
				if (err) return callback(err);
				callback(undefined, true);
			});
		}
		return function assessItems(params, callback) {
			var items = params.items;
			async_js.filterSeries(items, _each, callback);
		};
	}();

	core.refreshLootProfileItems = function factory() {
		function _isSalvage(aco) {
			return (aco.mcm != mcmSalvageBag);
		}
		function _compareItems(a, b) {
			if (a.eqm < b.eqm) return -1;
			if (a.eqm > b.eqm) return 1;
			if (a.szName < b.szName) return -1;
			if (a.szName > b.szName) return 1;
			if (core.getFullName(a) < core.getFullName(b)) return -1;
			if (core.getFullName(a) > core.getFullName(b)) return 1;
			return 0;
		}
		function _onFinishedItems(err) {
			if (err) return core.error("Error while populating LP items list", err);
			core.info("List populated.");
		}
		return function refreshLootProfileItems(params) {
			core.debug("<refreshLootProfileItems>");
			if (core.initializedInventoryData == false || core.refreshingItemData == true) {
				core.warn("SkunkTrader hasn't finished scanning inventory, prices for all items may not be ready.");
				return;
			}
			
			var inventory = core.getInventoryItems();
			inventory = inventory.filter(_isSalvage);

			inventory.sort(_compareItems);

			var cancellation = params && params.cancellation || new core.Cancellation();

			core.info("Populating items list...");

			core.controls.lstLPItems.clear();
			core.controls.lstLPItems.addRow("", 0, "Item Name", "Price");

			async_js.eachSeries(inventory, eachItem, _onFinishedItems);
			function eachItem(aco, callback) {
				if (cancellation && cancellation.canceled) return callback(cancellation.error || new core.Error("CANCELLATION"));
				
				var profiles = core.getItemProfiles({aco: aco});
				if (profiles && profiles.length > 0) {
					var price = core.getItemProfilePrice({
						aco: aco
					});

					//core.console(core.getFullName(aco), "price: " + price);
					core.controls.lstLPItems.addRow(aco.oid, icon_textures + aco.icon, core.getFullName(aco), price);
				} else {
					core.controls.lstLPItems.addRow(aco.oid, icon_textures + aco.icon, {value: core.getFullName(aco), color: "&H777777"}, "");
				}
				core.setImmediate(callback);
			}

			// Stop processing if user exits tab.
			core.controls.lpNotebook.once("onControlEvent", function onControlEvent() {
				cancellation.canceled = true;
			});
		};
	}();

	core.itemLootProfiles = {};
	core.initializedInventoryData = false;
	core.refreshItemData = function factory() {
		return function refreshItemData(params, callback) {
			core.debug("<refreshItemData>");
			var items = params.items;

			function finish() {
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}
			
			var profiles = core.getEnabledLootProfiles();
			
			var numItemsWithProfiles = 0;
			
			var started = new Date().getTime();
			async_js.forEachSeries(items, function each(aco, callback) {
				//core.debug("Assessing " + aco.szName + "...");

				delete core.itemLootProfiles[ aco.oid ]; // reset

				if (aco.oai) {
					return onAssess();
				} else {
					return core.assessAco({
						aco: aco
					}, onAssess);
				}
				
				function onAssess(err) {
					if (err) {
						if (err.message == "ASSESS_TIMED_OUT") {
							return core.assessAco({
								aco: aco
							}, onAssess);
						}
						core.warn("Error while assessing item", err);
						return callback(err);
					}

					var item = core.flattenAco({aco: aco});
					if (!core.scannedInventory.equals(item.oid, item, ["time", "oai.time"])) {
						core.scannedInventory.set(item.oid, item);
					}

					if (core.LootProfile3 && profiles) {
						core.debug("Checking " + aco + " for LP3 profiles...");
						return async_js.eachSeries(profiles, function onProfile(profile, callback) {
							//core.console("e2: " + (new Date().getTime() - s2));
							
							item._cloned = true;

							//if (core.getFullName(item).match(/Bronze Katar/i)) {
							//	core.console("item: " + JSON.stringify(item, undefined, "\t"));
							//}

							profile.evaluateRulesAsync({
								aco: item
							}, onEvaleRules);
							function onEvaleRules(err, results) {
								if (err) {
									core.warn("Error while evaluating rules", err);
									return callback(err);
								}
								core.debug(item.szName + " matches " + profile.data.name + ": " + results.match);
								if (results && results.match) {
									core.itemLootProfiles[ item.oid ] = core.itemLootProfiles[ item.oid ] || [];
									core.itemLootProfiles[ item.oid ].push(profile.data.name);
									numItemsWithProfiles += 1;
								}
								core.setImmediate(callback);
							}
						}, function onProfiles() { // err, results
							// Call callback without error.
							return core.setImmediate(callback);
						});
					//} else {
					//	core.debug("LP3 not loaded or no profiles available.");
					}
					return core.setImmediate(callback);
				}

			}, function onDone(err) {
				if (err) {
					core.warn("Error while scanning inventory", err);
					return finish(err);
				}
				var elapsed = new Date().getTime() - started;
				core.console("Finished scanning inventory after " + elapsed + "ms.");
				
				return finish(undefined, {
					numItemsWithProfiles: numItemsWithProfiles
				});
			});
		};
	}();
	
	/*
	core.refreshItemProfiles = function refreshItemProfiles(params, callback) {
		core.debug("<refreshItemProfiles>");
		
		callback = callback || noop;
		var items = params.items;
		var cancellation = params && params.cancellation || new core.Cancellation();
		
		var profiles = core.getEnabledLootProfiles();
		var numItemsWithProfiles = 0;
		
		async_js.eachSeries(items, onItem, onDoneItems);
		function onItem(aco, callback) {
			if (cancellation && cancellation.canceled) return callback(cancellation.error || new core.Error("CANCELLATION"));
			async_js.eachSeries(profiles, onProfile, callback);
			function onProfile(profile, callback) {
				if (cancellation && cancellation.canceled) return callback(cancellation.error || new core.Error("CANCELLATION"));
				delete core.itemLootProfiles[ aco.oid ]; // reset
				aco._cloned = true;

				profile.evaluateRulesAsync({
					aco         : aco,
					cancellation: cancellation
				}, onEvaleRules);
				function onEvaleRules(err, results) {
					if (err) return callback(err);
					if (cancellation && cancellation.canceled) return callback(cancellation.error || new core.Error("CANCELLATION"));
					core.debug(aco.szName + " matches " + profile.data.name + ": " + results.match);
					if (results && results.match) {
						core.itemLootProfiles[ aco.oid ] = core.itemLootProfiles[ aco.oid ] || [];
						core.itemLootProfiles[ aco.oid ].push(profile.data.name);
						numItemsWithProfiles += 1;
					}
					core.setImmediate(callback);
				}
			}
		}

		function onDoneItems(err) {
			if (err) {
				if (!cancellation.canceled)	core.error("Error while assessing inventory", err);
				return callback(err);
			}
			callback(undefined, {
				numItemsWithProfiles: numItemsWithProfiles
			});
		}

		core.once("onInterruptLootProfileRefresh", function() {
			cancellation.canceled = true;
		});
	};
	*/
	
	core.getEnabledLootProfiles = function getEnabledLootProfiles() {
		core.debug("<getEnabledLootProfiles>");
		if (typeof core.LootProfile3 === "undefined") {
			return;
		}
		var profiles = [];
		var profile;
		for (var key in config.lootProfiles) {
			if (!config.lootProfiles[key].enabled) continue;
			profile = core.LootProfile3.getProfileByName(key);
			if (profile) {
				profiles.push(profile);
			}
		}
		return profiles;
	};

	core.getItemProfiles = function factory() {
		function _onlyEnabled(profileName) {
			return config.lootProfiles[profileName].enabled;
		}
		return function getItemProfiles(params) {
			var aco = params.aco;
			if (core.itemLootProfiles[aco.oid]) {
				return core.itemLootProfiles[aco.oid].filter(_onlyEnabled);
			}
		};
	}();

	core.getItemProfilePrice = function factory() {
		function _getPrice(profileName) {
			return config.lootProfiles[profileName].price;
		}
		function _getMax(a, b) {
			return Math.max(a, b);
		}
		return function getItemProfilePrice(params) {
			var aco = params.aco;
			var profiles = core.getItemProfiles({aco: aco});
			if (profiles && profiles.length > 0) {
				return profiles.map(_getPrice).reduce(_getMax, 0);
			}
		};
	}();
	

	core.isNumber = function isNumber(value) {
		return isInt(value) || isFloat(value);
	};
	
	core.getAssessedInventoryItems = function factory() {
		function _getAssessed(aco) {
			if (aco.oai) {
				return aco;
			} else {
				var cloned = core.scannedInventory.get(aco.oid);
				if (cloned) {
					cloned._cloned = true;
					return cloned;
				}
			}
		}
		function _removeUndefined(aco) {
			return typeof aco !== "undefined";
		}
		return function getAssessedInventoryItems() {
			var items = core.getInventoryItems();
			return items.map(_getAssessed).filter(_removeUndefined);
		};
	}();

	/*
	 * handleCountCommand() Handle a 'count <name>' command from a player.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.name - (partial) item name.
	 * @param {string} params.szSender - Sender's name.
	 */
	core.handleCountCommand = function factory() {
		function _compare(a, b) {
			if (a.name > b.name) return -1;
			if (a.name < b.name) return 1;
			if (a.count < b.count) return -1;
			if (a.count > b.count) return 1;
			return 0;
		}
		function _getNameAndCount(value) {
			return value.name + " * " + value.count;
		}
		return function handleCountCommand(params) {
			var name = params.name;
			var szSender = params.szSender;
			var json = params.json || false;
			var szMsg = params.szMsg;

			return core.getItemsFromChat({
				szName  : name,
				szSender: szSender
			}, function onItems(err, results) {
				if (err) {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.errorOccurred, {
							message: err.message
						})
					});
				}
				
				var names = {};
				results.forEach(function(aco) {
					var szName = core.getFullName(aco);
					names[szName] = names[szName] || [];
					names[szName].push(aco);
				});

				var sorted = Object.keys(names).map(function(key) {
					return {
						name : key,
						count: names[key].length
					};
				});

				sorted.sort(_compare);

				if (json == true) {
					var payload = {};
					sorted.forEach(function(value) {
						payload[value.name] = value.count;
					});
					
					core.tell({
						szRecipient: szSender, 
						szMsg      : "// " + szMsg + ": " + JSON.stringify(payload)
					});
				} else {
					if (sorted.length == 0) {
						core.tell({
							szRecipient: szSender, 
							szMsg      : core.arrayTemplate(_l.noItemMatching, {
								name: name
							})
						});
						
						var proposal = core.proposeCorrectItem({
							name: name
						});
						
						core.tell({
							szRecipient: szSender, 
							szMsg      : "// Perhaps you meant '" + proposal + "'."
						});
						return;
					}

					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.countResponse, {
							items: sorted.map(_getNameAndCount).join(", ")
						})
					});
				}
			});
		};
	}();

	core.proposeCorrectItem = function proposeCorrectItem(params) {
		var name = params.name;
		var items = core.getItemsForSale();
		var singleNames = {};
		items.forEach(function(aco) {
			var name = core.getFullName(aco);
			if (name.match(/(.*) \((.*)\)/i)) {
				name = RegExp.$1;
			}
			singleNames[name] = true;
		});

		return core.propose({
			dictionary: Object.keys(singleNames),
			word      : name,
			ignoreCase: true
		});
	};

	/*
	 * handleCheckCommand() Handle a 'check <name>' command from a player.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szName - (partial) item name.
	 * @param {string} params.szSender - Sender's name.
	 * @param {string} params.value - (optional) item pyreal value.
	 */
	core.handleCheckCommand = function handleCheckCommand(params) {
		var szSender = params.szSender;
		var szName = params.szName;

		//var value = params.value;

		// Find items that match the name.
		core.getItemsFromChat({
			szName: szName,

			//	value   : value,
			szSender: szSender
		}, onItems);

		function onItems(err, matches) {
			core.debug("<onItems>", "err: " + err, "matches: " + matches);
			if (err) {
				if (err.message == "NO_MATCHES") {
					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.noItemMatching, {
							name: err.szName
						})
					});

					var proposal = core.proposeCorrectItem({
						name: err.szName
					});
					
					core.tell({
						szRecipient: szSender, 
						szMsg      : "// Perhaps you meant '" + proposal + "'."
					});
					
					return;
				} else if (err.message == "TOO_MANY_MATCHES") {
					var matches = err.matches;

					core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.tooManyMatches, {
							name : err.szName,
							count: matches.length
						})
					});
					
					// Max 10 items told to customer.
					//matches.splice(Math.min(10, matches.length));
					
					// Send item IDs and their stats.
					async_js.eachSeries(matches, function each(aco, callback) {
						var stats = err.itemStats[aco.oid];
						core.tell({
							szRecipient: szSender,
							szMsg      : aco.oid + ": " + stats
						});
						
						// Wait a second before sending the next tell, to prevent flooding.
						core.setTimeout(callback, ONE_SECOND);
					}, function(err) {
						core.info("done sending items.", err);
					});
					return;
				}
				
				
				// Unhandled error.
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}
			
			//core.console("onItems", JSON.stringify(matches));
			
			var aco = matches[0];
			var szName = core.getFullName(aco);
			
			// Check the sell value of the item.
			var sellValue = core.getSellValue({
				aco     : aco, 
				szPlayer: szSender
			});
			core.debug("sellValue: " + sellValue);
			
			
			// Check if it's for sale.
			if (typeof sellValue === "undefined") {
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.itemNotForSale, {
						item: szName
					})
				});
			}

			// Add the salvage's workmanship to the string.
			if (aco.mcm == mcmSalvageBag) {
				szName += " [w" + core.round(aco.workmanship, 2) + "]";
			}
			core.debug("szName: " + szName);

			var amount = 1;
			if (aco && (!core.isUnique(aco) || aco.mcm == mcmSalvageBag)) {
				amount = core.getInvRegexCount(core.getFullName(aco));
			}

			core.tell({
				szRecipient: szSender, 
				szMsg      : core.arrayTemplate(_l.itemPriceAmount, {
					item  : szName,
					value : sellValue,
					amount: amount
				})
			});
			
			//if (aco.mcm == mcmSalvageBag) {
			//	core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.salvageWorkmanshipExample)});
			//}

			return;
		}
	};

	core.getItemSpells = function getItemSpells(aco) {
		var spells = [];
		if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid) {
			var spellID, spell;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < aco.oai.iei.cspellid; i++) {
				spellID = aco.oai.iei.rgspellid(i);
				spell = skapi.SpellInfoFromSpellid(spellID);
				spells.push(spell);
			}
		}
		return spells;
	};

	// Convert VBScript dictionary to JS object.
	core.dictionaryToObject = function dictionaryToObject(dictionary) {
		var obj = {};
		var keys = new Enumerator(dictionary);
		var key;
		// eslint-disable-next-line no-restricted-syntax
		for (; !keys.atEnd(); keys.moveNext()) {
			key = keys.item();
			obj[key] = dictionary.Item(key);
		}
		return obj;
	};

	core.isNumeric = function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	};

	core.getMatchingPointProfiles = function factory() {
		return function getMatchingPointProfiles(params, callback) {
			core.debug("<getMatchingPointProfiles>");
			var aco = params.aco;
			core.debug("aco: " + aco);
			
			var keys = Object.keys(config.pointsProfiles);
			return async_js.filterSeries(keys, function(profileName, callback) {
				var info = config.pointsProfiles[profileName];
				if (!info || !info.enabled) return callback(undefined, false);
				var profile = core.LootProfile3.getProfileByName(profileName);
				if (!profile) {
					return callback(undefined, false);
				}
				core.debug("evaluateRulesAsync...");
				return profile.evaluateRulesAsync({
					aco: aco
				}, onEvaled);
				function onEvaled(err, results) {
					if (err) return callback(err);
					callback(undefined, results.match);
				}
			}, callback);
		};
	}();

	core.decimalCounter = function decimalCounter(number) {
		var hasDecimal = /[.,]/gi;
		var numberString = String(number);
		if (!hasDecimal.test(numberString)) {
			return 0;
		}
		var regex = /[.,](\d*)/;
		var _a = numberString.match(regex), result = _a[1];
		return result.length;
	};
	
	var slotNames = core.slotNames = {};
	slotNames[eqmHead] = ["head"];
	slotNames[eqmChestUnder] = ["shirt", "chestunder"];
	slotNames[eqmAbdomenUnder] = ["abdomen"];
	slotNames[eqmHands] = ["hand", "hands"];
	slotNames[eqmUpperLegsUnder] = ["pants"];
	slotNames[eqmFeet] = ["feet"];
	slotNames[eqmChestOuter] = ["chest"];
	slotNames[eqmUpperArmsOuter] = ["arms"];
	slotNames[eqmLowerArmsOuter] = ["lowerarms"];
	slotNames[eqmUpperLegsOuter] = ["legs"];
	slotNames[eqmLowerLegsOuter] = ["lowerlegs"];
	slotNames[eqmNecklace] = ["necklace"];
	slotNames[eqmBracelets] = ["bracelet", "bracelets"];
	slotNames[eqmRings] = ["ring", "rings"];
	slotNames[eqmWeapon] = ["weapon"];
	slotNames[eqmShield] = ["shield"];
	slotNames[eqmRangedWeapon] = ["ranged"];
	slotNames[eqmFocusWeapon] = ["wand"];

	var skillNames = core.skillNames = {};
	skillNames[skidLightWeapons] = ["light"];
	skillNames[skidHeavyWeapons] = ["heavy"];
	skillNames[skidFinesseWeapons] = ["finesse"];
	skillNames[skidTwoHanded] = ["2h", "twohand"];
	skillNames[skidMissileWeapons] = ["missile"];
	skillNames[skidWarMagic] = ["war"];
	skillNames[skidVoidMagic] = ["void"];
	
	var elementNames = core.elementNames = {};
	elementNames[dmtySlashing] = ["slash", "slashing"];
	elementNames[dmtyPiercing] = ["pierce", "piercing"];
	elementNames[dmtyBludgeoning] = ["bludgeon", "bludgeoning"];
	elementNames[dmtyCold] = ["cold"];
	elementNames[dmtyFire] = ["fire"];
	elementNames[dmtyAcid] = ["acid"];
	elementNames[dmtyElectrical] = ["electrical", "light", "lightning"];
	elementNames[ELEMENT_NETHER] = ["nether"];

	var rendNames = core.rendNames = {};
	rendNames[REND_CIRTICAL_STRIKE] = ["cs", "critical strike", "crit"];
	rendNames[REND_CRIPPLING_BLOW] = ["cb", "crippling blow", "crip"];
	rendNames[REND_ARMOR_RENDERING] = ["ar", "armor rendering"];
	rendNames[REND_SLASH] = ["slash"];
	rendNames[REND_PIERCING] = ["pierce", "piercing"];
	rendNames[REND_BLUDGEONING] = ["bludgeon", "bludgeoning"];
	rendNames[REND_ACID] = ["acid"];
	rendNames[REND_COLD] = ["cold"];
	rendNames[REND_ELECTRICAL] = ["electrical", "light", "lightning"];
	rendNames[REND_FIRE] = ["fire"];

	core.levenshtein = function factory() {
		// https://github.com/words/levenshtein-edit-distance/blob/master/license
		var cache = [];
		var codes = [];
		return function levenshtein(value, other, insensitive) {
			var length;
			var lengthOther;
			var code;
			var result;
			var distance;
			var distanceOther;
			var index;
			var indexOther;

			if (value === other) {
				return 0;
			}

			length = value.length;
			lengthOther = other.length;

			if (length === 0) {
				return lengthOther;
			}

			if (lengthOther === 0) {
				return length;
			}

			if (insensitive) {
				value = value.toLowerCase();
				other = other.toLowerCase();
			}

			index = 0;

			// eslint-disable-next-line no-restricted-syntax
			while (index < length) {
				codes[index] = value.charCodeAt(index);
				cache[index] = ++index;
			}

			indexOther = 0;

			// eslint-disable-next-line no-restricted-syntax
			while (indexOther < lengthOther) {
				code = other.charCodeAt(indexOther);
				result = distance = indexOther++;
				index = -1;

				// eslint-disable-next-line no-restricted-syntax
				while (++index < length) {
					distanceOther = code === codes[index] ? distance : distance + 1;
					distance = cache[index];
					cache[index] = result =
						distance > result
							? distanceOther > result
								? result + 1
								: distanceOther
							: distanceOther > distance
								? distance + 1
								: distanceOther;
				}
			}

			return result;
		};
	}();

	core.propose = function factory() {
		// https://github.com/liushuping/propose/blob/master/LICENSE
		return function propose(params) {
			var word = params.word;
			var dictionary = params.dictionary;
			var threshold = (!params || params.threshold === undefined ? 0 : params.threshold);
			var ignoreCase = (!params || params.ignoreCase === undefined ? false : params.ignoreCase);

			var ratio;
			var distance;
			var proposed;
			var max_ratio = 0;

			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < dictionary.length; ++i) {
				if (ignoreCase)
					distance = core.levenshtein(word, dictionary[i], true);
				else
					distance = core.levenshtein(word, dictionary[i]);

				if (distance > word.length)
					ratio = 1 - distance / dictionary[i].length;
				else
					ratio = 1 - distance / word.length;

				//core.console("word: " + word, "dictionary[]: " + dictionary[i], "distance: " + distance, "ratio: " + ratio);

				if (ratio > max_ratio) {
					max_ratio = ratio;
					proposed = dictionary[i];
				}
			}

			if (max_ratio >= threshold)
				return proposed;

			return null;
		};
	}();

	core.sortInventory = function sortInventory(params, callback) {
		var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);

		var executionTime = core.lastExecutionTime;
		

		var acf = skapi.AcfNew();
		acf.olc = olcInventory;
		
		
		var inventory = core.coacoToArray(acf.CoacoGet());
		inventory.sort(core.compare_items);
		inventory.reverse();
		
		var packs = core.getPacks();
		packs.reverse();
		
		async_js.eachSeries(packs, function(acoContainer, callback) {
			var items = inventory.splice(0, acoContainer.citemMax);
			
			verbose(items.length + " items for " + acoContainer.szName, "inventory: " + inventory.length);
			
			async_js.timesSeries(acoContainer.citemMax, function(intSlot, callback) {
				if (executionTime != core.lastExecutionTime) return;
				if (items.length == 0) return callback();
				
				var acoCurrent = acoContainer.AcoFromIitem(intSlot);
				var acoPreferred = items.splice(items.length - 1, 1)[0];

				if (acoCurrent && acoCurrent.oid == acoPreferred.oid) {
					//verbose("Skipping " + core.getFullName(acoPreferred), "container: " + acoContainer.szName, "slot: " + intSlot);
					return core.setTimeout(callback, 0);
				}

				if (!acoCurrent || acoContainer.citemContents < acoContainer.citemMax) {
					verbose("Moving " + core.getFullName(acoPreferred) + " to " + acoContainer.szName + "'s " + intSlot + " slot...");
					return core.moveToContainer({aco: acoPreferred, acoContainer: acoContainer, iitem: intSlot, thisArg: {aco: acoPreferred}}, onMoveToContainer);
				}

				verbose("Moving " + core.getFullName(acoCurrent) + " to main pack...");
				return core.moveToPack({aco: acoCurrent, thisArg: {aco: acoCurrent}}, onMoveToPack);
			
				// callbacks
				function onMoveToContainer(err, results) {
					if (executionTime != core.lastExecutionTime) return;
					if (err) {
						if (err.message == "TOO_BUSY") {
							verbose("Too busy, short delay1...");
							return core.setTimeout(core.moveToContainer, TWO_SECONDS, {aco: acoPreferred, acoContainer: acoContainer, iitem: intSlot, thisArg: {aco: acoPreferred}}, onMoveToContainer);
						} else if (err.message == "MOVE_TO_CONTAINER_TIMED_OUT") {
							verbose("Move to container timed out, trying again...");
							return core.moveToContainer({aco: acoPreferred, acoContainer: acoContainer, iitem: intSlot, thisArg: {aco: acoPreferred}}, onMoveToContainer);
						} else if (err.message == "ITEM_INVALID") {
							core.warn("Moving " + this.aco.szName + " to container threw ITEM_INVALID error. Phantom?");
							core.phantomItems[this.aco.oid] = true;
							return callback(undefined, true);
						}
						return callback(err);
					}
					return callback(undefined, results);
				}
			
				function onMoveToPack(err) {
					if (executionTime != core.lastExecutionTime) return;
					if (err) {
						if (err.message == "TOO_BUSY") {
							verbose("Too busy, short delay2...");
							return core.setTimeout(core.moveToPack, TWO_SECONDS, {aco: acoCurrent, thisArg: {aco: acoCurrent}}, onMoveToPack);
						} else if (err.message == "MOVE_TO_PACK_TIMED_OUT") {
							verbose("Move to pack timed out, trying again...");
							return core.moveToPack({aco: acoCurrent, thisArg: {aco: acoCurrent}}, onMoveToPack);
						} else if (err.message == "ITEM_INVALID") {
							core.warn("Moving " + this.aco.szName + " to pack threw ITEM_INVALID error. Phantom?");
							core.phantomItems[this.aco.oid] = true;
							return callback(undefined, true);
						}
						return callback(err);
					}
					verbose("Moving " + core.getFullName(acoPreferred) + " to " + acoContainer.szName + "'s " + intSlot + " slot...");
					return core.moveToContainer({aco: acoPreferred, acoContainer: acoContainer, iitem: intSlot, thisArg: {aco: acoPreferred}}, onMoveToContainer);
				}
			}, callback);
		}, callback);
	};

	core.getPacks = function factory() {
		return function getPacks() {
			var packs = [];
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < skapi.cpack; i++) {
				packs.push(skapi.AcoFromIpackIitem(i, iitemNil));
			}
			return packs;
		};
	}();

	core.getItemTotalXPToLevel = function getItemTotalXPToLevel(params) {
		core.debug("<getItemTotalXPToLevel>");
		var totalXp = params.totalXp;
		var baseXp = params.baseXp;
		var maxLevel = params.maxLevel;
		var xpStyle = params.xpStyle;

		var level = 0;
		if (xpStyle == 1) { // Fixed
			level = Math.floor(totalXp / baseXp);
		} else if (xpStyle == 2) { // ScalesWithLevel
			var levelXP = baseXp;
			var remainXP = totalXp;
			// eslint-disable-next-line no-restricted-syntax
			while (remainXP >= levelXP) {
				level++;
				remainXP -= levelXP;
				levelXP *= 2;
			}
		} else if (xpStyle == 3) { // FixedPlusBase
			if (totalXp >= baseXp && totalXp < baseXp * 3) {
				level = 1;
			} else {
				level = Math.floor((totalXp - baseXp) / baseXp);
			}
		}
		
		if (level > maxLevel) {
			level = maxLevel;
		}
		
		return level;
	};

	core.getItemLevelToTotalXP = function getItemLevelToTotalXP(params) {
		var itemLevel = params.itemLevel;
		var baseXP = params.baseXP;
		var maxLevel = params.maxLevel;
		var xpStyle = params.xpStyle;
		
		if (itemLevel < 1) {
			return 0;
		}
		
		if (itemLevel > maxLevel) {
			itemLevel = maxLevel;
		}
		
		if (itemLevel == 1) {
			return baseXP;
		}
		
		if (xpStyle == 1) { // Fixed
			return itemLevel * baseXP;
		} else if (xpStyle == 2) { // ScalesWithLevel
			var levelXP = baseXP;
			var totalXP = baseXP;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = itemLevel - 1; i > 0; i--) {
				levelXP *= 2;
				totalXP += levelXP;
			}
			return totalXP;
		} else if (xpStyle == 3) { // FixedPlusBase
			return itemLevel * baseXP + baseXP;
		}
	};

	core.showWeapons = function factory() {
		
		function finesseTab(parent) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: parent, 
					label : "Finesse"
				})
			});
		
			// eslint-disable-next-line no-unused-vars
			var notebook = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "finesseNotebook"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
			var lstItems = new SkunkSchema.List({
				parent: layout, 
				name  : "lstFinesseItems"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // name
				.on("onShow", function() {
					lstItems.clear();
					var items = core.getInventoryItems().filter(function(aco) {
						var skid = aco["oai.ibi.skidWieldReq"] 
							|| aco["oai.iwi.skid"]
							|| aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq
							|| aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						return skid == skidFinesseWeapons;
					});
					items.sort(core.compare_items);
					var tiers = {};
					items.forEach(function(aco) {
						var lvlWieldReq = aco["oai.ibi.lvlWieldReq"] || aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq || 0;
						tiers[lvlWieldReq] = tiers[lvlWieldReq] || [];
						tiers[lvlWieldReq].push(aco);
					});
					var keys = Object.keys(tiers);
					keys.sort();
					keys.forEach(function(lvlWieldReq) {
						lstItems.addRow("", "Tier " + lvlWieldReq);
						var items = tiers[lvlWieldReq];
						items.forEach(addRow);
					});
					
					function addRow(aco) {
						lstItems.addRow(aco.oid, "  " + core.getItemStringSync({aco: aco}));
					}
					core.info("Finesse weapons populated.");
				})
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var oid = row[0];
					if (oid == 0) return;
					
					var aco = skapi.AcoFromOid(oid);
					skapi.SelectAco(aco);
				});
		}
		
		function heavyTab(parent) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: parent, 
					label : "Heavy"
				})
			});
			
			// eslint-disable-next-line no-unused-vars
			var notebook = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "heavyNotebook"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
			var lstItems = new SkunkSchema.List({
				parent: layout, 
				name  : "lstHeavyItems"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // name
				.on("onShow", function() {
					lstItems.clear();
					var items = core.getInventoryItems().filter(function(aco) {
						var skid = aco["oai.ibi.skidWieldReq"] 
							|| aco["oai.iwi.skid"]
							|| aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq
							|| aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						return skid == skidHeavyWeapons;
					});
					items.sort(core.compare_items);
					var tiers = {};
					items.forEach(function(aco) {
						var lvlWieldReq = aco["oai.ibi.lvlWieldReq"] || aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq || 0;
						tiers[lvlWieldReq] = tiers[lvlWieldReq] || [];
						tiers[lvlWieldReq].push(aco);
					});
					var keys = Object.keys(tiers);
					keys.sort();
					keys.forEach(function(lvlWieldReq) {
						lstItems.addRow("", "Tier " + lvlWieldReq);
						var items = tiers[lvlWieldReq];
						items.forEach(addRow);
					});
					function addRow(aco) {
						lstItems.addRow(aco.oid, "  " + core.getItemStringSync({aco: aco}));
					}
					core.info("Heavy weapons populated.");
				})
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var oid = row[0];
					if (oid == 0) return;
					
					var aco = skapi.AcoFromOid(oid);
					skapi.SelectAco(aco);
				});
		}
		
		function lightTab(parent) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: parent, 
					label : "Light"
				})
			});
		
			// eslint-disable-next-line no-unused-vars
			var notebook = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "lightNotebook"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
			var lstItems = new SkunkSchema.List({
				parent: layout, 
				name  : "lstLightItems"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // name
				.on("onShow", function() {
					lstItems.clear();
					var items = core.getInventoryItems().filter(function(aco) {
						var skid = aco["oai.ibi.skidWieldReq"] 
							|| aco["oai.iwi.skid"]
							|| aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq
							|| aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						return skid == skidLightWeapons;
					});
					items.sort(core.compare_items);
					var tiers = {};
					items.forEach(function(aco) {
						var lvlWieldReq = aco["oai.ibi.lvlWieldReq"] || aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq || 0;
						tiers[lvlWieldReq] = tiers[lvlWieldReq] || [];
						tiers[lvlWieldReq].push(aco);
					});
					var keys = Object.keys(tiers);
					keys.sort();
					keys.forEach(function(lvlWieldReq) {
						lstItems.addRow("", "Tier " + lvlWieldReq);
						var items = tiers[lvlWieldReq];
						items.forEach(addRow);
					});
					function addRow(aco) {
						lstItems.addRow(aco.oid, "  " + core.getItemStringSync({aco: aco}));
					}
					core.info("Light weapons populated.");
				})
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var oid = row[0];
					if (oid == 0) return;
					
					var aco = skapi.AcoFromOid(oid);
					skapi.SelectAco(aco);
				});
		}
		
		function missileTab(parent) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: parent, 
					label : "Missile"
				})
			});
			
			// eslint-disable-next-line no-unused-vars
			var notebook = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "missileNotebook"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
			var lstItems = new SkunkSchema.List({
				parent: layout, 
				name  : "lstMissileItems"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // name
				.on("onShow", function() {
					lstItems.clear();
					var items = core.getInventoryItems().filter(function(aco) {
						var skid = aco["oai.ibi.skidWieldReq"] 
							|| aco["oai.iwi.skid"]
							|| aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq
							|| aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						return skid == skidMissileWeapons;
					});
					items.sort(core.compare_items);
					var tiers = {};
					items.forEach(function(aco) {
						var lvlWieldReq = aco["oai.ibi.lvlWieldReq"] || aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq || 0;
						tiers[lvlWieldReq] = tiers[lvlWieldReq] || [];
						tiers[lvlWieldReq].push(aco);
					});
					var keys = Object.keys(tiers);
					keys.sort();
					keys.forEach(function(lvlWieldReq) {
						lstItems.addRow("", "Tier " + lvlWieldReq);
						var items = tiers[lvlWieldReq];
						items.forEach(addRow);
					});
					function addRow(aco) {
						lstItems.addRow(aco.oid, "  " + core.getItemStringSync({aco: aco}));
					}
					core.info("Missile weapons populated.");
				})
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var oid = row[0];
					if (oid == 0) return;
					
					var aco = skapi.AcoFromOid(oid);
					skapi.SelectAco(aco);
				});
		}
		
		function warTab(parent) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: parent, 
					label : "War"
				})
			});
			
			// eslint-disable-next-line no-unused-vars
			var notebook = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "warNotebook"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());

			var lstItems = new SkunkSchema.List({
				parent: layout, 
				name  : "lstWarItems"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // name
				.on("onShow", function() {
					lstItems.clear();
					var items = core.getInventoryItems().filter(function(aco) {
						var skid = aco["oai.ibi.skidWieldReq"] 
							|| aco["oai.iwi.skid"]
							|| aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq
							|| aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						return skid == skidWarMagic;
					});
					items.sort(core.compare_items);
					var tiers = {};
					items.forEach(function(aco) {
						var lvlWieldReq = aco["oai.ibi.lvlWieldReq"] || aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq || 0;
						tiers[lvlWieldReq] = tiers[lvlWieldReq] || [];
						tiers[lvlWieldReq].push(aco);
					});
					var keys = Object.keys(tiers);
					keys.sort();
					keys.forEach(function(lvlWieldReq) {
						lstItems.addRow("", "Tier " + lvlWieldReq);
						var items = tiers[lvlWieldReq];
						items.forEach(addRow);
					});
					function addRow(aco) {
						lstItems.addRow(aco.oid, "  " + core.getItemStringSync({aco: aco}));
					}
				})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var oid = row[0];
					if (oid == 0) return;
					
					var aco = skapi.AcoFromOid(oid);
					skapi.SelectAco(aco);
				});
		}
		
		function voidTab(parent) {
			var layout = new SkunkSchema.FixedLayout({
				parent: new SkunkSchema.Page({
					parent: parent, 
					label : "Void"
				})
			});
			
			// eslint-disable-next-line no-unused-vars
			var notebook = new SkunkSchema.Notebook({
				parent: layout, 
				name  : "voidTab"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight());
				
			var lstItems = new SkunkSchema.List({
				parent: layout, 
				name  : "lstVoidItems"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight())
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: 0}) // oid
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth()}) // name
				.on("onShow", function() {
					lstItems.clear();
					var items = core.getInventoryItems().filter(function(aco) {
						var skid = aco["oai.ibi.skidWieldReq"] 
							|| aco["oai.iwi.skid"]
							|| aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq
							|| aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						return skid == skidVoidMagic;
					});
					items.sort(core.compare_items);
					var tiers = {};
					items.forEach(function(aco) {
						var lvlWieldReq = aco["oai.ibi.lvlWieldReq"] || aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq || 0;
						tiers[lvlWieldReq] = tiers[lvlWieldReq] || [];
						tiers[lvlWieldReq].push(aco);
					});
					var keys = Object.keys(tiers);
					keys.sort();
					keys.forEach(function(lvlWieldReq) {
						lstItems.addRow("", "Tier " + lvlWieldReq);
						var items = tiers[lvlWieldReq];
						items.forEach(addRow);
					});
					function addRow(aco) {
						lstItems.addRow(aco.oid, "  " + core.getItemStringSync({aco: aco}));
					}
					core.info("Void weapons populated.");
				})
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					//var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					var row = control.rows[intRow];
					var oid = row[0];
					if (oid == 0) return;
					
					var aco = skapi.AcoFromOid(oid);
					skapi.SelectAco(aco);
				});
		}
		
		return function showWeapons() {
			var view = core.controls.view = core.createView({
				title : "Weapons", 
				width : 400, 
				height: 300, 
				icon  : 9770
			});
			
			
			var fixedLayout = new SkunkSchema.FixedLayout({parent: view});
			
			var skillNotebook = new SkunkSchema.Notebook({
				parent: fixedLayout, 
				name  : "skillNotebook"
			})
				.setWidth(fixedLayout.getWidth())
				// eslint-disable-next-line no-magic-numbers
				.setHeight(fixedLayout.getHeight() - 20);

			finesseTab(skillNotebook);
			heavyTab(skillNotebook);
			lightTab(skillNotebook);
			missileTab(skillNotebook);
			warTab(skillNotebook);
			voidTab(skillNotebook);
			
			// eslint-disable-next-line no-unused-vars
			var btnQuit = core.controls.btnQuit = new SkunkSchema.PushButton({
				parent: fixedLayout, 
				name  : "btnQuit", 
				text  : "Quit"
			})
				.setAnchor(fixedLayout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				// eslint-disable-next-line no-magic-numbers
				.setWidth(40)
				// eslint-disable-next-line no-unused-vars
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					view.removeControls();
				});

			view.showControls(true);
		};
	}();

	core.getPointsPath = function getPointsPath() {
		if (config.sharePricesPerWorld == true) {
			return _dir + "src\\data\\" + skapi.szWorld + "\\points.json";
		} else {
			return _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\points.json";
		}
	};

	core.getPricesPath = function getPricesPath() {
		if (config.sharePricesPerWorld == true) {
			return _dir + "src\\data\\" + skapi.szWorld + "\\sellingValues.json";
		} else {
			return _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\sellingValues.json";
		}
	};

	core.getSalvagePricesPath = function getSalvagePricesPath() {
		if (config.sharePricesPerWorld == true) {
			return _dir + "src\\data\\" + skapi.szWorld + "\\salvagePrices.json";
		} else {
			return _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\salvagePrices.json";
		}
	};


	core.parseSearchQuery = function parseSearchQuery(query) {
		var results = query.match(/([\+\-]?[a-z]+\:[^ ]+|[\+\-]?"[^"]+"|[\+\-]?[\w\+\-]+)/ig);
		var json = {include: {labels: {}, words: []}, exclude: {labels: {}, words: []}};

		results.forEach(function(result) {
			var regExpLabelMatch = result.match(/([\+\-]?)(.*):(.*)/);
			var reWord = result.match(/([\+|\-]?)("[^"]+"|[\w\+\-]+)/);
			if (regExpLabelMatch) {
				var type = regExpLabelMatch[1] !== '-' ? 'include' : 'exclude';
				var label = regExpLabelMatch[2];
				var value = regExpLabelMatch[3];
				var arr = json[type].labels[label] = json[type].labels[label] || [];
				arr.push(value);
			} else if (reWord) {
				json[(reWord[1] !== '-' ? 'include' : 'exclude')].words.push(reWord[2].replace(/\"([^\"]+)\"/, "$1"));
			}
		});
		return json;
	};
	
	/*
	var json = core.parseSearchQuery('black opal salvage workmanship:5 spell:armor spell:major');
	core.console("json: " + JSON.stringify(json, undefined, "\t"));

	var name = json.include.words.join(" ");
	core.console("name: " + name);
	*/

	core.callTradeAdd = (function factory() {
		// SkunkWorks uses a plugin (skapi) to communicate events and actions to and from the console window that executes the script.
		// Each time SkunkWorks calls aco.AddToTrade(), a message is sent to the plugin to call ACHooks, which is done each frame refresh. If you want to add many items to the trade window, it takes many frames, seeming slow.
		// This workaround spawns a new view frame with a function that accepts a string of item Ids, then calls ACHooks directly.
		// I have however noticed a bug with Windows 10 where the frame containing a script function doesn't get removed from the Decal frame in game. But creating a new frame by the name replaces the old one. =/
		return function callTradeAdd(params) {
			var oids = params.oids;
			var schema = '<view title="ACHooks" width="180" height="50"><script language="JavaScript" src="src/ACHooks.js"/>\
				<control progid="DecalControls.FixedLayout" clipped="0">\
					<control progid="DecalControls.StaticText" name="text" width="200" height="20" text="SkunkTrader helper frame, ignore this" left="0" top="0"/>\
				</control>\
			</view>';
			skapi.ShowControls(schema, false);
			skapi.CallPanelFunction("ACHooks", 'TradeAdd', oids.join(","));
			skapi.RemoveControls("ACHooks");
		};
	})();

	core.callTradeEnd = (function factory() {
		// SkunkWorks uses a plugin (skapi) to communicate events and actions to and from the console window that executes the script.
		// Each time SkunkWorks calls aco.AddToTrade(), a message is sent to the plugin to call ACHooks, which is done each frame refresh. If you want to add many items to the trade window, it takes many frames, seeming slow.
		// This workaround spawns a new view frame with a function that accepts a string of item Ids, then calls ACHooks directly.
		// I have however noticed a bug with Windows 10 where the frame containing a script function doesn't get removed from the Decal frame in game. But creating a new frame by the name replaces the old one. =/
		return function callTradeEnd() {
			/*
			var schema = '<view title="ACHooks" width="180" height="50"><script language="JavaScript" src="src/ACHooks.js"/>\
				<control progid="DecalControls.FixedLayout" clipped="0">\
					<control progid="DecalControls.StaticText" name="text" width="200" height="20" text="SkunkTrader helper frame, ignore this" left="0" top="0"/>\
				</control>\
			</view>';
			skapi.ShowControls(schema, false);
			skapi.CallPanelFunction("ACHooks", 'TradeEnd');
			skapi.RemoveControls("ACHooks");
			*/
			skapi.TradeEnd();
		};
	})();


	//	https://github.com/nepsilon/search-query-parser/blob/master/LICENSE
	core.searchQueryParser = function searchQueryParser(string, options) {

		// Set a default options object when none is provided
		if (!options) {
			options = {
				offsets: true
			};
		} else {
			// If options offsets was't passed, set it to true
			options.offsets = (typeof options.offsets === 'undefined' ? true : options.offsets);
		}

		if (!string) {
			string = '';
		}

		// When a simple string, return it
		if (-1 === string.indexOf(':') && !options.tokenize) {
			return string;
		} else if (!options.keywords && !options.ranges && !options.tokenize) {
			// When no keywords or ranges set, treat as a simple string
			return string;
		} else {
			// Otherwise parse the advanced query syntax
			// Our object to store the query object
			var query = {
				text: []
			};

			// When offsets is true, create their array
			if (options.offsets) {
				query.offsets = [];
			}
			var exclusion = {};
			var terms = [];

			// Get a list of search terms respecting single and double quotes
			var regex = /(\S+:'(?:[^'\\]|\\.)*')|(\S+:"(?:[^"\\]|\\.)*")|(-?"(?:[^"\\]|\\.)*")|(-?'(?:[^'\\]|\\.)*')|\S+|\S+:\S+/g;
			var match;
			while ((match = regex.exec(string)) !== null) {
				var term = match[0];
				var sepIndex = term.indexOf(':');
				if (sepIndex !== -1) {
					//var split = term.split(':'),
					var key = term.slice(0, sepIndex),
						val = term.slice(sepIndex + 1);

					// Strip surrounding quotes
					val = val.replace(/^\"|\"$|^\'|\'$/g, '');

					// Strip backslashes respecting escapes
					val = (String(val)).replace(/\\(.?)/g, function(s, n1) {
						switch (n1) {
						case '\\':
							return '\\';
						case '0':
							return '\u0000';
						case '':
							return '';
						default:
							return n1;
						}
					});
					terms.push({
						keyword    : key,
						value      : val,
						offsetStart: match.index,
						offsetEnd  : match.index + term.length
					});
				} else {
					var isExcludedTerm = false;
					if (term[0] === '-') {
						isExcludedTerm = true;
						term = term.slice(1);
					}

					// Strip surrounding quotes
					term = term.replace(/^\"|\"$|^\'|\'$/g, '');

					// Strip backslashes respecting escapes
					term = (String(term)).replace(/\\(.?)/g, function(s, n1) {
						switch (n1) {
						case '\\':
							return '\\';
						case '0':
							return '\u0000';
						case '':
							return '';
						default:
							return n1;
						}
					});

					if (isExcludedTerm) {
						if (exclusion['text']) {
							if (exclusion['text'] instanceof Array) {
								exclusion['text'].push(term);
							} else {
								exclusion['text'] = [exclusion['text']];
								exclusion['text'].push(term);
							}
						} else {
							// First time seeing an excluded text term
							exclusion['text'] = term;
						}
					} else {
						terms.push({
							text       : term,
							offsetStart: match.index,
							offsetEnd  : match.index + term.length
						});
					}
				}
			}

			// Reverse to ensure proper order when pop()'ing.
			terms.reverse();

			// For each search term
			var term;
			// eslint-disable-next-line no-cond-assign
			while (term = terms.pop()) {
				// When just a simple term
				if (term.text) {
					// We add it as pure text
					query.text.push(term.text);

					// When offsets is true, push a new offset
					if (options.offsets) {
						query.offsets.push(term);
					}
				} else {
					// We got an advanced search syntax
					var key = term.keyword;

					// Check if the key is a registered keyword
					options.keywords = options.keywords || [];
					var isKeyword = false;
					var isExclusion = false;
					if (!/^-/.test(key)) {
						isKeyword = !(-1 === options.keywords.indexOf(key));
					} else if (key[0] === '-') {
						var _key = key.slice(1);
						isKeyword = !(-1 === options.keywords.indexOf(_key));
						if (isKeyword) {
							key = _key;
							isExclusion = true;
						}
					}

					// Check if the key is a registered range
					options.ranges = options.ranges || [];
					var isRange = !(-1 === options.ranges.indexOf(key));

					// When the key matches a keyword
					if (isKeyword) {
						// When offsets is true, push a new offset
						if (options.offsets) {
							query.offsets.push({
								keyword    : key,
								value      : term.value,
								offsetStart: isExclusion ? term.offsetStart + 1 : term.offsetStart,
								offsetEnd  : term.offsetEnd
							});
						}

						var value = term.value;

						// When value is a thing
						if (value.length) {
							// Get an array of values when several are there
							var values = value.split(',');
							if (isExclusion) {
								if (exclusion[key]) {
									// ...many times...
									if (exclusion[key] instanceof Array) {
										// ...and got several values this time...
										if (values.length > 1) {
											// ... concatenate both arrays.
											exclusion[key] = exclusion[key].concat(values);
										} else {
											// ... append the current single value.
											exclusion[key].push(value);
										}
									} else {
										// We saw that keyword only once before
										// Put both the current value and the new
										// value in an array
										exclusion[key] = [exclusion[key]];
										exclusion[key].push(value);
									}
								} else {
									// First time we see that keyword
									// ...and got several values this time...
									if (values.length > 1) {
										// ...add all values seen.
										exclusion[key] = values;
									} else {
										// Got only a single value this time
										// Record its value as a string
										if (options.alwaysArray) {
											// ...but we always return an array if option alwaysArray is true
											exclusion[key] = [value];
										} else {
											// Record its value as a string
											exclusion[key] = value;
										}
									}
								}
							} else {
								// If we already have seen that keyword...
								if (query[key]) {
									// ...many times...
									if (query[key] instanceof Array) {
										// ...and got several values this time...
										if (values.length > 1) {
											// ... concatenate both arrays.
											query[key] = query[key].concat(values);
										} else {
											// ... append the current single value.
											query[key].push(value);
										}
									} else {
										// We saw that keyword only once before
										// Put both the current value and the new
										// value in an array
										query[key] = [query[key]];
										query[key].push(value);
									}
								} else {
									// First time we see that keyword
									// ...and got several values this time...
									if (values.length > 1) {
										// ...add all values seen.
										query[key] = values;
									} else {
										// Got only a single value this time
										if (options.alwaysArray) {
											// ...but we always return an array if option alwaysArray is true
											query[key] = [value];
										} else {
											// Record its value as a string
											query[key] = value;
										}
									}
								}
							}
						}
					} else if (isRange) {
						// The key allows a range
						// When offsets is true, push a new offset
						if (options.offsets) {
							query.offsets.push(term);
						}

						var value = term.value;

						// Range are separated with a dash
						var rangeValues = value.split('-');

						// When both end of the range are specified
						// keyword:XXXX-YYYY
						query[key] = {};
						if (2 === rangeValues.length) {
							query[key].from = rangeValues[0];
							query[key].to = rangeValues[1];
						} else if (!rangeValues.length % 2) {
							// When pairs of ranges are specified
							// keyword:XXXX-YYYY,AAAA-BBBB
						} else {
						// When only getting a single value,
						// or an odd 	er of values
							query[key].from = value;
						}
					} else {
						// We add it as pure text
						var text = term.keyword + ':' + term.value;
						query.text.push(text);

						// When offsets is true, push a new offset
						if (options.offsets) {
							query.offsets.push({
								text       : text,
								offsetStart: term.offsetStart,
								offsetEnd  : term.offsetEnd
							});
						}
					}
				}
			}

			// Concatenate all text terms if any
			if (query.text.length) {
				if (!options.tokenize) {
					query.text = query.text.join(' ').trim();
				}
			} else {
				// Just remove the attribute text when it's empty
				delete query.text;
			}

			// Return forged query object
			query.exclude = exclusion;
			return query;
		}

	};

	core.getItemsMatchingSearchString = (function factory() {
		function _nameMatch(aco) {
			var value = this.value;
			var verbose = (!params || this.verbose === undefined ? core.debug : this.verbose);
			
			var regExp = new RegExp(core.escapeRegExp(value), "i");
			var result = core.getFullName(aco).match(regExp);
			//verbose("<_nameMatch> regExp: " + regExp + ", result: " + result);
			return result;
		}

		function _spellNameMatch(spell) {
			var regExp = this.regExp;
			return spell.szName.match(regExp);
		}
		function _spellMatch(aco) {
			var value = this.value;
			var regExp = new RegExp(value, "i");
			var spells = core.getItemSpells(aco);
			return spells.some(_spellNameMatch, {regExp: regExp});
		}
	
		function _matchWorkmanshipFrom(aco) {
			var value = this.value;
			var workmanship = core.getWorkmanshipSync({aco: aco});
			return workmanship >= value;
		}
		function _matchWorkmanshipTo(aco) {
			var value = this.value;
			var workmanship = core.getWorkmanshipSync({aco: aco});
			return workmanship <= value;
		}
		
		function _matchWieldExact(aco) {
			var value = this.value;
			var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq;
			return itemValue == value;
		}
		function _matchWieldFrom(aco) {
			var value = this.value;
			var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq;
			return itemValue >= value;
		}
		function _matchWieldTo(aco) {
			var value = this.value;
			var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.lvlWieldReq;
			return itemValue <= value;
		}
	
		function _matchValueExact(aco) {
			var value = this.value;
			var itemValue = aco.cpyValue;
			return itemValue == value;
		}
		function _matchValueFrom(aco) {
			var value = this.value;
			var itemValue = aco.cpyValue;
			return itemValue >= value;
		}
		function _matchValueTo(aco) {
			var value = this.value;
			var itemValue = aco.cpyValue;
			return itemValue <= value;
		}
	
		function _matchEqm(aco) {
			var eqm = this.eqm;
			return aco.eqm & eqm;
		}
	
		function _valueMatchTargetLowerCase(value) {
			var target = this.target;
			return value.toLowerCase() == target.toLowerCase();
		}
	
		function _findEqmMatch(eqm) {
			var target = this.target;
			return core.slotNames[eqm].find(_valueMatchTargetLowerCase, {target: target});
		}
	
		function _findSkidMatch(skid) {
			var target = this.target;
			return core.skillNames[skid].find(_valueMatchTargetLowerCase, {target: target});
		}
	
		function _findRendMask(key) {
			var target = this.target;
			return core.rendNames[key].find(_valueMatchTargetLowerCase, {target: target});
		}
	
		function _filterBySet(aco) {
			var target = this.target;
			var itemSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
			var setName = itemSet && core.setNames[itemSet];
			if (!setName) return false;
			var regExp = new RegExp(core.escapeRegExp(target), "i");
			return setName.match(regExp);
		}
	
		function _acoInMatches(aco) {
			return this.matches[aco.oid] == true;
		}
	
		function _filterNameAndValue(aco) {
			if (aco.cpyValue != this.value) return false;
			var regExp = new RegExp(core.escapeRegExp(this.name), "i");
			return core.getFullName(aco).match(regExp);
		}
	
		return function getItemsMatchingSearchString(params) {
			var string = params.string;
			var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
			
			var parsed = core.searchQueryParser(string, {keywords: ['spell', 'slot', 'skill', 'rend', 'set'], ranges: ['workmanship', 'wield', 'value'], offsets: false});
			verbose("parsed: " + JSON.stringify(parsed));
			
			var items = core.getItemsForSale();
			verbose("getItemsForSale: " + items.length);
			
			
			if (typeof parsed === "string") {
				return items.filter(_nameMatch, {value: parsed, parsed: parsed});
			}

			if (typeof parsed.text === "string") {
				var parts = parsed.text.split(":");
				if (parts.length > 1) {
					var name = parts[0];
					var value = Number(parts[1]);
					items = items.filter(_filterNameAndValue, {name: name, value: value});
				} else {
					items = items.filter(_nameMatch, {value: parsed.text});
				}
			}

			if (typeof parsed.workmanship === "object") {
				var from = parsed.workmanship.from;
				if (core.isNumeric(from)) {
					from = Number(from);
					items = items.filter(_matchWorkmanshipFrom, {value: from});
				}
				var to = parsed.workmanship.to;
				verbose("to: " + to, core.isNumeric(to));
				if (core.isNumeric(to)) {
					to = Number(to);
					items = items.filter(_matchWorkmanshipTo, {value: to});
				}
			}

			if (typeof parsed.wield === "object") {
				var from = parsed.wield.from;
				var to = parsed.wield.to;
				if (core.isNumeric(from) && !core.isNumeric(to)) {
					items = items.filter(_matchWieldExact, {value: Number(from)});
				} else {
					if (core.isNumeric(from)) {
						items = items.filter(_matchWieldFrom, {value: Number(from)});
					}
					if (core.isNumeric(to)) {
						items = items.filter(_matchWieldTo, {value: Number(to)});
					}
				}
			}
			
			if (typeof parsed.value === "object") {
				var from = parsed.value.from;
				var to = parsed.value.to;
				if (core.isNumeric(from) && !core.isNumeric(to)) {
					items = items.filter(_matchValueExact, {value: Number(from)});
				} else {
					if (core.isNumeric(from)) {
						items = items.filter(_matchValueFrom, {value: Number(from)});
					}
					if (core.isNumeric(to)) {
						items = items.filter(_matchValueTo, {value: Number(to)});
					}
				}
			}

			if (typeof parsed.slot === "string") {
				var eqm = Object.keys(core.slotNames).find(_findEqmMatch, {target: parsed.slot});
				if (eqm) {
					items = items.filter(_matchEqm, {eqm: eqm});
					
				}
			} else if (core.isArray(parsed.slot)) {
				var matches = {};
				parsed.slot.forEach(function(slot) {
					var eqm = Object.keys(core.slotNames).find(_findEqmMatch, {target: slot});
					if (eqm) {
						items.forEach(function(aco) {
							if (aco.eqm & eqm) {
								matches[aco.oid] = true;
							}
						});
					}
				});
				items = items.filter(_acoInMatches, {matches: matches});
			}
			
			if (typeof parsed.skill === "string") {
				var skid = Object.keys(core.skillNames).find(_findSkidMatch, {target: parsed.skill});
				if (skid) {
					items = items.filter(function(aco) {
						var itemSkill;
						if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq) {
							itemSkill = aco.oai.ibi.skidWieldReq;
						} else if (aco.oai && aco.oai.iwi && aco.oai.iwi.skid) {
							itemSkill = aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
						}
						return itemSkill == skid;
					});
				}
			} else if (core.isArray(parsed.skill)) {
				var matches = {};
				parsed.skill.forEach(function(skill) {
					var skid = Object.keys(core.skillNames).find(_findSkidMatch, {target: skill});
					if (skid) {
						items.forEach(function(aco) {
							var itemSkill;
							if (aco.oai && aco.oai.ibi && aco.oai.ibi.skidWieldReq) {
								itemSkill = aco.oai.ibi.skidWieldReq;
							} else if (aco.oai && aco.oai.iwi && aco.oai.iwi.skid) {
								itemSkill = aco.oai && aco.oai.iwi && aco.oai.iwi.skid;
							}
							if (itemSkill == skid) {
								matches[aco.oid] = true;
							}
						});
					}
				});
				items = items.filter(_acoInMatches, {matches: matches});
			}
			
			if (typeof parsed.rend === "string") {
				var rendMask = Object.keys(core.rendNames).find(_findRendMask, {target: parsed.rend});
				items = items.filter(function(aco) {
					var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_IMBUED);
					return itemValue == rendMask;
				});
			} else if (core.isArray(parsed.rend)) {
				var matches = {};
				parsed.rend.forEach(function(rend) {
					var rendMask = Object.keys(core.rendNames).find(_findRendMask, {target: rend});
					items.forEach(function(aco) {
						var itemValue = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_IMBUED);
						if (itemValue == rendMask) {
							matches[aco.oid] = true;
						}
					});
				});
				items = items.filter(_acoInMatches, {matches: matches});
			}
			
			if (typeof parsed.set === "string") {
				items = items.filter(_filterBySet, {target: parsed.set});
			} else if (core.isArray(parsed.set)) {
				var matches = {};
				parsed.set.forEach(function(set) {
					items.forEach(function(aco) {
						var itemSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
						var setName = itemSet && core.setNames[itemSet];
						if (!setName) return false;
						var regExp = new RegExp(core.escapeRegExp(set), "i");
						if (setName.match(regExp)) {
							matches[aco.oid] = true;
						}
					});
				});
				items = items.filter(_acoInMatches, {matches: matches});
			}
			
			if (typeof parsed.spell === "string") {
				items = items.filter(_spellMatch, {value: parsed.spell});
			} else if (core.isArray(parsed.spell)) {
				parsed.spell.forEach(function(spell) {
					items = items.filter(_spellMatch, {value: spell});
				});
			}

			return items;
		};
	})();

	core.getItemInvInfo = (function factory() {
		var cache = {};
		return function getItemInvInfo(params) {
			var oid = params.oid;
			if (typeof cache[oid] !== "undefined") {
				return cache[oid];
			}

			var inventoryInfo = core.getJsonFile({
				path: _dir + "src\\data\\" + skapi.szWorld + "\\" + skapi.acoChar.szName + "\\inventoryInfo.json"
			}) || {};
			
			cache[oid] = inventoryInfo[oid];
			return cache[oid];
		};
	})();

	var attackTypes = core.attackTypes = {};
	attackTypes.Punch               = 0x0001;
	attackTypes.Thrust              = 0x0002;
	attackTypes.Slash               = 0x0004;
	attackTypes.Kick                = 0x0008;
	attackTypes.OffhandPunch        = 0x0010;
	attackTypes.DoubleSlash         = 0x0020;
	attackTypes.TripleSlash         = 0x0040;
	attackTypes.DoubleThrust        = 0x0080;
	attackTypes.TripleThrust        = 0x0100;
	attackTypes.OffhandThrust       = 0x0200;
	attackTypes.OffhandSlash        = 0x0400;
	attackTypes.OffhandDoubleSlash  = 0x0800;
	attackTypes.OffhandTripleSlash  = 0x1000;
	attackTypes.OffhandDoubleThrust = 0x2000;
	attackTypes.OffhandTripleThrust = 0x4000;
	attackTypes.Unarmed             = attackTypes.Punch | attackTypes.Kick | attackTypes.OffhandPunch;
	attackTypes.DoubleStrike        = attackTypes.DoubleSlash | attackTypes.DoubleThrust | attackTypes.OffhandDoubleSlash | attackTypes.OffhandDoubleThrust;
	attackTypes.TripleStrike        = attackTypes.TripleSlash | attackTypes.TripleThrust | attackTypes.OffhandTripleSlash | attackTypes.OffhandTripleThrust;
	attackTypes.Offhand             = attackTypes.OffhandThrust | attackTypes.OffhandSlash | attackTypes.OffhandDoubleSlash | attackTypes.OffhandTripleSlash | attackTypes.OffhandDoubleThrust | attackTypes.OffhandTripleThrust;
	attackTypes.Thrusts             = attackTypes.Thrust | attackTypes.DoubleThrust | attackTypes.TripleThrust | attackTypes.OffhandThrust | attackTypes.OffhandDoubleThrust | attackTypes.OffhandTripleThrust;
	attackTypes.Slashes             = attackTypes.Slash | attackTypes.DoubleSlash | attackTypes.TripleSlash | attackTypes.OffhandSlash | attackTypes.OffhandDoubleSlash | attackTypes.OffhandTripleSlash;
	attackTypes.Punches             = attackTypes.Punch | attackTypes.OffhandPunch;
	attackTypes.MultiStrike         = attackTypes.DoubleStrike | attackTypes.TripleStrike;

	core.getDamageMin = function getDamageMin(aco) {
		var max = core.getDamageMax(aco);
		var scaleDamageRange = aco["oai.iwi.scaleDamageRange"] || aco.oai && aco.oai.iwi && aco.oai.iwi.scaleDamageRange || 0;
		return max - (scaleDamageRange * max);
	};

	core.getDamageMax = function getDamageMax(aco) {
		//core.debug("<getDamageMax> " + aco);
		var dhealth = aco["oai.iwi.dhealth"] || aco.oai && aco.oai.iwi && aco.oai.iwi.dhealth || 0;
		var spells = core.getItemSpells(aco);
		if (spells) {
			spells.forEach(function(spell) {
				if (spell.szName.match("Legendary Blood Thirst")) {
					dhealth += SPELL_LEGENDARY_BLOOD_THIRST_BONUS;
				} else if (spell.szName.match("Epic Blood Thirst")) {
					dhealth += SPELL_EPIC_BLOOD_THIRST_BONUS;
				} else if (spell.szName.match("Major Blood Thirst")) {
					dhealth += SPELL_MAJOR_BLOOD_THIRST_BONUS;
				} else if (spell.szName.match("Minor Blood Thirst")) {
					dhealth += SPELL_MINOR_BLOOD_THIRST_BONUS;
				}
			});
		}
		
		var attackType = aco["oai.ibi.raw.1002F"] || aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ATTACK_TYPE) || 0;
		if (attackType & attackTypes.TripleStrike) {
			dhealth = dhealth * 3;
		} else if (attackType & attackTypes.DoubleStrike) {
			dhealth = dhealth * 2;
		}
		
		return dhealth;
	};

	core.getDamageAvg = function getDamageAvg(aco) {
		var max = core.getDamageMax(aco);
		var scaleDamageRange = aco["oai.iwi.scaleDamageRange"] || aco.oai && aco.oai.iwi && aco.oai.iwi.scaleDamageRange || 0;
		var min = max - (scaleDamageRange * max);
		return (max + min) / 2;
	};

	core.getScalePvMElemBonus = function getScalePvMElemBonus(aco) {
		var value = 0;
		if (typeof aco["oai.iwi.scalePvMElemBonus"] !== "undefined") {
			value = aco["oai.iwi.scalePvMElemBonus"];
		} else if (aco.oai && aco.oai.iwi) {
			value = aco.oai.iwi.scalePvMElemBonus;
		}

		if (aco.oai && aco.oai.iei) {
			var spell, szName;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < aco.oai.iei.cspellid; i++) {
				var spell = skapi.SpellInfoFromSpellid(i);
				if (!spell) continue;
				szName = spell.szName;
				if (szName.match("Legendary Spirit Thirst")) {
					value += SPELL_LEGENDARY_SPIRIT_THRIST_BONUS;
				} else if (szName.match("Epic Spirit Thirst")) {
					value += SPELL_EPIC_SPIRIT_THRIST_BONUS;
				} else if (szName.match("Major Spirit Thirst")) {
					value += SPELL_MAJOR_SPIRIT_THRIST_BONUS;
				} else if (szName.match("Minor Spirit Thirst")) {
					value += SPELL_MINOR_SPIRIT_THRIST_BONUS;
				}
			}
		}
		return value;
	};

	core.getDefenseBonus = function getDefenseBonus(aco) {
		var value = 0;
		if (typeof aco["oai.iwi.scaleDefenseBonus"] !== "undefined") {
			value = aco["oai.iwi.scaleDefenseBonus"];
		} else if (aco.oai && aco.oai.iwi) {
			value = aco.oai.iwi.scaleDefenseBonus;
		}

		if (aco.oai && aco.oai.iei) {
			var spell, szName;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < aco.oai.iei.cspellid; i++) {
				var spell = skapi.SpellInfoFromSpellid(i);
				if (!spell) continue;
				szName = spell.szName;
				
				if (szName.match("Legendary Defender")) {
					value += SPELL_LEGENDARY_DEFENDER_BONUS;
					break;
				} else if (szName.match("Epic Defender")) {
					value += SPELL_EPIC_DEFENDER_BONUS;
					break;
				} else if (szName.match("Major Defender")) {
					value += SPELL_MAJOR_DEFENDER_BONUS;
					break;
				} else if (szName.match("Minor Defender")) {
					value += SPELL_MINOR_DEFENDER_BONUS;
					break;
				}
			}
		}
		return value;
	};


	core.getMissileElemBonus = function getMissileElemBonus(aco) { // dhealthElemBonus
		var value = 0;
		if (typeof aco["oai.iwi.dhealthElemBonus"] !== "undefined") {
			value = aco["oai.iwi.dhealthElemBonus"];
		} else if (aco.oai && aco.oai.iwi) {
			value = aco.oai.iwi.dhealthElemBonus;
		}

		if (aco.oai && aco.oai.iei) {
			var spell, szName;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < aco.oai.iei.cspellid; i++) {
				var spell = skapi.SpellInfoFromSpellid(i);
				if (!spell) continue;
				szName = spell.szName;
				if (szName.match("Legendary Blood Thirst")) {
					value += SPELL_LEGENDARY_BLOOD_THIRST_BONUS;
				} else if (szName.match("Epic Blood Thirst")) {
					value += SPELL_EPIC_BLOOD_THIRST_BONUS;
				} else if (szName.match("Major Blood Thirst")) {
					value += SPELL_MAJOR_BLOOD_THIRST_BONUS;
				} else if (szName.match("Minor Blood Thirst")) {
					value += SPELL_MINOR_BLOOD_THIRST_BONUS;
				}
			}
		}
		return value;
	};

	core.inscribeAetheria = function inscribeAetheria(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		var items = core.getInventoryItems().filter(function(aco) {
			return (aco.eqm & (EQM_AETHERIA_BLUE | EQM_AETHERIA_YELLOW | EQM_AETHERIA_RED));
		});
		
		logger("We have " + items.length + " aetheria.");
		if (items.length == 0) {
			return callback(undefined, true);
		}

		var assess = items.filter(function(aco) {
			return !aco.oai.fSuccess;
		});
		
		logger("We need to assess " + assess.length + " items.");
		
		return async_js.eachSeries(assess, function each(aco, callback) {
			core.assessAco({aco: aco}, callback);
		}, onAssessed);
		
		function onAssessed(err) {
			if (err) return callback(err);
			logger("Finished assessing items, checking inscriptions...");

			
			//logger();
			
			var inscriptions = items.map(function(aco) {
				if (!aco.fExists) return;
				
				var szInscription = aco.oai && aco.oai.ibi && aco.oai.ibi.szInscription;
				var szInscriber = aco.oai && aco.oai.ibi && aco.oai.ibi.szInscriber;
				
				//logger(i, szInscriber, szInscription);
				
				if (szInscriber && szInscriber != skapi.acoChar.szName) return;
				
				var string = 'add ' + core.getFullName(aco);

				var spells = core.getAcoSpells({aco: aco}).map(function(spellid) {
					return skapi.SpellInfoFromSpellid(spellid);
				});
				
				spells.forEach(function(spell) {
					if (spell.szName.match(/Surge of (.*)/i)) {
						var text = RegExp.$1;
						string += " spell:" + text;
					}
				});
				
				//core.setNames
				var itemSet = aco.oai && aco.oai.ibi && aco.oai.ibi.raw.Item(RAW_ITEMSET);
				var setName = itemSet && core.setNames[itemSet];
				
				if (setName && setName.match(/Sigil of (.*)/i)) {
					var text = RegExp.$1;
					string += " set:" + text;
				}
				
				string += "\nadd " + aco.oid;
				
				if (string == szInscription) return;
				
				return {
					aco   : aco, 
					string: string
				};
			}).filter(function(info) {
				return info != null;
			});
			
			//logger("inscriptions: " + inscriptions.length);
			
			logger("We should inscribe " + inscriptions.length + " items.");
			if (inscriptions.length == 0) return callback();
		
			if (!core.layoutLoaded) {
				core.loadUILayout();
			}

			return async_js.eachSeries(inscriptions, function each(info, callback) {
				var aco = info.aco;
				var string = info.string;
				
				logger("Examining " + aco.szName + " (" + aco.oid + ")...");
				return core.examineItem({
					logger: logger,
					aco   : aco
				}, function onAssess(err) {
					if (err) return callback(err);
					
					SkunkLayout.inscriptionBox.click();
					SkunkLayout.clearText();
					skapi.PasteSz(string);
					skapi.Keys("{shift}", kmoUp);	// Try to unshift so we don't Shift-Esc logout accidentally
					skapi.Keys(cmidEscapeKey);

					// Assess again to update the aco object.
					core.assessAco({aco: aco}, callback);
				});
			}, onInscribed);
		}
		
		function onInscribed(err) {
			if (err) return callback(err);
			callback(undefined, true);
		}
	};
	
	core.examineItem = function examineItem(params, callback) {
		//var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		
		function finish() {
			if (handler) core.removeHandler(evidNil, handler);
			if (tid) core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnAssessItem = function OnAssessItem(aco2) {
			finish(undefined, (aco2.oid == aco.oid));
		};
		core.addHandler(evidOnAssessItem, handler);

		SkunkLayout.examineCloseButton.click();
		skapi.SelectAco(aco);
		skapi.Keys(cmidExamineSelected);

		var tid = core.setTimeout(core.timedOut, ONE_SECOND * 5, finish, "EXAMINE_ITEM_TIMED_OUT");
	};
	
	function getSpellIdsOnItems(params) {
		//var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var items = params.items;

		var spellIds = {};
		items.forEach(function(aco) {
			var ids = core.getAcoSpells({aco: aco});

			//core.console("ids: " + ids.join());
			ids.forEach(function(id) {
				spellIds[id] = true;
			});
		});
		return Object.keys(spellIds);
	}
	
	core.exportInventoryHTML = function exportInventoryHTML() {
		
		var inventory = core.getInventoryItems();
		
		core.info("inventory: " + inventory.length);
		
		core.info("Assessing items...");
		
		core.assessItems({items: inventory}, function(err, items) {
			if (err) return core.warn("Error assessing items: " + err);
			var spellIds = getSpellIdsOnItems({items: items});

			//core.info("spells: " + spells.join());
			core.info("Prepping item data...");
			
			var spellData = {};
			
			spellIds.forEach(function(id) {
				var spell = skapi.SpellInfoFromSpellid(id);
				spellData[id] = {
					szName: spell.szName,
					szDesc: spell.szDesc
				};
			});
			
			var itemPrices = {};
			items.forEach(function(aco) {
				var sellValue = core.getSellValue({
					aco: aco
				});
				itemPrices[aco.oid] = sellValue;
			});

			items.sort(core.compare_items);
			var flattened = core.getFlattenItems({
				items: items
			});
			
			core.info("Writing html...");
			var template = core.readFileSync("./src/resources/inventory.template.html");
			var html = core.stringTemplate(template, {
				title: "Inventory of " + skapi.szWorld + "-" + skapi.acoChar.szName,
				
				// Put each item on its own line so user can manually remove items in notepad.
				items: "[\n" + flattened.map(function(aco) {
					return JSON.stringify(aco);
				}).join(",\n") + "\n]",
				spellData : JSON.stringify(spellData),
				itemPrices: JSON.stringify(itemPrices)
			});
			
			var exportPath = "./exports/Inventory of " + skapi.szWorld + "-" + skapi.acoChar.szName + ".html";
			core.writeFileSync(exportPath, html);
			core.info("Saved inventory to " + exportPath);
		});
	};

	core.getInventoryRegex = function getInventoryRegex(regExp) {
		return core.getInventoryItems().filter(function(aco) {
			var szFull = core.getFullName(aco);
			return szFull && szFull.match(regExp) !== null;
		});
	};

	core.dropExcessItems = function dropExcessItems(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var dropItems = config.dropItems.slice().filter(function(value) {
			return value.enabled;
		});

		async_js.eachSeries(dropItems, function each(item, callback) {
			var items, count;
			async_js.whilst(
				function test() { 
					items = core.getInventoryRegex(new RegExp(core.escapeRegExp(item.itemName), "i"));
					count = items.reduce(function(tally, aco) {
						return tally += aco.citemStack;
					}, 0);

					//logger("itemName: " + item.itemName + ", items: " + items.length, "count: " + count);
					return count > item.keepAmount; 
				},
				function iter(callback) {
					var remaining = count - item.keepAmount;
					logger("items: " + items.length, "count: " + count, "keepAmount: " + item.keepAmount, "remaining: " + remaining);
					
					
					items.sort(function(a, b) {
						if (a.workmanship < b.workmanship) return -1;
						if (a.workmanship > b.workmanship) return 1;
						return 0;
					});
					
					var aco = items[0];
					
					if (aco.citemStack <= remaining) {
						core.info("We have " + count + " items matching '" + item.itemName + "' (" + count + ">" + item.keepAmount + "). Dropping " + core.getFullName(aco) + "...");
						return core.dropItem({
							aco: aco
						}, callback);
					}
					
					core.info("We have " + count + " items matching '" + item.itemName + "' (" + count + ">" + item.keepAmount + "). Dropping " + core.getFullName(aco) + " * " + remaining + "...");
					var splitParams = {aco: aco, quantity: remaining, logger: logger};
					return core.splitAco(splitParams, function onSplit(err, acoSplit) {
						if (err) {
							if (err.message == "TOO_BUSY_ONE_AT_A_TIME") return core.splitAco(splitParams, onSplit);
							return callback(err);
						}
						if (acoSplit.citemStack > remaining) {
							return callback(new core.Error("BAD_SPLIT"));
						}
						return core.dropItem({
							aco: acoSplit
						}, callback);
					});
				},
				callback);
		}, callback);
	};

	core.addItemSetToTrade = function addItemSetToTrade(params) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var setName = params.setName;
		var szSender = params.szSender;

		var setItems = core.getEnabledItemSetItems({
			setName: setName
		});
		
		if (setItems.length == 0) {
			return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.noItemEnabledSet)});
		}

		var item, matches, count;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < setItems.length; i++) {
			item = setItems[i];
			matches = core.getSellableItemMatch({
				szName: item.itemName,
				oid   : item.oid,
				logger: logger
			});
			count = matches.reduce(function(tally, aco) {
				return tally += aco.citemStack;
			}, 0);
			
			logger("matches: " + matches.length, "count: " + count, "quantity: " + item.quantity);
			if (count < item.quantity) {
				return core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.outOfItemSet, {name: item.itemName})});
			}
		}
		core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addingItemToTrade, {name: setName, count: setItems.length})});

		var lastTradeResetTime = core.lastTradeResetTime;
		async_js.eachSeries(setItems, function each(item, callback) {
			var matches = core.getSellableItemMatch({
				szName: item.itemName,
				oid   : item.oid
			});

			if (matches.length == 0) return callback(new core.Error("NOT_ENOUGH_AVAILABLE"));
			var aco = matches[0];
			
			var password = core.getSellPassword({
				aco: aco
			});
			
			if (typeof password !== "undefined") {
				core.info("Asking for password (" + password + ")...");
				
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.requestPassword, {item: aco.szName})});
				
				core.waitForPassword({
					szPlayer: szSender,
					password: password
				}, function onPassword(err) {
					if (err) return callback(err);

					// Password was correct, add item to trade window.
					core.addMatchToTradeWindow({
						matches : matches,
						szSender: szSender,
						amount  : item.quantity,
						szName  : item.itemName,
						silent  : true,
						skipRestrictedSaleCheck: true
					}, callback);
				});

			} else {
				// Add items to trade window.
				core.addMatchToTradeWindow({
					matches : matches,
					szSender: szSender,
					amount  : item.quantity,
					szName  : item.itemName,
					silent  : true,
					skipRestrictedSaleCheck: true
				}, callback);
			}

		}, function finished(err) {
			if (err) {
				skapi.TradeReset();
				if (err.message == "RESTRICTED_STACK_COUNT") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.restrictedStackableSale, {
							item: err.item
						})
					});
				} else if (err.message == "NOT_ENOUGH_AVAILABLE") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.noMoreItemAvailable, {
							item: err.szName
						})
					});
				} else if (err.message == "FAILED_TO_SPLIT") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.failedToSplitItem, {
							item: err.item
						})
					});
				} else if (err.message == "RESTRICTED_INDIVIDUAL_SALE") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.individualSaleRestricted, {
							item   : err.item,
							setName: err.setName
						})
					});
					
				} else if (err.message == "INCORRECT_PASSWORD") {
					return core.tell({
						szRecipient: szSender, 
						szMsg      : core.arrayTemplate(_l.incorrectPassword, {
							provided: err.provided
						})
					});
				}

				// Unhandled error.
				return core.tell({
					szRecipient: szSender, 
					szMsg      : core.arrayTemplate(_l.errorOccurred, {
						message: err.message
					})
				});
			}

			// Check if trade window reset awhile we were adding items.
			if (lastTradeResetTime != core.lastTradeResetTime) {
				core.warn("Failed to add " + setName + "'s items to the trade window!");
				core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.failedToAddItem, {item: setName})});
				skapi.TradeReset();
				return;
			}

			core.tell({szRecipient: szSender, szMsg: core.arrayTemplate(_l.addedSetToTrade, {name: setName})});
		});
		
	};

	core.showSelectProfileView = function showSelectProfileView(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		
		function finish() {
			skapi.RemoveControls(viewTitle);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var viewTitle = "Config profile";
		var Element = SkunkSchema11.Element;
		var schema = Element("view", {title: viewTitle, icon: 1234, width: 200, height: 150}, [
			Element("control", {progid:'DecalControls.FixedLayout'}, [
				Element("control", {progid:'DecalControls.StaticText',  width: 100, height: 20, text: "Selected Profile: "}),
				Element("control", {progid:'DecalControls.Choice',      width: 100, height: 20, top: 0, left: 100, name: "choProfiles", selected: 0}, function(parent) {
					var children = [];
					children.push(Element("option", {text: "Select..."}));
					var selected = 0;
					var profiles = core.skunkConfig.getProfiles();
					profiles.forEach(function(profile, i) {
						children.push(Element("option", {text: profile}));
						options.push(profile);
						if (profile == core.skunkConfig._keyProfile) {
							selected = i + 1;
						}
					});
					parent.attrs.selected = selected;
					return children;
				}),

				Element("control", {progid:'DecalControls.PushButton', name: "btnNewProfile",    width: 100, height: 20, top: 20, left: 100, text: "New Profile"}),
				Element("control", {progid:'DecalControls.PushButton', name: "btnDeleteProfile", width: 100, height: 20, top: 40, left: 100, text: "Delete Profile"}),

				Element("control", {progid:'DecalControls.PushButton', name: "btnCancel", width: 100, height: 20, top: 100, left: 0, text: "Cancel"}),
				Element("control", {progid:'DecalControls.PushButton', name: "btnSave", width: 100, height: 20, top: 100, left: 100, text: "Save"})
			])
		]);

		var options = ["Selected..."];
		function refreshProfileMenu() {
			options = ["Selected..."];
			skapi.GetControlProperty(viewTitle, "choProfiles", "Clear()");
			skapi.GetControlProperty(viewTitle, "choProfiles", "AddChoice('Select...')");
			var profiles = core.skunkConfig.getProfiles();
			var selected = 0;
			profiles.forEach(function(profile, i) {
				skapi.GetControlProperty(viewTitle, "choProfiles", "AddChoice('" + profile + "')");
				options.push(profile);
				if (profile == core.skunkConfig._keyProfile) {
					selected = i + 1;
				}
			});
			skapi.SetControlProperty(viewTitle, "choProfiles", "selected", selected);
		}
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			logger("<OnControlEvent>", szPanel, szControl, dictSzValue(szControl));
			if (szPanel != viewTitle) return;
			if (szControl == "btnNewProfile") {
				SkunkSchema.showEditPrompt({
					title: "Profile name",
					value: ""
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking for point value", err);
					if (results == "") return core.warn("Name cannnot be blank.");
					core.skunkConfig.createProfile(results, defaultConfig);
					refreshProfileMenu();
				});
			} else if (szControl == "btnDeleteProfile") {
				return SkunkSchema11.getControlPropertyAsync({
					panelName: viewTitle,
					controlName: "choProfiles",
					property: "selected"
				}, function(err, results) {
					if (err) finish(err);
					var name = options[results];
					core.info("results: " + results, "name: " + name);
					if (name == "Default") {
						return core.warn("Can't delete the Default profile.");
					}
					core.skunkConfig.deleteProfile(name);
					refreshProfileMenu();
				});
			} else if (szControl == "btnSave") {
				return SkunkSchema11.getControlPropertyAsync({
					panelName: viewTitle,
					controlName: "choProfiles",
					property: "selected"
				}, function(err, results) {
					if (err) finish(err);
					finish(undefined, options[results]);
				});
				
			} else if (szControl == "btnCancel") {
				finish(new core.Error("CANCELED"));
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);

		var string = schema.toXML();
		var pretty = SkunkSchema11.prettifyXml(string);
		core.console("pretty: " + pretty);
		skapi.ShowControls(string, true);
	};
	
	core.compressPreviousLogs = function compressPreviousLogs() {
		var path = "./logs/" + skapi.szWorld + "/" + skapi.acoChar.szName;
		if (!core.existsSync(path)) {
			return;
		}
		var files = core.readdirSync2(path);
		files.forEach(function(info) {
			if (info.ExtensionName == "log") {
				core.compressFileSync(info.Path, info.Path + ".zip");
			}
		});
	};

	core.showNewUI = function showNewUI() {
		
		var viewTitle = core.title + " (B)";
		
		var viewWidth = 300;
		var viewHeight = 240;
		
		var Element = SkunkSchema12.Element;
		var schema = Element("view", {title: viewTitle, icon: 0, width: viewWidth, height: viewHeight + 32}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.Notebook', name: "nbMain", width: viewWidth, height: viewHeight - 20}, [
					Element("page", {label: "Status"}, [
						Element("control", {progid: 'DecalControls.list', name: "lstStatus"}, [
							Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth})
						]) // lstStatus
					]),
					Element("page", {label: "Config"}, [
						Element("control", {progid: 'DecalControls.FixedLayout'}, [
							Element("control", {progid: 'DecalControls.Notebook', name: "nbConfig", width: viewWidth, height: viewHeight - 36}, [
								Element("page", {label: "1"}, [
									Element("control", {progid: 'DecalControls.FixedLayout'}, [
										Element("control", {progid: 'DecalControls.Notebook', name: "nbConfig1", width: viewWidth, height: viewHeight - 52}, [
											Element("page", {label: "General"}, [
												Element("control", {progid: 'DecalControls.list', name: "lstConfigGeneral"}, [
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth-75}),
													Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 75}),
												]).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
													var intCol = parseInt(value.split(",")[0], 10);
													var intRow = parseInt(value.split(",")[1], 10);
													var option = configOptions[intRow - 1];
													if (intCol == 0) {
														core.info(option.title + ": " + option.value());
													} else if (intCol == 1) {
														option.click();
													}
												}) // lstConfigGeneral
											]),
											Element("page", {label: "Selling"}, [
												Element("control", {progid: 'DecalControls.FixedLayout'}, [
													Element("control", {progid: 'DecalControls.Notebook', name: "bnSelling", left: 0, top: 0, width: viewWidth, height: viewHeight - 68}, [
														Element("page", {label: "Common"}, [
															Element("control", {progid: 'DecalControls.FixedLayout'}, [
																Element("control", {progid: 'DecalControls.list', name: "lstCommon", left: 0, top: 0, width: viewWidth, height: viewHeight-104}, [
																	//Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth})
																	Element("column", {progid: "DecalControls.CheckColumn"}),
																	Element("column", {progid: "DecalControls.IconColumn"}),
																	Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 190}),
																	Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 30}),
																	Element("column", {progid: "DecalControls.CheckColumn"}),
																]).on("onShow", function(szPanel, szControl, dictSzValue, value) {
																	refreshCommonList();
																}).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
																	//core.info("lstCommon ", value);
																	var intCol = parseInt(value.split(",")[0], 10);
																	var intRow = parseInt(value.split(",")[1], 10);
		
																	if (intRow == 0) {// header
																		if (intCol == 0) {
																			core.info("Enable/disable selling a item.");
																		} else if (intCol == 1) {
																			core.info("Item icon");
																		} else if (intCol == 2) {
																			core.info("Item name");
																		} else if (intCol == 3) {
																			core.info("Item price (points)");
																		} else if (intCol == 4) {
																			core.info("Bulk price set");
																		}
																		return;
																	}

																	var aco = listedCommonItems[intRow - 1];
																	var szFull = core.getFullName(aco);

																	if (intCol == 0) {
																		this.getControlProperty("data(" + intCol + "," + intRow + ")", function(err, results) {
																			if (err) return core.warn("Error getting list value", err);
																			var enabled = results;
																			core.info("Selling " + szFull + ": " + enabled);
																			core.setSellingValue({
																				aco    : aco,
																				szItem : szFull,
																				enabled: enabled
																			});
																		});
																	}
																	
																	
																	
																	
																	
																}), // lstCommon
																
																
																//Element("control", {progid: 'DecalControls.PushButton', name: "btnCommonScan",    left: 0, top: viewHeight - 104,   width: 80,  height: 20, text: "Scan"}).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
																//	core.info("Refreshing common list...");
																//	refreshCommonList();
																//}),
																Element("control", {progid: 'DecalControls.PushButton', name: "btnCommonBulkPrice",    left: 220, top: viewHeight - 104,   width: 80,  height: 20, text: "Bulk Set Price"})
															])
														]),
														Element("page", {label: "Unique"}, [
															Element("control", {progid: 'DecalControls.FixedLayout'}, [
																Element("control", {progid: 'DecalControls.list', name: "lstUnique", left: 0, top: 0, width: viewWidth, height: viewHeight-104}, [
																	Element("column", {progid: "DecalControls.TextColumn", fixedwidth: viewWidth})
																]).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
																	
																}), // lstUnique
																Element("control", {progid: 'DecalControls.PushButton', name: "btnUniqueScan",    left: 0, top: viewHeight - 104,   width: 80,  height: 20, text: "Scan"}).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
																	core.info("nothing...");
																}), 
																Element("control", {progid: 'DecalControls.PushButton', name: "btnUniqueBulkPrice",    left: 220, top: viewHeight - 104,   width: 80,  height: 20, text: "Bulk Set Price"})
															])
														])
													])
												])
											])
										]) // nbConfig1
									])
								])
							]) // nbConfig
						])
					])
				]), // nbMain
				
				//Element("control", {progid: 'DecalControls.PushButton', name: "btnTest",    left: 0, top: 0,   width: 1000,  height: 1000, text: "Test"})
				
				
				Element("control", {progid: 'DecalControls.Checkbox',   name: "chkActive",  left: 0,            top: viewHeight - 20,   width: 100, height: 20, text: "Active", checked: config.active && "True" || "False"}).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
					value = value == "True";
					config.active = value
					core.info("Active: " + value);
					core.saveCharacterConfig();
					
					if (value == true) {
						core.emit("onActivate");
					} else {
						core.emit("onDeactivate");
					}
				}), // chkActive
				Element("control", {progid: 'DecalControls.PushButton', name: "btnQuit",    left: viewWidth-40, top: viewHeight - 20,   width: 40,  height: 20, text: "Quit"}).on("onControlEvent", function(szPanel, szControl, dictSzValue, value) {
					core.info("Quitting...");
					core.quitting = true;
					core.disable();
				}) // btnQuit
			])
		]);

		schema.showControls(true);
		
		var lstStatus = schema.getElement("lstStatus");
		function refreshStatus() {
			lstStatus.getControlProperty("Clear()");

			var uptime = (new Date().getTime() - core.stats.initializeTime) / ONE_SECOND;
			var stUptime = core.getElapsedString(uptime);
			lstStatus.getControlProperty("AddRow()");
			lstStatus.setControlProperty("Data(0, 0)", "Uptime: " + stUptime);
			
			var totalVisits = core.stats.totalVisits || 0;
			lstStatus.getControlProperty("AddRow()");
			lstStatus.setControlProperty("Data(0, 1)", "Total visits: " + totalVisits);
			
			var unique = 0;
			for (var key in core.stats.uniqueVisis) {
				if (!Object.prototype.hasOwnProperty.call(core.stats.uniqueVisis, key)) continue;
				unique += 1;
			}
			lstStatus.getControlProperty("AddRow()");
			lstStatus.setControlProperty("Data(0, 2)", "Unique player visits: " + unique);
			
			var itemsSold = core.stats.itemsSold || 0;
			lstStatus.getControlProperty("AddRow()");
			lstStatus.setControlProperty("Data(0, 3)", "Items sold: " + itemsSold);
			
			var itemsReceived = core.stats.itemsReceived || 0;
			lstStatus.getControlProperty("AddRow()");
			lstStatus.setControlProperty("Data(0, 4)", "Items received: " + itemsReceived);
			
			var profit = core.stats.profit || 0;
			lstStatus.getControlProperty("AddRow()");
			lstStatus.setControlProperty("Data(0, 5)", "Profit: " + core.round(profit, config.valueRounding) + " points");
			
		}
		refreshStatus();
		core.setInterval(refreshStatus, 5 * ONE_SECOND);

		var configOptions = [];
		
		/*
		configOptions["!Config profile"] = {
			value: function() {
				return String(core.skunkConfig._keyProfile);
			},
			click: function() {
				core.info("Config profiles are a work in progress, old settings haven't been migrated yet.");
				return core.showSelectProfileView({}, function(err, results) {
					if (err) return core.warn("Error: ", err);
					core.info("results: " + results);
					core.skunkConfig.setProfileKey(results);
					core.skunkConfig.loadProfile();
					refreshConfigList();
				});
			}
		};
		*/
		
		configOptions.push({
			title: "Log chat to file",
			value: function() {
				return String(core.skunkConfig.profile.logChatToFile);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Enable debugging to log?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.skunkConfig.profile.logChatToFile = results;
					core.skunkConfig.saveProfile();
					refreshConfigList();
				});
			}
		});

		configOptions.push({
			title: "Load LootProfile3 into memory",
			value: function() {
				return String(core.skunkConfig.profile.loadLootProfile3);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Enable debugging to log?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					core.skunkConfig.profile.loadLootProfile3 = results;
					core.skunkConfig.saveProfile();
					refreshConfigList();
					core.info("Restart script if you enabled this option.");
				});
			}
		});

		configOptions.push({
			title: "Debug to log file",
			value: function() {
				return String(config.debugToLog);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Enable debugging to log?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.debugToLog = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Drop excess items",
			value: function() {
				return String(config.dropItemsEnabled);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Drop excess items?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.dropItemsEnabled = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Say announcments in chat",
			value: function() {
				return String(config.chatAnnouncments);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Say announcments in chat"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.chatAnnouncments = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Logout on portal space",
			value: function() {
				return String(config.logoutOnPortal);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Logout on portal space"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.logoutOnPortal = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Show identical common items in trade window",
			value: function() {
				return String(config.showDuplicateCommonItems);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Show identical common items?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.showDuplicateCommonItems = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Show identical unique items in trade window",
			value: function() {
				return String(config.showDuplicateUniqueItems);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Show identical unique items?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.showDuplicateUniqueItems = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Location string",
			value: function() {
				return String(config.myLocation);
			},
			click: function() {
				SkunkSchema.showEditPrompt({
					title: "Location string",
					value: config.myLocation
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking for point value", err);
					config.myLocation = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});

		configOptions.push({
			title: "Show UI on startup",
			value: function() {
				return String(config.showGUIOnStartup);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Show UI at startup?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.showGUIOnStartup = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Login script enabled",
			value: function() {
				return String(config.autoStartScript);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Login script enabled"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.autoStartScript = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Login script termination",
			value: function() {
				return String(config.terminateLoginScript);
			},
			click: function() {
				core.info("If the SkunkTrader_LoginScript runs without the 'Login Script' setting enabled, terminate skunkworks process.");
				SkunkSchema.showBooleanPrompt({
					title: "Login script termination"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.terminateLoginScript = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Sell stackable items one at a time",
			value: function() {
				return String(config.restrictStackableSales);
			},
			click: function() {
				core.info("GDLe may have a bug that prevents some items from transfering to the buyer. This may fix it for now.");
				
				SkunkSchema.showBooleanPrompt({
					title: "Allow Resell Price Change"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.restrictStackableSales = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});

		configOptions.push({
			title: "Equip items on request",
			value: function() {
				return String(config.equipItemsOnRequest);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Equip items on request"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.equipItemsOnRequest = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Equip items on request",
			value: function() {
				return String(config.equipItemsOnRequest);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Equip items on request"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.equipItemsOnRequest = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Save prices/points by world",
			value: function() {
				return String(config.sharePricesPerWorld);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Save prices per world"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.sharePricesPerWorld = results;
					core.saveCharacterConfig();
					refreshConfigList();
					
					core.salvagePricing = new core.Cache({
						path: core.getSalvagePricesPath()
					}).load();

				});
			}
		});
		
		configOptions.push({
			title: "Price decay enabled (unique items only)",
			value: function() {
				return String(config.priceDecayEnabled);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Show identical items?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.priceDecayEnabled = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Price decay percentage per day",
			value: function() {
				return String(config.priceDecayPercentage);
			},
			click: function() {

				return SkunkSchema.showEditPrompt({
					title: "Input price decay percentage",
					value: config.priceDecayPercentage
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking for point value", err);
					if (!core.isNumeric(results)) {
						core.warn("Value must be a number.");
						return SkunkSchema.showEditPrompt({
							title: "Input price decay percentage",
							value: config.priceDecayPercentage
						}, onValue);
					}
					if (results < 0 || results > 1) {
						core.warn("Number must be between 0 and 1.");
						return SkunkSchema.showEditPrompt({
							title: "Input price decay percentage",
							value: config.priceDecayPercentage
						}, onValue);
					}
					config.priceDecayPercentage = Number(results);
					core.saveCharacterConfig();
					refreshConfigList();
					
					var value = 25;
					core.console("With a item priced 25 points, here's the price for the next 100 days.");
					
					// eslint-disable-next-line no-restricted-syntax
					for (var i = 1; i <= 100; i++) {
						value = value - (value * config.priceDecayPercentage);
						core.console("Day #" + i, "Price: " + core.round(value, config.valueRounding));
					}
				});
				
				
			}
		});
		
		configOptions.push({
			title: "Keep inventory sorted",
			value: function() {
				return String(config.keepInventorySorted);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Keep inventory sorted"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.keepInventorySorted = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		
		configOptions.push({
			title: "Allow partially used items as payment",
			value: function() {
				return String(config.allowPartiallyUsedItemsAsPayment);
			},
			click: function() {
				SkunkSchema.showBooleanPrompt({
					title: "Allow partially used payment?"
				}, function onValue(err, results) {
					if (err) return core.warn("Error asking question", err);
					config.allowPartiallyUsedItemsAsPayment = results;
					core.saveCharacterConfig();
					refreshConfigList();
				});
			}
		});
		configOptions.sort(function(a, b) {
			if (a.title < b.title) return -1;
			if (a.title > b.title) return 1;
			return 0;
		});
		
		var lstConfigGeneral = schema.getElement("lstConfigGeneral");
		function refreshConfigList() {
			lstConfigGeneral.getControlProperty("Clear()");
			lstConfigGeneral.getControlProperty("AddRow()");
			lstConfigGeneral.setControlProperty("Data(0, 0)", "Description");
			lstConfigGeneral.setControlProperty("Data(1, 0)", "Value");

			configOptions.forEach(function(option, i) {
				var title = option.title;
				var value = option.value();
				if (value === "undefined") value = "undefined";
				lstConfigGeneral.getControlProperty("AddRow()");
				lstConfigGeneral.setControlProperty("Data(0, " + (i + 1) + ")", title);
				lstConfigGeneral.setControlProperty("Data(1, " + (i + 1) + ")", value);
			});
		}
		refreshConfigList();
		
		var listedCommonItems;
		var lstCommon = schema.getElement("lstCommon");
		function refreshCommonList() {
			lstCommon.getControlProperty("Clear()");
			lstCommon.getControlProperty("AddRow()");
			lstCommon.setControlProperty("Data(2, 0)", "Item");
			lstCommon.setControlProperty("Data(3, 0)", "Price");
			
			var items = listedCommonItems = core.getCommonItems();
			items.sort(function(a, b) {
				var aName = core.getFullName(a);
				var bName = core.getFullName(b);
				if (aName < bName) return -1;
				if (aName > bName) return 1;
				return 0;
			});
			
			var path = core.getPricesPath();
			var sellingValues = core.getJsonFile({path: path}) || {};
			var commonValues = sellingValues && sellingValues.common || {};

			var inv = core.getInventoryItems();
			items.forEach(function(aco, i) {
				var szFull = core.getFullName(aco);
				var enabled = commonValues[szFull] && commonValues[szFull].enabled || false;

				var value = "?";
				if (commonValues[szFull] && typeof commonValues[szFull].value !== "undefined") {
					value = commonValues[szFull].value;
				}

				szFull += " * " + inv.filter(function _filterItemName(aco) {
					return core.getFullName(aco) == this.name;
				}, {name: szFull}).reduce(function _tallyItems(tally, aco) {
					return tally + aco.citemStack;
				}, 0);

				lstCommon.getControlProperty("AddRow()");
				lstCommon.setControlProperty("Data(0, " + (i + 1) + ")", enabled);
				lstCommon.setControlProperty("Data(1, " + (i + 1) + ", 1)", icon_textures + aco.icon);
				lstCommon.setControlProperty("Data(2, " + (i + 1) + ")", szFull);
				lstCommon.setControlProperty("Data(3, " + (i + 1) + ")", value);
				lstCommon.setControlProperty("Data(4, " + (i + 1) + ")", false);
			});
		}
		//refreshCommonList();
		
	};

	return core;
}));

if (typeof SkunkTrader !== "undefined") {
	main = SkunkTrader.main;
}