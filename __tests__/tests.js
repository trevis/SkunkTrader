/********************************************************************************************\
	Tests using Jest (jestjs.io). Requires Node.js with Jest installed.
\********************************************************************************************/

// Fake Skunkworks API (skapi) object for our tests to use.
function SKAPI() {
	this.handlers = {};
	this.acoChar = {
		szName: "PLAYER"
	};
	this.szWorld = "WORLD";
}
SKAPI.prototype.AddHandler = function AddHandler(evid, handler) {
	var handlers = this.handlers;
	handlers[evid] = handlers[evid] || [];
	handlers[evid].push(handler);
};
SKAPI.prototype.RemoveHandler = function RemoveHandler(evid, handler) {
	var handlers = this.handlers;
	if (!handlers[evid]) return;
	
	if (evid == evidNil) {
		// Remove all events linked to handler.
		
		var h;
		for (var e in handlers) {
			if (!handlers.hasOwnProperty(e)) continue;
			for (var i = handlers[e].length - 1; i >= 0; i--) {
				h = handlers[e][i];
				if (h == handler) {
					handlers[e].splice(i, 1);
					continue;
				}
			}
		}
	} else if (handlers[evid]) {
		for (var i = handlers[evid].length - 1; i >= 0; i--) {
			h = handlers[evid][i];
			if (h == handler) {
				handlers[evid].splice(i, 1);
				continue;
			}
		}
	}
};
SKAPI.prototype.TimerNew = function TimerNew() {
	return {};
};
SKAPI.prototype.OutputLine = function OutputLine() {};
SKAPI.prototype.SelectAco = function SelectAco() {};
SKAPI.prototype.SetStackCount = function SetStackCount() {};
SKAPI.prototype.spoofEvent = function spoofEvent(evid, eventName, args) {
	var handlers = this.handlers;
	if (!handlers[evid]) return;
	var handler;
	for (var i = 0; i < handlers[evid].length; i++) {
		handler = handlers[evid][i];
		if (typeof handler[eventName] === "function") {
			handler[eventName].apply(handler, args);
		}
	}
	
	// Most of our functions call their callback in a timer so that they're called asyncly.
	jest.advanceTimersByTime(1); 
};
/*
SKAPI.prototype.AcfNew = function AcfNew() {
	return {
		CoacoGet: function() {
			return {
				Count: 0
			};
		}
	};
};
*/
SKAPI.prototype.Tell = function Tell() {};
SKAPI.prototype.Chat = function Chat() {};


skapi = global_skapi = new SKAPI(); // global for modules to see.

// Load skapi variables.
require("./../modules/SkapiDefs.js");

// Load SkunkTrader modules.
EventEmitter = require("./../modules/SkunkSuite/EventEmitter.js");
require("./../modules/SkunkSuite/LibStub.js");
require("./../modules/SkunkSuite/SkunkScript-1.0/SkunkScript-1.0.js");
require("./../modules/SkunkSuite/SkunkTimer-1.0/SkunkTimer-1.0.js");
require("./../modules/SkunkSuite/SkunkEvent-1.0/SkunkEvent-1.0.js");
require("./../modules/SkunkSuite/SkunkAssess-1.0/SkunkAssess-1.0.js");
require("./../modules/SkunkSuite/SkunkComm-1.0/SkunkComm-1.0.js");
require("./../modules/SkunkSuite/SkunkSynchronous-1.0/SkunkSynchronous-1.0.js");
require("./../modules/SkunkSuite/SkunkLogger-1.0/SkunkLogger-1.0.js");
jstoxml = require("./../modules/SkunkSuite/SkunkSchema-1.0/jstoxml.js");
require("./../modules/SkunkSuite/SkunkSchema-1.0/SkunkSchema-1.0.js");
Stream = require("./../modules/SkunkSuite/Stream.js");
require("./../modules/SkunkSuite/SkunkFS-1.0/SkunkFS-1.0.js");
require("./../modules/SkunkSuite/SkunkCache-1.0/SkunkCache-1.0.js");

require("./../modules/SkunkSuite/SkunkActions/AnimationFilter.js");
require("./../modules/SkunkSuite/SkunkActions/EffectsFilter.js");
require("./../modules/SkunkSuite/SkunkActions/SkunkActions.js");

async_js = require("./../modules/async.js");

// Use fake timers.
jest.useFakeTimers();

// Load SkunkTrader into memory.
var core = global.SkunkTrader = require("./../src/core.js");
require("./../src/locales.js");
core.isActive = function() {return true};
core.setInterval = setInterval;
core.setTimeout = setTimeout;


beforeEach(() => {
	skapi = new SKAPI();
});

// Start tests.
test('Split a item and expect a new stack', function(done) {
	// Our fake items.
	var initalItem = {
		MoveToPack: function() {},
		szName    : "Item"
	};
	var expectedItem = {
		szName    : "Item",
		citemStack: 1
	};

	// Function to test.
	core.splitAco({
		aco     : initalItem,
		quantity: 1
	}, function onSplit(err, results) {
		expect(err).toBeUndefined();
		expect(results).toBe(expectedItem);
		done();
	});

	// Spoof event fire.
	skapi.spoofEvent(evidOnAddToInventory, "OnAddToInventory", [expectedItem]);
});

test('Split a item and expect too busy', function(done) {
	// Our fake items.
	var initalItem = {
		MoveToPack: function() {},
		szName    : "Item"
	};

	// Function to test.
	core.splitAco({
		aco     : initalItem,
		quantity: 1
	}, function onSplit(err, results) {
		expect(err.message).toBe("TOO_BUSY");
		expect(results).toBeUndefined();
		done();
	});

	// Spoof event fire.
	skapi.spoofEvent(evidOnTipMessage, "OnTipMessage", ["You're too busy!"]);
});

test('help responce', function(done) {
	jest.spyOn(core, 'tell').mockImplementationOnce(function tell(params) {
		expect(params.szRecipient).toBe("SENDER");
		expect(params.szMsg).toBe(core.locales.helpResponse);
		done();
	});

	core.onMessage({
		evid    : evidOnTell,
		szSender: "SENDER",
		szMsg   : "help"
	});
});

/*
test('Price check for item in inventory', function(done) {
	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [{
			szName: "Swamp Stone"
		}];
	});
	
	jest.spyOn(core, 'getSellValue').mockImplementationOnce(function() {
		return 2;
	});

	core.getItemPrice({
		szName  : "Swamp Stone",
		szSender: "SENDER"
	}, function onPrice(err, results) {
		//console.log("onPrice", err, results);
		expect(err).toBeUndefined();
		expect(results).toMatchObject({
			szName: 'Swamp Stone', 
			value : 2
		});
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

/*
test('Check price when player has discount.', function(done) {
	core.config.players = {};
	core.config.players["Bob"] = {
		playerDiscount: 25 // 25%
	};
	
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10; // Item value of 5 points
	});

	jest.spyOn(core, 'getAllegianceInfo').mockImplementationOnce(function() {
		return; // none;
	});

	var value = core.getSellValue({
		szPlayer  : "Bob"
	});

	expect(value).toBe(7.5);
	done();
});
*/

/*
test('Check price when player\'s patron has discount.', function(done) {
	core.config.players = {};
	core.config.players["John"] = {
		patronDiscount: 30 // 30%
	};

	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10; // Item value of 10 points
	});

	jest.spyOn(core, 'getAllegianceInfo').mockImplementationOnce(function() {
		return {
			szPatron: "John"
		};
	});

	var value = core.getSellValue({
		szPlayer  : "Bob"
	});

	expect(value).toBe(7);
	done();
});
*/

/*
test('Check price when player\'s monarch has discount.', function(done) {
	core.config.players = {};
	core.config.players["John"] = {
		monarchDiscount: 35 // 35%
	};

	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 15; 
	});

	jest.spyOn(core, 'getAllegianceInfo').mockImplementationOnce(function() {
		return {
			szMonarch: "John"
		};
	});

	var value = core.getSellValue({
		szPlayer  : "Bob"
	});

	expect(value).toBe(9.75);
	done();
});
*/

/*
test('Price check for item NOT in inventory', function(done) {
	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [];
	});
	
	jest.spyOn(core, 'getSellValue').mockImplementationOnce(function() {
		return 3;
	});
	
	core.getItemPrice({
		szName  : "Yellow Jewel",
		szSender: "SENDER"
	}, function onPrice(err, results) {
		//console.log("onPrice", err, results);
		expect(results).toBeUndefined();
		expect(err.message).toBe("NO_MATCHES");
		expect(err.szName).toBe("Yellow Jewel");
		jest.clearAllMocks();
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

/*
test('Price check for item with too many matches', function(done) {
	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [{
			szName: "foo"
		}, {
			szName: "bar"
		}];
	});
	
	core.getItemPrice({
		szName  : "Yellow Jewel",
		szSender: "SENDER"
	}, function onPrice(err, results) {
		//console.log("onPrice", err, results);
		expect(results).toBeUndefined();
		expect(err.message).toBe("TOO_MANY_MATCHES");
		expect(err.szName).toBe("Yellow Jewel");
		expect(err.szMatches).toBe("bar, foo");
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

test('Get sellable item matching name', function() {
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [{
			szName: "Yellow Jewel"
		}, {
			szName: "Swamp Stone"
		}, {
			szName: "Red Gem"
		}];
	});

	var matches = core.getSellableItemMatch({
		szName: "Swamp Stone"
	});

	expect(matches[0]).toMatchObject({
		szName: "Swamp Stone"
	});
});

/*
test('Find sellable item with no value matches', function() {
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [{
			szName: "Yellow Jewel"
		}, {
			szName: "Swamp Stone"
		}, {
			szName: "Red Gem"
		}, {
			szName  : "Pants",
			cpyValue: 100
		}];
	});
	
	var matches = core.getSellableItemMatch({
		szName: "Pants",
		value : 500
	});

	expect(matches.length).toBe(0);
});
*/

/*
test('Find sellable item with matches', function() {
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [{
			szName: "Yellow Jewel"
		}, {
			szName: "Swamp Stone"
		}, {
			szName: "Red Gem"
		}, {
			szName  : "Shirt",
			cpyValue: 1000
		}];
	});
	
	
	var matches = core.getSellableItemMatch({
		szName: "Shirt",
		value : 1000
	});

	expect(matches[0]).toMatchObject({
		szName  : "Shirt",
		cpyValue: 1000
	});
});
*/

/*
test('Find sellable items matching pyreal values', function() {
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [{
			szName: "Yellow Jewel"
		}, {
			szName: "Swamp Stone"
		}, {
			szName: "Red Gem"
		}, {
			szName  : "Shirt",
			cpyValue: 1000
		}];
	});
	
	var matches = core.getSellableItemMatch({
		values: [1000]
	});

	expect(matches[0]).toMatchObject({
		szName  : "Shirt",
		cpyValue: 1000
	});
});
*/

/*
test('Find item matches with a bad pyreal value', function(done) {
	core.findItemMatches({
		szValue: "a"
	}, function onResults(err, results) {
		//console.log("onResults", err, results);
		expect(results).toBeUndefined();
		expect(err.message).toBe("VALUE_NOT_NUMBER");
		expect(err.szValue).toBe("a");
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

/*
test('Find item with no matches', function(done) {
	
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [];
	});
	
	core.findItemMatches({
		szName: "Swamp Stone"
	}, function onResults(err, results) {
		//console.log("onResults", err, results);
		expect(results).toBeUndefined();
		expect(err.message).toBe("NO_MATCHES");
		expect(err.szName).toBe("Swamp Stone");
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

/*
test('Find items with too many matches', function(done) {
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [{
			szName: "Swamp Stone"
		}, {
			szName: "Swamp Stuff"
		}];
	});
	
	core.findItemMatches({
		szName: "Swamp"
	}, function onResults(err, results) {
		//console.log("onResults", err, results);
		expect(results).toBeUndefined();
		expect(err.message).toBe("TOO_MANY_MATCHES");
		expect(err.szName).toBe("Swamp");
		expect(err.szMatches).toBe("Swamp Stone, Swamp Stuff");
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

/*
test('Find items with one match', function(done) {
	
	// Fake item that matches our find string.
	var item = { 
		szName: "Swamp Stone"
	};
	
	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [item];
	});

	core.findItemMatches({
		szName: "Swamp"
	}, function onResults(err, results) {
		//console.log("onResults", err, results);
		expect(err).toBeUndefined();
		expect(results.matches[0]).toBe(item);
		done();
	});
	jest.advanceTimersByTime(1);
});
*/

test('Process add request from non partner', function(done) {
	jest.spyOn(core, 'tell').mockImplementationOnce(function tell(params) {
		//console.log("<tell>", params);
		expect(params.szRecipient).toBe("SENDER");
		expect(params.szMsg[0]).toBe("// You are not my trade partner.");
		done();
	});

	core.processAddRequest({
		szName  : "Swamp Stone",
		szSender: "SENDER"
	});
});

/*
test('Process add request when we don\'t have item', function(done) {
	core.tradePartner = {
		szName: "SENDER"
	};

	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [];
	});

	jest.spyOn(core, 'tell').mockImplementationOnce(function tell(params) {
		//console.log("<tell>", params);
		expect(params.szRecipient).toBe("SENDER");
		expect(params.szMsg[0]).toBe('// Found no items matching \'Swamp Stone\'.');
		done();
	});

	core.processAddRequest({
		szSender: "SENDER",
		szName  : "Swamp Stone"
	});
	jest.advanceTimersByTime(1);
});
*/

/*
test('Process add request when window is full', function(done) {
	//core.debug = console.log;
	
	core.tradePartner = {
		szName: "SENDER"
	};

	jest.spyOn(core, 'getItemsForSale').mockImplementationOnce(function() {
		return [];
	});
	
	core.allItemsShown = true;

	jest.spyOn(core, 'tell').mockImplementationOnce(function tell(params) {
		//console.log("<tell>", params);
		expect(params.szRecipient).toBe("SENDER");
		expect(params.szMsg[0]).toBe('// Clear the trade window before requesting items to be added.');
		done();
	});

	core.processAddRequest({
		szSender: "SENDER",
		szName  : "Swamp Stone"
	});
	core.allItemsShown = false;
});
*/

/*
test('Process add request when we have item', function(done) {
	core.tradePartner = {
		szName: "SENDER"
	};
	
	//core.debug = console.log;

	function MoveToPack(a, b, c) {
		//console.log("<MoveToPack>");
		expect(a).toBe(0);
		expect(b).toBe(0);
		expect(c).toBe(false);
		done();
	}
    const hookMoveToPack = jest.fn(MoveToPack);

	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [{
			szName    : "Swamp Stone",
			MoveToPack: hookMoveToPack
		}];
	});

	//core.allItemsShown = false;
	jest.spyOn(core, 'tell').mockImplementationOnce(function tell(params) {
		//console.log("<tell>", params);
		expect(params.szRecipient).toBe("SENDER");
		expect(params.szMsg[0]).toBe('// Adding Swamp Stone to the trade window...');
	});

	jest.spyOn(core, 'getSellPassword').mockImplementationOnce(function() {
		return;
	});

	core.processAddRequest({
		szSender: "SENDER",
		szName  : "Swamp Stone"
	});
	jest.advanceTimersByTime(1000);
	expect(hookMoveToPack).toHaveBeenCalled();
});
*/

/*
test('Process add request for multiple items (stack)', function(done) {
	core.tradePartner = {
		szName: "SENDER"
	};
	
	//core.debug = console.log;

	function MoveToPack(a, b, c) {
		//console.log("<MoveToPack>");
		expect(a).toBe(0);
		expect(b).toBe(0);
		expect(c).toBe(false);
		done();
	}

	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [{
			szName    : "Swamp Stone",
			MoveToPack: MoveToPack
		}];
	});

	//core.allItemsShown = false;
	jest.spyOn(core, 'tell').mockImplementationOnce(function tell(params) {
		//console.log("<tell>", params);
		expect(params.szRecipient).toBe("SENDER");
		expect(params.szMsg[0]).toBe('// Adding multiple Swamp Stone to the trade window...');
	});

	jest.spyOn(core, 'getSellPassword').mockImplementationOnce(function() {
		return;
	});

	core.processAddRequest({
		szSender: "SENDER",
		szName  : "Swamp Stone",
		amount  : 2
	});
	jest.advanceTimersByTime(1);
});
*/

test('Process add request that has a item set individual restriction.', function(done) {
	core.tradePartner = {
		szName: "SENDER"
	};

	core.config.itemSets = {};
	core.config.itemSets["Ancient Armor"] = {
		restrictIndividualSales: true
	};
	
	jest.spyOn(core, 'getMatchingItemSet').mockImplementationOnce(function() {
		return "Ancient Armor";
	});

	core.addMatchToTradeWindow({
		matches: [
			{
				szName: "Ancient Boots"
			}
		]
	}, onResult);
	
	function onResult(err, results) {
		//console.log("<onResult>", err, results);
		expect(results).toBeUndefined();
		expect(err.message).toBe("RESTRICTED_INDIVIDUAL_SALE");
		expect(err.item).toBe("Ancient Boots");
		expect(err.setName).toBe("Ancient Armor");
		done();
	}
	
	jest.advanceTimersByTime(1);
});

// Check price of the current trade window.

// Test partner accepting trade with sufficient funds.

// Test partner accepting trade with insufficient funds.

// Test squelched player opening trade. 

test('EquipItem a item', function(done) {
	var acoItem = {
		Use: function() {
			acoItem.eqmWearer = eqmAll;
		}
	};
	
	core.equipItem({
		aco: acoItem
	}, function onComplete(err, results) {
		expect(err).toBeUndefined();
		expect(results).toBe(true);
		done();
	});

	jest.advanceTimersByTime(1000);
});

test('EquipItem a item that\'s already equipped', function(done) {
	var acoItem = {
		Use: function() {
			acoItem.eqmWearer = eqmAll;
		},
		eqmWearer: eqmAll
	};
	
	core.equipItem({
		aco: acoItem
	}, function onComplete(err, results) {
		expect(err).toBeUndefined();
		done();
	});

	jest.advanceTimersByTime(1000);
});

test('EquipItem a item when busy', function(done) {
    const Use = jest.fn(function(a, b, c) {
		//expect(a).toBe(0);
		//expect(b).toBe(0);
		//expect(c).toBe(false);
		//done();
	});
	
	var acoItem = {
		Use: Use
	};
	
	core.equipItem({
		aco: acoItem
	}, function onComplete(err, results) {
		expect(results).toBeUndefined();
		expect(err.message).toBe("TOO_BUSY");
		done();
	});

	expect(Use).toHaveBeenCalled();
	skapi.spoofEvent(evidOnTipMessage, "OnTipMessage", ["You're too busy!"]);
});

test('UnequipItem a item', function(done) {
    const MoveToPack = jest.fn(function(a, b, c) {
		expect(a).toBe(0);
		expect(b).toBe(0);
		expect(c).toBe(false);
	});
	
	var acoItem = {
		MoveToPack: MoveToPack,
		oid       : 123
	};
	
	core.unequipItem({
		aco: acoItem
	}, function onComplete(err, results) {
		expect(err).toBeUndefined();
		expect(results).toBe(true);
		done();
	});

	expect(MoveToPack).toHaveBeenCalled();
	skapi.spoofEvent(evidOnAddToInventory, "OnAddToInventory", [acoItem]);
});

/*
test('UnequipItem a item we\'re already wearing', function(done) {
	var acoItem = {
		eqmWearer : 0
	};
	
	core.unequipItem({
		aco: acoItem
	}, function onComplete(err, results) {
		expect(err).toBeUndefined();
		done();
	});

	jest.advanceTimersByTime(1000);
});
*/

test('UnequipItem a item when we\'re busy', function(done) {
	const MoveToPack = jest.fn(function(a, b, c) {
		expect(a).toBe(0);
		expect(b).toBe(0);
		expect(c).toBe(false);
		done();
	});
	
	var acoItem = {
		MoveToPack: MoveToPack
	};
	
	core.unequipItem({
		aco: acoItem
	}, function onComplete(err, results) {
		expect(results).toBeUndefined();
		expect(err.message).toBe("TOO_BUSY");
		done();
	});

	expect(MoveToPack).toHaveBeenCalled();
	skapi.spoofEvent(evidOnTipMessage, "OnTipMessage", ["You're too busy!"]);
});

/*
test('getItemFromChat with no matches', function(done) {
	core.getItemFromChat({
		szSender: "Sender",
		szName  : "Leather Gloves"
	}, function onComplete(err, results) {
		expect(results).toBeUndefined();
		expect(err.message).toBe("NO_MATCHES");
		done();
	});
	
	jest.advanceTimersByTime(1000);
});
*/

/*
test('getItemFromChat with too many matches', function(done) {
	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [{szName: "White Leather Gloves"}, {szName: " Black Leather Gloves"}];
	});
	
	core.getItemFromChat({
		szSender: "Sender",
		szName  : "Leather Gloves"
	}, function onComplete(err, results) {
		expect(results).toBeUndefined();
		expect(err.message).toBe("TOO_MANY_MATCHES");
		done();
	});
	
	jest.advanceTimersByTime(1000);
});
*/

/*
test('getItemFromChat with a match', function(done) {
	var aco = {szName: "White Leather Gloves"};
	jest.spyOn(core, 'getSellableItemMatch').mockImplementationOnce(function() {
		return [aco];
	});
	
	core.getItemFromChat({
		szSender: "Sender",
		szName  : "Leather Gloves"
	}, function onComplete(err, results) {
		expect(err).toBeUndefined();
		expect(results).toBe(aco);
		done();
	});
	
	jest.advanceTimersByTime(250);
});
*/

/*
test('processEquipRequest with no matches', function(done) {
	jest.spyOn(core.AsyncEvent, 'getItemFromChat').mockImplementationOnce(function(params, callback) {
		callback({message: "NO_MATCHES", szName: "Leather Gloves"});
	});

	skapi.Tell = jest.fn(function(szRecipient, szText) {
		expect(szRecipient).toBe("Bob");
		expect(szText).toBe("// Found no items matching 'Leather Gloves'.");
		done();
	});

	core.processEquipRequest({
		szSender: "Bob",
		szName  : "Leather Gloves"
	});

	jest.advanceTimersByTime(250);
	expect(skapi.Tell).toHaveBeenCalled();
});
*/

/*
test('processEquipRequest with too many matches', function(done) {
	jest.spyOn(core.AsyncEvent, 'getItemFromChat').mockImplementationOnce(function(params, callback) {
		callback({message: "TOO_MANY_MATCHES", szName: "Leather Gloves", szMatches: "White Leather Gloves, Black Leather Gloves"});
	});

	skapi.Tell = jest.fn(function(szRecipient, szText) {
		expect(szRecipient).toBe("Bob");
		expect(szText).toBe("// Found too many items matching 'Leather Gloves' (White Leather Gloves, Black Leather Gloves). Be more specific or add the item's value, ex: 'add leather boots:5555'");
		done();
	});

	core.processEquipRequest({
		szSender: "Bob",
		szName  : "Leather Gloves"
	});

	jest.advanceTimersByTime(250);
	expect(skapi.Tell).toHaveBeenCalled();
});
*/

/*
test('processEquipRequest with unequippable item', function(done) {
	jest.spyOn(core.AsyncEvent, 'getItemFromChat').mockImplementationOnce(function(params, callback) {
		callback(undefined, {eqm: 0, szName: "Blue Leather Gloves"});
	});

	skapi.Tell = jest.fn(function(szRecipient, szText) {
		expect(szRecipient).toBe("Bob");
		expect(szText).toBe("// Blue Leather Gloves is not equippable.");
		done();
	});
	

	core.processEquipRequest({
		szSender: "Bob",
		szName  : "Leather Gloves"
	});

	//jest.advanceTimersByTime(250);
	expect(skapi.Tell).toHaveBeenCalled();
});
*/

/*
test('processEquipRequest with already equipped item', function(done) {
	jest.spyOn(core.AsyncEvent, 'getItemFromChat').mockImplementationOnce(function(params, callback) {
		callback(undefined, {eqmWearer: eqmAll, szName: "Blue Leather Gloves"});
	});

	skapi.Tell = jest.fn(function(szRecipient, szText) {
		expect(szRecipient).toBe("Bob");
		expect(szText).toBe("// Blue Leather Gloves is already equipped.");
		done();
	});
	

	core.processEquipRequest({
		szSender: "Bob",
		szName  : "Leather Gloves"
	});

	//jest.advanceTimersByTime(250);
	expect(skapi.Tell).toHaveBeenCalled();
});
*/

/*
test('processEquipRequest with item to equip', function(done) {
	
	const item = {
		szName: "Blue Leather Gloves",
		Use   : jest.fn(function() {
			this.eqmWearer = eqmAll;
		})
	};
	
	// Mock a item we found to equip.
	jest.spyOn(core.AsyncEvent, 'getItemFromChat').mockImplementationOnce(function(params, callback) {
		callback(undefined, item);
	});

	// Mock our currently equipped gear.
	jest.spyOn(core, 'getEquippedItems').mockImplementationOnce(function(params, callback) {
		return [];
	});

	// Mock unequipping our currently worn gear.
	jest.spyOn(core.AsyncEvent, 'unequipItem').mockImplementationOnce(function(params, callback) {
		callback();
	});

	jest.spyOn(core, 'tell').mockImplementationOnce(function(params, callback) {
		expect(params.szRecipient).toBe("Bob");
		expect(params.szMsg).toStrictEqual(["// Attempting to equip Blue Leather Gloves..."]);
		done()
	});

	core.processEquipRequest({
		szSender: "Bob",
		szName  : "Leather Gloves"
	});

	expect(item.Use).toHaveBeenCalled();
	jest.advanceTimersByTime(250);
});
*/

/*
test('processEquipRequest with item we can\'t wield', function(done) {
	// Function that should be called to equip a item.
	const Use = jest.fn(function() {});
	
	// Mock a item we found to equip.
	jest.spyOn(core.AsyncEvent, 'getItemFromChat').mockImplementationOnce(function(params, callback) {
		callback(undefined, {
			szName: "Blue Leather Gloves", 
			Use: Use,
			eqmWearer: 0
		});
	});

	// Mock our currently equipped gear.
	jest.spyOn(core, 'getEquippedItems').mockImplementationOnce(function(params, callback) {
		return [];
	});

	// Mock unequipping our currently worn gear.
	jest.spyOn(core.AsyncEvent, 'unequipItem').mockImplementationOnce(function(params, callback) {
		callback();
	});

	skapi.Tell = jest.fn(function(szRecipient, szText) {
		expect(szRecipient).toBe("Bob");
		expect(szText).toBe("// Attempting to equip Blue Leather Gloves...");

		skapi.Tell = jest.fn(function(szRecipient, szText) {
			expect(szRecipient).toBe("Bob");
			expect(szText).toBe("// I am unable to wield Blue Leather Gloves.");
			done();
		});
	});
	
	// Mock our currently equipped gear.
	jest.spyOn(core.AsyncEvent, 'equipItem').mockImplementationOnce(function(params, callback) {
		callback(new core.Error("CANT_BE_WIELDED").set("szItem", "Blue Leather Gloves"));
	});

	core.processEquipRequest({
		szSender: "Bob",
		szName  : "Leather Gloves"
	});
});
*/

/*
test('Add multiple items by value', function(done) {
	const hookAddToTrade = jest.fn(function AddToTrade(a, b, c) {
		skapi.spoofEvent(evidOnTradeAdd, "OnTradeAdd", [this, 1]);
	});

	jest.spyOn(core, 'findItemMatches').mockImplementationOnce(function(params, callback) {
		callback(undefined, {
			matches: [
				{
					szName    : "Steel Salvage 1",
					cpyValue: 1000,
					AddToTrade: hookAddToTrade,
					material: materialSteel
				},
				{
					szName    : "Steel Salvage 2",
					cpyValue: 2000,
					AddToTrade: hookAddToTrade,
					material: materialSteel
				},
				{
					szName    : "Steel Salvage 3",
					cpyValue: 3000,
					AddToTrade: hookAddToTrade,
					material: materialSteel
				}
			],
			justNumbers: true
		});
	});
	
	jest.spyOn(core, 'getSellPassword').mockImplementationOnce(function() {
		return;
	});
	
	jest.spyOn(core, 'getMatchingItemSet').mockImplementation(function() {
		return;
	});

	jest.spyOn(core, 'tell').mockImplementation(function tell(params) {
		if (params.szMsg[0] == "// Added item(s) to trade window.") {
			done();
		}
	});
	
	core.processAddRequest({
		szName  : "1000 2000 3000",
		szSender: "SENDER"
	});
	expect(hookAddToTrade).toHaveBeenCalledTimes(3);
	
	jest.advanceTimersByTime(100);
});
*/

//getSellValue
test('Check if player discount works.', function(done) {
	core.config.players = {};
	core.config.players["Bob"] = {
		playerDiscount: 25 // 25%
	};
	
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	
	core.allegianceInfo = {get:function(){}};
	
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		/*
{
		"szPatron": "Duke Kurik",
		"szMonarch": "Taikou Verb",
		"oidMonarch": 1342177598,
		"date": "2020-05-02 01-43-34",
		"time": 1588405414642
	}
		*/
		return {};
		
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(7.5);
	done();
});

test('Check if patron discount works.', function(done) {
	core.config.players = {};
	core.config.players["Bob's Patron"] = {
		patronDiscount: 25 // 25%
	};
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	core.allegianceInfo = {get:function(){}};
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		return {
			"szPatron": "Bob's Patron"
		};
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(7.5);
	done();
});

test('Check if monarch discount works.', function(done) {
	core.config.players = {};
	core.config.players["Bob's Monarch"] = {
		monarchDiscount: 25 // 25%
	};
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	core.allegianceInfo = {get:function(){}};
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		return {
			"szMonarch": "Bob's Monarch"
		};
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(7.5);
	done();
});

test('Check if discount priority works', function(done) {
	core.config.players = {};
	core.config.players["Bob's Monarch"] = {
		monarchDiscount: 25
	};
	core.config.players["Bob's Patron"] = {
		patronDiscount: 50
	};
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	core.allegianceInfo = {get:function(){}};
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		return {
			"szMonarch": "Bob's Monarch",
			"szPatron": "Bob's Patron"
		};
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(5);
	done();
});

test('Check if global discount works', function(done) {
	core.config.globalDiscount = 25;
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(7.5);
	done();
});

test('Use global discount if it\'s higher than allegiance discount', function(done) {
	core.config.globalDiscount = 50;
	core.config.players = {};
	core.config.players["Bob's Monarch"] = {
		monarchDiscount: 25
	};
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	core.allegianceInfo = {get:function(){}};
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		return {
			"szMonarch": "Bob's Monarch"
		};
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(5);
	done();
});

test('Use allegiance discount if it\'s higher than global discount', function(done) {
	core.config.globalDiscount = 25;
	core.config.players = {};
	core.config.players["Bob's Monarch"] = {
		monarchDiscount: 50
	};
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	core.allegianceInfo = {get:function(){}};
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		return {
			"szMonarch": "Bob's Monarch"
		};
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(5);
	done();
});

test('Discount should not exceed 100%', function(done) {
	core.config.globalDiscount = 125;
	jest.spyOn(core, 'getAssignedSellValue').mockImplementationOnce(function() {
		return 10;
	});
	core.allegianceInfo = {get:function(){}};
	jest.spyOn(core.allegianceInfo, 'get').mockImplementationOnce(function() {
		return {
			"szMonarch": "Bob's Monarch"
		};
	});
	var value = core.getSellValue({
		aco     : {}, 
		szPlayer: "Bob"
	});
	expect(value).toBe(0);
	done();
});
