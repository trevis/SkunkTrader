#Asheron's Call: Throne of Destiny Keymap File
#
#Most users will never want or need to edit this file by hand. To edit your 
#keymap, click the options button, select the Gameplay Options tab, and click 
#'Configure Keyboard'.
#
#However it is possible to configure your keyboard by editing this file.
#
#Important notes: AC:TD will generate a brand-new acclient.keymap in your
#My Documents\Asheron's Call folder if it tries to use one and can't find one.
#So if you do completely break your keymap, you can start over by deleting
#this file.
#
#AC:TD writes to the keymap file on shutdown of the client. If you make changes 
#in this file while the client is running, it will overwrite them on shutdown.
#
#If you make changes in the Configure Keyboard options and save them to a new
#file, that will be the default keymap file the next time AC:TD starts up.
#
#Key bindings can be edited under the 'Bindings' header. There are several key fields.
#
#The first field is the Action. This is defined by the game. To find the name of a
#particular Action, bind a key to it in the Configure Keyboard menu in the client.
#Exit the client and you will see the name in the Bindings header. 
#
#The second field is the device. Devices are enumerated under the Devices header. 
#Generally device 0 is keyboard, device 1 is mouse, other devices follow.
#
#The third field is the key or action. A full list of available actions for this field
#can be found in Microsoft's DirectX documentation at http://msdn.microsoft.com.
#
#The fourth field is the subcontrol. It can be used to define positive or negative axis
#for an X, Y or Z input, as well as other options.
#
#The fifth field is also optional, it defines which metakeys need to be depressed. The
#metakeys are defined and enumerated under the Metakeys header. The number next to
#the metakey is a bit, metakeys can be combined. 
#
#For example the default for Left Shift (DIK_LSHIFT) is 1, and Left Control 
#(DIK_LCONTROL) is 2. I could bind a key to CTRL-SHIFT-M by using DIK_M 0x00000003.
#
#It is possible to edit the Metakey enumeration but this is not recommended for any but
#the most advanced computer users.
#
#The sixth, optional field is the activation type. Generally this is only used for mouse
#buttons and the most common type is MouseDblClick, but there are other options.
#
#Again, more detailed documentation can be found at http://msdn.microsoft.com. 
#
#Turbine cannot provide support for advanced keymap editing.
#
#
"User Defined Keymap" [ 00000000-0000-0000-0000-000000000000 ]

Devices
[
  Keyboard [ GUID_SysKeyboard ]
  Mouse [ GUID_SysMouse ]
  Virtual [ GUID_Virtual ]
  Joystick [ A0C91D70-D974-11EA-8001-444553540000 ]
  Joystick [ A0C9E0C0-D974-11EA-8002-444553540000 ]
  Joystick [ A0CA07D0-D974-11EA-8003-444553540000 ]
  Joystick [ A0CA55F0-D974-11EA-8004-444553540000 ]
  Joystick [ A0CAF230-D974-11EA-8005-444553540000 ]
  Joystick [ A0CB8E70-D974-11EA-8006-444553540000 ]
  Joystick [ A0CBB580-D974-11EA-8007-444553540000 ]
  Joystick [ A0CBDC90-D974-11EA-8008-444553540000 ]
]

MetaKeys
[
  1 [ 0 DIK_LSHIFT ]
  2 [ 0 DIK_LCONTROL ]
  2 [ 0 DIK_RCONTROL ]
  3 [ 0 DIK_LMENU ]
  3 [ 0 DIK_RALT ]
  4 [ 0 DIK_LWIN ]
  4 [ 0 DIK_RWIN ]
  5 [ 1 DIMOFS_BUTTON3 ]
  6 [ 1 DIMOFS_BUTTON4 ]
]

Bindings
[
  MovementCommands
  [
    MovementJump [ "" [ 0 DIK_SPACE ] ]
    MovementForward [ "" [ 0 DIK_W ] ]
    MovementForward [ "" [ 0 DIK_UPARROW ] ]
    MovementBackup [ "" [ 0 DIK_X ] ]
    MovementBackup [ "" [ 0 DIK_DOWNARROW ] ]
    MovementTurnLeft [ "" [ 0 DIK_A ] ]
    MovementTurnLeft [ "" [ 0 DIK_LEFT ] ]
    MovementTurnRight [ "" [ 0 DIK_D ] ]
    MovementTurnRight [ "" [ 0 DIK_RIGHTARROW ] ]
    MovementStrafeLeft [ "" [ 0 DIK_Z ] ]
    MovementStrafeLeft [ "" [ 0 DIK_A ] 0x00000004 ]
    MovementStrafeLeft [ "" [ 0 DIK_LEFT ] 0x00000004 ]
    MovementStrafeRight [ "" [ 0 DIK_C ] ]
    MovementStrafeRight [ "" [ 0 DIK_D ] 0x00000004 ]
    MovementStrafeRight [ "" [ 0 DIK_RIGHTARROW ] 0x00000004 ]
    MovementWalkMode [ "" [ 0 DIK_LSHIFT ] ]
    MovementRunLock [ "" [ 0 DIK_Q ] ]
    MovementStop [ "" [ 0 DIK_S ] ]
    Ready [ "" [ 0 DIK_Y ] ]
    Sitting [ "" [ 0 DIK_G ] ]
    Crouch [ "" [ 0 DIK_H ] ]
    Sleeping [ "" [ 0 DIK_B ] ]
  ]

  ItemSelectionCommands
  [
    SelectionPickUp [ "" [ 0 DIK_F ] ]
    SelectionSplitStack [ "" [ 0 DIK_T ] ]
    SelectionPreviousSelection [ "" [ 0 DIK_P ] ]
    SelectionClosestCompassItem [ "" [ 0 DIK_BACK ] ]
    SelectionPreviousCompassItem [ "" [ 0 DIK_MINUS ] ]
    SelectionNextCompassItem [ "" [ 0 DIK_EQUALS ] ]
    SelectionClosestItem [ "" [ 0 DIK_BACKSLASH ] ]
    SelectionPreviousItem [ "" [ 0 DIK_LBRACKET ] ]
    SelectionNextItem [ "" [ 0 DIK_RBRACKET ] ]
    SelectionClosestMonster [ "" [ 0 DIK_APOSTROPHE ] ]
    SelectionPreviousMonster [ "" [ 0 DIK_L ] ]
    SelectionNextMonster [ "" [ 0 DIK_SEMICOLON ] ]
    SelectionLastAttacker [ "" [ 0 DIK_HOME ] ]
    SelectionClosestPlayer [ "" [ 0 DIK_SLASH ] ]
    SelectionPreviousPlayer [ "" [ 0 DIK_COMMA ] ]
    SelectionNextPlayer [ "" [ 0 DIK_PERIOD ] ]
    SelectionPreviousFellow [ "" [ 0 DIK_N ] ]
    SelectionNextFellow [ "" [ 0 DIK_M ] ]
  ]

  UICommands
  [
    SelectionExamine [ "" [ 0 DIK_E ] ]
    CaptureScreenshot [ "" [ 0 DIK_NUMPADSTAR ] ]
    ToggleHelp [ "" [ 0 DIK_F1 ] ]
    TogglePluginManager [ "" [ 0 DIK_F1 ] 0x00000003 ]
    ToggleAllegiancePanel [ "" [ 0 DIK_F3 ] ]
    ToggleFellowshipPanel [ "" [ 0 DIK_F4 ] ]
    ToggleSpellbookPanel [ "" [ 0 DIK_F5 ] ]
    ToggleSpellComponentsPanel [ "" [ 0 DIK_F6 ] ]
    ToggleAttributesPanel [ "" [ 0 DIK_F8 ] ]
    ToggleSkillsPanel [ "" [ 0 DIK_F9 ] ]
    ToggleWorldPanel [ "" [ 0 DIK_F10 ] ]
    ToggleOptionsPanel [ "" [ 0 DIK_F11 ] ]
    ToggleInventoryPanel [ "" [ 0 DIK_F12 ] ]
    ToggleFloatingChatWindow1 [ "" [ 0 DIK_1 ] 0x00000004 ]
    ToggleFloatingChatWindow2 [ "" [ 0 DIK_2 ] 0x00000004 ]
    ToggleFloatingChatWindow3 [ "" [ 0 DIK_3 ] 0x00000004 ]
    ToggleFloatingChatWindow4 [ "" [ 0 DIK_4 ] 0x00000004 ]
    USE [ "" [ 0 DIK_R ] ]
    EscapeKey [ "" [ 0 DIK_ESCAPE ] ]
    LOGOUT [ "" [ 0 DIK_ESCAPE ] 0x00000001 ]
  ]

  QuickslotCommands
  [
    UseQuickSlot_1 [ "" [ 0 DIK_1 ] ]
    UseQuickSlot_2 [ "" [ 0 DIK_2 ] ]
    UseQuickSlot_3 [ "" [ 0 DIK_3 ] ]
    UseQuickSlot_4 [ "" [ 0 DIK_4 ] ]
    UseQuickSlot_5 [ "" [ 0 DIK_5 ] ]
    UseQuickSlot_6 [ "" [ 0 DIK_6 ] ]
    UseQuickSlot_7 [ "" [ 0 DIK_7 ] ]
    UseQuickSlot_8 [ "" [ 0 DIK_8 ] ]
    UseQuickSlot_9 [ "" [ 0 DIK_9 ] ]
    UseQuickSlot_14 [ "" [ 0 DIK_5 ] 0x00000004 ]
    UseQuickSlot_15 [ "" [ 0 DIK_6 ] 0x00000004 ]
    UseQuickSlot_16 [ "" [ 0 DIK_7 ] 0x00000004 ]
    UseQuickSlot_17 [ "" [ 0 DIK_8 ] 0x00000004 ]
    UseQuickSlot_18 [ "" [ 0 DIK_9 ] 0x00000004 ]
    CreateShortcut [ "" [ 0 DIK_0 ] ]
    UseQuickSlot_1 [ "" [ 0 DIK_1 ] 0x00000002 ]
    UseQuickSlot_2 [ "" [ 0 DIK_2 ] 0x00000002 ]
    UseQuickSlot_3 [ "" [ 0 DIK_3 ] 0x00000002 ]
    UseQuickSlot_4 [ "" [ 0 DIK_4 ] 0x00000002 ]
    UseQuickSlot_5 [ "" [ 0 DIK_5 ] 0x00000002 ]
    UseQuickSlot_6 [ "" [ 0 DIK_6 ] 0x00000002 ]
    UseQuickSlot_7 [ "" [ 0 DIK_7 ] 0x00000002 ]
    UseQuickSlot_8 [ "" [ 0 DIK_8 ] 0x00000002 ]
    UseQuickSlot_9 [ "" [ 0 DIK_9 ] 0x00000002 ]
    CreateShortcut [ "" [ 0 DIK_0 ] 0x00000002 ]
  ]

  ToggleChatEntry
  [
    ToggleChatEntry [ "" [ 0 DIK_TAB ] ]
  ]

  ChatCommands
  [
    EnterChatMode [ "" [ 0 DIK_RETURN ] ]
  ]

  Combat
  [
    CombatToggleCombat [ "" [ 0 DIK_GRAVE ] ]
  ]

  MeleeCombat
  [
    CombatDecreaseAttackPower [ "" [ 0 DIK_INSERT ] ]
    CombatIncreaseAttackPower [ "" [ 0 DIK_PGUP ] ]
    CombatLowAttack [ "" [ 0 DIK_DELETE ] ]
    CombatMediumAttack [ "" [ 0 DIK_END ] ]
    CombatHighAttack [ "" [ 0 DIK_PGDN ] ]
  ]

  MissileCombat
  [
    CombatDecreaseMissileAccuracy [ "" [ 0 DIK_INSERT ] ]
    CombatIncreaseMissileAccuracy [ "" [ 0 DIK_PGUP ] ]
    CombatAimLow [ "" [ 0 DIK_DELETE ] ]
    CombatAimMedium [ "" [ 0 DIK_END ] ]
    CombatAimHigh [ "" [ 0 DIK_PGDN ] ]
  ]

  MagicCombat
  [
    CombatPrevSpellTab [ "" [ 0 DIK_INSERT ] ]
    CombatNextSpellTab [ "" [ 0 DIK_PGUP ] ]
    CombatPrevSpell [ "" [ 0 DIK_DELETE ] ]
    CombatCastCurrentSpell [ "" [ 0 DIK_END ] ]
    CombatNextSpell [ "" [ 0 DIK_PGDN ] ]
    CombatFirstSpellTab [ "" [ 0 DIK_INSERT ] 0x00000002 ]
    CombatLastSpellTab [ "" [ 0 DIK_PGUP ] 0x00000002 ]
    CombatFirstSpell [ "" [ 0 DIK_DELETE ] 0x00000002 ]
    CombatLastSpell [ "" [ 0 DIK_PGDN ] 0x00000002 ]
    UseSpellSlot_1 [ "" [ 0 DIK_1 ] ]
    UseSpellSlot_2 [ "" [ 0 DIK_2 ] ]
    UseSpellSlot_3 [ "" [ 0 DIK_3 ] ]
    UseSpellSlot_4 [ "" [ 0 DIK_4 ] ]
    UseSpellSlot_5 [ "" [ 0 DIK_5 ] ]
    UseSpellSlot_6 [ "" [ 0 DIK_6 ] ]
    UseSpellSlot_7 [ "" [ 0 DIK_7 ] ]
    UseSpellSlot_8 [ "" [ 0 DIK_8 ] ]
    UseSpellSlot_9 [ "" [ 0 DIK_9 ] ]
  ]

  Emotes
  [
    PointState [ "" [ 0 DIK_K ] ]
    Laugh [ "" [ 0 DIK_I ] ]
    Wave [ "" [ 0 DIK_J ] ]
    Cheer [ "" [ 0 DIK_O ] ]
    Cry [ "" [ 0 DIK_U ] ]
  ]

  TargetedUsage
  [
    SelectLeft [ "" [ 1 DIMOFS_BUTTON0 ] ]
    SelectRight [ "" [ 1 DIMOFS_BUTTON1 ] ]
  ]

  CameraControls
  [
    CameraActivateAlternateMode [ "" [ 0 DIK_NUMPADSLASH ] ]
    CameraActivateAlternateMode [ "" [ 0 DIK_F2 ] ]
    CameraInstantMouseLook [ "" [ 1 DIMOFS_BUTTON2 ] ]
    CameraRotateLeft [ "" [ 0 DIK_NUMPAD4 ] ]
    CameraRotateRight [ "" [ 0 DIK_NUMPAD6 ] ]
    CameraRotateUp [ "" [ 0 DIK_NUMPAD8 ] ]
    CameraRotateDown [ "" [ 0 DIK_NUMPAD2 ] ]
    CameraMoveToward [ "" [ 0 DIK_NUMPADMINUS ] ]
    CameraMoveAway [ "" [ 0 DIK_NUMPADPLUS ] ]
    CameraViewDefault [ "" [ 0 DIK_NUMPAD0 ] ]
    CameraViewFirstPerson [ "" [ 0 DIK_DECIMAL ] ]
    CameraViewLookDown [ "" [ 0 DIK_NUMPAD5 ] ]
    CameraViewMapMode [ "" [ 0 DIK_NUMPADENTER ] ]
  ]

  CameraAlternateControls
  [
    CameraRotateLeft [ "" [ 0 DIK_LEFT ] ]
    CameraRotateRight [ "" [ 0 DIK_RIGHTARROW ] ]
    CameraRotateUp [ "" [ 0 DIK_UPARROW ] ]
    CameraRotateDown [ "" [ 0 DIK_DOWNARROW ] ]
  ]

  SystemKeys
  [
    AltEnter [ "" [ 0 DIK_RETURN ] 0x00000004 ]
    AltTab [ "" [ 0 DIK_TAB ] 0x00000004 ]
    AltF4 [ "" [ 0 DIK_F4 ] 0x00000004 ]
    CtrlShiftEsc [ "" [ 0 DIK_ESCAPE ] 0x00000003 ]
  ]

  MouseCommands
  [
    PointerX [ "" [ 1 DIMOFS_X ] 0x00000000 Analog ]
    PointerY [ "" [ 1 DIMOFS_Y ] 0x00000000 Analog ]
    SelectLeft [ "" [ 1 DIMOFS_BUTTON0 ] ]
    SelectRight [ "" [ 1 DIMOFS_BUTTON1 ] ]
    SelectMid [ "" [ 1 DIMOFS_BUTTON2 ] ]
    SelectDblLeft [ "" [ 1 DIMOFS_BUTTON0 ] 0x00000000 MouseDblClick ]
    SelectDblRight [ "" [ 1 DIMOFS_BUTTON1 ] 0x00000000 MouseDblClick ]
    SelectDblMid [ "" [ 1 DIMOFS_BUTTON2 ] 0x00000000 MouseDblClick ]
  ]

  ScrollableControls
  [
    ScrollUp [ "" [ 1 DIMOFS_Z AxisPositive ] ]
    ScrollDown [ "" [ 1 DIMOFS_Z AxisNegative ] ]
    ScrollUp [ "" [ 0 DIK_UPARROW ] 0x00000002 ]
    ScrollDown [ "" [ 0 DIK_DOWNARROW ] 0x00000002 ]
  ]

  EditControls
  [
    CursorCharLeft [ "" [ 0 DIK_LEFT ] ]
    CursorCharRight [ "" [ 0 DIK_RIGHTARROW ] ]
    CursorPreviousLine [ "" [ 0 DIK_UPARROW ] ]
    CursorNextLine [ "" [ 0 DIK_DOWNARROW ] ]
    CursorPreviousPage [ "" [ 0 DIK_PGUP ] ]
    CursorNextPage [ "" [ 0 DIK_PGDN ] ]
    CursorWordLeft [ "" [ 0 DIK_LEFT ] 0x00000002 ]
    CursorWordRight [ "" [ 0 DIK_RIGHTARROW ] 0x00000002 ]
    CursorStartOfLine [ "" [ 0 DIK_HOME ] ]
    CursorStartOfDocument [ "" [ 0 DIK_HOME ] 0x00000002 ]
    CursorEndOfLine [ "" [ 0 DIK_END ] ]
    CursorEndOfDocument [ "" [ 0 DIK_END ] 0x00000002 ]
    EscapeKey [ "" [ 0 DIK_ESCAPE ] ]
    AcceptInput [ "" [ 0 DIK_RETURN ] ]
    DeleteKey [ "" [ 0 DIK_DELETE ] ]
    BackspaceKey [ "" [ 0 DIK_BACK ] ]
  ]

  CopyAndPasteControls
  [
    CopyText [ "" [ 0 DIK_C ] 0x00000002 ]
    CopyText [ "" [ 0 DIK_INSERT ] 0x00000002 ]
    CutText [ "" [ 0 DIK_X ] 0x00000002 ]
    CutText [ "" [ 0 DIK_DELETE ] 0x00000001 ]
    PasteText [ "" [ 0 DIK_V ] 0x00000002 ]
    PasteText [ "" [ 0 DIK_INSERT ] 0x00000001 ]
  ]

  DialogBoxes
  [
    EscapeKey [ "" [ 0 DIK_ESCAPE ] ]
    AcceptInput [ "" [ 0 DIK_RETURN ] ]
  ]
]
