@echo off
set /p version="Version: "

"C:\Program Files\7-Zip\7z.exe" a ../SkunkTrader_%version%.zip ../SkunkTrader/ -xr!.vs -xr!.git -xr!logs -xr!zip.bat -xr!.gitmodules -xr!.gitignore -x!SkunkTrader/src/data/* -x!SkunkTrader/configs/* -x!SkunkTrader/src/configs/* -x!SkunkTrader/exports/*