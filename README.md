# SkunkTrader  

A [SkunkWorks](http://skunkworks.sourceforge.net) trade bot script for Asheron's Call emulator server.

## Functionality
- Assign point values to items to sell.
- Assign point values to items to accept as payment.
- Display all items for sale in tradewindow upon being apporched.
- Respond to `check` command to get the price of an item.
- Respond to `add` command to add an item to the trade window.
- Commands differentiate between similar named items by specifying a value. (`check Steel Salvage:98765`)
- Respond to `points` command to list the items we accept as payment on their point value.
- Respond to `total` command to say the total cost of items in the trade window.
- Respond to `buy` command to finialize a sale.
- If a player overpays for an item, their excess balance is saved for future use.
- Respond to `$check <item>` in open/general chat if you have the item available.
- Respond to `loc` with your location.
- Grant Monarch / Patron / Player discounts.
- Ability to sell items in sets rather than individually. Like quest armor pieces.
- `equip` command to equip items upon request.

## Installation
- Install Decal. [decaldev.com](https://www.decaldev.com)
- Download SkunkWorks for Asheron's Call. [SkunkWorks35-500.exe](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks35-500.exe/download)  
- Install SkunkWorks to `C:/Games/Skunkworks`. Some users experience problems running SkunkWorks in the default Program Files directory.
- Download updated SkunkWorks skapi.dll file. [SkunkWorks3.5.509.zip](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks3.5.509.zip/download)
- Extract SkunkWorks3.5.509.zip to your SkunkWorks directory, overwriting the existing skapi.dll file.
- Download [SkunkTrader](https://gitlab.com/Cyprias/SkunkTrader/-/releases) and extract it to your SkunkWorks directory. 
- Log into Asheron's Call, click the SkunkWorks icon on your Decal bar and select SkunkTrader.swx from the dropdown menu. You may need to use the browser [...] button to locate it.
- (Recommended) Open Decal's options (from your windows start bar) and enable 'Multiple Views'.


## Updating SkunkTrader
- Download a newer version from [here](https://gitlab.com/Cyprias/SkunkTrader/-/releases)  
- Open the zip and extract the files over your existing SkunkTrader folder, overwriting the previous files. Your settings/prices will remain intact.  

## FAQ
##### How do I assign a sell price to an item?
On the Selling tab, click the Scan button. Once the Common & Unique lists are populated, click the price column. A smaller window will appear. Input a number, hit [enter] on your keyboard, then click the Save button.

##### What does the Salvage tab mean?
Beside each salvage name are columns for workmanships 1 through 10. Clicking one of the columns will bring up a window to assign a price value to that workmanship. SkunkTrader will use the price equal or below the bag's workmanship. So if you have prices assigned to **w1** and **w8**, bags w1-7 will use the **w1** price and bags w8-10 will use the **w8** price.

##### How can others supply me with items to sell?
On to the Config > Players tab, click **New Player**. Input the player's name, then in the list enable the checkbox to the right of their name. That player can now place items in the trade window and tell you `resell <value>`, you'll accept trade and begin reselling that item for that price.

##### How do I give people discounts?
On the Config > Players tab, click **New Player**. Input the player's name. To give a player discount click the **Pla D** column and input a number between 0 and 100 for a percentage discount. To give a discount to a player's vassals, click the **Pat D** column. To give a discount to followers of a monarch, click the **Mon D** column.

##### How do I sell items in sets rather than individually?
On to the Selling > Item Sets tab, click **New Set** to create a new set name. Then below in the **Item Name** textbox type a item name and click **Add**, repeat for other item names. If setup correctly, when a player asks to add one of the items to the trade window, the bot will notify them that the item can only be sold as a set, and for them to use `addset <set name>` to add the items to the trade window.

##### How do I run SkunkTrader automatically when logging in?  
Run SkunkTrader so it shows up in the in game dropdown menu.
Open ThwargLauncher, go into the `Advanced View`, click the `OnLogin Cmds` button, select your trade bot character(s) and input `/swc run SkunkTrader.swx` into the text area and click Save.  
Launch AC and see if SkunkTrader runs upon login.

###### Alternate Method  
Launch SkunkTrader, goto the *Config* > *Main* tab and enable the *Enable Login Script* setting.  
On the console window (outside the game), click *Configure* > *Login Script*. Enable *Run this script on login* and click the [...] button to locate the *SkunkTrader_LoginScript.swx* file.  
(Optional) You can also use the [SkunkLauncher](https://gitlab.com/Cyprias/ScriptLauncher) script to run different scripts on different characters.  

##### I'm getting a `>> Skunk***.js: File not found` error. What do I do?  
Simple answer: The zip file GitLab creates lack certain files. Goto the [releases](https://gitlab.com/Cyprias/SkunkTrader/-/releases) page and use the `Download: SkunkTrader_x.x.xxx.zip` link in the description.   
Technical answer: This repo uses [Git submodules](https://www.atlassian.com/git/tutorials/git-submodule) to share common files used in multiple SkunkWorks projects. GitLab doesn't include submodules files in their download link.  

##### I'm getting `error parsing keymap` error.
Try copying the `acclient.keymap` file from the `Skunktrader/docs` folder into your `C:/Users/USERNAME/Documents/Asheron's Call` folder. You may need to reapply any custom keybinds you had set.

##### I can't see Skunkworks' icon on the Decal bar.
Try installing the following libraries, then restart your PC.  
- Microsoft .NET Framework 2.0 Service Pack 2 [Microsoft.com](https://www.microsoft.com/en-us/download/details.aspx?id=1639) [Softpedia.com](https://www.softpedia.com/get/Others/Signatures-Updates/Microsoft-NET-Framework-Service-Pack.shtml)    
- [MSXML (Microsoft XML Parser) 3.0 Service Pack 4 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=23412)  
- [MSXML 4.0 Service Pack 3 (Microsoft XML Core Services)](https://www.microsoft.com/en-us/download/details.aspx?id=15697)  
- [Microsoft Core XML Services (MSXML) 6.0](https://www.microsoft.com/en-us/download/details.aspx?id=3988)  

##### Why does SkunkTrader reduce my frame rate so much?  
SkunkWorks uses Decal's old style render engine which isn't as efficient as the newer [Virindi Views](http://www.virindi.net/wiki/index.php/Virindi_Views) render engine that many plugins use today.  
Minimizing SkunkTrader's UI should alleviate the issue.

##### How do I set a password to certain tiems?
Select the item and type `/sw st password` into chat.  
A window will popup asking for a password.  
When someone attempts to buy said item they'll be asked for the password before the trade will complete.

## Known issues
- The [GDLe](https://gdleac.com/) emulator has a protocol issue where when we trade away an item, SkunkWorks thinks the item's still in our inventory. I've tried to compensate for this, you may see 'phantom item' messages. This issue doesn't appear to on [ACE](https://emulator.ac/) emulator.  

## External links
- [SkunkTrader source repo](https://gitlab.com/Cyprias/SkunkTrader)  
- [SkunkWorks Discord channel](https://discord.gg/z5wXR5K)  
- [Asheron's Call Trade Bot Setup](https://www.youtube.com/watch?v=Z4AvWtGG2hQ)  

## Contributors
- [Cyprias](https://gitlab.com/cyprias)
- [Trevis](https://gitlab.com/trevis)

## License
MIT License	(http://opensource.org/licenses/MIT)